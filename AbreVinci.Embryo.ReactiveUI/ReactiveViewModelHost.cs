﻿namespace AbreVinci.Embryo.ReactiveUI;

/// <summary>
/// Utility that allow view model hierarchies to work as if they were bound to views.
/// Since view models are not active until change notifications are subscribed to,
/// they would otherwise just contain "default" values for all of their properties.
/// </summary>
public class ReactiveViewModelHost : IReactiveViewModelHost
{
	private static readonly ThreadLocal<Stack<IReactiveViewModelHost>> _currentHostStack = new(() => new Stack<IReactiveViewModelHost>());
	internal static IReactiveViewModelHost? Current => _currentHostStack.Value!.Any() ? _currentHostStack.Value!.Peek() : null;

	/// <summary>
	/// View model host constructor.
	/// </summary>
	/// <param name="scheduler">Allows you to specify a scheduler to be used to override the default ui scheduler. Good alternatives here when it comes to a testing environment are Scheduler.Immediate or TestScheduler.</param>
	/// <param name="autoActivate">Specifies if the view models will automatically be activated upon construction.</param>
    public ReactiveViewModelHost(IScheduler scheduler, bool autoActivate)
    {
		UiScheduler = scheduler;
		AutoActivate = autoActivate;
    }

	/// <summary>
	/// Specifies if the view models will automatically be activated upon construction.
	/// </summary>
	public bool AutoActivate { get; }

	/// <summary>
	/// Any UI scheduler override that has been set.
	/// </summary>
	public IScheduler UiScheduler { get; }
	
	/// <summary>
	/// Hosts the given view model using the options of this host. 
	/// Unless otherwise specified, any children in the view model's tree will also be hosted by this host.
	/// </summary>
	/// <typeparam name="TViewModel">The type of the view model to host.</typeparam>
	/// <param name="create">A factory function creating the view model to host.</param>
	/// <returns>The created view model.</returns>
    public TViewModel HostViewModel<TViewModel>(Func<TViewModel> create)
    {
		try
		{
			_currentHostStack.Value!.Push(this);
			return create();
		}
		finally
		{
            // ReSharper disable once RedundantSuppressNullableWarningExpression
            _currentHostStack.Value!.Pop();
		}
    }
}
