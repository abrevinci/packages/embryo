﻿namespace AbreVinci.Embryo.ReactiveUI;

/// <summary>
/// An observable list. Use <see cref="ReactiveViewModel.BindList{TValue}(IObservable{IImmutableList{TValue}})"/> to create one for simple model items
/// and use <see cref="ReactiveViewModel.BindListAsViewModels{TValue, TViewModel}(IObservable{IImmutableList{TValue}}, Func{TValue, TViewModel}, Func{TViewModel, TValue, bool})"/>
/// to create one for view model items.
/// </summary>
/// <typeparam name="T">The item type.</typeparam>
public interface IReactiveList<T> : IReactiveCollectionView<T>, IObservable<IImmutableList<T>>, IDisposable
{
}
