﻿namespace AbreVinci.Embryo.ReactiveUI;

/// <summary>
/// Interface representing a window controlled by a <see cref="IReactiveWindowController{TViewModel}"/>.
/// </summary>
public interface IReactiveWindow
{
	/// <summary>
	/// The window's content (the view model).
	/// </summary>
	object? Content { get; }

	/// <summary>
	/// Show the window.
	/// </summary>
	void Show();

	/// <summary>
	/// Hide the window.
	/// </summary>
	void Hide();

	/// <summary>
	/// Close the window.
	/// </summary>
	void Close();

	/// <summary>
	/// Raised when the window is closed.
	/// </summary>
	event EventHandler Closed;
}
