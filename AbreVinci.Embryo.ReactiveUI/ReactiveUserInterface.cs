﻿using AbreVinci.Embryo.ReactiveUI.Internal;

namespace AbreVinci.Embryo.ReactiveUI;

/// <summary>
/// Base class for a user interface capable of creating and holding window controllers.
/// </summary>
public abstract class ReactiveUserInterface : IDisposable
{
	private readonly IReactiveViewModelHost _viewModelHost;
	private readonly IReactiveWindowFactory _windowFactory;
	private readonly CompositeDisposable _disposables;

	/// <summary>
	/// Base constructor.
	/// </summary>
	/// <param name="viewModelHost">The view model host to use for view models.</param>
	/// <param name="windowFactory">The window factory to use to create windows.</param>
	protected ReactiveUserInterface(
		IReactiveViewModelHost viewModelHost,
		IReactiveWindowFactory windowFactory)
	{
		_viewModelHost = viewModelHost;
		_windowFactory = windowFactory;
		_disposables = new CompositeDisposable();
	}

	/// <summary>
	/// Adds a window controller capable of a visible and a closed state.
	/// </summary>
	/// <typeparam name="TViewModel">The type of the root view model for this window.</typeparam>
	/// <param name="isOpen">An observable stream telling the window controller if the window should be open or not.</param>
	/// <param name="windowName">The name of the window to create.</param>
	/// <param name="createViewModel">A view mode factory function.</param>
	/// <param name="onClosed">This gets call if the window is closed from the UI (not when isOpen emits false).</param>
	/// <returns>The created window controller.</returns>
	/// <example>
	/// [!code-csharp[Example](../../DocSnippets/ReactiveUI/ReactiveUserInterfaceBase/AddWindowWithIsOpen/UserInterface.cs#Snippet)]
	/// </example>
	protected IReactiveWindowController<TViewModel> AddWindow<TViewModel>(
		IObservable<bool> isOpen,
		string windowName,
		Func<TViewModel> createViewModel,
		Action onClosed) where TViewModel : notnull
	{
		var windowController = CreateWindowController(
			isOpen.Select(v => v ? ReactiveWindowControllerState.WindowVisible : ReactiveWindowControllerState.WindowClosed),
			windowName,
			createViewModel,
			onClosed);

		if (windowController is IDisposable disposable)
		{
			_disposables.Add(disposable);
		}

		return windowController;
	}

	/// <summary>
	/// Adds a window controller capable of controlling a window that is visible from application start and until it is closed (normally the main window).
	/// </summary>
	/// <typeparam name="TViewModel">The type of the root view model for this window.</typeparam>
	/// <param name="windowName">The name of the window to create.</param>
	/// <param name="createViewModel">A view mode factory function.</param>
	/// <returns>The created window controller.</returns>
	/// <example>
	/// [!code-csharp[Example](../../DocSnippets/ReactiveUI/ReactiveUserInterfaceBase/AddWindow/UserInterface.cs#Snippet)]
	/// </example>
	protected IReactiveWindowController<TViewModel> AddWindow<TViewModel>(
		string windowName,
		Func<TViewModel> createViewModel) where TViewModel : notnull
	{
		var windowController = CreateWindowController(
			Observable.Return(ReactiveWindowControllerState.WindowVisible),
			windowName,
			createViewModel,
			() => { });

		if (windowController is IDisposable disposable)
		{
			_disposables.Add(disposable);
		}

		return windowController;
	}

	/// <summary>
	/// Adds a window controller capable of a visible, a hidden, and a closed state.
	/// </summary>
	/// <typeparam name="TViewModel">The type of the root view model for this window.</typeparam>
	/// <param name="state">An observable stream of window controller states telling the controller what state the window should be in.</param>
	/// <param name="windowName">The name of the window to create.</param>
	/// <param name="createViewModel">A view mode factory function.</param>
	/// <param name="onClosed">This gets call if the window is closed from the UI (not when state emits a WindowClosed state).</param>
	/// <returns>The created window controller.</returns>
	/// <example>
	/// [!code-csharp[Example](../../DocSnippets/ReactiveUI/ReactiveUserInterfaceBase/AddWindowWithState/UserInterface.cs#Snippet)]
	/// </example>
	protected IReactiveWindowController<TViewModel> AddWindow<TViewModel>(
		IObservable<ReactiveWindowControllerState> state,
		string windowName,
		Func<TViewModel> createViewModel,
		Action onClosed) where TViewModel : notnull
	{
		var windowController = CreateWindowController(
			state,
			windowName,
			createViewModel,
			onClosed);

		if (windowController is IDisposable disposable)
		{
			_disposables.Add(disposable);
		}

		return windowController;
	}

	/// <summary>
	/// Disposes this instance's window controllers.
	/// </summary>
	public void Dispose()
	{
		_disposables.Dispose();
	}

	private IReactiveWindowController<TViewModel> CreateWindowController<TViewModel>(
		IObservable<ReactiveWindowControllerState> state,
		string windowName,
		Func<TViewModel> createViewModel,
		Action onClosed) where TViewModel : notnull
	{
		return new ReactiveWindowController<TViewModel>(
			state, 
			windowName, 
			() => _viewModelHost.HostViewModel(createViewModel), 
			onClosed, 
			_windowFactory, 
			_viewModelHost.UiScheduler);
	}
}
