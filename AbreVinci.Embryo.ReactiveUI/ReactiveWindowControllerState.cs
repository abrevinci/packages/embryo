﻿namespace AbreVinci.Embryo.ReactiveUI;

/// <summary>
/// Window controller state determining the current state of a controlled window.
/// </summary>
public enum ReactiveWindowControllerState
{
	/// <summary>
	/// The window is created and visible and the view model exists.
	/// </summary>
	WindowVisible,

	/// <summary>
	/// The window is created but hidden and the view model exists.
	/// </summary>
	WindowHidden,

	/// <summary>
	/// The window does not exist (is closed) and the view model does not exist.
	/// </summary>
	WindowClosed
}
