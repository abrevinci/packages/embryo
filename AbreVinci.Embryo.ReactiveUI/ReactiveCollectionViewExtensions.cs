﻿using AbreVinci.Embryo.ReactiveUI.Internal;

namespace AbreVinci.Embryo.ReactiveUI;

/// <summary>
/// A set of extension methods providing transforming operations on collection views such as sorting and filtering.
/// </summary>
public static class ReactiveCollectionViewExtensions
{
	/// <summary>
	/// Create a collection view in which the items are sorted according to the <paramref name="compare"/> function.
	/// </summary>
	/// <typeparam name="T">The element type.</typeparam>
	/// <param name="source">The underlying source collection (may be a reactive list or another collection view).</param>
	/// <param name="compare">A comparison function that should be used for sorting. It should return 1 if the first parameter is greater than the second, -1 if the second parameter is greater than the first, and 0 if they are considered equal.</param>
	/// <returns>A collection view with sorted elements.</returns>
	public static IReactiveCollectionView<T> Sort<T>(this IReactiveCollectionView<T> source, Func<T, T, int> compare)
	{
		return new SortedReactiveCollectionView<T, object>(null, source, (_, a, b) => compare(a, b));
	}

	/// <summary>
	/// Create a collection view in which the items are sorted according to the <paramref name="compare"/> function.
	/// </summary>
	/// <typeparam name="T">The element type.</typeparam>
	/// <typeparam name="TSortParams">The type holding any sort parameters used.</typeparam>
	/// <param name="source">The underlying source collection (may be a reactive list or another collection view).</param>
	/// <param name="sortParams">An observable stream of sort parameters.</param>
	/// <param name="compare">A comparison function that should be used for sorting. It should return 1 if the first parameter is greater than the second, -1 if the second parameter is greater than the first, and 0 if they are considered equal.</param>
	/// <returns>A collection view with sorted elements.</returns>
	public static IReactiveCollectionView<T> Sort<T, TSortParams>(this IReactiveCollectionView<T> source, IObservable<TSortParams> sortParams, Func<TSortParams, T, T, int> compare)
	{
		return new SortedReactiveCollectionView<T, TSortParams>(sortParams, source, compare);
	}

	/// <summary>
	/// Create a collection view in which the items are filtered according to the <paramref name="filter"/> function.
	/// </summary>
	/// <typeparam name="T">The element type.</typeparam>
	/// <param name="source">The underlying source collection (may be a reactive list or another collection view).</param>
	/// <param name="filter">A filter function that should be used for filtering. It should return rue if the item should be included, false otherwise.</param>
	/// <returns>A collection view with filtered elements.</returns>
	public static IFilteredReactiveCollectionView<T> Filter<T>(this IReactiveCollectionView<T> source, Func<T, bool> filter)
	{
		return new FilteredReactiveCollectionView<T, bool>(null, source, (_, e) => filter(e));
	}

	/// <summary>
	/// Create a collection view in which the items are filtered according to the <paramref name="filter"/> function.
	/// </summary>
	/// <typeparam name="T">The element type.</typeparam>
	/// <typeparam name="TFilterParams">The type holding any filter parameters used.</typeparam>
	/// <param name="source">The underlying source collection (may be a reactive list or another collection view).</param>
	/// <param name="filterParams">An observable stream of filter parameters.</param>
	/// <param name="filter">A filter function that should be used for filtering. It should return rue if the item should be included, false otherwise.</param>
	/// <returns>A collection view with filtered elements.</returns>
	public static IFilteredReactiveCollectionView<T> Filter<T, TFilterParams>(this IReactiveCollectionView<T> source, IObservable<TFilterParams> filterParams, Func<TFilterParams, T, bool> filter)
	{
		return new FilteredReactiveCollectionView<T, TFilterParams>(filterParams, source, filter);
	}

	/// <summary>
	/// Create a collection view in which the items are paged/limited according to the <paramref name="startIndex"/> and <paramref name="maxCount"/>.
	/// </summary>
	/// <typeparam name="T">The element type.</typeparam>
	/// <param name="source">The underlying source collection (may be a reactive list or another collection view).</param>
	/// <param name="startIndex">The start index to include in the resulting collection view.</param>
	/// <param name="maxCount">The maximum number of elements to include in the resulting collection view.</param>
	/// <returns>A collection view with paged/limited elements.</returns>
	public static IPagedReactiveCollectionView<T> Page<T>(this IReactiveCollectionView<T> source, int startIndex, int maxCount)
	{
		return new PagedReactiveCollectionView<T, bool>(null, source, _ => startIndex, _ => maxCount);
	}


	/// <summary>
	/// Create a collection view in which the items are paged/limited according to the <paramref name="getStartIndex"/> and <paramref name="getMaxCount"/> functions.
	/// </summary>
	/// <typeparam name="T">The element type.</typeparam>
	/// <typeparam name="TPagingParams">The type holding any paging parameters used.</typeparam>
	/// <param name="source">The underlying source collection (may be a reactive list or another collection view).</param>
	/// <param name="pagingParams">An observable stream of paging parameters.</param>
	/// <param name="getStartIndex">Function to compute the start index to include in the resulting collection view.</param>
	/// <param name="getMaxCount">Function to compute the maximum number of elements to include in the resulting collection view.</param>
	/// <returns>A collection view with paged/limited elements.</returns>
	public static IPagedReactiveCollectionView<T> Page<T, TPagingParams>(this IReactiveCollectionView<T> source, IObservable<TPagingParams> pagingParams, Func<TPagingParams, int> getStartIndex, Func<TPagingParams, int> getMaxCount)
	{
		return new PagedReactiveCollectionView<T, TPagingParams>(pagingParams, source, getStartIndex, getMaxCount);
	}
}
