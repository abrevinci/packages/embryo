﻿namespace AbreVinci.Embryo.ReactiveUI;

/// <summary>
/// Used internally to allow property bindings to raise property changed notifications on their view model.
/// </summary>
public interface IReactiveViewModel : INotifyPropertyChanged
{
	/// <summary>
	/// Raise property changed notification with the given <paramref name="args"/>.
	/// </summary>
	/// <param name="args">The arguments to pass to <see cref="System.ComponentModel.INotifyPropertyChanged.PropertyChanged"/>.</param>
	void RaisePropertyChanged(PropertyChangedEventArgs args);
}
