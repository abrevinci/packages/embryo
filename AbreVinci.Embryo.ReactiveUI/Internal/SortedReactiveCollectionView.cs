﻿namespace AbreVinci.Embryo.ReactiveUI.Internal;

internal class SortedReactiveCollectionView<T, TSortParams> : IReactiveCollectionView<T>, IDisposable, IReactiveCollectionViewContext
{
	// ReSharper disable once StaticMemberInGenericType
	private static readonly PropertyChangedEventArgs _isEmptyEventArgs = new(nameof(IsEmpty));
	// ReSharper disable once StaticMemberInGenericType
	private static readonly PropertyChangedEventArgs _countEventArgs = new(nameof(Count));

	private readonly IObservable<TSortParams>? _sortParamsSource;
	private readonly IReactiveCollectionView<T> _source;
	private readonly Func<TSortParams, T, T, int> _compare;
	private readonly CompositeDisposable _disposables;
	private readonly List<T> _original;
	private readonly List<int> _sortedIndexes;

	private IDisposable? _subscription;
	private Comparer _comparer;
	private int _activationCount;
	private bool _isDisposed;

	public SortedReactiveCollectionView(
		IObservable<TSortParams>? sortParamsSource, 
		IReactiveCollectionView<T> source, 
		Func<TSortParams, T, T, int> compare)
	{
		var parentContext = source as IReactiveCollectionViewContext;

		_sortParamsSource = sortParamsSource;
		_source = source;
		_compare = compare;
		_disposables = new CompositeDisposable();
		_original = new List<T>();
		_sortedIndexes = new List<int>();
		_comparer = new Comparer(default!, compare, _original);

		UiScheduler = source.UiScheduler;
		ViewModelHost = source.ViewModelHost;

		if (ViewModelHost.AutoActivate)
		{
			Activate();
			_activationCount++;
		}

		parentContext?.AddLinkedDisposable(this);
	}

	private void Activate()
	{
		if (_sortParamsSource != null)
		{
			Debug.Assert(_subscription == null);
			_subscription = _sortParamsSource.DistinctUntilChanged().Subscribe(sortParams =>
			{
				_comparer = new Comparer(sortParams, _compare, _original);
				Refresh();
			});
		}
		else
		{
			Refresh();
		}
		_source.CollectionChanged += OnSourceCollectionChanged;
	}

	private void Deactivate()
	{
		if (!_isDisposed)
		{
			_source.CollectionChanged -= OnSourceCollectionChanged;

			Debug.Assert(_subscription != null || _sortParamsSource == null);
			_subscription?.Dispose();
			_subscription = null;
		}
	}

	public bool IsEmpty => _sortedIndexes.Count == 0;
	public int Count => _sortedIndexes.Count;

	public IScheduler UiScheduler { get; }
	public IReactiveViewModelHost ViewModelHost { get; }

	private event NotifyCollectionChangedEventHandler? InternalCollectionChanged;

	public event NotifyCollectionChangedEventHandler? CollectionChanged
	{
		add
		{
			if (_activationCount == 0)
			{
				Activate();
			}
			InternalCollectionChanged += value;
			_activationCount++;
		}
		remove
		{
			_activationCount--;
			InternalCollectionChanged -= value;
			if (_activationCount == 0)
			{
				Deactivate();
			}
		}
	}

	private event PropertyChangedEventHandler? InternalPropertyChanged;

	public event PropertyChangedEventHandler? PropertyChanged
	{
		add
		{
			if (_activationCount == 0)
			{
				Activate();
			}
			InternalPropertyChanged += value;
			_activationCount++;
		}
		remove
		{
			_activationCount--;
			InternalPropertyChanged -= value;
			if (_activationCount == 0)
			{
				Deactivate();
			}
		}
	}

	public void Dispose()
	{
		if (!_isDisposed)
		{
			_subscription?.Dispose();
			_subscription = null;
			_disposables.Dispose();
			_isDisposed = true;
		}
	}

	private void HandleItemAdded(T item, int index)
	{
		NotifyOfPropertyChanges(() =>
		{
			if (item is INotifyPropertyChanged npc)
				npc.PropertyChanged += OnItemPropertyChanged;

			_original.Insert(index, item);

			var insertPos = _sortedIndexes.BinarySearch(index, _comparer);
			if (insertPos < 0)
				insertPos = ~insertPos;

			for (var i = 0; i < _sortedIndexes.Count; i++)
			{
				if (_sortedIndexes[i] >= index)
					_sortedIndexes[i]++;
			}

			_sortedIndexes.Insert(insertPos, index);

			RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, insertPos));
		});
	}

	private void HandleItemRemoved(T item, int index)
	{
		NotifyOfPropertyChanges(() =>
		{
			var removePos = _sortedIndexes.IndexOf(index);
			_sortedIndexes.RemoveAt(removePos);

			for (var i = 0; i < _sortedIndexes.Count; i++)
			{
				if (_sortedIndexes[i] > index)
					_sortedIndexes[i]--;
			}

			_original.RemoveAt(index);

			if (item is INotifyPropertyChanged npc)
				npc.PropertyChanged -= OnItemPropertyChanged;

			RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, removePos));
		});
	}

	private void Refresh()
	{
		NotifyOfPropertyChanges(() =>
		{
			foreach (var npc in _original.OfType<INotifyPropertyChanged>())
				npc.PropertyChanged -= OnItemPropertyChanged;

			_original.Clear();
			_sortedIndexes.Clear();

			_original.AddRange(_source);

			foreach (var npc in _original.OfType<INotifyPropertyChanged>())
				npc.PropertyChanged += OnItemPropertyChanged;

			_sortedIndexes.AddRange(_original.Select((_, i) => i).OrderBy(i => i, _comparer));

			RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
		});
	}

	private void OnSourceCollectionChanged(object? sender, NotifyCollectionChangedEventArgs e)
	{
		if (e.Action == NotifyCollectionChangedAction.Add && e.NewItems is { Count: 1 } newItems && newItems[0] is T newItem)
		{
			HandleItemAdded(newItem, e.NewStartingIndex);
		}
		else if (e.Action == NotifyCollectionChangedAction.Remove && e.OldItems is { Count: 1 } oldItems && oldItems[0] is T oldItem)
		{
			HandleItemRemoved(oldItem, e.OldStartingIndex);
		}
		else
		{
			Refresh();
		}
	}

	private void OnItemPropertyChanged(object? sender, PropertyChangedEventArgs e)
	{
		NotifyOfPropertyChanges(() =>
		{
			if (sender is T item)
			{
				var index = _original.IndexOf(item);
				var oldSortedPos = _sortedIndexes.IndexOf(index);

				_sortedIndexes.RemoveAt(oldSortedPos);

				var sortedPos = _sortedIndexes.BinarySearch(index, _comparer);
				if (sortedPos < 0)
					sortedPos = ~sortedPos;

				_sortedIndexes.Insert(sortedPos, index);

				if (sortedPos != oldSortedPos)
					RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Move, item, sortedPos, oldSortedPos));
			}
		});
	}

	private void NotifyOfPropertyChanges(Action action)
	{
		var isEmpty = IsEmpty;
		var count = Count;

		action();

		if (IsEmpty != isEmpty)
			RaisePropertyChanged(_isEmptyEventArgs);
		if (Count != count)
			RaisePropertyChanged(_countEventArgs);
	}

	private void RaiseCollectionChanged(NotifyCollectionChangedEventArgs e)
	{
		UiScheduler.Schedule(() => InternalCollectionChanged?.Invoke(this, e));
	}

	private void RaisePropertyChanged(PropertyChangedEventArgs e)
	{
		UiScheduler.Schedule(() => InternalPropertyChanged?.Invoke(this, e));
	}

	public IEnumerator<T> GetEnumerator() => _sortedIndexes.Select(i => _original[i]).GetEnumerator();
	IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

	private class Comparer : IComparer<int>
	{
		private readonly TSortParams _sortParams;
		private readonly Func<TSortParams, T, T, int> _compare;
		private readonly List<T> _original;

		public Comparer(TSortParams sortParams, Func<TSortParams, T, T, int> compare, List<T> original)
		{
			_sortParams = sortParams;
			_compare = compare;
			_original = original;
		}

		public int Compare(int x, int y)
		{
			var comparison = _compare(_sortParams, _original[x], _original[y]);
			if (comparison == 0)
				comparison = x - y;
			return comparison;
		}
	}

	public void AddLinkedDisposable(IDisposable disposable)
	{
		_disposables.Add(disposable);
	}
}
