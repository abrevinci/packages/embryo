﻿namespace AbreVinci.Embryo.ReactiveUI.Internal;

internal class ReactiveTestWindow : IReactiveWindow
{
	private readonly Action? _onShow;
	private readonly Action? _onHide;
	private readonly Action? _onClose;

	public ReactiveTestWindow(object? content, Action? onShow, Action? onHide, Action? onClose)
	{
		Content = content;

		_onShow = onShow;
		_onHide = onHide;
		_onClose = onClose;
	}

	public object? Content { get; }

	public void Show()
	{
		_onShow?.Invoke();
	}

	public void Hide()
	{
		_onHide?.Invoke();
	}

	public void Close()
	{
		_onClose?.Invoke();
		(Content as IDisposable)?.Dispose();
		Closed?.Invoke(this, EventArgs.Empty);
	}

	public event EventHandler? Closed;
}
