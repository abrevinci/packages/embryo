﻿using ListDiff;

namespace AbreVinci.Embryo.ReactiveUI.Internal;

internal class ReactiveListAsViewModels<TModel, TViewModel> : IReactiveList<TViewModel>, IReactiveCollectionViewContext
{
	// ReSharper disable once StaticMemberInGenericType
	private static readonly PropertyChangedEventArgs _isEmptyEventArgs = new(nameof(IsEmpty));
	// ReSharper disable once StaticMemberInGenericType
	private static readonly PropertyChangedEventArgs _countEventArgs = new(nameof(Count));

	private readonly IObservable<IImmutableList<TModel>> _values;
    private readonly Func<TViewModel, TModel, bool> _match;
	private readonly Func<TModel, TViewModel> _create;
	private readonly Action<TViewModel> _delete;
    private readonly Subject<IImmutableList<TViewModel>> _source;
    private readonly CompositeDisposable _disposables;

	private ObservableCollection<TViewModel>? _viewModels;
	private int _activations;
	private IDisposable? _valuesSubscription;
	private bool _isDisposed;

	public ReactiveListAsViewModels(
		IObservable<IImmutableList<TModel>> values,
		Func<TViewModel, TModel, bool> match,
		Func<TModel, TViewModel> create,
		Action<TViewModel> delete,
		IScheduler uiScheduler,
		IReactiveViewModelHost host)
	{
        _values = values;
        _match = match;
		_create = create;
		_delete = delete;
        _source = new Subject<IImmutableList<TViewModel>>();
		_disposables = new CompositeDisposable();
		
		UiScheduler = uiScheduler;
		ViewModelHost = host;

		if (host.AutoActivate)
        {
			_viewModels = new ObservableCollection<TViewModel>();
			Activate();
			_activations++;
        }
	}

	public bool IsEmpty => _viewModels?.Any() != true;
	public int Count => _viewModels?.Count ?? default;

	public IScheduler UiScheduler { get; }
	public IReactiveViewModelHost ViewModelHost { get; }

	private void Activate()
    {
		Debug.Assert(_valuesSubscription == null);

		_valuesSubscription = _values
			.DistinctUntilChanged(StateComparer<IImmutableList<TModel>>.Instance)
			.Subscribe(SetValues);
	}

	private void Deactivate()
    {
		if (!_isDisposed)
		{
			Debug.Assert(_valuesSubscription != null);

			_valuesSubscription?.Dispose();
			_valuesSubscription = null;
		}
	}

	private event PropertyChangedEventHandler? InternalPropertyChanged;

	public event PropertyChangedEventHandler? PropertyChanged
	{
		add
		{
			if (_activations == 0 && _viewModels == null)
			{
				_viewModels = new ObservableCollection<TViewModel>();
			}
			if (_viewModels != null)
			{
				InternalPropertyChanged += value;
			}
			if (_activations == 0)
			{
				Activate();
			}
			_activations++;
		}
		remove
		{
			_activations--;
			if (_activations == 0)
			{
				Deactivate();
			}
			if (_viewModels != null)
			{
				InternalPropertyChanged -= value;
			}
			if (_activations == 0 && _viewModels != null)
			{
				_viewModels = null;
			}
		}
	}

	public event NotifyCollectionChangedEventHandler? CollectionChanged
	{
		add
		{
			if (_activations == 0 && _viewModels == null)
			{
				_viewModels = new ObservableCollection<TViewModel>();
			}
			if (_viewModels != null)
			{
				_viewModels.CollectionChanged += value;
			}
			if (_activations == 0)
			{
				Activate();
			}
			_activations++;
		}
		remove
		{
			_activations--;
			if (_activations == 0)
			{
				Deactivate();
			}
			if (_viewModels != null)
			{
				_viewModels.CollectionChanged -= value;
			}
			if (_activations == 0 && _viewModels != null)
			{
				foreach (var viewModel in _viewModels)
					_delete(viewModel);
				_viewModels = null;
			}
		}
	}

	public IDisposable Subscribe(IObserver<IImmutableList<TViewModel>> observer)
	{
		return _source.Subscribe(observer);
	}

	public void Dispose()
	{
		if (!_isDisposed)
		{
			_source.OnCompleted();
			_source.Dispose();
			_valuesSubscription?.Dispose();

			if (_viewModels != null)
			{
				foreach (var viewModel in _viewModels)
					_delete(viewModel);
				_viewModels = null;
			}				

			_disposables.Dispose();
			_isDisposed = true;
		}
	}

	private void SetValues(IImmutableList<TModel>? values)
	{
		Debug.Assert(_viewModels != null);

		var viewModels = _viewModels!.ToList();

		if (values == null || values.Count == 0)
		{
			foreach (var viewModel in viewModels)
				_delete(viewModel);
			viewModels.Clear();
		}
		else
		{
			viewModels.MergeInto(values, _match, _create, (_, _) => { }, _delete);
		}

		_source.OnNext(viewModels.ToImmutableArray());

		UiScheduler.Schedule(() =>
		{
			if (_viewModels != null)
			{
				var isEmptyBefore = _viewModels.Count == 0;
				var countBefore = _viewModels.Count;
				_viewModels.MergeInto(viewModels, (v1, v2) => StateComparer<TViewModel>.Instance.Equals(v1, v2));
				if (isEmptyBefore != (_viewModels.Count == 0))
					InternalPropertyChanged?.Invoke(this, _isEmptyEventArgs);
				if (countBefore != _viewModels.Count)
					InternalPropertyChanged?.Invoke(this, _countEventArgs);
			}
		});
	}

	public IEnumerator<TViewModel> GetEnumerator() => _viewModels?.GetEnumerator() ?? Enumerable.Empty<TViewModel>().GetEnumerator();
	IEnumerator IEnumerable.GetEnumerator() => _viewModels?.GetEnumerator() ?? Enumerable.Empty<TViewModel>().GetEnumerator();
	
	public void AddLinkedDisposable(IDisposable disposable)
	{
		_disposables.Add(disposable);
	}
}
