﻿namespace AbreVinci.Embryo.ReactiveUI.Internal;

internal class ReactiveWindowController<TViewModel> : IReactiveWindowController<TViewModel>, IDisposable where TViewModel : notnull
{
	private readonly string _windowName;
	private readonly Func<TViewModel> _createViewModel;
	private readonly Action _onClosed;
	private readonly IReactiveWindowFactory _windowFactory;
	private readonly IDisposable _subscription;

	public ReactiveWindowController(
		IObservable<ReactiveWindowControllerState> state,
		string windowName,
		Func<TViewModel> createViewModel,
		Action onClosed,
		IReactiveWindowFactory windowFactory,
		IScheduler dispatcherScheduler)
	{
		_windowName = windowName;
		_createViewModel = createViewModel;
		_onClosed = onClosed;
		_windowFactory = windowFactory;

		State = ReactiveWindowControllerState.WindowClosed;

		_subscription = state
			.ObserveOn(dispatcherScheduler)
			.Subscribe(UpdateState);
	}

	public IReactiveWindow? Window { get; private set; }
	public TViewModel? ViewModel => Window != null ? (TViewModel?)Window.Content : default;
	public ReactiveWindowControllerState State { get; private set; }

	public void Dispose()
	{
		_subscription.Dispose();
	}

	private void UpdateState(ReactiveWindowControllerState state)
	{
		if (state != State)
		{
			if (state == ReactiveWindowControllerState.WindowVisible)
				Show();
			else if (state == ReactiveWindowControllerState.WindowHidden)
				Hide();
			else if (state == ReactiveWindowControllerState.WindowClosed)
				Close();
		}
	}

	private void Show()
	{
		if (Window == null)
		{
			Window = _windowFactory.CreateWindow(_windowName, _createViewModel);
			Window.Closed += (_, _) =>
			{
				if (State != ReactiveWindowControllerState.WindowClosed)
				{
					State = ReactiveWindowControllerState.WindowClosed;
					Window = null;
					_onClosed();
				}
			};
		}

		State = ReactiveWindowControllerState.WindowVisible;
		Window.Show();
	}

	private void Hide()
	{
		State = ReactiveWindowControllerState.WindowHidden;
		Window?.Hide();
	}

	private void Close()
	{
		State = ReactiveWindowControllerState.WindowClosed;
		Window?.Close();
		Window = null;
	}
}
