﻿namespace AbreVinci.Embryo.ReactiveUI.Internal;

internal interface IReactiveBinding
{
	void Activate();
	void Deactivate();
}
