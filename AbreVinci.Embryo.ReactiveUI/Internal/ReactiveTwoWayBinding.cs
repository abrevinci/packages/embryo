﻿namespace AbreVinci.Embryo.ReactiveUI.Internal;

internal class ReactiveTwoWayBinding<T> : IReactiveTwoWayBinding<T>, IReactiveBinding
{
	private readonly WeakReference<IReactiveViewModel> _viewModel;
	private readonly PropertyChangedEventArgs _propertyChangedEventArgs;
	private readonly IObservable<T> _value;
	private readonly bool _raiseLatestValueOnSubscribe;
	private readonly IScheduler _uiScheduler;
    private readonly Subject<T> _source;

	private IDisposable? _valueSubscription;
	private T _currentValue;
	private bool _isDisposed;
	private int _activations;

	public ReactiveTwoWayBinding(
		IObservable<T> value, 
		IReactiveViewModel viewModel, 
		string propertyName, 
		bool raiseLatestValueOnSubscribe,
		IScheduler uiScheduler,
		IReactiveViewModelHost host)
	{
        _viewModel = new WeakReference<IReactiveViewModel>(viewModel);
		_propertyChangedEventArgs = new PropertyChangedEventArgs(propertyName);
		_value = value;
		_raiseLatestValueOnSubscribe = raiseLatestValueOnSubscribe;
		_uiScheduler = uiScheduler;
        _source = new Subject<T>();
		_currentValue = default!;

#if DEBUG
		Debug.WriteLine($"Creating two way binding for {Target}");
#endif

		if (host.AutoActivate)
        {
			Activate();
			_activations++;
        }
	}

#if DEBUG
	public string Target => _viewModel.TryGetTarget(out var target) ? $"{target.GetType().Name}.{_propertyChangedEventArgs.PropertyName}" : $"<unknown>.{_propertyChangedEventArgs.PropertyName}";
#endif

	public void Activate()
	{
		if (_activations == 0)
		{
			Debug.Assert(_valueSubscription == null);

#if DEBUG
			Debug.WriteLine($"Activating two way binding for {Target}");
#endif

			_valueSubscription = _value
				.DistinctUntilChanged(StateComparer<T>.Instance)
				.Do(SetValueInternal)
				.ObserveOn(_uiScheduler)
				.Subscribe(_ => RaisePropertyChanged());
		}
		_activations++;
	}

	public void Deactivate()
	{
		if (!_isDisposed)
		{
			_activations--;
			if (_activations == 0)
			{
				Debug.Assert(_valueSubscription != null);

#if DEBUG
				Debug.WriteLine($"Deactivating two way binding for {Target}");
#endif

				_valueSubscription?.Dispose();
				_valueSubscription = null;
			}
		}
	}

	public T Value
	{
		get => _currentValue;
		set => SetValue(value);
	}

	public IDisposable Subscribe(IObserver<T> observer)
	{
		if (_raiseLatestValueOnSubscribe)
			observer.OnNext(_currentValue);
		return _source.Subscribe(observer);
	}

	public void Dispose()
	{
		if (!_isDisposed)
		{
#if DEBUG
			Debug.WriteLine($"Disposing two way binding for {Target}");
#endif
			_source.OnCompleted();
			_source.Dispose();
			_valueSubscription?.Dispose();
			_isDisposed = true;
		}
	}

	private void SetValue(T value)
	{
		if (!_isDisposed)
		{
			SetValueInternal(value);
			_source.OnNext(value);
		}
	}

	private void SetValueInternal(T value)
	{
		_currentValue = value;
	}

	private void RaisePropertyChanged()
	{
		if (_viewModel.TryGetTarget(out var viewModel))
			viewModel.RaisePropertyChanged(_propertyChangedEventArgs);
	}
}
