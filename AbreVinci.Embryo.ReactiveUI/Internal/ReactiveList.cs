﻿using ListDiff;

namespace AbreVinci.Embryo.ReactiveUI.Internal;

internal class ReactiveList<T> : IReactiveList<T>, IReactiveCollectionViewContext
{
	// ReSharper disable once StaticMemberInGenericType
	private static readonly PropertyChangedEventArgs _isEmptyEventArgs = new(nameof(IsEmpty));
	// ReSharper disable once StaticMemberInGenericType
	private static readonly PropertyChangedEventArgs _countEventArgs = new(nameof(Count));

	private readonly IObservable<IImmutableList<T>> _values;
	private readonly Func<T, T, bool> _match;
    private readonly Subject<IImmutableList<T>> _source;
    private readonly CompositeDisposable _disposables;

	private ObservableCollection<T>? _currentValues;
	private int _activations;
	private IDisposable? _valuesSubscription;
	private bool _isDisposed;

	public ReactiveList(
		IObservable<IImmutableList<T>> values, 
		Func<T, T, bool> match, 
		IScheduler uiScheduler,
		IReactiveViewModelHost host)
	{
        _values = values;
		_match = match;
		_source = new Subject<IImmutableList<T>>();
        _disposables = new CompositeDisposable();
        UiScheduler = uiScheduler;
        ViewModelHost = host;

		if (host.AutoActivate)
        {
			_currentValues = new ObservableCollection<T>();
			Activate();
			_activations++;
        }
	}

	public bool IsEmpty => _currentValues?.Any() != true;
	public int Count => _currentValues?.Count ?? default;

	public IScheduler UiScheduler { get; }
	public IReactiveViewModelHost ViewModelHost { get; }

	private void Activate()
	{
		Debug.Assert(_valuesSubscription == null);

		_valuesSubscription = _values
			.DistinctUntilChanged(StateComparer<IImmutableList<T>>.Instance)
			.Subscribe(SetValues);
	}

	private void Deactivate()
	{
		if (!_isDisposed)
		{
			Debug.Assert(_valuesSubscription != null);

			_valuesSubscription?.Dispose();
			_valuesSubscription = null;
		}
	}

	private event PropertyChangedEventHandler? InternalPropertyChanged;

	public event PropertyChangedEventHandler? PropertyChanged
	{
		add
		{
			if (_activations == 0 && _currentValues == null)
			{
				_currentValues = new ObservableCollection<T>();
			}
			if (_currentValues != null)
			{
				InternalPropertyChanged += value;
			}
			if (_activations == 0)
			{
				Activate();
			}
			_activations++;
		}
		remove
		{
			_activations--;
			if (_activations == 0)
			{
				Deactivate();
			}
			if (_currentValues != null)
			{
				InternalPropertyChanged -= value;
			}
			if (_activations == 0 && _currentValues != null)
			{
				_currentValues = null;
			}
		}
	}

	public event NotifyCollectionChangedEventHandler? CollectionChanged
	{
		add
		{
			if (_activations == 0 && _currentValues == null)
			{
				_currentValues = new ObservableCollection<T>();
			}
			if (_currentValues != null)
			{
				_currentValues.CollectionChanged += value;
			}
			if(_activations == 0)
			{
				Activate();
			}
			_activations++;
		}
		remove
		{
			_activations--;
			if(_activations == 0)
			{
				Deactivate();
			}
			if (_currentValues != null)
			{
				_currentValues.CollectionChanged -= value;
			}
			if (_activations == 0 && _currentValues != null)
            {
				_currentValues = null;
            }
		}
	}

	public IDisposable Subscribe(IObserver<IImmutableList<T>> observer)
	{
		return _source.Subscribe(observer);
	}

	public void Dispose()
	{
		if (!_isDisposed)
		{
			_source.OnCompleted();
			_source.Dispose();
			_valuesSubscription?.Dispose();
			_currentValues = null;
			_disposables.Dispose();
			_isDisposed = true;
		}
	}

	private void SetValues(IImmutableList<T>? values)
	{
		Debug.Assert(_currentValues != null);

		var temp = _currentValues!.ToList();

		if (values == null || values.Count == 0)
		{
			temp.Clear();
		}
		else
		{
			temp.MergeInto(values, _match);
		}

		_source.OnNext(temp.ToImmutableArray());

		UiScheduler.Schedule(() =>
		{
			if (_currentValues != null)
			{
				var isEmptyBefore = _currentValues.Count == 0;
				var countBefore = _currentValues.Count;
				_currentValues.MergeInto(temp, (v1, v2) => StateComparer<T>.Instance.Equals(v1, v2));
				if (isEmptyBefore != (_currentValues.Count == 0))
					InternalPropertyChanged?.Invoke(this, _isEmptyEventArgs);
				if (countBefore != _currentValues.Count)
					InternalPropertyChanged?.Invoke(this, _countEventArgs);
			}
		});
	}

	public IEnumerator<T> GetEnumerator() => _currentValues?.GetEnumerator() ?? Enumerable.Empty<T>().GetEnumerator();
	IEnumerator IEnumerable.GetEnumerator() => _currentValues?.GetEnumerator() ?? Enumerable.Empty<T>().GetEnumerator();
	
	public void AddLinkedDisposable(IDisposable disposable)
	{
		_disposables.Add(disposable);
	}
}
