﻿namespace AbreVinci.Embryo.ReactiveUI.Internal;

internal class FilteredReactiveCollectionView<T, TFilterParams> : IFilteredReactiveCollectionView<T>, IDisposable, IReactiveCollectionViewContext
{
	// ReSharper disable once StaticMemberInGenericType
	private static readonly PropertyChangedEventArgs _isEmptyEventArgs = new(nameof(IsEmpty));
	// ReSharper disable once StaticMemberInGenericType
	private static readonly PropertyChangedEventArgs _countEventArgs = new(nameof(Count));
	// ReSharper disable once StaticMemberInGenericType
	private static readonly PropertyChangedEventArgs _hasHiddenItemsEventArgs = new(nameof(HasHiddenItems));
	// ReSharper disable once StaticMemberInGenericType
	private static readonly PropertyChangedEventArgs _sourceCountEventArgs = new(nameof(SourceCount));

	private readonly IObservable<TFilterParams>? _filterParamsSource;
	private readonly IReactiveCollectionView<T> _source;
	private readonly Func<TFilterParams, T, bool> _filter;
	private readonly CompositeDisposable _disposables;
	private readonly List<T> _original;
	private readonly List<int> _filteredIndexes;

	private IDisposable? _subscription;
	private TFilterParams _filterParams;
	private int _activationCount;
	private bool _isDisposed;

	public FilteredReactiveCollectionView(
		IObservable<TFilterParams>? filterParamsSource, 
		IReactiveCollectionView<T> source, 
		Func<TFilterParams, T, bool> filter)
	{
		var parentContext = source as IReactiveCollectionViewContext;

		_filterParamsSource = filterParamsSource;
		_source = source;
		_filter = filter;
		_disposables = new CompositeDisposable();
		_original = new List<T>();
		_filteredIndexes = new List<int>();
		_filterParams = default!;

		UiScheduler = source.UiScheduler;
		ViewModelHost = source.ViewModelHost;

		if (ViewModelHost.AutoActivate)
		{
			Activate();
			_activationCount++;
		}

		parentContext?.AddLinkedDisposable(this);
	}

	private void Activate()
	{
		if (_filterParamsSource != null)
		{
			Debug.Assert(_subscription == null);
			_subscription = _filterParamsSource.DistinctUntilChanged().Subscribe(filterParams =>
			{
				_filterParams = filterParams;
				Refresh();
			});
		}
		else
		{
			Refresh();
		}
		_source.CollectionChanged += OnSourceCollectionChanged;
	}

	private void Deactivate()
	{
		if (!_isDisposed)
		{
			_source.CollectionChanged -= OnSourceCollectionChanged;

			Debug.Assert(_subscription != null || _filterParamsSource == null);
			_subscription?.Dispose();
			_subscription = null;
		}
	}

	public bool IsEmpty => _filteredIndexes.Count == 0;
	public int Count => _filteredIndexes.Count;
	public bool HasHiddenItems => _filteredIndexes.Count < _original.Count;
	public int SourceCount => _original.Count;

	public IScheduler UiScheduler { get; }
	public IReactiveViewModelHost ViewModelHost { get; }

	private event NotifyCollectionChangedEventHandler? InternalCollectionChanged;

	public event NotifyCollectionChangedEventHandler? CollectionChanged
	{
		add
		{
			if (_activationCount == 0)
			{
				Activate();
			}
			InternalCollectionChanged += value;
			_activationCount++;
		}
		remove
		{
			_activationCount--;
			InternalCollectionChanged -= value;
			if (_activationCount == 0)
			{
				Deactivate();
			}
		}
	}

	private event PropertyChangedEventHandler? InternalPropertyChanged;

	public event PropertyChangedEventHandler? PropertyChanged
	{
		add
		{
			if (_activationCount == 0)
			{
				Activate();
			}
			InternalPropertyChanged += value;
			_activationCount++;
		}
		remove
		{
			_activationCount--;
			InternalPropertyChanged -= value;
			if (_activationCount == 0)
			{
				Deactivate();
			}
		}
	}

	public void Dispose()
	{
		if (!_isDisposed)
		{
			_subscription?.Dispose();
			_subscription = null;
			_disposables.Dispose();
			_isDisposed = true;
		}
	}

	private void HandleItemAdded(T item, int index)
	{
		NotifyOfPropertyChanges(() =>
		{
			if (item is INotifyPropertyChanged npc)
				npc.PropertyChanged += OnItemPropertyChanged;

			_original.Insert(index, item);

			var accepted = _filter(_filterParams, item);
			var insertPos = -1;
			if (accepted)
			{
				insertPos = _filteredIndexes.FindIndex(i => i >= index);
				if (insertPos == -1)
					insertPos = _filteredIndexes.Count;
			}

			for (var i = 0; i < _filteredIndexes.Count; i++)
			{
				if (_filteredIndexes[i] >= index)
					_filteredIndexes[i]++;
			}

			if (accepted)
			{
				_filteredIndexes.Insert(insertPos, index);
				RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, insertPos));
			}
		});
	}

	private void HandleItemRemoved(T item, int index)
	{
		NotifyOfPropertyChanges(() =>
		{
			var removePos = _filteredIndexes.IndexOf(index);
			if (removePos >= 0)
				_filteredIndexes.RemoveAt(removePos);
			
			for (var i = 0; i < _filteredIndexes.Count; i++)
			{
				if (_filteredIndexes[i] > index)
					_filteredIndexes[i]--;
			}

			_original.RemoveAt(index);

			if (item is INotifyPropertyChanged npc)
				npc.PropertyChanged -= OnItemPropertyChanged;

			if (removePos >= 0)
				RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, removePos));
		});
	}

	private void Refresh()
	{
		NotifyOfPropertyChanges(() =>
		{
			foreach (var npc in _original.OfType<INotifyPropertyChanged>())
				npc.PropertyChanged -= OnItemPropertyChanged;

			_original.Clear();
			_filteredIndexes.Clear();

			_original.AddRange(_source);

			foreach (var npc in _original.OfType<INotifyPropertyChanged>())
				npc.PropertyChanged += OnItemPropertyChanged;

			_filteredIndexes.AddRange(_original.Select((_, i) => i).Where(i => _filter(_filterParams, _original[i])));

			RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
		});
	}

	private void OnSourceCollectionChanged(object? sender, NotifyCollectionChangedEventArgs e)
	{
		if (e.Action == NotifyCollectionChangedAction.Add && e.NewItems is { Count: 1 } newItems && newItems[0] is T newItem)
		{
			HandleItemAdded(newItem, e.NewStartingIndex);
		}
		else if (e.Action == NotifyCollectionChangedAction.Remove && e.OldItems is { Count: 1 } oldItems && oldItems[0] is T oldItem)
		{
			HandleItemRemoved(oldItem, e.OldStartingIndex);
		}
		else
		{
			Refresh();
		}
	}

	private void OnItemPropertyChanged(object? sender, PropertyChangedEventArgs e)
	{
		NotifyOfPropertyChanges(() =>
		{
			if (sender is T item)
			{
				var index = _original.IndexOf(item);
				if (_filter(_filterParams, item))
				{
					if (!_filteredIndexes.Contains(index))
					{
						var insertPos = _filteredIndexes.FindIndex(i => i >= index);
						if (insertPos == -1)
							insertPos = _filteredIndexes.Count;
					
						_filteredIndexes.Insert(insertPos, index);
						RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, insertPos));
					}
				}
				else
				{
					var removePos = _filteredIndexes.IndexOf(index);
					if (removePos >= 0)
					{
						_filteredIndexes.RemoveAt(removePos);
						RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, removePos));
					}
				}
			}
		});
	}

	private void NotifyOfPropertyChanges(Action action)
	{
		var isEmpty = IsEmpty;
		var hasHiddenItems = HasHiddenItems;
		var count = Count;
		var sourceCount = SourceCount;

		action();

		if (IsEmpty != isEmpty)
			RaisePropertyChanged(_isEmptyEventArgs);
		if (HasHiddenItems != hasHiddenItems)
			RaisePropertyChanged(_hasHiddenItemsEventArgs);
		if (Count != count)
			RaisePropertyChanged(_countEventArgs);
		if (SourceCount != sourceCount)
			RaisePropertyChanged(_sourceCountEventArgs);
	}

	private void RaiseCollectionChanged(NotifyCollectionChangedEventArgs e)
	{
		UiScheduler.Schedule(() => InternalCollectionChanged?.Invoke(this, e));
	}

	private void RaisePropertyChanged(PropertyChangedEventArgs e)
	{
		UiScheduler.Schedule(() => InternalPropertyChanged?.Invoke(this, e));
	}

	public IEnumerator<T> GetEnumerator() => _filteredIndexes.Select(i => _original[i]).GetEnumerator();
	IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

	public void AddLinkedDisposable(IDisposable disposable)
	{
		_disposables.Add(disposable);
	}
}
