﻿namespace AbreVinci.Embryo.ReactiveUI.Internal;

internal class ReactiveCommand<T> : IReactiveCommand<T>
{
	private readonly Subject<T> _trigger;
    private readonly IObservable<bool> _canExecute;
    private readonly bool _initialCanExecuteValue;
    private readonly IScheduler _uiScheduler;
    private IDisposable? _canExecuteSubscription;
	private bool _currentCanExecute;
	private bool _isDisposed;
	private int _activations;

	public ReactiveCommand(
		IObservable<bool> canExecute, 
		bool initialCanExecuteValue, 
		IScheduler uiScheduler, 
		IReactiveViewModelHost host)
	{
        _trigger = new Subject<T>();
        _canExecute = canExecute;
        _initialCanExecuteValue = initialCanExecuteValue;
        _uiScheduler = uiScheduler;

        if (host.AutoActivate)
        {
			Activate();
			_activations++;
        }
    }

	private void Activate()
    {
		Debug.Assert(_canExecuteSubscription == null);

		_canExecuteSubscription = _canExecute
			.StartWith(_initialCanExecuteValue)
			.DistinctUntilChanged()
			.Do(SetCanExecute)
			.ObserveOn(_uiScheduler)
			.Subscribe(_ => RaiseCanExecuteChanged());
	}

	private void Deactivate()
    {
		if (!_isDisposed)
		{
			Debug.Assert(_canExecuteSubscription != null);

			_canExecuteSubscription?.Dispose();
			_canExecuteSubscription = null;
		}
	}

	public IDisposable Subscribe(IObserver<T> observer)
	{
		return _trigger.Subscribe(observer);
	}

	public bool CanExecute(object? parameter)
	{
		return _currentCanExecute;
	}

	public void Execute(object? parameter)
	{
		_trigger.OnNext((T)parameter!);
	}

	private event EventHandler? InternalCanExecuteChanged;

	public event EventHandler? CanExecuteChanged
    {
        add
        {
			InternalCanExecuteChanged += value;
			if (_activations == 0)
            {
				Activate();
            }
			_activations++;
		}
		remove
        {
			_activations--;
			if (_activations == 0)
            {
				Deactivate();
            }
			InternalCanExecuteChanged -= value;
        }
    }

	public void Dispose()
	{
		if (!_isDisposed)
		{
			_trigger.OnCompleted();
			_trigger.Dispose();
			_canExecuteSubscription?.Dispose();
			_isDisposed = true;
		}
	}

	private void SetCanExecute(bool value)
	{
		_currentCanExecute = value;
	}

	private void RaiseCanExecuteChanged()
	{
		InternalCanExecuteChanged?.Invoke(this, EventArgs.Empty);
	}
}

internal class ReactiveCommand : ReactiveCommand<object>, IReactiveCommand
{
	public ReactiveCommand(IObservable<bool> canExecuteSource, bool initialCanExecuteValue, IScheduler uiScheduler, IReactiveViewModelHost host)
		: base(canExecuteSource, initialCanExecuteValue, uiScheduler, host)
	{
	}

	public IDisposable Subscribe(Action onNext)
	{
		return this.Subscribe(_ => onNext());
	}
}
