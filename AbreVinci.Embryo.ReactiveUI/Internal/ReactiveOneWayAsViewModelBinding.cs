﻿namespace AbreVinci.Embryo.ReactiveUI.Internal;

internal class ReactiveOneWayAsViewModelBinding<TModel, TViewModel> : IReactiveOneWayBinding<TViewModel>, IReactiveBinding
{
	private readonly WeakReference<IReactiveViewModel> _viewModel;
	private readonly PropertyChangedEventArgs _propertyChangedEventArgs;
    private readonly IObservable<TModel> _value;
    private readonly Func<TViewModel, TModel, bool> _match;
	private readonly Func<TModel, TViewModel> _create;
	private readonly Action<TViewModel> _delete;
	private readonly bool _raiseLatestValueOnSubscribe;
    private readonly IScheduler _uiScheduler;
    private readonly Subject<TViewModel> _source;
		
	private IDisposable? _valueSubscription;
	private bool _isDisposed;
	private int _activations;

	public ReactiveOneWayAsViewModelBinding(
		IObservable<TModel> value,
		IReactiveViewModel viewModel,
		string propertyName,
		Func<TViewModel, TModel, bool> match,
		Func<TModel, TViewModel> create,
		Action<TViewModel> delete,
		bool raiseLatestValueOnSubscribe,
		IScheduler uiScheduler,
		IReactiveViewModelHost host)
	{
        _viewModel = new WeakReference<IReactiveViewModel>(viewModel);
		_propertyChangedEventArgs = new PropertyChangedEventArgs(propertyName);
        _value = value;
        _match = match;
		_create = create;
		_delete = delete;
		_raiseLatestValueOnSubscribe = raiseLatestValueOnSubscribe;
        _uiScheduler = uiScheduler;
        _source = new Subject<TViewModel>();
		Value = default!;

#if DEBUG
		Debug.WriteLine($"Creating one way as view model binding for {Target}");
#endif

		if (host.AutoActivate)
		{
			Activate();
			_activations++;
		}
	}

#if DEBUG
	public string Target => _viewModel.TryGetTarget(out var target) ? $"{target.GetType().Name}.{_propertyChangedEventArgs.PropertyName}" : $"<unknown>.{_propertyChangedEventArgs.PropertyName}";
#endif

	public void Activate()
	{
		if (_activations == 0)
		{
			Debug.Assert(_valueSubscription == null);

#if DEBUG
			Debug.WriteLine($"Activating one way as view model binding for {Target}");
#endif

			_valueSubscription = _value
				.DistinctUntilChanged(StateComparer<TModel>.Instance)
				.Do(SetValue)
				.ObserveOn(_uiScheduler)
				.Subscribe(_ => RaisePropertyChanged());
		}
		_activations++;
	}

	public void Deactivate()
	{
		if (!_isDisposed)
		{
			_activations--;
			if (_activations == 0)
			{
				Debug.Assert(_valueSubscription != null);

#if DEBUG
				Debug.WriteLine($"Dectivating one way as view model binding for {Target}");
#endif

				_valueSubscription?.Dispose();
				_valueSubscription = null;
			}
		}
	}

	public TViewModel Value { get; private set; }

	public IDisposable Subscribe(IObserver<TViewModel> observer)
	{
		if (_raiseLatestValueOnSubscribe)
			observer.OnNext(Value);
		return _source.Subscribe(observer);
	}

	public void Dispose()
	{
		if (!_isDisposed)
		{
#if DEBUG
			Debug.WriteLine($"Disposing one way as view model binding for {Target}");
#endif
			_source.OnCompleted();
			_source.Dispose();
			_valueSubscription?.Dispose();

			if (Value != null)
				_delete(Value);

			_isDisposed = true;
		}
	}

	private void SetValue(TModel value)
	{
		if (Value == null && value == null)
			return;

		if (Value != null && value != null && _match(Value, value))
			return;

		if (Value != null)
			_delete(Value);

		Value = value != null ? _create(value) : default!;
		_source.OnNext(Value!);
	}

	private void RaisePropertyChanged()
	{
		if (_viewModel.TryGetTarget(out var viewModel))
			viewModel.RaisePropertyChanged(_propertyChangedEventArgs);
	}
}
