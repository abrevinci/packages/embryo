﻿namespace AbreVinci.Embryo.ReactiveUI.Internal;

internal class PagedReactiveCollectionView<T, TPagingParams> : IPagedReactiveCollectionView<T>, IDisposable, IReactiveCollectionViewContext
{
	// ReSharper disable once StaticMemberInGenericType
	private static readonly PropertyChangedEventArgs _isEmptyEventArgs = new(nameof(IsEmpty));
	// ReSharper disable once StaticMemberInGenericType
	private static readonly PropertyChangedEventArgs _countEventArgs = new(nameof(Count));
	// ReSharper disable once StaticMemberInGenericType
	private static readonly PropertyChangedEventArgs _hasHiddenItemsBeforeEventArgs = new(nameof(HasHiddenItemsBefore));
	// ReSharper disable once StaticMemberInGenericType
	private static readonly PropertyChangedEventArgs _hasHiddenItemsAfterEventArgs = new(nameof(HasHiddenItemsAfter));
	// ReSharper disable once StaticMemberInGenericType
	private static readonly PropertyChangedEventArgs _sourceCountEventArgs = new(nameof(SourceCount));

	private readonly IObservable<TPagingParams>? _pagingParamsSource;
	private readonly IReactiveCollectionView<T> _source;
	private readonly Func<TPagingParams, int> _getStartIndex;
	private readonly Func<TPagingParams, int> _getMaxCount;
	private readonly CompositeDisposable _disposables;
	private readonly List<T> _original;
	private readonly List<int> _pagedIndexes;

	private IDisposable? _subscription;
	private TPagingParams _pagingParams;
	private int _activationCount;
	private bool _isDisposed;

	public PagedReactiveCollectionView(
		IObservable<TPagingParams>? pagingParamsSource,
		IReactiveCollectionView<T> source,
		Func<TPagingParams, int> getStartIndex,
		Func<TPagingParams, int> getMaxCount)
	{
		var parentContext = source as IReactiveCollectionViewContext;

		_pagingParamsSource = pagingParamsSource;
		_source = source;
		_getStartIndex = getStartIndex;
		_getMaxCount = getMaxCount;
		_disposables = new CompositeDisposable();
		_original = new List<T>();
		_pagedIndexes = new List<int>();
		_pagingParams = default!;

		UiScheduler = source.UiScheduler;
		ViewModelHost = source.ViewModelHost;

		if (ViewModelHost.AutoActivate)
		{
			Activate();
			_activationCount++;
		}

		parentContext?.AddLinkedDisposable(this);
	}

	private void Activate()
	{
		if (_pagingParamsSource != null)
		{
			Debug.Assert(_subscription == null);
			_subscription = _pagingParamsSource.DistinctUntilChanged().Subscribe(pagingParams =>
			{
				_pagingParams = pagingParams;
				Refresh();
			});
		}
		else
		{
			Refresh();
		}
		_source.CollectionChanged += OnSourceCollectionChanged;
	}

	private void Deactivate()
	{
		if (!_isDisposed)
		{
			_source.CollectionChanged -= OnSourceCollectionChanged;

			Debug.Assert(_subscription != null || _pagingParamsSource == null);
			_subscription?.Dispose();
			_subscription = null;
		}
	}

	public bool IsEmpty => _pagedIndexes.Count == 0;
	public int Count => _pagedIndexes.Count;
	public bool HasHiddenItemsBefore => _original.Any() && _pagedIndexes.All(i => i != 0);
	public bool HasHiddenItemsAfter => _original.Any() && _pagedIndexes.All(i => i != _original.Count - 1);
	public int SourceCount => _original.Count;

	public IScheduler UiScheduler { get; }
	public IReactiveViewModelHost ViewModelHost { get; }

	private event NotifyCollectionChangedEventHandler? InternalCollectionChanged;

	public event NotifyCollectionChangedEventHandler? CollectionChanged
	{
		add
		{
			if (_activationCount == 0)
			{
				Activate();
			}
			InternalCollectionChanged += value;
			_activationCount++;
		}
		remove
		{
			_activationCount--;
			InternalCollectionChanged -= value;
			if (_activationCount == 0)
			{
				Deactivate();
			}
		}
	}

	private event PropertyChangedEventHandler? InternalPropertyChanged;

	public event PropertyChangedEventHandler? PropertyChanged
	{
		add
		{
			if (_activationCount == 0)
			{
				Activate();
			}
			InternalPropertyChanged += value;
			_activationCount++;
		}
		remove
		{
			_activationCount--;
			InternalPropertyChanged -= value;
			if (_activationCount == 0)
			{
				Deactivate();
			}
		}
	}

	public void Dispose()
	{
		if (!_isDisposed)
		{
			_subscription?.Dispose();
			_subscription = null;
			_disposables.Dispose();
			_isDisposed = true;
		}
	}

	private void HandleItemAdded(T item, int index)
	{
		NotifyOfPropertyChanges(() =>
		{
			if (item is INotifyPropertyChanged npc)
				npc.PropertyChanged += OnItemPropertyChanged;

			_original.Insert(index, item);
		
			var startIndex = _getStartIndex(_pagingParams);
			var count = _getMaxCount(_pagingParams);
			var endIndex = startIndex + count;

			_pagedIndexes.Clear();
			_pagedIndexes.AddRange(Enumerable.Range(startIndex, count).Where(i => i < _original.Count));

			if (index < startIndex)
			{
				if (_original.Count > startIndex) // shifting in first item
					RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, _original[startIndex], 0));
				if (_original.Count > endIndex) // pushing out last item
					RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, _original[endIndex], count));
			}
			else if (index < endIndex)
			{
				RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, index - startIndex));
				if (_original.Count > endIndex) // pushing out last item
					RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, _original[endIndex], count));
			}
		});
	}

	private void HandleItemRemoved(T item, int index)
	{
		NotifyOfPropertyChanges(() =>
		{
			var startIndex = _getStartIndex(_pagingParams);
			var count = _getMaxCount(_pagingParams);
			var endIndex = startIndex + count;

			var originalBefore = _original.ToList();

			_original.RemoveAt(index);

			_pagedIndexes.Clear();
			_pagedIndexes.AddRange(Enumerable.Range(startIndex, count).Where(i => i < _original.Count));

			if (item is INotifyPropertyChanged npc)
				npc.PropertyChanged -= OnItemPropertyChanged;

			if (index < startIndex)
			{
				if (originalBefore.Count > endIndex) // shifting in last item
					RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, originalBefore[endIndex], count));
				RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, originalBefore[startIndex], 0));
			}
			else if (index < endIndex)
			{
				if (originalBefore.Count > endIndex) // shifting in last item
					RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, originalBefore[endIndex], count));
				RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, index - startIndex));
			}
		});
	}

	private void Refresh()
	{
		NotifyOfPropertyChanges(() =>
		{
			_original.Clear();
			_pagedIndexes.Clear();

			_original.AddRange(_source);

			_pagedIndexes.AddRange(_original.Select((_, i) => i).Skip(_getStartIndex(_pagingParams)).Take(_getMaxCount(_pagingParams)));

			RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
		});
	}

	private void OnSourceCollectionChanged(object? sender, NotifyCollectionChangedEventArgs e)
	{
		if (e.Action == NotifyCollectionChangedAction.Add && e.NewItems is { Count: 1 } newItems && newItems[0] is T newItem)
		{
			HandleItemAdded(newItem, e.NewStartingIndex);
		}
		else if (e.Action == NotifyCollectionChangedAction.Remove && e.OldItems is { Count: 1 } oldItems && oldItems[0] is T oldItem)
		{
			HandleItemRemoved(oldItem, e.OldStartingIndex);
		}
		else
		{
			Refresh();
		}
	}

	private void OnItemPropertyChanged(object? sender, PropertyChangedEventArgs e)
	{
	}

	private void NotifyOfPropertyChanges(Action action)
	{
		var isEmpty = IsEmpty;
		var hasHiddenItemsBefore = HasHiddenItemsBefore;
		var hasHiddenItemsAfter = HasHiddenItemsAfter;
		var count = Count;
		var sourceCount = SourceCount;

		action();

		if (IsEmpty != isEmpty)
			RaisePropertyChanged(_isEmptyEventArgs);
		if (HasHiddenItemsBefore != hasHiddenItemsBefore)
			RaisePropertyChanged(_hasHiddenItemsBeforeEventArgs);
		if (HasHiddenItemsAfter != hasHiddenItemsAfter)
			RaisePropertyChanged(_hasHiddenItemsAfterEventArgs);
		if (Count != count)
			RaisePropertyChanged(_countEventArgs);
		if (SourceCount != sourceCount)
			RaisePropertyChanged(_sourceCountEventArgs);
	}

	private void RaiseCollectionChanged(NotifyCollectionChangedEventArgs e)
	{
		UiScheduler.Schedule(() => InternalCollectionChanged?.Invoke(this, e));
	}

	private void RaisePropertyChanged(PropertyChangedEventArgs e)
	{
		UiScheduler.Schedule(() => InternalPropertyChanged?.Invoke(this, e));
	}

	public IEnumerator<T> GetEnumerator() => _pagedIndexes.Select(i => _original[i]).GetEnumerator();
	IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

	public void AddLinkedDisposable(IDisposable disposable)
	{
		_disposables.Add(disposable);
	}
}
