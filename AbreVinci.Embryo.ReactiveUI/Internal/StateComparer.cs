namespace AbreVinci.Embryo.ReactiveUI.Internal;

internal class StateComparer<T>
{
	private class Value : IEqualityComparer<T>
	{
		public bool Equals(T? x, T? y)
		{
			return x == null && y == null || (x != null && y != null && x.Equals(y));
		}

		public int GetHashCode(T obj)
		{
			return RuntimeHelpers.GetHashCode(obj);
		}
	}

	private class Reference : IEqualityComparer<T>
	{
		public bool Equals(T? x, T? y)
		{
			if (ReferenceEquals(x, y))
				return true;

			if (x == null || y == null)
				return false;

			return x.Equals(y);
		}

		public int GetHashCode(T obj)
		{
			return RuntimeHelpers.GetHashCode(obj);
		}
	}

	public static readonly IEqualityComparer<T> Instance = typeof(T).IsValueType ? new Value() : new Reference();
}
