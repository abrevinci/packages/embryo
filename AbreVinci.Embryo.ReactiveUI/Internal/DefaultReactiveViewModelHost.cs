﻿namespace AbreVinci.Embryo.ReactiveUI.Internal;

internal class DefaultReactiveViewModelHost : IReactiveViewModelHost
{
	public static IReactiveViewModelHost Instance { get; } = new DefaultReactiveViewModelHost();

    public bool AutoActivate => false;

    public IScheduler UiScheduler => throw new NotSupportedException();

    public TViewModel HostViewModel<TViewModel>(Func<TViewModel> create)
    {
		return create();
    }
}
