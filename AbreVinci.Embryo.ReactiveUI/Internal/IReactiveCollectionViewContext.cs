﻿namespace AbreVinci.Embryo.ReactiveUI.Internal;

internal interface IReactiveCollectionViewContext
{
	void AddLinkedDisposable(IDisposable disposable);
}
