﻿namespace AbreVinci.Embryo.ReactiveUI;

/// <summary>
/// A reactive collectionview that has had a Filter operator applied to it. This exposes a few useful properties.
/// </summary>
/// <typeparam name="T">The item type.</typeparam>
public interface IFilteredReactiveCollectionView<out T> : IReactiveCollectionView<T>
{
	/// <summary>
	/// Whether or not there are any items that did not pass the filter.
	/// </summary>
	bool HasHiddenItems { get; }

	/// <summary>
	/// The number of items in the source collection (the total number of items before applying the Filter operator).
	/// </summary>
	int SourceCount { get; }
}
