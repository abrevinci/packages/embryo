﻿namespace AbreVinci.Embryo.ReactiveUI;

/// <summary>
/// An observable one way property binding. If you are using AbreVinci.Embryo.Fody, use <see cref="ReactiveViewModel.BindOneWay{TValue}(IObservable{TValue})"/> 
/// to create one for a simple model property and use 
/// <see cref="ReactiveViewModel.BindOneWayAsViewModel{TValue, TViewModel}(IObservable{TValue}, Func{TValue, TViewModel}, Func{TViewModel, TValue, bool})"/>
/// to create one for view model items. If you are not using AbreVinci.Embryo.Fody, you should instead use
/// <see cref="ReactiveViewModel.CreateOneWayBinding{TValue}(IObservable{TValue}, string)"/> and
/// <see cref="ReactiveViewModel.CreateOneWayAsViewModelBinding{TValue, TViewModel}(IObservable{TValue}, Func{TValue, TViewModel}, Func{TViewModel, TValue, bool}, string)"/>
/// respectively.
/// </summary>
/// <typeparam name="T">The property type.</typeparam>
public interface IReactiveOneWayBinding<out T> : IObservable<T>, IDisposable
{
	/// <summary>
	/// The current value of the property binding.
	/// </summary>
	T Value { get; }
}
