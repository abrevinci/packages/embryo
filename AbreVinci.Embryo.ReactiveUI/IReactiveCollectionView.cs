﻿namespace AbreVinci.Embryo.ReactiveUI;

/// <summary>
/// An observable list.
/// </summary>
/// <typeparam name="T">The item type.</typeparam>
public interface IReactiveCollectionView<out T> : IEnumerable<T>, INotifyPropertyChanged, INotifyCollectionChanged
{
	/// <summary>
	/// Whether or not the list is empty.
	/// </summary>
	bool IsEmpty { get; }

	/// <summary>
	/// The number of items in the list.
	/// </summary>
	int Count { get; }

	/// <summary>
	/// The UI scheduler of this collection.
	/// </summary>
	IScheduler UiScheduler { get; }

	/// <summary>
	/// The view model host of this collection.
	/// </summary>
	IReactiveViewModelHost ViewModelHost { get; }
}
