﻿using AbreVinci.Embryo.ReactiveUI.Internal;
using AbreVinci.Embryo.ReactiveUI.Testing;

namespace AbreVinci.Embryo.ReactiveUI;

/// <summary>
/// Base class for view models that want an easy way to bind to IObservable streams and automatically handle change notifications.
/// </summary>
/// <remarks>
/// The view model bindings that this class creates are only active when someone has subscribed to INotifyPropertyChanged.PropertyChanged,
/// INotifyCollectionChanged.CollectionChanged and ICommand.CanExecuteChanged respectively. In other cases, properties will return default
/// values, collections will be empty and commands will not be executable.
/// </remarks>
public abstract class ReactiveViewModel : IReactiveViewModel, IDisposable
{
	/// <summary>
	/// When using AbreVinci.Embryo.Fody to weave automatic property bindings, sometimes the IL weaver may decide to ignore properties
	/// if the weaving would otherwise introduce errors. Setting this to true will cause the application to throw exceptions in those cases.
	/// Default true.
	/// </summary>
	[ExcludeFromCodeCoverage]
	public static bool ForceBindings { get; set; } = true;

	private readonly IReactiveViewModelHost _host;
	private readonly IScheduler _uiScheduler;
    private readonly CompositeDisposable _disposables;
	private readonly List<IReactiveBinding> _propertyBindings;

	private int _propertyChangedListenerCount;
	private event PropertyChangedEventHandler? InternalPropertyChanged;

	/// <summary>
	/// ReactiveViewModel constructor.
	/// </summary>
	/// <param name="uiScheduler">Optional specification of the scheduler that should run change notifications. Passing null will use the scheduler of the startup thread.</param>
    protected ReactiveViewModel(IScheduler? uiScheduler = null)
	{
		var host = ReactiveViewModelHost.Current;
		if (uiScheduler == null && host == null)
			throw new InvalidOperationException("If you do not specify a scheduler when creating a view model, you must use a ReactiveViewModelHost which is automatically used when you use the ReactiveUserInterface.");

		_host = host ?? DefaultReactiveViewModelHost.Instance;
		_uiScheduler = uiScheduler ?? host!.UiScheduler;
        _disposables = new CompositeDisposable();
	    _propertyBindings = new List<IReactiveBinding>();
    }

    /// <summary> 
    /// Raised when any reactive property has changed. 
    /// </summary> 
    public event PropertyChangedEventHandler? PropertyChanged
	{
		add
		{
			InternalPropertyChanged += value;
			if (_propertyChangedListenerCount == 0)
			{
				foreach (var binding in _propertyBindings)
				{
					binding.Activate();
				}
			}
			_propertyChangedListenerCount++;
		}
		remove
		{
			_propertyChangedListenerCount--;
			if (_propertyChangedListenerCount == 0)
			{
				foreach (var binding in _propertyBindings)
				{
					binding.Deactivate();
				}
			}
			InternalPropertyChanged -= value;
		}
	}

	/// <summary>
	/// Raises the PropertyChanged event with the given arguments. Note that this is usually not needed in client code.
	/// </summary>
	/// <param name="args">The arguments to pass along with the PropertyChanged event.</param>
	public void RaisePropertyChanged(PropertyChangedEventArgs args)
	{
		InternalPropertyChanged?.Invoke(this, args);
	}

	/// <summary>
	/// Disposes this view model and ensures that any remaining subscriptions for properties, lists, commands, and child view models are disposed.
	/// </summary>
	public void Dispose()
    {
		GC.SuppressFinalize(this);
        _disposables.Dispose();
    }

	/// <summary>
	/// Adds a child view model to this view model.
	/// </summary>
	/// <typeparam name="TChildViewModel">The type of the child view model</typeparam>
	/// <param name="create">Factory method for the child view model.</param>
	/// <returns>The added child view model.</returns>
	/// <remarks>
	/// If the child view model is disposable, it will be disposed when <see cref="Dispose"/> is called.
	/// </remarks>
	/// <example>
	///	[!code-csharp[Example](../../DocSnippets/ReactiveUI/ReactiveViewModelBase/AddChildViewModel/ViewModel.cs#Snippet)]
	/// </example>
	protected TChildViewModel AddChildViewModel<TChildViewModel>(Func<TChildViewModel> create)
    {
        var child = _host.HostViewModel(create);
        if (child is IDisposable disposable)
            _disposables.Add(disposable);
        return child;
    }

	/// <summary>
	/// For use with AbreVinci.Embryo.Fody. Compiles to <see cref="CreateOneWayBinding{TValue}(IObservable{TValue}, string)"/>.
	/// </summary>
	/// <example>
	///	[!code-csharp[Example](../../DocSnippets/ReactiveUI/ReactiveViewModelBase/BindOneWay/ViewModel.cs#Snippet)]
	/// AbreVinci.Embryo.Fody translates this to:
	/// [!code-csharp[Example](../../DocSnippets/ReactiveUI/ReactiveViewModelBase/CreateOneWayBinding/ViewModel.cs#Snippet)]
	/// </example>
	[ExcludeFromCodeCoverage]
	protected TValue BindOneWay<TValue>(IObservable<TValue> value)
    {
		// disable unused warnings
		_ = value;
		if (ForceBindings)
		{
			throw new InvalidOperationException("BindOneWay is never meant to be called. It is meant to be used together with the AbreVinci.Embryo.Fody package and would be translated to CreateOneWayBinding by the means of IL weaving.");
		}
		return default!;
	}

	/// <summary>
	/// Creates a binding for use with a get-only property.
	/// </summary>
	/// <typeparam name="TValue">The property type.</typeparam>
	/// <param name="value">The stream of values to observe.</param>
	/// <param name="propertyName">The name of the property (for change notifications).</param>
	/// <returns>A binding instance. Have the property getter return <see cref="IReactiveOneWayBinding{T}.Value"/>.</returns>
	/// <example>
	///	[!code-csharp[Example](../../DocSnippets/ReactiveUI/ReactiveViewModelBase/CreateOneWayBinding/ViewModel.cs#Snippet)]
	/// </example>
	protected IReactiveOneWayBinding<TValue> CreateOneWayBinding<TValue>(IObservable<TValue> value, string propertyName)
    {
	    var binding = new ReactiveOneWayBinding<TValue>(value, this, propertyName, false, _uiScheduler, _host);
        _propertyBindings.Add(binding);
        _disposables.Add(binding);
        return binding;
    }

	/// <summary>
	/// For use with AbreVinci.Embryo.Fody. Compiles to <see cref="CreateOneWayAsViewModelBinding{TValue, TViewModel}(IObservable{TValue}, Func{TValue, TViewModel}, Func{TViewModel, TValue, bool}, string)"/>.
	/// </summary>
	/// <example>
	///	[!code-csharp[Example](../../DocSnippets/ReactiveUI/ReactiveViewModelBase/BindOneWayAsViewModel/ViewModel.cs#Snippet)]
	/// AbreVinci.Embryo.Fody translates this to:
	/// [!code-csharp[Example](../../DocSnippets/ReactiveUI/ReactiveViewModelBase/CreateOneWayAsViewModelBinding/ViewModel.cs#Snippet)]
	/// </example>
	[ExcludeFromCodeCoverage]
	protected TViewModel BindOneWayAsViewModel<TValue, TViewModel>(IObservable<TValue> value, Func<TValue, TViewModel> create, Func<TViewModel, TValue, bool> match)
    {
		// disable unused warnings
		_ = (value, create, match);
		if (ForceBindings)
		{
			throw new InvalidOperationException("BindOneWayAsViewModel is never meant to be called. It is meant to be used together with the AbreVinci.Embryo.Fody package and would be translated to CreateOneWayAsViewModelBinding by the means of IL weaving.");
		}
		return default!;
    }

	/// <summary>
	/// Creates a binding for use with a get-only view model property. 
	/// Whenever a new value is received, a new view model is created for it if <paramref name="match"/> returns false for the existing view model.
	/// </summary>
	/// <typeparam name="TValue">The type of the incoming value.</typeparam>
	/// <typeparam name="TViewModel">The type of view model to transform that value to.</typeparam>
	/// <param name="value">The value observable stream.</param>
	/// <param name="create">Factory function for the view model based on the value.</param>
	/// <param name="match">A predicate that is supposed to return true when a view model should be reused (when it represents the same model).</param>
	/// <param name="propertyName">The name of the view model property (for change notifications).</param>
	/// <returns>A binding instance. Have the property getter return <see cref="IReactiveOneWayBinding{T}.Value"/>.</returns>
	/// <example>
	/// [!code-csharp[Example](../../DocSnippets/ReactiveUI/ReactiveViewModelBase/CreateOneWayAsViewModelBinding/ViewModel.cs#Snippet)]
	/// </example>
	protected IReactiveOneWayBinding<TViewModel> CreateOneWayAsViewModelBinding<TValue, TViewModel>(IObservable<TValue> value, Func<TValue, TViewModel> create, Func<TViewModel, TValue, bool> match, string propertyName)
    {
		var binding = new ReactiveOneWayAsViewModelBinding<TValue, TViewModel>(value, this, propertyName, match, v => _host.HostViewModel(() => create(v)), vm => (vm as IDisposable)?.Dispose(), false, _uiScheduler, _host);
		_propertyBindings.Add(binding);
		_disposables.Add(binding);
		return binding;
    }

	/// <summary>
	/// For use with AbreVinci.Embryo.Fody. Compiles to <see cref="CreateTwoWayBinding{TValue}(IObservable{TValue}, Action{TValue}, string)"/>.
	/// </summary>
	/// <example>
	///	[!code-csharp[Example](../../DocSnippets/ReactiveUI/ReactiveViewModelBase/BindTwoWay/ViewModel.cs#Snippet)]
	/// AbreVinci.Embryo.Fody translates this to:
	/// [!code-csharp[Example](../../DocSnippets/ReactiveUI/ReactiveViewModelBase/CreateTwoWayBinding/ViewModel.cs#Snippet)]
	/// </example>
	[ExcludeFromCodeCoverage]
	protected TValue BindTwoWay<TValue>(IObservable<TValue> value, Action<TValue> onSet)
    {
		// disable unused warnings
		_ = (value, onSet);
		if (ForceBindings)
		{
			throw new InvalidOperationException("BindTwoWay is never meant to be called. It is meant to be used together with the AbreVinci.Embryo.Fody package and would be translated to CreateTwoWayBinding by the means of IL weaving.");
		}
		return default!;
	}

	/// <summary>
	/// Creates a binding for use with a get, set property.
	/// </summary>
	/// <typeparam name="TValue">The property type.</typeparam>
	/// <param name="value">The stream of values to observe.</param>
	/// <param name="onSet">A callback that will be called when the value has been set using the property setter (not when it has been emitted from the value stream).</param>
	/// <param name="propertyName">The name of the property (for change notifications).</param>
	/// <returns>A binding instance. Have the property getter return <see cref="IReactiveTwoWayBinding{T}.Value"/> and the property setter set the value to <see cref="IReactiveTwoWayBinding{T}.Value"/>.</returns>
	/// <example>
	/// [!code-csharp[Example](../../DocSnippets/ReactiveUI/ReactiveViewModelBase/CreateTwoWayBinding/ViewModel.cs#Snippet)]
	/// </example>
	protected IReactiveTwoWayBinding<TValue> CreateTwoWayBinding<TValue>(IObservable<TValue> value, Action<TValue> onSet, string propertyName)
    {
	    var binding = new ReactiveTwoWayBinding<TValue>(value, this, propertyName, false, _uiScheduler, _host);
	    binding.Subscribe(onSet);
		_propertyBindings.Add(binding);
		_disposables.Add(binding);
		return binding;
    }

	/// <summary>
	/// Creates an observable list for use with an IEnumerable property of values.
	/// The list automatically emits appropriate CollectionChanged events whenever differences are found between lists from <paramref name="values"/>.
	/// </summary>
	/// <typeparam name="TValue">The list element type.</typeparam>
	/// <param name="values">The stream of lists to observe.</param>
	/// <returns>The created list.</returns>
	/// <example>
	/// [!code-csharp[Example](../../DocSnippets/ReactiveUI/ReactiveViewModelBase/BindList/ViewModel.cs#Snippet)]
	/// </example>
	protected IReactiveList<TValue> BindList<TValue>(IObservable<IImmutableList<TValue>> values)
    {
		var list = new ReactiveList<TValue>(values, (v1, v2) => Equals(v1, v2), _uiScheduler, _host);
		_disposables.Add(list);
		return list;
	}

	/// <summary>
	/// Creates an observable list for use with an IEnumerable property of view models.
	/// The list automatically generates view models and emits appropriate CollectionChanged events whenever differences are found 
	/// between value lists from <paramref name="values"/>. Any removed view models are automatically disposed if they are disposable.
	/// </summary>
	/// <typeparam name="TValue">The source element value type.</typeparam>
	/// <typeparam name="TViewModel">The type of view model to transform source values into.</typeparam>
	/// <param name="values">The stream of value lists to observe.</param>
	/// <param name="create">Factory function for view model based on value.</param>
	/// <param name="match">A predicate that is supposed to return true when a view model should be reused (when it represents the same model).</param>
	/// <returns>The created list.</returns>
	/// <example>
	/// [!code-csharp[Example](../../DocSnippets/ReactiveUI/ReactiveViewModelBase/BindListAsViewModels/ViewModel.cs#Snippet)]
	/// </example>
	protected IReactiveList<TViewModel> BindListAsViewModels<TValue, TViewModel>(IObservable<IImmutableList<TValue>> values, Func<TValue, TViewModel> create, Func<TViewModel, TValue, bool> match)
    {
		var list = new ReactiveListAsViewModels<TValue, TViewModel>(values, match, v => _host.HostViewModel(() => create(v)), vm => (vm as IDisposable)?.Dispose(), _uiScheduler, _host);
		_disposables.Add(list);
		return list;
    }

	/// <summary>
	/// Creates a command for use with ICommand properties.
	/// </summary>
	/// <param name="action">The action to call when ICommand.Execute is called.</param>
	/// <param name="canExecute">An optional stream of bool values to observe for ICommand.CanExecute and ICommand.CanExecuteChanged.</param>
	/// <param name="initialCanExecute">The default ICommand.CanExecute return value if <paramref name="canExecute"/> is null or if no values have been emitted yet.</param>
	/// <returns>The created command.</returns>
	/// <example>
	/// [!code-csharp[Example](../../DocSnippets/ReactiveUI/ReactiveViewModelBase/BindCommand/ViewModel.cs#Snippet)]
	/// </example>
	protected IReactiveCommand BindCommand(Action action, IObservable<bool>? canExecute = null, bool initialCanExecute = true)
	{
		var command = new ReactiveCommand(canExecute ?? Observable.Never<bool>(), initialCanExecute, _uiScheduler, _host);
		command.Subscribe(action);
		_disposables.Add(command);
		return command;
	}

	/// <summary>
	/// As <see cref="BindCommand(Action, IObservable{bool}?, bool)"/>, but allows command parameters to be passed to the action.
	/// </summary>
	/// <example>
	/// [!code-csharp[Example](../../DocSnippets/ReactiveUI/ReactiveViewModelBase/BindCommandWithParam/ViewModel.cs#Snippet)]
	/// </example>
	protected IReactiveCommand<TParam> BindCommand<TParam>(Action<TParam> action, IObservable<bool>? canExecute = null, bool initialCanExecute = true)
	{
		var command = new ReactiveCommand<TParam>(canExecute ?? Observable.Never<bool>(), initialCanExecute, _uiScheduler, _host);
		command.Subscribe(action);
		_disposables.Add(command);
		return command;
	}
}
