﻿namespace AbreVinci.Embryo.ReactiveUI;

/// <summary>
/// Represents a window controller that controls the creation of a window and its view model as well as the current window state.
/// </summary>
/// <typeparam name="TViewModel"></typeparam>
public interface IReactiveWindowController<out TViewModel> where TViewModel : notnull
{
	/// <summary>
	/// The window that is being controlled by this window controller or null when the window does not exist (<see cref="State"/> is <see cref="ReactiveWindowControllerState.WindowClosed"/>).
	/// </summary>
	IReactiveWindow? Window { get; }

	/// <summary>
	/// The root view model of the window or null when the window does not exist (<see cref="State"/> is <see cref="ReactiveWindowControllerState.WindowClosed"/>).
	/// </summary>
	TViewModel? ViewModel { get; }

	/// <summary>
	/// The current window controller state.
	/// </summary>
	ReactiveWindowControllerState State { get; }
}
