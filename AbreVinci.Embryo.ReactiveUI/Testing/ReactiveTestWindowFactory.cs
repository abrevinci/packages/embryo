﻿using AbreVinci.Embryo.ReactiveUI.Internal;

namespace AbreVinci.Embryo.ReactiveUI.Testing;

/// <summary>
/// Window factory to use in a test environment.
/// </summary>
public class ReactiveTestWindowFactory : IReactiveWindowFactory
{
	private readonly Action<string>? _onShow;
	private readonly Action<string>? _onHide;
	private readonly Action<string>? _onClose;

	/// <summary>
	/// Constructor.
	/// </summary>
	/// <param name="onShow">Optional callback to call when a created window is shown (this gets the window name as a parameter).</param>
	/// <param name="onHide">Optional callback to call when a created window is hidden (this gets the window name as a parameter).</param>
	/// <param name="onClose">Optional callback to call when a created window is closed (this gets the window name as a parameter).</param>
	public ReactiveTestWindowFactory(
		Action<string>? onShow = null, 
		Action<string>? onHide = null, 
		Action<string>? onClose = null)
	{
		_onShow = onShow;
		_onHide = onHide;
		_onClose = onClose;
	}

	/// <summary>
	/// Creates a test window with the view model.
	/// </summary>
	/// <typeparam name="TViewModel">The type of the view model.</typeparam>
	/// <param name="windowName">The name of the window to create (used for onShow, onHide and onClose callbacks).</param>
	/// <param name="createViewModel">A factory function to create the view model (this will be wrapped with the view mode host).</param>
	/// <returns>The created window.</returns>
	public IReactiveWindow CreateWindow<TViewModel>(string windowName, Func<TViewModel> createViewModel) where TViewModel : notnull
	{
		return new ReactiveTestWindow(
			createViewModel(),
			() => _onShow?.Invoke(windowName),
			() => _onHide?.Invoke(windowName),
			() => _onClose?.Invoke(windowName));
	}
}
