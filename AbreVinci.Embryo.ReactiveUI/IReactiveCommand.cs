﻿using System.Windows.Input;

namespace AbreVinci.Embryo.ReactiveUI;

/// <summary>
/// An observable <see cref="ICommand"/>. Use <see cref="ReactiveViewModel.BindCommand(Action, IObservable{bool}?, bool)"/> to create an instance.
/// </summary>
public interface IReactiveCommand : IObservable<object>, ICommand, IDisposable
{
	/// <summary>
	/// Allows subscription using an action that ignores the command parameter from <see cref="ICommand.Execute(object?)"/>.
	/// </summary>
	/// <param name="onNext">The callback to be called when the <see cref="ICommand"/> is executed.</param>
	/// <returns>A disposable representing the subscription. Dispose to unsubscribe.</returns>
	IDisposable Subscribe(Action onNext);
}

/// <summary>
/// An observable <see cref="ICommand"/> allowing a command parameter. Use <see cref="ReactiveViewModel.BindCommand{TParam}(Action{TParam}, IObservable{bool}?, bool)"/> to create an instance.
/// </summary>
/// <typeparam name="T"></typeparam>
public interface IReactiveCommand<out T> : IObservable<T>, ICommand, IDisposable
{
}
