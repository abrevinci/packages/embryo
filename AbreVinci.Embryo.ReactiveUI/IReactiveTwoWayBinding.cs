﻿namespace AbreVinci.Embryo.ReactiveUI;

/// <summary>
/// An observable two way property binding. If you are using AbreVinci.Embryo.Fody, use 
/// <see cref="ReactiveViewModel.BindTwoWay{TValue}(IObservable{TValue}, Action{TValue})"/>
/// to create an instance. If you are not using AbreVinci.Embryo.Fody, use
/// <see cref="ReactiveViewModel.CreateTwoWayBinding{TValue}(IObservable{TValue}, Action{TValue}, string)"/>
/// instead.
/// </summary>
/// <typeparam name="T">The property type.</typeparam>
public interface IReactiveTwoWayBinding<T> : IObservable<T>, IDisposable
{
	/// <summary>
	/// The current value of the property binding.
	/// </summary>
	T Value { get; set; }
}
