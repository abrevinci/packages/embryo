﻿namespace AbreVinci.Embryo.ReactiveUI;

/// <summary>
/// Factory for reactive windows (to be used by window controllers).
/// </summary>
public interface IReactiveWindowFactory
{
	/// <summary>
	/// 
	/// </summary>
	/// <typeparam name="TViewModel"></typeparam>
	/// <param name="windowName"></param>
	/// <param name="createViewModel"></param>
	/// <returns></returns>
	IReactiveWindow CreateWindow<TViewModel>(string windowName, Func<TViewModel> createViewModel) where TViewModel : notnull;
}
