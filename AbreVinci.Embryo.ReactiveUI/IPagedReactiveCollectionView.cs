﻿namespace AbreVinci.Embryo.ReactiveUI;

/// <summary>
/// A reactive collectionview that has had a Page operator applied to it. This exposes a few useful properties.
/// </summary>
/// <typeparam name="T">The item type.</typeparam>
public interface IPagedReactiveCollectionView<out T> : IReactiveCollectionView<T>
{
	/// <summary>
	/// Whether or not there are any items before the currently active paging window (of lower index).
	/// </summary>
	bool HasHiddenItemsBefore { get; }

	/// <summary>
	/// Whether or not there are any items after the currently active paging window (of higher index).
	/// </summary>
	bool HasHiddenItemsAfter { get; }

	/// <summary>
	/// The number of items in the source collection (the total number of items before applying the Page operator).
	/// </summary>
	int SourceCount { get; }
}
