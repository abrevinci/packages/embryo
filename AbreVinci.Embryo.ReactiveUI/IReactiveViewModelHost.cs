﻿namespace AbreVinci.Embryo.ReactiveUI;

/// <summary>
/// Represents a view model host that can optionally enable auto activation of reactive view models, collections, commands and property bindings.
/// It may also override the scheduler used to schedule things on the UI thread. This value will be used if no scheduler is passed in explicitly
/// to the reactive view model constructor. If no scheduler override is supplied anywhere, the default UI dispatcher scheduler will be used.
/// </summary>
public interface IReactiveViewModelHost
{
	/// <summary>
	/// Allows any view models hosted in this host to automatically be activated, meaning their bound observables will be subscribed to.
	/// This normally happens by the UI when the UI subscribes to change notifications, but this can be tedious to do manually when writing tests
	/// for instance.
	/// </summary>
	bool AutoActivate { get; }

	/// <summary>
	/// The scheduler that will be used for UI notifications. Normally a dispatcher based scheduler.
	/// When testing, you should be using Scheduler.Immediate or TestScheduler.
	/// </summary>
	IScheduler UiScheduler { get; }

	/// <summary>
	/// Creates a view model with the specified factory function applying the requested scheduler and/or automatic activation to it upon construction.
	/// </summary>
	/// <typeparam name="TViewModel">The type of view model to host.</typeparam>
	/// <param name="create">Factory function for creating the view model.</param>
	/// <returns>The created and hosted vew model.</returns>
	TViewModel HostViewModel<TViewModel>(Func<TViewModel> create);
}
