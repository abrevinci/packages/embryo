﻿using System.Linq;
using AbreVinci.Embryo.Entities;

namespace AbreVinci.Embryo.ReactiveUI.Tests.ReactiveCollectionViewExtensionsTests;

public class SortWithParams : CollectionTestBaseWithChildViewModels<bool, SortWithParams.TestViewModel>
{
	public class TestViewModel : ReactiveViewModel
	{
		public TestViewModel(
			IObservable<IImmutableList<int>> ids, 
			IObservable<bool> ascending, 
			Func<int, TestChildViewModel> createChild,
			IScheduler testScheduler) : base(testScheduler)
		{
			A = BindListAsViewModels(ids, createChild, (vm, id) => vm.Id == id)
				.Sort(ascending, (asc, v1, v2) => asc ? v1.Value.CompareTo(v2.Value) : v2.Value.CompareTo(v1.Value));
		}

		public IReactiveCollectionView<TestChildViewModel> A { get; }
	}

	public SortWithParams() : base(
		getInitialValues: () => new EntityCollection<int, int>().Add(1, 1).Add(2, 3).Add(3, 6).Add(4, 2), 
		getInitialParams: () => true,
		createViewModel: (ids, ascending, createChild, scheduler) => new TestViewModel(ids, ascending, createChild, scheduler), 
		getItems: vm => vm.A,
		subscribeCollectionChangedEventArgs: new[]
		{
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 1, Value = 1 }, 0),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 2, Value = 3 }, 1),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 3, Value = 6 }, 2),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 4, Value = 2 }, 1)
		},
		subscribePropertyChangedEventArgs: new[]
		{
			new PropertyChangedEventArgs(nameof(TestViewModel.A.IsEmpty)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.Count)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.Count)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.Count)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.Count))
		},
		resetPropertyChangedEventArgs: new[]
		{
			new PropertyChangedEventArgs(nameof(TestViewModel.A.IsEmpty)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.Count))
		})
	{
	}

	[Fact]
	public void ShouldBeEmptyAfterConstruction()
	{
		ViewModel.A.Should().BeEmpty();
	}

	[Fact]
	public void ShouldHaveIsEmptyTrueAfterConstruction()
	{
		ViewModel.A.IsEmpty.Should().BeTrue();
	}

	[Fact]
	public void ShouldHaveCount0AfterConstruction()
	{
		ViewModel.A.Count.Should().Be(0);
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldSetInitialSortedListWhenSubscribing(Subscription subscription)
	{
		Subscribe(subscription);
		ViewModel.A.Select(v => v.Value).Should().BeEquivalentTo(new[] { 1, 2, 3, 6 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldKeepTheListSortedWhenAddingItems(Subscription subscription)
	{
		Subscribe(subscription);
		EmitValues(v => v.Add(5, 5));
		ViewModel.A.Select(v => v.Value).Should().BeEquivalentTo(new[] { 1, 2, 3, 5, 6 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldKeepTheListSortedWhenRemovingItems(Subscription subscription)
	{
		Subscribe(subscription);
		EmitValues(v => v.Remove(2));
		ViewModel.A.Select(v => v.Value).Should().BeEquivalentTo(new[] { 1, 2, 6 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldUpdateSortOrderWhenParamsChange(Subscription subscription)
	{
		Subscribe(subscription);
		EmitParams(_ => false);
		ViewModel.A.Select(v => v.Value).Should().BeEquivalentTo(new[] { 6, 3, 2, 1 });
	}

	[Fact]
	public void ShouldRaiseCollectionChangedResetWhenParamsChange()
	{
		Subscribe(Subscription.CollectionChanged);
		EmitParams(_ => false);
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Append(
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset)));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldUpdateSortOrderWhenItemValuePropertyChanges(Subscription subscription)
	{
		Subscribe(subscription);
		EmitValues(v => v.Set(4, 4));
		ViewModel.A.Select(v => v.Value).Should().BeEquivalentTo(new[] { 1, 3, 4, 6 });
	}

	[Fact]
	public void ShouldRaiseCollectionChangedWhenItemValuePropertyChanges()
	{
		Subscribe(Subscription.CollectionChanged);
		EmitValues(v => v.Set(4, 4));
		CollectionChangedEventArgs.Should().BeEquivalentTo(new[]
		{
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 1, Value = 1 }, 0),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 2, Value = 3 }, 1),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 3, Value = 6 }, 2),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 4, Value = 4 }, 1),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Move, new { Id = 4, Value = 4 }, 2, 1)
		});
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldHaveIsEmptyFalseWhenSubscribingIfNotEmpty(Subscription subscription)
	{
		Subscribe(subscription);
		ViewModel.A.IsEmpty.Should().BeFalse();
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldHaveInitialCountWhenSubscribing(Subscription subscription)
	{
		Subscribe(subscription);
		ViewModel.A.Count.Should().Be(4);
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenSubscribing(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		CollectionChangedEventArgs.Should().BeEquivalentTo(SubscribeCollectionChangedEventArgs);
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenSubscribing(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		PropertyChangedEventArgs.Should().BeEquivalentTo(SubscribePropertyChangedEventArgs);
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenEmittingSourceListWithAddedElement(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Add(5, 5));
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Append(
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 5, Value = 5 }, 3)));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenEmittingSourceListWithAddedElement(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Add(5, 5));
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Append(
				new PropertyChangedEventArgs(nameof(TestViewModel.A.Count))));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenEmittingSourceListWithRemovedElement(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Remove(2));
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Append(
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, new { Id = 2, Value = 3 }, 2)));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenEmittingSourceListWithRemovedElement(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Remove(2));
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Append(
				new PropertyChangedEventArgs(nameof(TestViewModel.A.Count))));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldNotRaiseCollectionChangedWhenEmittingEqualSourceList(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(_ => InitialValues);
		CollectionChangedEventArgs.Should().BeEquivalentTo(SubscribeCollectionChangedEventArgs);
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldNotRaisePropertyChangedWhenEmittingEqualSourceList(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(_ => InitialValues);
		PropertyChangedEventArgs.Should().BeEquivalentTo(SubscribePropertyChangedEventArgs);
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenResubscribing(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		Subscribe(subscriptions);
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Concat(SubscribeCollectionChangedEventArgs));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenResubscribing(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		Subscribe(subscriptions);
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Concat(ResetPropertyChangedEventArgs).Concat(SubscribePropertyChangedEventArgs));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenResubscribingToChangedStream(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		EmitValues(v => v.Set(2, 5));
		Subscribe(subscriptions);
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Concat(new []
			{
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset),
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 1, Value = 1 }, 0),
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 2, Value = 5 }, 1),
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 3, Value = 6 }, 2),
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 4, Value = 2 }, 1)
			}));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenResubscribingToChangedStream(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		EmitValues(v => v.Set(2, 5));
		Subscribe(subscriptions);
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Concat(ResetPropertyChangedEventArgs).Concat(SubscribePropertyChangedEventArgs));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenResubscribingThenChangingStream(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		Subscribe(subscriptions);
		EmitValues(v => v.Add(5, 5));
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Concat(SubscribeCollectionChangedEventArgs).Append(
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 5, Value = 5 }, 3)));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenResubscribingThenChangingStream(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		Subscribe(subscriptions);
		EmitValues(v => v.Add(5, 5));
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Concat(ResetPropertyChangedEventArgs).Concat(SubscribePropertyChangedEventArgs).Append(
				new PropertyChangedEventArgs(nameof(TestViewModel.A.Count))));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged)]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	public void ShouldUnsubscribeFromValueListWhenViewModelIsDisposed(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		IsSubscribedToValues.Should().BeTrue();
		ViewModel.Dispose();
		IsSubscribedToValues.Should().BeFalse();
	}
}
