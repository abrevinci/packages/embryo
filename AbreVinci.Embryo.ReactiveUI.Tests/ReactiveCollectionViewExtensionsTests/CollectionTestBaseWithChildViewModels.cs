﻿using System.Linq;
using System.Reactive.Linq;
using AbreVinci.Embryo.Core;
using AbreVinci.Embryo.Entities;

namespace AbreVinci.Embryo.ReactiveUI.Tests.ReactiveCollectionViewExtensionsTests;

public abstract class CollectionTestBaseWithChildViewModels<TParams, TViewModel> : CollectionTestBase where TViewModel : ReactiveViewModel
{
	private readonly Func<EntityCollection<int, int>> _getInitialValues;
	private readonly Func<TParams> _getInitialParams;
	private readonly Func<TViewModel, IReactiveCollectionView<TestChildViewModel>> _getItems;
	private readonly TestScheduler _testScheduler;
	private readonly PropertyChangedEventHandler _propertyChangedHandler;
	private readonly NotifyCollectionChangedEventHandler _collectionChangedHandler;
	
	private readonly BehaviorSubject<EntityCollection<int, int>> _values;
	private readonly BehaviorSubject<TParams> _params;
	
	protected CollectionTestBaseWithChildViewModels(
		Func<EntityCollection<int, int>> getInitialValues, 
		Func<TParams> getInitialParams,
		Func<IObservable<IImmutableList<int>>, IObservable<TParams>, Func<int, TestChildViewModel>, IScheduler, TViewModel> createViewModel, 
		Func<TViewModel, IReactiveCollectionView<TestChildViewModel>> getItems,
		NotifyCollectionChangedEventArgs[] subscribeCollectionChangedEventArgs,
		PropertyChangedEventArgs[] subscribePropertyChangedEventArgs,
		PropertyChangedEventArgs[] resetPropertyChangedEventArgs)
	{
		_getInitialValues = getInitialValues;
		_getInitialParams = getInitialParams;
		_getItems = getItems;
		_testScheduler = new TestScheduler();
		PropertyChangedEventArgs = new List<PropertyChangedEventArgs>();
		_propertyChangedHandler = (_, e) => PropertyChangedEventArgs.Add(e);
		CollectionChangedEventArgs = new List<NotifyCollectionChangedEventArgs>();
		_collectionChangedHandler = (_, e) => CollectionChangedEventArgs.Add(e);
		SubscribeCollectionChangedEventArgs = subscribeCollectionChangedEventArgs;
		SubscribePropertyChangedEventArgs = subscribePropertyChangedEventArgs;
		ResetPropertyChangedEventArgs = resetPropertyChangedEventArgs;

		_values = new BehaviorSubject<EntityCollection<int, int>>(InitialValues);
		_params = new BehaviorSubject<TParams>(InitialParams);

		var createChildVm = (int id) => new TestChildViewModel(id, _values.SelectNotNull(vs => vs.TryGet(id, out var v) ? v : (int?)null), _testScheduler);
		ViewModel = createViewModel(_values.Select(v => v.Ids), _params, createChildVm, _testScheduler);

		_testScheduler.Start();
	}

	public enum Subscription
	{
		CollectionChanged,
		PropertyChanged
	}

	protected void Subscribe(params Subscription[] subscriptions)
	{
		foreach (var subscription in subscriptions)
		{
			if (subscription == Subscription.CollectionChanged)
				_getItems(ViewModel).CollectionChanged += _collectionChangedHandler;
			else
				_getItems(ViewModel).PropertyChanged += _propertyChangedHandler;
		}

		_testScheduler.Start();
	}

	protected void Unsubscribe(params Subscription[] subscriptions)
	{
		foreach (var subscription in subscriptions.Reverse())
		{
			if (subscription == Subscription.CollectionChanged)
				_getItems(ViewModel).CollectionChanged -= _collectionChangedHandler;
			else
				_getItems(ViewModel).PropertyChanged -= _propertyChangedHandler;
		}

		_testScheduler.Start();
	}

	protected void EmitValues(Func<EntityCollection<int, int>, EntityCollection<int, int>> update)
	{
		_values.OnNext(update(_values.Value));
		_testScheduler.Start();
	}

	protected void EmitParams(Func<TParams, TParams> update)
	{
		_params.OnNext(update(_params.Value));
		_testScheduler.Start();
	}
	
	protected EntityCollection<int, int> InitialValues => _getInitialValues();
	protected TParams InitialParams => _getInitialParams();
	protected List<PropertyChangedEventArgs> PropertyChangedEventArgs { get; }
	protected List<NotifyCollectionChangedEventArgs> CollectionChangedEventArgs { get; }

	protected NotifyCollectionChangedEventArgs[] SubscribeCollectionChangedEventArgs { get; }
	protected PropertyChangedEventArgs[] SubscribePropertyChangedEventArgs { get; }
	protected PropertyChangedEventArgs[] ResetPropertyChangedEventArgs { get; }

	protected TViewModel ViewModel { get; }

	protected bool IsSubscribedToValues => _values.HasObservers;
}
