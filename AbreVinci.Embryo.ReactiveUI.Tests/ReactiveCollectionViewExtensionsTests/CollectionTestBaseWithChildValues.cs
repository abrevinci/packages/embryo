﻿using System.Linq;

namespace AbreVinci.Embryo.ReactiveUI.Tests.ReactiveCollectionViewExtensionsTests;

public abstract class CollectionTestBaseWithChildValues<TParams, TViewModel> : CollectionTestBase where TViewModel : ReactiveViewModel
{
	private readonly Func<IImmutableList<int>> _getInitialValues;
	private readonly Func<TParams> _getInitialParams;
	private readonly Func<TViewModel, IReactiveCollectionView<int>> _getItems;
	private readonly TestScheduler _testScheduler;
	private readonly PropertyChangedEventHandler _propertyChangedHandler;
	private readonly NotifyCollectionChangedEventHandler _collectionChangedHandler;
	
	private readonly BehaviorSubject<IImmutableList<int>> _values;
	private readonly BehaviorSubject<TParams> _params;
	
	protected CollectionTestBaseWithChildValues(
		Func<IImmutableList<int>> getInitialValues, 
		Func<TParams> getInitialParams,
		Func<IObservable<IImmutableList<int>>, IObservable<TParams>, IScheduler, TViewModel> createViewModel, 
		Func<TViewModel, IReactiveCollectionView<int>> getItems,
		NotifyCollectionChangedEventArgs[] subscribeCollectionChangedEventArgs,
		PropertyChangedEventArgs[] subscribePropertyChangedEventArgs,
		PropertyChangedEventArgs[] resetPropertyChangedEventArgs)
	{
		_getInitialValues = getInitialValues;
		_getInitialParams = getInitialParams;
		_getItems = getItems;
		_testScheduler = new TestScheduler();
		PropertyChangedEventArgs = new List<PropertyChangedEventArgs>();
		_propertyChangedHandler = (_, e) => PropertyChangedEventArgs.Add(e);
		CollectionChangedEventArgs = new List<NotifyCollectionChangedEventArgs>();
		_collectionChangedHandler = (_, e) => CollectionChangedEventArgs.Add(e);
		SubscribeCollectionChangedEventArgs = subscribeCollectionChangedEventArgs;
		SubscribePropertyChangedEventArgs = subscribePropertyChangedEventArgs;
		ResetPropertyChangedEventArgs = resetPropertyChangedEventArgs;

		_values = new BehaviorSubject<IImmutableList<int>>(InitialValues);
		_params = new BehaviorSubject<TParams>(InitialParams);

		ViewModel = createViewModel(_values, _params, _testScheduler);

		_testScheduler.Start();
	}

	public enum Subscription
	{
		CollectionChanged,
		PropertyChanged
	}

	protected void Subscribe(params Subscription[] subscriptions)
	{
		foreach (var subscription in subscriptions)
		{
			if (subscription == Subscription.CollectionChanged)
				_getItems(ViewModel).CollectionChanged += _collectionChangedHandler;
			else
				_getItems(ViewModel).PropertyChanged += _propertyChangedHandler;
		}

		_testScheduler.Start();
	}

	protected void Unsubscribe(params Subscription[] subscriptions)
	{
		foreach (var subscription in subscriptions.Reverse())
		{
			if (subscription == Subscription.CollectionChanged)
				_getItems(ViewModel).CollectionChanged -= _collectionChangedHandler;
			else
				_getItems(ViewModel).PropertyChanged -= _propertyChangedHandler;
		}

		_testScheduler.Start();
	}

	protected void EmitValues(Func<IImmutableList<int>, IImmutableList<int>> update)
	{
		_values.OnNext(update(_values.Value));
		_testScheduler.Start();
	}

	protected void EmitParams(Func<TParams, TParams> update)
	{
		_params.OnNext(update(_params.Value));
		_testScheduler.Start();
	}
	
	protected IImmutableList<int> InitialValues => _getInitialValues();
	protected TParams InitialParams => _getInitialParams();
	protected List<PropertyChangedEventArgs> PropertyChangedEventArgs { get; }
	protected List<NotifyCollectionChangedEventArgs> CollectionChangedEventArgs { get; }

	protected NotifyCollectionChangedEventArgs[] SubscribeCollectionChangedEventArgs { get; }
	protected PropertyChangedEventArgs[] SubscribePropertyChangedEventArgs { get; }
	protected PropertyChangedEventArgs[] ResetPropertyChangedEventArgs { get; }

	protected TViewModel ViewModel { get; }

	protected bool IsSubscribedToValues => _values.HasObservers;
}
