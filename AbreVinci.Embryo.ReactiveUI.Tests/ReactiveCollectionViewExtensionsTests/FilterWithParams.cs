﻿using System.Linq;
using AbreVinci.Embryo.Entities;

namespace AbreVinci.Embryo.ReactiveUI.Tests.ReactiveCollectionViewExtensionsTests;

public class FilterWithParams : CollectionTestBaseWithChildViewModels<bool, FilterWithParams.TestViewModel>
{
	public class TestViewModel : ReactiveViewModel
	{
		public TestViewModel(
			IObservable<IImmutableList<int>> ids, 
			IObservable<bool> even,
			Func<int, TestChildViewModel> createChild,
			IScheduler testScheduler) : base(testScheduler)
		{
			A = BindListAsViewModels(ids, createChild, (vm, id) => vm.Id == id)
				.Filter(even, (e, v) => v.Value % 2 == (e ? 0 : 1));
		}

		public IFilteredReactiveCollectionView<TestChildViewModel> A { get; }
	}

	public FilterWithParams() : base(
		getInitialValues: () => new EntityCollection<int, int>().Add(1, 1).Add(2, 3).Add(3, 6).Add(4, 2),
		getInitialParams: () => true,
		createViewModel: (ids, even, createChild, scheduler) => new TestViewModel(ids, even, createChild, scheduler),
		getItems: vm => vm.A,
		subscribeCollectionChangedEventArgs: new[]
		{
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 3, Value = 6 }, 0),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 4, Value = 2 }, 1)
		},
		subscribePropertyChangedEventArgs: new[]
		{
			new PropertyChangedEventArgs(nameof(TestViewModel.A.HasHiddenItems)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.IsEmpty)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.Count)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.Count)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount))
		},
		resetPropertyChangedEventArgs: new[]
		{
			new PropertyChangedEventArgs(nameof(TestViewModel.A.IsEmpty)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.HasHiddenItems)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.Count)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount))
		})
	{
	}
	
	[Fact]
	public void ShouldBeEmptyAfterConstruction()
	{
		ViewModel.A.Should().BeEmpty();
	}

	[Fact]
	public void ShouldHaveIsEmptyTrueAfterConstruction()
	{
		ViewModel.A.IsEmpty.Should().BeTrue();
	}

	[Fact]
	public void ShouldHaveCount0AfterConstruction()
	{
		ViewModel.A.Count.Should().Be(0);
	}

	[Fact]
	public void ShouldHaveHasHiddenItemsFalseAfterConstruction()
	{
		ViewModel.A.HasHiddenItems.Should().BeFalse();
	}

	[Fact]
	public void ShouldHaveSourceCount0AfterConstruction()
	{
		ViewModel.A.SourceCount.Should().Be(0);
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldSetInitialFilteredListWhenSubscribing(Subscription subscription)
	{
		Subscribe(subscription);
		ViewModel.A.Select(v => v.Value).Should().BeEquivalentTo(new[] { 6, 2 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldKeepTheListFilteredWhenAddingIncludedItems(Subscription subscription)
	{
		Subscribe(subscription);
		EmitValues(v => v.Add(5, 8));
		ViewModel.A.Select(v => v.Value).Should().BeEquivalentTo(new[] { 6, 2, 8 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldKeepTheListFilteredWhenAddingExcludedItems(Subscription subscription)
	{
		Subscribe(subscription);
		EmitValues(v => v.Add(5, 5));
		ViewModel.A.Select(v => v.Value).Should().BeEquivalentTo(new[] { 6, 2 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldKeepTheListFilteredWhenRemovingIncludedItems(Subscription subscription)
	{
		Subscribe(subscription);
		EmitValues(v => v.Remove(4));
		ViewModel.A.Select(v => v.Value).Should().BeEquivalentTo(new[] { 6 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldKeepTheListFilteredWhenRemovingExcludedItems(Subscription subscription)
	{
		Subscribe(subscription);
		EmitValues(v => v.Remove(2));
		ViewModel.A.Select(v => v.Value).Should().BeEquivalentTo(new[] { 6, 2 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldUpdateFilteringWhenParamsChange(Subscription subscription)
	{
		Subscribe(subscription);
		EmitParams(_ => false);
		ViewModel.A.Select(v => v.Value).Should().BeEquivalentTo(new[] { 1, 3 });
	}

	[Fact]
	public void ShouldRaiseCollectionChangedResetWhenParamsChange()
	{
		Subscribe(Subscription.CollectionChanged);
		EmitParams(_ => false);
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Append(
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset)));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldUpdateFilteredItemsWhenItemValuePropertyChangesToBeIncluded(Subscription subscription)
	{
		Subscribe(subscription);
		EmitValues(v => v.Set(2, 4));
		ViewModel.A.Select(v => v.Value).Should().BeEquivalentTo(new[] { 4, 6, 2 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldUpdateFilteredItemsWhenItemValuePropertyChangesToBeExcluded(Subscription subscription)
	{
		Subscribe(subscription);
		EmitValues(v => v.Set(3, 5));
		ViewModel.A.Select(v => v.Value).Should().BeEquivalentTo(new[] { 2 });
	}

	[Fact]
	public void ShouldRaiseCollectionChangedWhenItemValuePropertyChangesToBeIncluded()
	{
		Subscribe(Subscription.CollectionChanged);
		EmitValues(v => v.Set(2, 4));
		CollectionChangedEventArgs.Should().BeEquivalentTo(new[]
		{
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 3, Value = 6 }, 0),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 4, Value = 2 }, 1),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 2, Value = 4 }, 0)
		});
	}

	[Fact]
	public void ShouldRaiseCollectionChangedWhenItemValuePropertyChangesToBeExcluded()
	{
		Subscribe(Subscription.CollectionChanged);
		EmitValues(v => v.Set(3, 5));
		CollectionChangedEventArgs.Should().BeEquivalentTo(new[]
		{
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 3, Value = 5 }, 0),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 4, Value = 2 }, 1),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, new { Id = 3, Value = 5 }, 0)
		});
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldHaveIsEmptyFalseWhenSubscribingIfNotEmpty(Subscription subscription)
	{
		Subscribe(subscription);
		ViewModel.A.IsEmpty.Should().BeFalse();
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldHaveInitialFilteredCountWhenSubscribing(Subscription subscription)
	{
		Subscribe(subscription);
		ViewModel.A.Count.Should().Be(2);
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldHaveHasHiddenItemsTrueWhenNotAllItemsPassTheFilterWhenSubscribing(Subscription subscription)
	{
		Subscribe(subscription);
		ViewModel.A.HasHiddenItems.Should().BeTrue();
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldHaveHasHiddenItemsFalseWhenAllItemsPassTheFilterWhenSubscribing(Subscription subscription)
	{
		EmitValues(_ => new EntityCollection<int, int>().Add(3, 6).Add(4, 2));
		Subscribe(subscription);
		ViewModel.A.HasHiddenItems.Should().BeFalse();
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldHaveInitialSourceCountWhenSubscribing(Subscription subscription)
	{
		Subscribe(subscription);
		ViewModel.A.SourceCount.Should().Be(4);
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenSubscribing(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		CollectionChangedEventArgs.Should().BeEquivalentTo(SubscribeCollectionChangedEventArgs);
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenSubscribing(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		PropertyChangedEventArgs.Should().BeEquivalentTo(SubscribePropertyChangedEventArgs);
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenEmittingSourceListWithAddedElement(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Add(5, 8));
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Append(
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 5, Value = 8 }, 2)));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenEmittingSourceListWithAddedElement(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Add(5, 8));
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Concat(new[]
			{
				new PropertyChangedEventArgs(nameof(TestViewModel.A.Count)),
				new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount))
			}));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenEmittingSourceListWithRemovedElement(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Remove(3));
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Append(
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, new { Id = 3, Value = 6 }, 0)));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenEmittingSourceListWithRemovedElement(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Remove(3));
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Concat(new []
			{
				new PropertyChangedEventArgs(nameof(TestViewModel.A.Count)),
				new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount))
			}));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldNotRaiseCollectionChangedWhenEmittingEqualSourceList(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(_ => InitialValues);
		CollectionChangedEventArgs.Should().BeEquivalentTo(SubscribeCollectionChangedEventArgs);
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldNotRaisePropertyChangedWhenEmittingEqualSourceList(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(_ => InitialValues);
		PropertyChangedEventArgs.Should().BeEquivalentTo(SubscribePropertyChangedEventArgs);
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenResubscribing(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		Subscribe(subscriptions);
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Concat(SubscribeCollectionChangedEventArgs));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenResubscribing(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		Subscribe(subscriptions);
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Concat(ResetPropertyChangedEventArgs).Concat(SubscribePropertyChangedEventArgs));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenResubscribingToChangedStream(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		EmitValues(v => v.Set(2, 4));
		Subscribe(subscriptions);
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Concat(new []
			{
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset),
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 2, Value = 4 }, 0),
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 3, Value = 6 }, 1),
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 4, Value = 2 }, 2)
			}));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenResubscribingToChangedStream(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		EmitValues(v => v.Set(2, 5));
		Subscribe(subscriptions);
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Concat(ResetPropertyChangedEventArgs).Concat(SubscribePropertyChangedEventArgs));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenResubscribingThenChangingStream(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		Subscribe(subscriptions);
		EmitValues(v => v.Add(5, 8));
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Concat(SubscribeCollectionChangedEventArgs).Append(
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 5, Value = 8 }, 2)));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenResubscribingThenChangingStream(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		Subscribe(subscriptions);
		EmitValues(v => v.Add(5, 8));
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Concat(ResetPropertyChangedEventArgs).Concat(SubscribePropertyChangedEventArgs).Concat(new []
			{
				new PropertyChangedEventArgs(nameof(TestViewModel.A.Count)),
				new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount))
			}));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged)]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	public void ShouldUnsubscribeFromValueListWhenViewModelIsDisposed(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		IsSubscribedToValues.Should().BeTrue();
		ViewModel.Dispose();
		IsSubscribedToValues.Should().BeFalse();
	}
}
