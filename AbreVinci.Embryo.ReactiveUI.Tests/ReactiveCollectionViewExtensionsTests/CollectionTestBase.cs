﻿namespace AbreVinci.Embryo.ReactiveUI.Tests.ReactiveCollectionViewExtensionsTests;

public abstract class CollectionTestBase
{
	public class TestChildViewModel : ReactiveViewModel
	{
		public TestChildViewModel(int id, IObservable<int> value, IScheduler testScheduler) : base(testScheduler)
		{
			Id = id;
			Value = BindOneWay(value);
		}
		
		public int Id { get; }
		public int Value { get; }
	}
}
