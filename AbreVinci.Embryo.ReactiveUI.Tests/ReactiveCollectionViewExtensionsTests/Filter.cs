﻿using System.Linq;

namespace AbreVinci.Embryo.ReactiveUI.Tests.ReactiveCollectionViewExtensionsTests;

public class Filter : CollectionTestBaseWithChildValues<bool, Filter.TestViewModel>
{
	public class TestViewModel : ReactiveViewModel
	{
		public TestViewModel(IObservable<IImmutableList<int>> a, IScheduler testScheduler) : base(testScheduler)
		{
			A = BindList(a)
				.Filter(v => v % 2 == 0);
		}

		public IFilteredReactiveCollectionView<int> A { get; }
	}
	
	public Filter() : base(
		getInitialValues: () => ImmutableArray.Create(1, 3, 6, 2),
		getInitialParams: () => true,
		createViewModel: (values, _, scheduler) => new TestViewModel(values, scheduler),
		getItems: vm => vm.A,
		subscribeCollectionChangedEventArgs: new[]
		{
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, 6, 0),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, 2, 1)
		},
		subscribePropertyChangedEventArgs: new[]
		{
			new PropertyChangedEventArgs(nameof(TestViewModel.A.HasHiddenItems)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.IsEmpty)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.Count)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.Count)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount))
		},
		resetPropertyChangedEventArgs: new[]
		{
			new PropertyChangedEventArgs(nameof(TestViewModel.A.IsEmpty)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.HasHiddenItems)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.Count)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount))
		})
	{
	}
	
	[Fact]
	public void ShouldBeEmptyAfterConstruction()
	{
		ViewModel.A.Should().BeEmpty();
	}

	[Fact]
	public void ShouldHaveIsEmptyTrueAfterConstruction()
	{
		ViewModel.A.IsEmpty.Should().BeTrue();
	}

	[Fact]
	public void ShouldHaveCount0AfterConstruction()
	{
		ViewModel.A.Count.Should().Be(0);
	}

	[Fact]
	public void ShouldHaveHasHiddenItemsFalseAfterConstruction()
	{
		ViewModel.A.HasHiddenItems.Should().BeFalse();
	}

	[Fact]
	public void ShouldHaveSourceCount0AfterConstruction()
	{
		ViewModel.A.SourceCount.Should().Be(0);
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldSetInitialFilteredListWhenSubscribing(Subscription subscription)
	{
		Subscribe(subscription);
		ViewModel.A.Should().BeEquivalentTo(new[] { 6, 2 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldKeepTheListFilteredWhenAddingIncludedItems(Subscription subscription)
	{
		Subscribe(subscription);
		EmitValues(v => v.Insert(1, 8));
		ViewModel.A.Should().BeEquivalentTo(new[] { 8, 6, 2 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldKeepTheListFilteredWhenAddingExcludedItems(Subscription subscription)
	{
		Subscribe(subscription);
		EmitValues(v => v.Add(5));
		ViewModel.A.Should().BeEquivalentTo(new[] { 6, 2 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldKeepTheListFilteredWhenRemovingIncludedItems(Subscription subscription)
	{
		Subscribe(subscription);
		EmitValues(v => v.Remove(2));
		ViewModel.A.Should().BeEquivalentTo(new[] { 6 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldKeepTheListFilteredWhenRemovingExcludedItems(Subscription subscription)
	{
		Subscribe(subscription);
		EmitValues(v => v.Remove(1));
		ViewModel.A.Should().BeEquivalentTo(new[] { 6, 2 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldHaveIsEmptyFalseWhenSubscribingIfNotEmpty(Subscription subscription)
	{
		Subscribe(subscription);
		ViewModel.A.IsEmpty.Should().BeFalse();
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldHaveInitialFilteredCountWhenSubscribing(Subscription subscription)
	{
		Subscribe(subscription);
		ViewModel.A.Count.Should().Be(2);
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldHaveHasHiddenItemsTrueWhenNotAllItemsPassTheFilterWhenSubscribing(Subscription subscription)
	{
		Subscribe(subscription);
		ViewModel.A.HasHiddenItems.Should().BeTrue();
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldHaveHasHiddenItemsFalseWhenAllItemsPassTheFilterWhenSubscribing(Subscription subscription)
	{
		EmitValues(_ => ImmutableArray.Create(6, 2));
		Subscribe(subscription);
		ViewModel.A.HasHiddenItems.Should().BeFalse();
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldHaveInitialSourceCountWhenSubscribing(Subscription subscription)
	{
		Subscribe(subscription);
		ViewModel.A.SourceCount.Should().Be(4);
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenSubscribing(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		CollectionChangedEventArgs.Should().BeEquivalentTo(SubscribeCollectionChangedEventArgs);
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenSubscribing(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		PropertyChangedEventArgs.Should().BeEquivalentTo(SubscribePropertyChangedEventArgs);
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenEmittingSourceListWithAddedElement(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Add(8));
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Append(
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, 8, 2)));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenEmittingSourceListWithAddedElement(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Add(8));
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Concat(new[]
			{
				new PropertyChangedEventArgs(nameof(TestViewModel.A.Count)),
				new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount))
			}));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenEmittingSourceListWithRemovedElement(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Remove(6));
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Append(
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, 6, 0)));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenEmittingSourceListWithRemovedElement(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Remove(6));
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Concat(new []
			{
				new PropertyChangedEventArgs(nameof(TestViewModel.A.Count)),
				new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount))
			}));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldNotRaiseCollectionChangedWhenEmittingEqualSourceList(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(_ => InitialValues);
		CollectionChangedEventArgs.Should().BeEquivalentTo(SubscribeCollectionChangedEventArgs);
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldNotRaisePropertyChangedWhenEmittingEqualSourceList(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(_ => InitialValues);
		PropertyChangedEventArgs.Should().BeEquivalentTo(SubscribePropertyChangedEventArgs);
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenResubscribing(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		Subscribe(subscriptions);
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Concat(SubscribeCollectionChangedEventArgs));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenResubscribing(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		Subscribe(subscriptions);
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Concat(ResetPropertyChangedEventArgs).Concat(SubscribePropertyChangedEventArgs));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenResubscribingToChangedStream(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		EmitValues(v => v.SetItem(1, 4));
		Subscribe(subscriptions);
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Concat(new []
			{
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset),
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, 4, 0),
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, 6, 1),
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, 2, 2)
			}));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenResubscribingToChangedStream(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		EmitValues(v => v.SetItem(1, 5));
		Subscribe(subscriptions);
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Concat(ResetPropertyChangedEventArgs).Concat(SubscribePropertyChangedEventArgs));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenResubscribingThenChangingStream(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		Subscribe(subscriptions);
		EmitValues(v => v.Add(8));
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Concat(SubscribeCollectionChangedEventArgs).Append(
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, 8, 2)));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenResubscribingThenChangingStream(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		Subscribe(subscriptions);
		EmitValues(v => v.Add(8));
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Concat(ResetPropertyChangedEventArgs).Concat(SubscribePropertyChangedEventArgs).Concat(new []
			{
				new PropertyChangedEventArgs(nameof(TestViewModel.A.Count)),
				new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount))
			}));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged)]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	public void ShouldUnsubscribeFromValueListWhenViewModelIsDisposed(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		IsSubscribedToValues.Should().BeTrue();
		ViewModel.Dispose();
		IsSubscribedToValues.Should().BeFalse();
	}
}
