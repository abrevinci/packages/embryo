﻿using System.Linq;

namespace AbreVinci.Embryo.ReactiveUI.Tests.ReactiveCollectionViewExtensionsTests;

public class Sort : CollectionTestBaseWithChildValues<bool, Sort.TestViewModel>
{
	public class TestViewModel : ReactiveViewModel
	{
		public TestViewModel(IObservable<IImmutableList<int>> a, IScheduler testScheduler) : base(testScheduler)
		{
			A = BindList(a)
				.Sort((v1, v2) => v1.CompareTo(v2));
		}

		public IReactiveCollectionView<int> A { get; }
	}

	public Sort() : base(
		getInitialValues: () => ImmutableArray.Create(1, 3, 6, 2),
		getInitialParams: () => true,
		createViewModel: (values, _, scheduler) => new TestViewModel(values, scheduler),
		getItems: vm => vm.A,
		subscribeCollectionChangedEventArgs: new[]
		{
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, 1, 0),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, 3, 1),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, 6, 2),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, 2, 1)
		},
		subscribePropertyChangedEventArgs: new[]
		{
			new PropertyChangedEventArgs(nameof(TestViewModel.A.IsEmpty)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.Count)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.Count)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.Count)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.Count))
		},
		resetPropertyChangedEventArgs: new[]
		{
			new PropertyChangedEventArgs(nameof(TestViewModel.A.IsEmpty)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.Count))
		})
	{
	}
	
	[Fact]
	public void ShouldBeEmptyAfterConstruction()
	{
		ViewModel.A.Should().BeEmpty();
	}

	[Fact]
	public void ShouldHaveIsEmptyTrueAfterConstruction()
	{
		ViewModel.A.IsEmpty.Should().BeTrue();
	}

	[Fact]
	public void ShouldHaveCount0AfterConstruction()
	{
		ViewModel.A.Count.Should().Be(0);
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldSetInitialSortedListWhenSubscribing(Subscription subscription)
	{
		Subscribe(subscription);
		ViewModel.A.Should().BeEquivalentTo(new[] { 1, 2, 3, 6 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldKeepTheListSortedWhenAddingItems(Subscription subscription)
	{
		Subscribe(subscription);
		EmitValues(v => v.Add(5));
		ViewModel.A.Should().BeEquivalentTo(new[] { 1, 2, 3, 5, 6 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldKeepTheListSortedWhenRemovingItems(Subscription subscription)
	{
		Subscribe(subscription);
		EmitValues(v => v.Remove(2));
		ViewModel.A.Should().BeEquivalentTo(new[] { 1, 3, 6 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldHaveIsEmptyFalseWhenSubscribingIfNotEmpty(Subscription subscription)
	{
		Subscribe(subscription);
		ViewModel.A.IsEmpty.Should().BeFalse();
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldHaveInitialCountWhenSubscribing(Subscription subscription)
	{
		Subscribe(subscription);
		ViewModel.A.Count.Should().Be(4);
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenSubscribing(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		CollectionChangedEventArgs.Should().BeEquivalentTo(SubscribeCollectionChangedEventArgs);
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenSubscribing(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		PropertyChangedEventArgs.Should().BeEquivalentTo(SubscribePropertyChangedEventArgs);
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenEmittingSourceListWithAddedElement(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Add(5));
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Append(
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, 5, 3)));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenEmittingSourceListWithAddedElement(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Add(5));
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Append(
				new PropertyChangedEventArgs(nameof(TestViewModel.A.Count))));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenEmittingSourceListWithRemovedElement(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Remove(2));
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Append(
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, 2, 1)));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenEmittingSourceListWithRemovedElement(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Remove(2));
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Append(
				new PropertyChangedEventArgs(nameof(TestViewModel.A.Count))));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldNotRaiseCollectionChangedWhenEmittingEqualSourceList(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(_ => InitialValues);
		CollectionChangedEventArgs.Should().BeEquivalentTo(SubscribeCollectionChangedEventArgs);
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldNotRaisePropertyChangedWhenEmittingEqualSourceList(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(_ => InitialValues);
		PropertyChangedEventArgs.Should().BeEquivalentTo(SubscribePropertyChangedEventArgs);
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenResubscribing(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		Subscribe(subscriptions);
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Concat(SubscribeCollectionChangedEventArgs));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenResubscribing(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		Subscribe(subscriptions);
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Concat(ResetPropertyChangedEventArgs).Concat(SubscribePropertyChangedEventArgs));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenResubscribingToChangedStream(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		EmitValues(v => v.SetItem(1, 5));
		Subscribe(subscriptions);
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Concat(new []
			{
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset),
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, 1, 0),
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, 5, 1),
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, 6, 2),
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, 2, 1)
			}));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenResubscribingToChangedStream(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		EmitValues(v => v.SetItem(1, 5));
		Subscribe(subscriptions);
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Concat(ResetPropertyChangedEventArgs).Concat(SubscribePropertyChangedEventArgs));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenResubscribingThenChangingStream(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		Subscribe(subscriptions);
		EmitValues(v => v.Add(5));
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Concat(SubscribeCollectionChangedEventArgs).Append(
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, 5, 3)));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenResubscribingThenChangingStream(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		Subscribe(subscriptions);
		EmitValues(v => v.Add(5));
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Concat(ResetPropertyChangedEventArgs).Concat(SubscribePropertyChangedEventArgs).Append(
				new PropertyChangedEventArgs(nameof(TestViewModel.A.Count))));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged)]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	public void ShouldUnsubscribeFromValueListWhenViewModelIsDisposed(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		IsSubscribedToValues.Should().BeTrue();
		ViewModel.Dispose();
		IsSubscribedToValues.Should().BeFalse();
	}
}
