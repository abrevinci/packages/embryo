﻿using System.Linq;

namespace AbreVinci.Embryo.ReactiveUI.Tests.ReactiveCollectionViewExtensionsTests;

public class Page : CollectionTestBaseWithChildValues<int, Page.TestViewModel>
{
	public class TestViewModel : ReactiveViewModel
	{
		public TestViewModel(IObservable<IImmutableList<int>> a, IScheduler testScheduler) : base(testScheduler)
		{
			A = BindList(a).Page(1, 2);
		}

		public IPagedReactiveCollectionView<int> A { get; }
	}
	
	public Page() : base(
		getInitialValues: () => ImmutableArray.Create(1, 3, 6, 2),
		getInitialParams: () => 0,
		createViewModel: (values, _, scheduler) => new TestViewModel(values, scheduler),
		getItems: vm => vm.A,
		subscribeCollectionChangedEventArgs: new[]
		{
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, 3, 0),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, 6, 1)
		},
		subscribePropertyChangedEventArgs: new[]
		{
			new PropertyChangedEventArgs(nameof(TestViewModel.A.HasHiddenItemsBefore)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.HasHiddenItemsAfter)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.IsEmpty)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.HasHiddenItemsAfter)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.Count)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.Count)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.HasHiddenItemsAfter)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount))
		},
		resetPropertyChangedEventArgs: new[]
		{
			new PropertyChangedEventArgs(nameof(TestViewModel.A.IsEmpty)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.HasHiddenItemsBefore)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.HasHiddenItemsAfter)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.Count)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount))
		})
	{
	}
	
	[Fact]
	public void ShouldBeEmptyAfterConstruction()
	{
		ViewModel.A.Should().BeEmpty();
	}

	[Fact]
	public void ShouldHaveIsEmptyTrueAfterConstruction()
	{
		ViewModel.A.IsEmpty.Should().BeTrue();
	}

	[Fact]
	public void ShouldHaveCount0AfterConstruction()
	{
		ViewModel.A.Count.Should().Be(0);
	}

	[Fact]
	public void ShouldHaveHasHiddenItemsBeforeFalseAfterConstruction()
	{
		ViewModel.A.HasHiddenItemsBefore.Should().BeFalse();
	}

	[Fact]
	public void ShouldHaveHasHiddenItemsAfterFalseAfterConstruction()
	{
		ViewModel.A.HasHiddenItemsAfter.Should().BeFalse();
	}

	[Fact]
	public void ShouldHaveSourceCount0AfterConstruction()
	{
		ViewModel.A.SourceCount.Should().Be(0);
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldSetInitialPagedListWhenSubscribing(Subscription subscription)
	{
		Subscribe(subscription);
		ViewModel.A.Should().BeEquivalentTo(new[] { 3, 6 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldKeepTheListPagedWhenAddingItemsBeforePage(Subscription subscription)
	{
		Subscribe(subscription);
		EmitValues(v => v.Insert(0, 5));
		ViewModel.A.Should().BeEquivalentTo(new[] { 1, 3 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldKeepTheListPagedWhenAddingItemsInsidePage(Subscription subscription)
	{
		Subscribe(subscription);
		EmitValues(v => v.Insert(2, 5));
		ViewModel.A.Should().BeEquivalentTo(new[] { 3, 5 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldKeepTheListPagedWhenAddingItemsAfterPage(Subscription subscription)
	{
		Subscribe(subscription);
		EmitValues(v => v.Add(5));
		ViewModel.A.Should().BeEquivalentTo(new[] { 3, 6 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldKeepTheListPagedWhenRemovingItemsBeforePage(Subscription subscription)
	{
		Subscribe(subscription);
		EmitValues(v => v.Remove(1));
		ViewModel.A.Should().BeEquivalentTo(new[] { 6, 2 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldKeepTheListPagedWhenRemovingItemsInsidePage(Subscription subscription)
	{
		Subscribe(subscription);
		EmitValues(v => v.Remove(6));
		ViewModel.A.Should().BeEquivalentTo(new[] { 3, 2 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldKeepTheListPagedWhenRemovingItemsAfterPage(Subscription subscription)
	{
		Subscribe(subscription);
		EmitValues(v => v.Remove(2));
		ViewModel.A.Should().BeEquivalentTo(new[] { 3, 6 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldHaveIsEmptyFalseWhenSubscribingIfNotEmpty(Subscription subscription)
	{
		Subscribe(subscription);
		ViewModel.A.IsEmpty.Should().BeFalse();
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldHaveInitialPagedCountWhenSubscribing(Subscription subscription)
	{
		Subscribe(subscription);
		ViewModel.A.Count.Should().Be(2);
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldHaveHasHiddenItemsBeforeTrueWhenSubscribingIfStartIndexIsGreaterThan0(Subscription subscription)
	{
		Subscribe(subscription);
		ViewModel.A.HasHiddenItemsBefore.Should().BeTrue();
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldHaveHasHiddenItemsAfterTrueWhenSubscribingIfThereAreItemsAfterPage(Subscription subscription)
	{
		Subscribe(subscription);
		ViewModel.A.HasHiddenItemsAfter.Should().BeTrue();
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldHaveInitialSourceCountWhenSubscribing(Subscription subscription)
	{
		Subscribe(subscription);
		ViewModel.A.SourceCount.Should().Be(4);
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenSubscribing(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		CollectionChangedEventArgs.Should().BeEquivalentTo(SubscribeCollectionChangedEventArgs);
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenSubscribing(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		PropertyChangedEventArgs.Should().BeEquivalentTo(SubscribePropertyChangedEventArgs);
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenEmittingSourceListWithAddedElementBeforePage(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Insert(0, 5));
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Concat(new []
			{
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, 1, 0),
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, 6, 2),
			}));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenEmittingSourceListWithAddedElementInsidePage(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Insert(2, 5));
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Concat(new []
			{
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, 5, 1),
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, 6, 2),
			}));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldNotRaiseCollectionChangedWhenEmittingSourceListWithAddedElementAfterPage(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Add(5));
		CollectionChangedEventArgs.Should().BeEquivalentTo(SubscribeCollectionChangedEventArgs);
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenEmittingSourceListWithAddedElement(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Add(8));
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Append(
				new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount))));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenEmittingSourceListWithRemovedElementBeforePage(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Remove(1));
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Concat(new []
			{
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, 2, 2),
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, 3, 0)
			}));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenEmittingSourceListWithRemovedElementInsidePage(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Remove(6));
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Concat(new []
			{
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, 2, 2),
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, 6, 1)
			}));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldNotRaiseCollectionChangedWhenEmittingSourceListWithRemovedElementAfterPage(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Remove(2));
		CollectionChangedEventArgs.Should().BeEquivalentTo(SubscribeCollectionChangedEventArgs);
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenEmittingSourceListWithRemovedElementBeforePage(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Remove(1));
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Concat(new []
			{
				new PropertyChangedEventArgs(nameof(TestViewModel.A.HasHiddenItemsAfter)),
				new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount))
			}));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenEmittingSourceListWithRemovedElementInsidePage(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Remove(6));
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Concat(new []
			{
				new PropertyChangedEventArgs(nameof(TestViewModel.A.HasHiddenItemsAfter)),
				new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount))
			}));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenEmittingSourceListWithRemovedElementAfterPage(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Remove(2));
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Concat(new []
			{
				new PropertyChangedEventArgs(nameof(TestViewModel.A.HasHiddenItemsAfter)),
				new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount))
			}));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldNotRaiseCollectionChangedWhenEmittingEqualSourceList(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(_ => InitialValues);
		CollectionChangedEventArgs.Should().BeEquivalentTo(SubscribeCollectionChangedEventArgs);
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldNotRaisePropertyChangedWhenEmittingEqualSourceList(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(_ => InitialValues);
		PropertyChangedEventArgs.Should().BeEquivalentTo(SubscribePropertyChangedEventArgs);
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenResubscribing(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		Subscribe(subscriptions);
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Concat(SubscribeCollectionChangedEventArgs));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenResubscribing(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		Subscribe(subscriptions);
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Concat(ResetPropertyChangedEventArgs).Concat(SubscribePropertyChangedEventArgs));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenResubscribingToChangedStream(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		EmitValues(v => v.SetItem(1, 4));
		Subscribe(subscriptions);
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Concat(new []
			{
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset),
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, 4, 0),
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, 6, 1)
			}));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenResubscribingToChangedStream(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		EmitValues(v => v.SetItem(1, 5));
		Subscribe(subscriptions);
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Concat(ResetPropertyChangedEventArgs).Concat(SubscribePropertyChangedEventArgs));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenResubscribingThenChangingStream(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		Subscribe(subscriptions);
		EmitValues(v => v.Insert(2, 8));
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Concat(SubscribeCollectionChangedEventArgs).Concat(new []
			{
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, 8, 1),
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, 6, 2)
			}));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenResubscribingThenChangingStream(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		Subscribe(subscriptions);
		EmitValues(v => v.Insert(2, 8));
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Concat(ResetPropertyChangedEventArgs).Concat(SubscribePropertyChangedEventArgs).Append(
				new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount))));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged)]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	public void ShouldUnsubscribeFromValueListWhenViewModelIsDisposed(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		IsSubscribedToValues.Should().BeTrue();
		ViewModel.Dispose();
		IsSubscribedToValues.Should().BeFalse();
	}
}
