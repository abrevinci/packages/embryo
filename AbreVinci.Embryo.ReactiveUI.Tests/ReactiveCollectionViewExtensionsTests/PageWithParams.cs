﻿using System.Linq;
using AbreVinci.Embryo.Entities;

namespace AbreVinci.Embryo.ReactiveUI.Tests.ReactiveCollectionViewExtensionsTests;

public class PageWithParams : CollectionTestBaseWithChildViewModels<int, PageWithParams.TestViewModel>
{
	public class TestViewModel : ReactiveViewModel
	{
		public TestViewModel(
			IObservable<IImmutableList<int>> ids,
			IObservable<int> page,
			Func<int, TestChildViewModel> createChild,
			IScheduler testScheduler) : base(testScheduler)
		{
			A = BindListAsViewModels(ids, createChild, (vm, id) => vm.Id == id)
				.Page(page, p => p * 2, _ => 2);
		}

		public IPagedReactiveCollectionView<TestChildViewModel> A { get; }
	}
	
	public PageWithParams() : base(
		getInitialValues: () => new EntityCollection<int, int>().Add(1, 1).Add(2, 3).Add(3, 6).Add(4, 2),
		getInitialParams: () => 0,
		createViewModel: (ids, page, createChild, scheduler) => new TestViewModel(ids, page, createChild, scheduler),
		getItems: vm => vm.A,
		subscribeCollectionChangedEventArgs: new[]
		{
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 1, Value = 1 }, 0),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 2, Value = 3 }, 1)
		},
		subscribePropertyChangedEventArgs: new[]
		{
			new PropertyChangedEventArgs(nameof(TestViewModel.A.IsEmpty)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.Count)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.Count)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.HasHiddenItemsAfter)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount))
		},
		resetPropertyChangedEventArgs: new[]
		{
			new PropertyChangedEventArgs(nameof(TestViewModel.A.IsEmpty)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.HasHiddenItemsAfter)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.Count)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount))
		})
	{
	}
	
	[Fact]
	public void ShouldBeEmptyAfterConstruction()
	{
		ViewModel.A.Should().BeEmpty();
	}

	[Fact]
	public void ShouldHaveIsEmptyTrueAfterConstruction()
	{
		ViewModel.A.IsEmpty.Should().BeTrue();
	}

	[Fact]
	public void ShouldHaveCount0AfterConstruction()
	{
		ViewModel.A.Count.Should().Be(0);
	}

	[Fact]
	public void ShouldHaveHasHiddenItemsBeforeFalseAfterConstruction()
	{
		ViewModel.A.HasHiddenItemsBefore.Should().BeFalse();
	}

	[Fact]
	public void ShouldHaveHasHiddenItemsAfterFalseAfterConstruction()
	{
		ViewModel.A.HasHiddenItemsAfter.Should().BeFalse();
	}

	[Fact]
	public void ShouldHaveSourceCount0AfterConstruction()
	{
		ViewModel.A.SourceCount.Should().Be(0);
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldSetInitialPagedListWhenSubscribing(Subscription subscription)
	{
		Subscribe(subscription);
		ViewModel.A.Select(v => v.Value).Should().BeEquivalentTo(new[] { 1, 3 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldKeepTheListPagedWhenAddingItems(Subscription subscription)
	{
		Subscribe(subscription);
		EmitValues(v => v.Add(5, 5));
		ViewModel.A.Select(v => v.Value).Should().BeEquivalentTo(new[] { 1, 3 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldKeepTheListPagedWhenRemovingItemsBeforePage(Subscription subscription)
	{
		EmitParams(_ => 1);
		Subscribe(subscription);
		EmitValues(v => v.Remove(1));
		ViewModel.A.Select(v => v.Value).Should().BeEquivalentTo(new[] { 2 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldKeepTheListPagedWhenRemovingItemsInsidePage(Subscription subscription)
	{
		Subscribe(subscription);
		EmitValues(v => v.Remove(2));
		ViewModel.A.Select(v => v.Value).Should().BeEquivalentTo(new[] { 1, 6 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldKeepTheListPagedWhenRemovingItemsAfterPage(Subscription subscription)
	{
		Subscribe(subscription);
		EmitValues(v => v.Remove(4));
		ViewModel.A.Select(v => v.Value).Should().BeEquivalentTo(new[] { 1, 3 });
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldUpdatePageWhenParamsChange(Subscription subscription)
	{
		Subscribe(subscription);
		EmitParams(_ => 1);
		ViewModel.A.Select(v => v.Value).Should().BeEquivalentTo(new[] { 6, 2 });
	}

	[Fact]
	public void ShouldRaiseCollectionChangedResetWhenParamsChange()
	{
		Subscribe(Subscription.CollectionChanged);
		EmitParams(_ => 1);
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Append(
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset)));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldHaveIsEmptyFalseWhenSubscribingIfNotEmpty(Subscription subscription)
	{
		Subscribe(subscription);
		ViewModel.A.IsEmpty.Should().BeFalse();
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldHaveInitialPagedCountWhenSubscribing(Subscription subscription)
	{
		Subscribe(subscription);
		ViewModel.A.Count.Should().Be(2);
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldHaveHasHiddenItemsBeforeTrueWhenSubscribingIfStartIndexIsGreaterThan0(Subscription subscription)
	{
		EmitParams(_ => 1);
		Subscribe(subscription);
		ViewModel.A.HasHiddenItemsBefore.Should().BeTrue();
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldHaveHasHiddenItemsAfterTrueWhenSubscribingIfThereAreItemsAfterPage(Subscription subscription)
	{
		Subscribe(subscription);
		ViewModel.A.HasHiddenItemsAfter.Should().BeTrue();
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldHaveInitialSourceCountWhenSubscribing(Subscription subscription)
	{
		Subscribe(subscription);
		ViewModel.A.SourceCount.Should().Be(4);
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenSubscribing(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		CollectionChangedEventArgs.Should().BeEquivalentTo(SubscribeCollectionChangedEventArgs);
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenSubscribing(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		PropertyChangedEventArgs.Should().BeEquivalentTo(SubscribePropertyChangedEventArgs);
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenEmittingSourceListWithAddedElement(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Add(5, 8));
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Append(
				new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount))));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenEmittingSourceListWithRemovedElementBeforePage(params Subscription[] subscriptions)
	{
		EmitParams(_ => 1);
		Subscribe(subscriptions);
		EmitValues(v => v.Remove(1));
		CollectionChangedEventArgs.Should().BeEquivalentTo(new[]
		{
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 3, Value = 6 }, 0),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 4, Value = 2 }, 1),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, new { Id = 3, Value = 6 }, 0)
		});
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenEmittingSourceListWithRemovedElementInsidePage(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Remove(2));
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Concat(new[]
			{
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 3, Value = 6 }, 2),
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, new { Id = 2, Value = 3 }, 1)
			}));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldNotRaiseCollectionChangedWhenEmittingSourceListWithRemovedElementAfterPage(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Remove(4));
		CollectionChangedEventArgs.Should().BeEquivalentTo(SubscribeCollectionChangedEventArgs);
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenEmittingSourceListWithRemovedElementBeforePage(params Subscription[] subscriptions)
	{
		EmitParams(_ => 1);
		Subscribe(subscriptions);
		EmitValues(v => v.Remove(1));
		PropertyChangedEventArgs.Should().BeEquivalentTo(new[]
		{
			new PropertyChangedEventArgs(nameof(TestViewModel.A.HasHiddenItemsBefore)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.HasHiddenItemsAfter)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.IsEmpty)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.HasHiddenItemsAfter)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.Count)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.Count)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.Count)),
			new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount))
		});
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenEmittingSourceListWithRemovedElementInsidePage(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Remove(2));
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Append(
				new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount))));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenEmittingSourceListWithRemovedElementAfterPage(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(v => v.Remove(4));
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Append(
				new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount))));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldNotRaiseCollectionChangedWhenEmittingEqualSourceList(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(_ => InitialValues);
		CollectionChangedEventArgs.Should().BeEquivalentTo(SubscribeCollectionChangedEventArgs);
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldNotRaisePropertyChangedWhenEmittingEqualSourceList(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		EmitValues(_ => InitialValues);
		PropertyChangedEventArgs.Should().BeEquivalentTo(SubscribePropertyChangedEventArgs);
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenResubscribing(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		Subscribe(subscriptions);
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Concat(SubscribeCollectionChangedEventArgs));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenResubscribing(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		Subscribe(subscriptions);
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Concat(ResetPropertyChangedEventArgs).Concat(SubscribePropertyChangedEventArgs));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenResubscribingToChangedStream(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		EmitValues(v => v.Set(2, 4));
		Subscribe(subscriptions);
		CollectionChangedEventArgs.Should().BeEquivalentTo(
			SubscribeCollectionChangedEventArgs.Concat(new[]
			{
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset),
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 1, Value = 1 }, 0),
				new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 2, Value = 4 }, 1)
			}));
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenResubscribingToChangedStream(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		EmitValues(v => v.Set(2, 5));
		Subscribe(subscriptions);
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Concat(ResetPropertyChangedEventArgs).Concat(SubscribePropertyChangedEventArgs));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	[InlineData(Subscription.CollectionChanged)]
	public void ShouldRaiseCollectionChangedWhenResubscribingThenChangingStream(params Subscription[] subscriptions)
	{
		EmitParams(_ => 2);
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		Subscribe(subscriptions);
		EmitValues(v => v.Add(5, 5));
		CollectionChangedEventArgs.Should().BeEquivalentTo(new[]
		{
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset),
			new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new { Id = 5, Value = 5 }, 0),
		});
	}

	[Theory]
	[InlineData(Subscription.CollectionChanged, Subscription.PropertyChanged)]
	[InlineData(Subscription.PropertyChanged)]
	public void ShouldRaisePropertyChangedWhenResubscribingThenChangingStream(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		Unsubscribe(subscriptions);
		Subscribe(subscriptions);
		EmitValues(v => v.Add(5, 5));
		PropertyChangedEventArgs.Should().BeEquivalentTo(
			SubscribePropertyChangedEventArgs.Concat(ResetPropertyChangedEventArgs).Concat(SubscribePropertyChangedEventArgs).Append(
				new PropertyChangedEventArgs(nameof(TestViewModel.A.SourceCount))));
	}

	[Theory]
	[InlineData(Subscription.PropertyChanged)]
	[InlineData(Subscription.CollectionChanged)]
	[InlineData(Subscription.PropertyChanged, Subscription.CollectionChanged)]
	public void ShouldUnsubscribeFromValueListWhenViewModelIsDisposed(params Subscription[] subscriptions)
	{
		Subscribe(subscriptions);
		IsSubscribedToValues.Should().BeTrue();
		ViewModel.Dispose();
		IsSubscribedToValues.Should().BeFalse();
	}
}
