﻿namespace AbreVinci.Embryo.ReactiveUI.Tests.ReactiveViewModelTests;

public class BindOneWayAsViewModel
{
    private class TestChildViewModel : ReactiveViewModel
    {
        public TestChildViewModel(int id, IObservable<string> value, IScheduler testScheduler) : base(testScheduler)
        {
            Id = id;
            Value = BindOneWay(value);
        }

        public int Id { get; }

        // ReSharper disable once MemberCanBePrivate.Local
        // ReSharper disable once UnusedAutoPropertyAccessor.Local
        public string Value { get; }
    }
    
    private class TestViewModel : ReactiveViewModel
    {
        public TestViewModel(IObservable<int> id, Func<int, TestChildViewModel> createChildViewModelForId, IScheduler testScheduler) : base(testScheduler)
        {
            ViewModelForId = BindOneWayAsViewModel(id, createChildViewModelForId, (vm, i) => vm.Id == i);
        }
        
        public TestChildViewModel ViewModelForId { get; }
    }

    private class InheritedViewModel : TestViewModel
    {
        public InheritedViewModel(IObservable<int> id, IObservable<int> id2, Func<int, TestChildViewModel> createChildViewModelForId, IScheduler testScheduler) : base(id, createChildViewModelForId, testScheduler)
        {
            ViewModelForId2 = BindOneWayAsViewModel(id2, createChildViewModelForId, (vm, i) => vm.Id == i);
        }

        public TestChildViewModel ViewModelForId2 { get; }
    }

    [Fact]
    public void ShouldNotSetValueAfterConstruction()
    {
        var id = new BehaviorSubject<int>(11);
        var values = new BehaviorSubject<string>("A");
        var testScheduler = new TestScheduler();

        using var vm = new TestViewModel(id, i => new TestChildViewModel(i, values, testScheduler), testScheduler);

        testScheduler.Start();
        vm.ViewModelForId.Should().Be(default);
    }

    [Fact]
    public void ShouldSetInitialValueWhenSubscribing()
    {
        var id = new BehaviorSubject<int>(11);
        var values = new BehaviorSubject<string>("A");
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(id, i => new TestChildViewModel(i, values, testScheduler), testScheduler);

        vm.PropertyChanged += (_, _) => { };

        testScheduler.Start();
        vm.ViewModelForId.Should().NotBeNull();
        vm.ViewModelForId.Id.Should().Be(11);
    }

    [Fact]
    public void ShouldRaisePropertyChangedWhenSubscribing()
    {
        var id = new BehaviorSubject<int>(11);
        var values = new BehaviorSubject<string>("A");
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(id, i => new TestChildViewModel(i, values, testScheduler), testScheduler);

        var propertyChangeCallProperties = new List<string>();
        vm.PropertyChanged += (_, e) => propertyChangeCallProperties.Add(e.PropertyName!); // emits once

        testScheduler.Start();
        propertyChangeCallProperties.Should().BeEquivalentTo("ViewModelForId");
    }

    [Fact]
    public void ShouldRaisePropertyChangedWhenEmittingDifferentSourceValues()
    {
        var id = new BehaviorSubject<int>(11);
        var values = new BehaviorSubject<string>("A");
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(id, i => new TestChildViewModel(i, values, testScheduler), testScheduler);

        var propertyChangeCallProperties = new List<string>();
        vm.PropertyChanged += (_, e) => propertyChangeCallProperties.Add(e.PropertyName!); // emits once
        id.OnNext(23); // emits

        testScheduler.Start();
        propertyChangeCallProperties.Should().BeEquivalentTo("ViewModelForId", "ViewModelForId");
    }

    [Fact]
    public void ShouldNotRaisePropertyChangedWhenEmittingEqualSourceValues()
    {
        var id = new BehaviorSubject<int>(11);
        var values = new BehaviorSubject<string>("A");
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(id, i => new TestChildViewModel(i, values, testScheduler), testScheduler);

        var propertyChangeCallProperties = new List<string>();
        vm.PropertyChanged += (_, e) => propertyChangeCallProperties.Add(e.PropertyName!); // emits once
        id.OnNext(11); // same as previous, does not emit

        testScheduler.Start();
        propertyChangeCallProperties.Should().BeEquivalentTo("ViewModelForId");
    }

    [Fact]
    public void ShouldRaisePropertyChangedWhenResubscribing()
    {
        var id = new BehaviorSubject<int>(11);
        var values = new BehaviorSubject<string>("A");
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(id, i => new TestChildViewModel(i, values, testScheduler), testScheduler);

        var propertyChangeCallProperties = new List<string>();
        void OnPropertyChanged(object? _, PropertyChangedEventArgs e) => propertyChangeCallProperties.Add(e.PropertyName!);

        vm.PropertyChanged += OnPropertyChanged; // emits once
        testScheduler.Start();
        vm.PropertyChanged -= OnPropertyChanged;

        vm.PropertyChanged += OnPropertyChanged; // emits once
        testScheduler.Start();

        propertyChangeCallProperties.Should().BeEquivalentTo("ViewModelForId", "ViewModelForId");
    }

    [Fact]
    public void ShouldRaisePropertyChangedWhenResubscribingToChangedStream()
    {
        var id = new BehaviorSubject<int>(11);
        var values = new BehaviorSubject<string>("A");
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(id, i => new TestChildViewModel(i, values, testScheduler), testScheduler);

        var propertyChangeCallProperties = new List<string>();
        void OnPropertyChanged(object? _, PropertyChangedEventArgs e) => propertyChangeCallProperties.Add(e.PropertyName!);

        vm.PropertyChanged += OnPropertyChanged; // emits once
        testScheduler.Start();

        vm.PropertyChanged -= OnPropertyChanged;

        id.OnNext(32); // currently inactive, does not emit

        vm.PropertyChanged += OnPropertyChanged; // emits once
        testScheduler.Start();

        propertyChangeCallProperties.Should().BeEquivalentTo("ViewModelForId", "ViewModelForId");
    }

    [Fact]
    public void ShouldRaisePropertyChangedWhenResubscribingThenChangingStream()
    {
        var id = new BehaviorSubject<int>(11);
        var values = new BehaviorSubject<string>("A");
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(id, i => new TestChildViewModel(i, values, testScheduler), testScheduler);

        var propertyChangeCallProperties = new List<string>();
        void OnPropertyChanged(object? _, PropertyChangedEventArgs e) => propertyChangeCallProperties.Add(e.PropertyName!);

        vm.PropertyChanged += OnPropertyChanged; // emits once
        testScheduler.Start();

        vm.PropertyChanged -= OnPropertyChanged;

        vm.PropertyChanged += OnPropertyChanged; // emits once

        id.OnNext(32); // emits

        testScheduler.Start();

        propertyChangeCallProperties.Should().BeEquivalentTo("ViewModelForId", "ViewModelForId", "ViewModelForId");
    }

    [Fact]
    public void ShouldUnsubscribeFromValueWhenViewModelIsDisposed()
    {
        var id = new BehaviorSubject<int>(11);
        var values = new BehaviorSubject<string>("A");
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(id, i => new TestChildViewModel(i, values, testScheduler), testScheduler);

        vm.PropertyChanged += (_, _) => { };
        testScheduler.Start();

        id.HasObservers.Should().BeTrue();
        vm.Dispose();
        id.HasObservers.Should().BeFalse();
    }

    [Fact]
    public void ShouldUnsubscribeFromChildValueWhenViewModelIsDisposed()
    {
        var id = new BehaviorSubject<int>(11);
        var values = new BehaviorSubject<string>("A");
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(id, i => new TestChildViewModel(i, values, testScheduler), testScheduler);

        vm.PropertyChanged += (_, _) => { };
        vm.ViewModelForId.PropertyChanged += (_, _) => { };
        testScheduler.Start();

        values.HasObservers.Should().BeTrue();
        vm.Dispose();
        values.HasObservers.Should().BeFalse();
    }

    [Fact]
    public void ShouldSetInitialValueWhenSubscribingForInheritedViewModel()
    {
        var id = new BehaviorSubject<int>(11);
        var id2 = new BehaviorSubject<int>(22);
        var values = new BehaviorSubject<string>("A");
        var testScheduler = new TestScheduler();
        using var vm = new InheritedViewModel(id, id2, i => new TestChildViewModel(i, values, testScheduler), testScheduler);

        vm.PropertyChanged += (_, _) => { };

        testScheduler.Start();
        vm.ViewModelForId.Should().NotBeNull();
        vm.ViewModelForId.Id.Should().Be(11);
        vm.ViewModelForId2.Should().NotBeNull();
        vm.ViewModelForId2.Id.Should().Be(22);
    }
}
