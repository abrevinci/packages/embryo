﻿namespace AbreVinci.Embryo.ReactiveUI.Tests.ReactiveViewModelTests;

public class PropertyTypeConversion
{
	private interface IInterface
	{
		public int V { get; }
	}

	private record Interface(int V) : IInterface;

	private interface IChildViewModel
	{
		public int V { get; }
	}

	public class ChildViewModel : ReactiveViewModel, IChildViewModel
	{
		public ChildViewModel(int v, IScheduler scheduler) : base(scheduler)
		{
			V = v;
		}

		public int V { get; }
	}

	private class TestViewModel : ReactiveViewModel
	{
		public TestViewModel(IObservable<Interface> i, Func<Interface, ChildViewModel> createChild, IScheduler scheduler) : base(scheduler)
		{
			I = BindOneWay(i);
			I2 = BindTwoWay(i, _ => { });
			Child = BindOneWayAsViewModel(i, createChild, (vm, m) => vm.V == m.V);
		}

		public IInterface I { get; }
		public IInterface I2 { get; set; }
		public IChildViewModel Child { get; }
	}
	
	[Fact]
	public void ShouldBeValid()
	{
		var i = new BehaviorSubject<Interface>(new Interface(31));
		var testScheduler = new TestScheduler();
		var vm = new TestViewModel(i, m => new ChildViewModel(m.V, testScheduler), testScheduler);

		vm.PropertyChanged += (_, _) => { };
		
		vm.I.V.Should().Be(31);
		vm.I2.V.Should().Be(31);
		vm.Child.V.Should().Be(31);

		i.OnNext(new Interface(7));
		vm.I.V.Should().Be(7);
		vm.I2.V.Should().Be(7);
		vm.Child.V.Should().Be(7);

		vm.I2 = new Interface(3);
		vm.I2.V.Should().Be(3);
	}
}
