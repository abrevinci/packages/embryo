﻿namespace AbreVinci.Embryo.ReactiveUI.Tests.ReactiveViewModelTests;

public class BindCommand
{
    private abstract record TestMessage
    {
        public record A : TestMessage;
        public record B(int Param) : TestMessage;

        private TestMessage() { }
    }

    private class TestViewModel : ReactiveViewModel
    {
        public TestViewModel(Action<TestMessage> dispatchMessage, IScheduler testScheduler, IObservable<bool>? canExecute = null, bool initialCanExecute = false) : base(testScheduler)
        {
            A = BindCommand(() => dispatchMessage(new TestMessage.A()), canExecute, initialCanExecute);
            B = BindCommand<int>(p => dispatchMessage(new TestMessage.B(p)), canExecute, initialCanExecute);
        }

        public IReactiveCommand A { get; }
        public IReactiveCommand<int> B { get; }
    }

    [Theory]
    [InlineData(false, null, true)]
    [InlineData(false, null, false)]
    [InlineData(true, null, true)]
    [InlineData(true, null, false)]
    [InlineData(true, true, true)]
    [InlineData(true, true, false)]
    [InlineData(true, false, true)]
    [InlineData(true, false, false)]
    public void ShouldNotBeAbleToExecuteAnythingAfterConstruction(bool useCanExecute, bool? canExecuteStartValue, bool initialCanExecute)
    {
        var testScheduler = new TestScheduler();
        IObservable<bool>? canExecute = (useCanExecute, canExecuteStartValue) switch
        {
            (true, null) => new Subject<bool>(),
            (true, { } b) => new BehaviorSubject<bool>(b),
            _ => null
        };

        using var vm = new TestViewModel(_ => { }, testScheduler, canExecute, initialCanExecute);

        testScheduler.Start();
        vm.A.CanExecute(null).Should().BeFalse();
        vm.B.CanExecute(null).Should().BeFalse();
    }

    [Theory]
    [InlineData(false, false)]
    [InlineData(false, true)]
    [InlineData(true, false)]
    [InlineData(true, true)]
    public void ShouldBeAbleToExecuteIfInitialCanExecuteIsTrueAfterSubscriptionWhenNoValueHasBeenEmitted(bool useCanExecute, bool initialCanExecute)
    {
        var testScheduler = new TestScheduler();
        IObservable<bool>? canExecute = useCanExecute ? new Subject<bool>() : null;
        using var vm = new TestViewModel(_ => { }, testScheduler, canExecute, initialCanExecute);

        vm.A.CanExecuteChanged += (_, _) => { };
        vm.B.CanExecuteChanged += (_, _) => { };

        testScheduler.Start();
        vm.A.CanExecute(null).Should().Be(initialCanExecute);
        vm.B.CanExecute(null).Should().Be(initialCanExecute);
    }

    [Theory]
    [InlineData(false, false)]
    [InlineData(false, true)]
    [InlineData(true, false)]
    [InlineData(true, true)]
    public void ShouldBeAbleToExecuteIfEmittedCanExecuteIsTrueAfterSubscriptionWhenValueHasBeenEmitted(bool initialCanExecute, bool emittedCanExecute)
    {
        var testScheduler = new TestScheduler();
        var canExecute = new Subject<bool>();
        using var vm = new TestViewModel(_ => { }, testScheduler, canExecute, initialCanExecute);

        vm.A.CanExecuteChanged += (_, _) => { };
        vm.B.CanExecuteChanged += (_, _) => { };
        canExecute.OnNext(emittedCanExecute);

        testScheduler.Start();
        vm.A.CanExecute(null).Should().Be(emittedCanExecute);
        vm.B.CanExecute(null).Should().Be(emittedCanExecute);
    }

    [Theory]
    [InlineData(true)]
    [InlineData(false)]
    public void ShouldRaiseCanExecuteChangedWhenSubscribing(bool initialCanExecute)
    {
        var testScheduler = new TestScheduler();
        var canExecute = new Subject<bool>();
        using var vm = new TestViewModel(_ => { }, testScheduler, canExecute, initialCanExecute);

        var canExecuteChangedCalls = 0;
        vm.A.CanExecuteChanged += (_, _) => canExecuteChangedCalls++; // emits once
        vm.B.CanExecuteChanged += (_, _) => canExecuteChangedCalls++; // emits once

        testScheduler.Start();
        canExecuteChangedCalls.Should().Be(2);
    }

    [Theory]
    [InlineData(true)]
    [InlineData(false)]
    public void ShouldRaiseCanExecuteChangedWhenEmittingDifferentCanExecuteValues(bool initialCanExecute)
    {
        var testScheduler = new TestScheduler();
        var canExecute = new Subject<bool>();
        using var vm = new TestViewModel(_ => { }, testScheduler, canExecute, initialCanExecute);

        var canExecuteChangedCalls = 0;
        vm.A.CanExecuteChanged += (_, _) => canExecuteChangedCalls++; // emits once
        vm.B.CanExecuteChanged += (_, _) => canExecuteChangedCalls++; // emits once
        canExecute.OnNext(!initialCanExecute); // emits twice

        testScheduler.Start();
        canExecuteChangedCalls.Should().Be(4);
    }

    [Theory]
    [InlineData(true)]
    [InlineData(false)]
    public void ShouldNotRaiseCanExecuteChangedWhenEmittingEqualCanExecuteValues(bool initialCanExecute)
    {
        var testScheduler = new TestScheduler();
        var canExecute = new Subject<bool>();
        using var vm = new TestViewModel(_ => { }, testScheduler, canExecute, initialCanExecute);

        var canExecuteChangedCalls = 0;
        vm.A.CanExecuteChanged += (_, _) => canExecuteChangedCalls++; // emits once
        vm.B.CanExecuteChanged += (_, _) => canExecuteChangedCalls++; // emits once
        canExecute.OnNext(initialCanExecute); // no change, does not emit

        testScheduler.Start();
        canExecuteChangedCalls.Should().Be(2);
    }

    [Theory]
    [InlineData(true)]
    [InlineData(false)]
    public void ShouldRaiseCanExecuteChangedWhenResubscribing(bool initialCanExecute)
    {
        var testScheduler = new TestScheduler();
        var canExecute = new Subject<bool>();
        using var vm = new TestViewModel(_ => { }, testScheduler, canExecute, initialCanExecute);

        var canExecuteChangedCalls = 0;
        void OnCanExecuteChanged(object? _, EventArgs e) => canExecuteChangedCalls++;

        vm.A.CanExecuteChanged += OnCanExecuteChanged; // emits once
        vm.B.CanExecuteChanged += OnCanExecuteChanged; // emits once
        testScheduler.Start();
        vm.A.CanExecuteChanged -= OnCanExecuteChanged;
        vm.B.CanExecuteChanged -= OnCanExecuteChanged;

        vm.A.CanExecuteChanged += OnCanExecuteChanged; // emits once
        vm.B.CanExecuteChanged += OnCanExecuteChanged; // emits once

        testScheduler.Start();
        canExecuteChangedCalls.Should().Be(4);
    }

    [Theory]
    [InlineData(true)]
    [InlineData(false)]
    public void ShouldRaiseCanExecuteChangedWhenResubscribingToChangedStream(bool initialCanExecute)
    {
        var testScheduler = new TestScheduler();
        var canExecute = new Subject<bool>();
        using var vm = new TestViewModel(_ => { }, testScheduler, canExecute, initialCanExecute);

        var canExecuteChangedCalls = 0;
        void OnCanExecuteChanged(object? _, EventArgs e) => canExecuteChangedCalls++;

        vm.A.CanExecuteChanged += OnCanExecuteChanged; // emits once
        vm.B.CanExecuteChanged += OnCanExecuteChanged; // emits once
        testScheduler.Start();
        vm.A.CanExecuteChanged -= OnCanExecuteChanged;
        vm.B.CanExecuteChanged -= OnCanExecuteChanged;
        canExecute.OnNext(!initialCanExecute); // currently inactive, does not emit

        vm.A.CanExecuteChanged += OnCanExecuteChanged; // emits once
        vm.B.CanExecuteChanged += OnCanExecuteChanged; // emits once

        testScheduler.Start();
        canExecuteChangedCalls.Should().Be(4);
    }

    [Theory]
    [InlineData(true)]
    [InlineData(false)]
    public void ShouldRaiseCanExecuteChangedWhenResubscribingThenChangingStream(bool initialCanExecute)
    {
        var testScheduler = new TestScheduler();
        var canExecute = new Subject<bool>();
        using var vm = new TestViewModel(_ => { }, testScheduler, canExecute, initialCanExecute);

        var canExecuteChangedCalls = 0;
        void OnCanExecuteChanged(object? _, EventArgs e) => canExecuteChangedCalls++;

        vm.A.CanExecuteChanged += OnCanExecuteChanged; // emits once
        vm.B.CanExecuteChanged += OnCanExecuteChanged; // emits once
        testScheduler.Start();
        vm.A.CanExecuteChanged -= OnCanExecuteChanged;
        vm.B.CanExecuteChanged -= OnCanExecuteChanged;

        vm.A.CanExecuteChanged += OnCanExecuteChanged; // emits once
        vm.B.CanExecuteChanged += OnCanExecuteChanged; // emits once

        canExecute.OnNext(!initialCanExecute); // emits twice

        testScheduler.Start();
        canExecuteChangedCalls.Should().Be(6);
    }

    [Theory]
    [InlineData(true)]
    [InlineData(false)]
    public void ShouldDispatchMessageOnExecutionRegardlessOfCanExecute(bool initialCanExecute)
    {
        var testScheduler = new TestScheduler();
        var canExecute = new Subject<bool>();
        var messages = new List<TestMessage>();
        using var vm = new TestViewModel(messages.Add, testScheduler, canExecute, initialCanExecute);
        vm.A.CanExecuteChanged += (_, _) => { };
        vm.B.CanExecuteChanged += (_, _) => { };
        testScheduler.Start();

        vm.A.Execute(null);
        vm.B.Execute(32);

        messages.Should().HaveCount(2);
        messages[0].Should().Be(new TestMessage.A());
        messages[1].Should().Be(new TestMessage.B(32));
    }

    [Fact]
    public void ShouldUnsubscribeFromCanExecuteWhenViewModelIsDisposed()
    {
        var testScheduler = new TestScheduler();
        var canExecute = new Subject<bool>();
        var messages = new List<TestMessage>();
        var vm = new TestViewModel(messages.Add, testScheduler, canExecute);

        vm.A.CanExecuteChanged += (_, _) => { };
        testScheduler.Start();

        canExecute.HasObservers.Should().BeTrue();
        vm.Dispose();
        canExecute.HasObservers.Should().BeFalse();
    }
}
