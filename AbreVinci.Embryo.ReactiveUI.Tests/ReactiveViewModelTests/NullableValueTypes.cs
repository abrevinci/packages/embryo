﻿namespace AbreVinci.Embryo.ReactiveUI.Tests.ReactiveViewModelTests;

public class NullableValueTypes
{
	private class TestChildViewModel : ReactiveViewModel
	{
		public TestChildViewModel(int id, IScheduler testScheduler) : base(testScheduler)
		{
			Id = id;
		}

		public int Id { get; }
	}

	private class TestViewModel : ReactiveViewModel
	{
		public TestViewModel(IObservable<int?> a, IObservable<int?> id, Action<int?> setId, Func<int, TestChildViewModel> createChildViewModelForId, IScheduler testScheduler) : base(testScheduler)
		{
			A = BindOneWay(a);
			Id = BindTwoWay(id, setId);
			ViewModelForId = BindOneWayAsViewModel(id, i => i != null ? createChildViewModelForId(i.Value) : null, (vm, i) => vm?.Id == i);
		}

		public int? A { get; }
		public int? Id { get; set; }
		public TestChildViewModel? ViewModelForId { get; }
	}

	[Fact]
	public void ShouldBeValid()
	{
		var a = new BehaviorSubject<int?>(null);
		var id = new BehaviorSubject<int?>(null);
		var testScheduler = new TestScheduler();
		var vm = new TestViewModel(a, id, i => id.OnNext(i), i => new TestChildViewModel(i, testScheduler), testScheduler);

		vm.PropertyChanged += (_, _) => { };

		vm.A.Should().BeNull();
		vm.Id.Should().BeNull();
		vm.ViewModelForId.Should().BeNull();

		a.OnNext(1);

		vm.A.Should().Be(1);

		id.OnNext(22);

		vm.Id.Should().Be(22);
		vm.ViewModelForId.Should().NotBeNull();
		vm.ViewModelForId!.Id.Should().Be(22);

		vm.Id = null;

		vm.Id.Should().BeNull();
		vm.ViewModelForId.Should().BeNull();

		vm.Id = 5;

		vm.Id.Should().Be(5);
		vm.ViewModelForId.Should().NotBeNull();
		vm.ViewModelForId!.Id.Should().Be(5);
	}
}
