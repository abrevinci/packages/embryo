﻿namespace AbreVinci.Embryo.ReactiveUI.Tests.ReactiveViewModelTests;

public class BindTwoWay
{
    private abstract record TestMessage
    {
        public record SetA(int Value) : TestMessage;
        public record SetB(int Value) : TestMessage;

        private TestMessage() { }
    }

    private class TestViewModel : ReactiveViewModel
    {
        public TestViewModel(IObservable<int> a, Action<TestMessage> dispatchMessage, IScheduler testScheduler) : base(testScheduler)
        {
            A = BindTwoWay(a, v => dispatchMessage(new TestMessage.SetA(v)));
        }

        public int A { get; set; }
    }

    private class InheritedViewModel : TestViewModel
    {
        public InheritedViewModel(IObservable<int> a, IObservable<int> b, Action<TestMessage> dispatchMessage, IScheduler testScheduler) : base(a, dispatchMessage, testScheduler)
        {
            B = BindTwoWay(b, v => dispatchMessage(new TestMessage.SetB(v)));
        }

        public int B { get; set; }
    }

    [Fact]
    public void ShouldNotSetValueAfterConstruction()
    {
        var a = new BehaviorSubject<int>(11);
        var testScheduler = new TestScheduler();

        var vm = new TestViewModel(a, _ => { }, testScheduler);

        testScheduler.Start();
        vm.A.Should().Be(default);
    }

    [Fact]
    public void ShouldSetInitialValueWhenSubscribing()
    {
        var a = new BehaviorSubject<int>(11);
        var testScheduler = new TestScheduler();
        var vm = new TestViewModel(a, _ => { }, testScheduler);

        vm.PropertyChanged += (_, _) => { };

        testScheduler.Start();
        vm.A.Should().Be(11);
    }

    [Fact]
    public void ShouldRaisePropertyChangedWhenSubscribing()
    {
        var a = new BehaviorSubject<int>(11);
        var testScheduler = new TestScheduler();
        var vm = new TestViewModel(a, _ => { }, testScheduler);

        var propertyChangeCallProperties = new List<string>();
        vm.PropertyChanged += (_, e) => propertyChangeCallProperties.Add(e.PropertyName!); // emits once

        testScheduler.Start();
        propertyChangeCallProperties.Should().BeEquivalentTo("A");
    }

    [Fact]
    public void ShouldRaisePropertyChangedWhenEmittingDifferentSourceValues()
    {
        var a = new BehaviorSubject<int>(11);
        var testScheduler = new TestScheduler();
        var vm = new TestViewModel(a, _ => { }, testScheduler);

        var propertyChangeCallProperties = new List<string>();
        vm.PropertyChanged += (_, e) => propertyChangeCallProperties.Add(e.PropertyName!); // emits once
        a.OnNext(23);

        testScheduler.Start();
        propertyChangeCallProperties.Should().BeEquivalentTo("A", "A");
    }

    [Fact]
    public void ShouldNotRaisePropertyChangedWhenEmittingEqualSourceValues()
    {
        var a = new BehaviorSubject<int>(11);
        var testScheduler = new TestScheduler();
        var vm = new TestViewModel(a, _ => { }, testScheduler);

        var propertyChangeCallProperties = new List<string>();
        vm.PropertyChanged += (_, e) => propertyChangeCallProperties.Add(e.PropertyName!); // emits once
        a.OnNext(11);

        testScheduler.Start();
        propertyChangeCallProperties.Should().BeEquivalentTo("A");
    }

    [Fact]
    public void ShouldNotDispatchMessageWhenEmittingSourceValues()
    {
        var a = new BehaviorSubject<int>(11);
        var testScheduler = new TestScheduler();
        var messageList = new List<TestMessage>();
        var vm = new TestViewModel(a, messageList.Add, testScheduler);

        vm.PropertyChanged += (_, _) => { }; // emits once
        a.OnNext(11);

        testScheduler.Start();
        messageList.Should().BeEmpty();
    }

    [Fact]
    public void ShouldDispatchMessageWhenSettingValue()
    {
        var a = new BehaviorSubject<int>(11);
        var testScheduler = new TestScheduler();
        var messageList = new List<TestMessage>();
        var vm = new TestViewModel(a, messageList.Add, testScheduler);

        vm.PropertyChanged += (_, _) => { }; // emits once
        vm.A = 32;

        testScheduler.Start();
        messageList.Should().ContainSingle().Which.Should().Be(new TestMessage.SetA(32));
    }

    [Fact]
    public void ShouldUnsubscribeFromValueWhenViewModelIsDisposed()
    {
        var a = new BehaviorSubject<int>(11);
        var testScheduler = new TestScheduler();
        var vm = new TestViewModel(a, _ => { }, testScheduler);

        vm.PropertyChanged += (_, _) => { };
        testScheduler.Start();

        a.HasObservers.Should().BeTrue();
        vm.Dispose();
        a.HasObservers.Should().BeFalse();
    }

    [Fact]
    public void ShouldSetInitialValueWhenSubscribingForInheritedViewModel()
    {
        var a = new BehaviorSubject<int>(11);
        var b = new BehaviorSubject<int>(22);
        var testScheduler = new TestScheduler();
        var vm = new InheritedViewModel(a, b, _ => { }, testScheduler);

        vm.PropertyChanged += (_, _) => { };

        testScheduler.Start();
        vm.A.Should().Be(11);
        vm.B.Should().Be(22);
    }

    [Fact]
    public void ShouldDispatchMessageWhenSettingBaseValueForInheritedViewModel()
    {
        var a = new BehaviorSubject<int>(11);
        var b = new BehaviorSubject<int>(22);
        var testScheduler = new TestScheduler();
        var messageList = new List<TestMessage>();
        var vm = new InheritedViewModel(a, b, messageList.Add, testScheduler);

        vm.PropertyChanged += (_, _) => { }; // emits once
        vm.A = 32;

        testScheduler.Start();
        messageList.Should().ContainSingle().Which.Should().Be(new TestMessage.SetA(32));
    }

    [Fact]
    public void ShouldDispatchMessageWhenSettingValueForInheritedViewModel()
    {
        var a = new BehaviorSubject<int>(11);
        var b = new BehaviorSubject<int>(22);
        var testScheduler = new TestScheduler();
        var messageList = new List<TestMessage>();
        var vm = new InheritedViewModel(a, b, messageList.Add, testScheduler);

        vm.PropertyChanged += (_, _) => { }; // emits once
        vm.B = 32;

        testScheduler.Start();
        messageList.Should().ContainSingle().Which.Should().Be(new TestMessage.SetB(32));
    }
}
