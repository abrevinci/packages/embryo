﻿namespace AbreVinci.Embryo.ReactiveUI.Tests.ReactiveViewModelTests;

public class AddChildViewModel
{
    private class TestChildViewModel : ReactiveViewModel
    {
        public TestChildViewModel(IObservable<string> value, IScheduler testScheduler) : base(testScheduler)
        {
            Value = BindOneWay(value);
        }

        // ReSharper disable once MemberCanBePrivate.Local
        // ReSharper disable once UnusedAutoPropertyAccessor.Local
        public string Value { get; }
    }

    private class TestViewModel : ReactiveViewModel
    {
        public TestViewModel(Func<TestChildViewModel> createChildViewModelForId, IScheduler testScheduler) : base(testScheduler)
        {
            Child = AddChildViewModel(createChildViewModelForId);
        }

        public TestChildViewModel Child { get; }
    }

    [Fact]
    public void ShouldSetChildViewModelDuringConstruction()
    {
        var value = new BehaviorSubject<string>("A");
        var testScheduler = new TestScheduler();
        
        using var vm = new TestViewModel(() => new TestChildViewModel(value, testScheduler), testScheduler);

        vm.Child.Should().NotBe(default);
    }

    [Fact]
    public void ShouldUnsubscribeFromChildValueWhenViewModelIsDisposed()
    {
        var value = new BehaviorSubject<string>("A");
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(() => new TestChildViewModel(value, testScheduler), testScheduler);

        vm.Child.PropertyChanged += (_, _) => { };
        testScheduler.Start();

        value.HasObservers.Should().BeTrue();
        vm.Dispose();
        value.HasObservers.Should().BeFalse();
    }
}
