﻿namespace AbreVinci.Embryo.ReactiveUI.Tests.ReactiveViewModelTests;

public class BranchInstructions
{
    private class TestChildViewModel : ReactiveViewModel
    {
        public TestChildViewModel(int id, IScheduler testScheduler) : base(testScheduler)
        {
            Id = id;
        }

        public int Id { get; }
    }

    private class TestViewModel : ReactiveViewModel
    {
        public TestViewModel(IObservable<int> a, IObservable<int> id, Func<int, TestChildViewModel> createChildViewModelForId, IScheduler testScheduler) : base(testScheduler)
        {
            A = BindOneWay(a);
            ViewModelForId = BindOneWayAsViewModel(id, createChildViewModelForId, (vm, i) => vm.Id == i);
        }

        // ReSharper disable once MemberCanBePrivate.Local
        // ReSharper disable once UnusedAutoPropertyAccessor.Local
        public int A { get; }

        // ReSharper disable once MemberCanBePrivate.Local
        // ReSharper disable once UnusedAutoPropertyAccessor.Local
        public TestChildViewModel ViewModelForId { get; }
    }
    
    // ReSharper disable once UnusedType.Local
    private class ReferenceViewModel : ReactiveViewModel
	{
        private readonly IReactiveOneWayBinding<int> _a;
        private readonly IReactiveOneWayBinding<TestChildViewModel> _viewModelForId;

		public ReferenceViewModel(IObservable<int> a, IObservable<int> id, Func<int, TestChildViewModel> createChildViewModelForId, IScheduler testScheduler) : base(testScheduler)
        {
            _a = CreateOneWayBinding(a, nameof(A));
            _viewModelForId = CreateOneWayAsViewModelBinding(id, createChildViewModelForId, (vm, i) => vm.Id == i, nameof(ViewModelForId));
		}

        public int A => _a.Value;
        public TestChildViewModel ViewModelForId => _viewModelForId.Value;
    }

    [Fact]
    public void ShouldBeValid()
	{
        var testScheduler = new TestScheduler();
        _ = new TestViewModel(new BehaviorSubject<int>(2), new BehaviorSubject<int>(2), _ => new TestChildViewModel(2, testScheduler), testScheduler);
	}
}
