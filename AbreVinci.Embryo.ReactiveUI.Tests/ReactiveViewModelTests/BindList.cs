﻿namespace AbreVinci.Embryo.ReactiveUI.Tests.ReactiveViewModelTests;

public class BindList
{
    private class TestViewModel : ReactiveViewModel
    {
        public TestViewModel(IObservable<IImmutableList<int>> a, IScheduler testScheduler) : base(testScheduler)
        {
            A = BindList(a);
        }

        public IReactiveList<int> A { get; }
    }

    [Fact]
    public void ShouldBeEmptyAfterConstruction()
    {
        var a = new BehaviorSubject<IImmutableList<int>>(ImmutableArray.Create(1, 2, 3));
        var testScheduler = new TestScheduler();

        using var vm = new TestViewModel(a, testScheduler);

        testScheduler.Start();
        vm.A.Should().BeEmpty();
    }

    [Fact]
    public void ShouldHaveIsEmptyTrueAfterConstruction()
    {
	    var a = new BehaviorSubject<IImmutableList<int>>(ImmutableArray.Create(1, 2, 3));
	    var testScheduler = new TestScheduler();

	    using var vm = new TestViewModel(a, testScheduler);

	    testScheduler.Start();
	    vm.A.IsEmpty.Should().BeTrue();
    }
	
    [Fact]
    public void ShouldHaveCount0AfterContstruction()
    {
	    var a = new BehaviorSubject<IImmutableList<int>>(ImmutableArray.Create(1, 2, 3));
	    var testScheduler = new TestScheduler();

	    using var vm = new TestViewModel(a, testScheduler);

	    testScheduler.Start();
	    vm.A.Count.Should().Be(0);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldSetInitialListWhenSubscribing(bool subscribeToCollection)
    {
        var a = new BehaviorSubject<IImmutableList<int>>(ImmutableArray.Create(1, 2, 3));
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(a, testScheduler);

        if (subscribeToCollection)
	        vm.A.CollectionChanged += (_, _) => { };
        else
	        vm.A.PropertyChanged += (_, _) => { };

        testScheduler.Start();
        vm.A.Should().BeEquivalentTo(new[] { 1, 2, 3 });
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldHaveInitialCountWhenSubscribing(bool subscribeToCollection)
    {
	    var a = new BehaviorSubject<IImmutableList<int>>(ImmutableArray.Create(1, 2, 3));
	    var testScheduler = new TestScheduler();
	    using var vm = new TestViewModel(a, testScheduler);

	    if (subscribeToCollection)
		    vm.A.CollectionChanged += (_, _) => { };
	    else
		    vm.A.PropertyChanged += (_, _) => { };

        testScheduler.Start();
	    vm.A.Count.Should().Be(3);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldRaiseCollectionChangedWhenSubscribing(bool subscribeToPropertyBefore)
    {
        var a = new BehaviorSubject<IImmutableList<int>>(ImmutableArray.Create(1, 2, 3));
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(a, testScheduler);

        var collectionChangedCalls = 0;

        if (subscribeToPropertyBefore)
	        vm.A.PropertyChanged += (_, _) => { };
        vm.A.CollectionChanged += (_, _) => collectionChangedCalls++; // emits once for each element

        testScheduler.Start();
        collectionChangedCalls.Should().Be(3);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldRaisePropertyChangedWhenSubscribing(bool subscribeToCollectionBefore)
    {
	    var a = new BehaviorSubject<IImmutableList<int>>(ImmutableArray.Create(1, 2, 3));
	    var testScheduler = new TestScheduler();
	    using var vm = new TestViewModel(a, testScheduler);

	    var propertyChangedCalls = 0;

	    if (subscribeToCollectionBefore)
		    vm.A.CollectionChanged += (_, _) => { };
        vm.A.PropertyChanged += (_, _) => propertyChangedCalls++; // emits twice

	    testScheduler.Start();
	    propertyChangedCalls.Should().Be(2);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldRaiseCollectionChangedWhenEmittingSourceListWithAddedElement(bool subscribeToPropertyBefore)
    {
        var sourceList = ImmutableArray.Create(1, 2, 3);
        var a = new BehaviorSubject<IImmutableList<int>>(sourceList);
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(a, testScheduler);

        var collectionChangedCallsWithAdd = 0;

        if (subscribeToPropertyBefore)
	        vm.A.PropertyChanged += (_, _) => { };
        vm.A.CollectionChanged += (_, e) =>  // emits three times (Add 1, Add 2, Add 3)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
                collectionChangedCallsWithAdd++;
        };

        a.OnNext(sourceList.Add(5)); // emits once (Add 5)

        testScheduler.Start();
        collectionChangedCallsWithAdd.Should().Be(4);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldRaisePropertyChangedWhenEmittingSourceListWithAddedElement(bool subscribeToCollectionBefore)
    {
	    var sourceList = ImmutableArray.Create(1, 2, 3);
	    var a = new BehaviorSubject<IImmutableList<int>>(sourceList);
	    var testScheduler = new TestScheduler();
	    using var vm = new TestViewModel(a, testScheduler);

	    var propertyChangedCalls = 0;

	    if (subscribeToCollectionBefore)
		    vm.A.CollectionChanged += (_, _) => { };
        vm.A.PropertyChanged += (_, _) => propertyChangedCalls++; // emits twice

        a.OnNext(sourceList.Add(5)); // emits once

	    testScheduler.Start();
	    propertyChangedCalls.Should().Be(3);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldRaiseCollectionChangedWhenEmittingSourceListWithRemovedElement(bool subscribeToPropertyBefore)
    {
        var sourceList = ImmutableArray.Create(1, 2, 3);
        var a = new BehaviorSubject<IImmutableList<int>>(sourceList);
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(a, testScheduler);

        var collectionChangedCallsWithRemove = 0;

        if (subscribeToPropertyBefore)
	        vm.A.PropertyChanged += (_, _) => { };
        vm.A.CollectionChanged += (_, e) => // emits three times (Add 1, Add 2, Add 3)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove)
                collectionChangedCallsWithRemove++;
        };

        a.OnNext(sourceList.Remove(2)); // emits once (Remove 2)

        testScheduler.Start();
        collectionChangedCallsWithRemove.Should().Be(1);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldRaisePropertyChangedWhenEmittingSourceListWithRemovedElement(bool subscribeToCollectionBefore)
    {
	    var sourceList = ImmutableArray.Create(1, 2, 3);
	    var a = new BehaviorSubject<IImmutableList<int>>(sourceList);
	    var testScheduler = new TestScheduler();
	    using var vm = new TestViewModel(a, testScheduler);

	    var propertyChangedCalls = 0;

        if (subscribeToCollectionBefore)
		    vm.A.CollectionChanged += (_, _) => { };
	    vm.A.PropertyChanged += (_, _) => propertyChangedCalls++; // emits twice

        a.OnNext(sourceList.Remove(2)); // emits once

	    testScheduler.Start();
	    propertyChangedCalls.Should().Be(3);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldNotRaiseCollectionChangedWhenEmittingEqualSourceList(bool subscribeToPropertyBefore)
    {
        var a = new BehaviorSubject<IImmutableList<int>>(ImmutableArray.Create(1, 2, 3));
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(a, testScheduler);

        var collectionChangedCalls = 0;

        if (subscribeToPropertyBefore)
	        vm.A.PropertyChanged += (_, _) => { };
        vm.A.CollectionChanged += (_, _) => collectionChangedCalls++; // emits three times (Add 1, Add 2, Add 3)

        a.OnNext(ImmutableArray.Create(1, 2, 3)); // does not emit

        testScheduler.Start();
        collectionChangedCalls.Should().Be(3);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldNotRaisePropertyChangedWhenEmittingEqualSourceList(bool subscribeToCollectionBefore)
    {
	    var a = new BehaviorSubject<IImmutableList<int>>(ImmutableArray.Create(1, 2, 3));
	    var testScheduler = new TestScheduler();
	    using var vm = new TestViewModel(a, testScheduler);

	    var propertyChangedCalls = 0;

	    if (subscribeToCollectionBefore)
		    vm.A.CollectionChanged += (_, _) => { };
        vm.A.PropertyChanged += (_, _) => propertyChangedCalls++; // emits twice

	    a.OnNext(ImmutableArray.Create(1, 2, 3)); // does not emit

	    testScheduler.Start();
	    propertyChangedCalls.Should().Be(2);
    }
	
    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldRaiseCollectionChangedWhenResubscribing(bool subscribeToPropertyBefore)
    {
        var a = new BehaviorSubject<IImmutableList<int>>(ImmutableArray.Create(1, 2, 3));
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(a, testScheduler);

        var collectionChangedCalls = 0;
        void OnPropertyChanged(object? _, PropertyChangedEventArgs e) { }
        void OnCollectionChanged(object? _, NotifyCollectionChangedEventArgs e) => collectionChangedCalls++;

        if (subscribeToPropertyBefore)
	        vm.A.PropertyChanged += OnPropertyChanged;
        vm.A.CollectionChanged += OnCollectionChanged; // emits three times (Add 1, Add 2, Add 3)

        testScheduler.Start();

        vm.A.CollectionChanged -= OnCollectionChanged;
        if (subscribeToPropertyBefore)
	        vm.A.PropertyChanged -= OnPropertyChanged;

        if (subscribeToPropertyBefore)
	        vm.A.PropertyChanged += OnPropertyChanged;
        vm.A.CollectionChanged += OnCollectionChanged; // emits three times (Add 1, Add 2, Add 3)

        testScheduler.Start();
        collectionChangedCalls.Should().Be(6);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldRaisePropertyChangedWhenResubscribing(bool subscribeToCollectionBefore)
    {
	    var a = new BehaviorSubject<IImmutableList<int>>(ImmutableArray.Create(1, 2, 3));
	    var testScheduler = new TestScheduler();
	    using var vm = new TestViewModel(a, testScheduler);

	    var propertyChangedCalls = 0;
	    void OnCollectionChanged(object? _, NotifyCollectionChangedEventArgs e) { }
	    void OnPropertyChanged(object? _, PropertyChangedEventArgs e) => propertyChangedCalls++;

	    if (subscribeToCollectionBefore)
		    vm.A.CollectionChanged += OnCollectionChanged;
        vm.A.PropertyChanged += OnPropertyChanged; // emits twice
	    testScheduler.Start();
	    vm.A.PropertyChanged -= OnPropertyChanged;
	    if (subscribeToCollectionBefore)
		    vm.A.CollectionChanged -= OnCollectionChanged;

	    if (subscribeToCollectionBefore)
		    vm.A.CollectionChanged += OnCollectionChanged;
        vm.A.PropertyChanged += OnPropertyChanged; // emits twice

	    testScheduler.Start();
	    propertyChangedCalls.Should().Be(4);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldRaiseCollectionChangedWhenResubscribingToChangedStream(bool subscribeToPropertyBefore)
    {
        var sourceList = ImmutableArray.Create(1, 2, 3);
        var a = new BehaviorSubject<IImmutableList<int>>(sourceList);
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(a, testScheduler);

        var collectionChangedCalls = 0;
        void OnPropertyChanged(object? _, PropertyChangedEventArgs e) { }
        void OnCollectionChanged(object? _, NotifyCollectionChangedEventArgs e) => collectionChangedCalls++;

        if (subscribeToPropertyBefore)
	        vm.A.PropertyChanged += OnPropertyChanged;
        vm.A.CollectionChanged += OnCollectionChanged; // emits three times (Add 1, Add 2, Add 3)
        testScheduler.Start();
        vm.A.CollectionChanged -= OnCollectionChanged;
        if (subscribeToPropertyBefore)
	        vm.A.PropertyChanged -= OnPropertyChanged;

        a.OnNext(sourceList.SetItem(1, 5)); // currently inactive, does not emit

        if (subscribeToPropertyBefore)
	        vm.A.PropertyChanged += OnPropertyChanged;
        vm.A.CollectionChanged += OnCollectionChanged; // emits three times (Add 1, Add 5, Add 3)

        testScheduler.Start();
        collectionChangedCalls.Should().Be(6);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldRaisePropertyChangedWhenResubscribingToChangedStream(bool subscribeToCollectionBefore)
    {
	    var sourceList = ImmutableArray.Create(1, 2, 3);
	    var a = new BehaviorSubject<IImmutableList<int>>(sourceList);
	    var testScheduler = new TestScheduler();
	    using var vm = new TestViewModel(a, testScheduler);

	    var propertyChangedCalls = 0;
	    void OnCollectionChanged(object? _, NotifyCollectionChangedEventArgs e) { }
        void OnPropertyChanged(object? _, PropertyChangedEventArgs e) => propertyChangedCalls++;

        if (subscribeToCollectionBefore)
	        vm.A.CollectionChanged += OnCollectionChanged;
        vm.A.PropertyChanged += OnPropertyChanged; // emits twice
	    testScheduler.Start();
	    vm.A.PropertyChanged -= OnPropertyChanged;
	    if (subscribeToCollectionBefore)
		    vm.A.CollectionChanged -= OnCollectionChanged;

        a.OnNext(sourceList.SetItem(1, 5)); // currently inactive, does not emit

        if (subscribeToCollectionBefore)
	        vm.A.CollectionChanged += OnCollectionChanged;
        vm.A.PropertyChanged += OnPropertyChanged; // emits twice

	    testScheduler.Start();
	    propertyChangedCalls.Should().Be(4);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldRaiseCollectionChangedWhenResubscribingThenChangingStream(bool subscribeToPropertyBefore)
    {
        var sourceList = ImmutableArray.Create(1, 2, 3);
        var a = new BehaviorSubject<IImmutableList<int>>(sourceList);
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(a, testScheduler);

        var collectionChangedCalls = 0;
        void OnPropertyChanged(object? _, PropertyChangedEventArgs e) { }
        void OnCollectionChanged(object? _, NotifyCollectionChangedEventArgs e) => collectionChangedCalls++;

        if (subscribeToPropertyBefore)
	        vm.A.PropertyChanged += OnPropertyChanged;
        vm.A.CollectionChanged += OnCollectionChanged; // emits three times (Add 1, Add 2, Add 3)
        testScheduler.Start();
        vm.A.CollectionChanged -= OnCollectionChanged;
        if (subscribeToPropertyBefore)
	        vm.A.PropertyChanged -= OnPropertyChanged;

        if (subscribeToPropertyBefore)
	        vm.A.PropertyChanged += OnPropertyChanged;
        vm.A.CollectionChanged += OnCollectionChanged; // emits three times (Add 1, Add 2, Add 3)

        testScheduler.Start();

        a.OnNext(sourceList.Add(5)); // emits once (Add 5)

        testScheduler.Start();
        collectionChangedCalls.Should().Be(7);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldRaisePropertyChangedWhenResubscribingThenChangingStream(bool subscribeToCollectionBefore)
    {
	    var sourceList = ImmutableArray.Create(1, 2, 3);
	    var a = new BehaviorSubject<IImmutableList<int>>(sourceList);
	    var testScheduler = new TestScheduler();
	    using var vm = new TestViewModel(a, testScheduler);

	    var collectionChangedCalls = 0;
	    void OnCollectionChanged(object? _, NotifyCollectionChangedEventArgs e) { }
        void OnPropertyChanged(object? _, PropertyChangedEventArgs e) => collectionChangedCalls++;

        if (subscribeToCollectionBefore)
	        vm.A.CollectionChanged += OnCollectionChanged;
        vm.A.PropertyChanged += OnPropertyChanged; // emits twice
	    testScheduler.Start();
	    vm.A.PropertyChanged -= OnPropertyChanged;
	    if (subscribeToCollectionBefore)
		    vm.A.CollectionChanged -= OnCollectionChanged;

	    if (subscribeToCollectionBefore)
		    vm.A.CollectionChanged += OnCollectionChanged;
        vm.A.PropertyChanged += OnPropertyChanged; // emits twice

	    testScheduler.Start();

	    a.OnNext(sourceList.Add(5)); // emits once

	    testScheduler.Start();
	    collectionChangedCalls.Should().Be(5);
    }

    [Theory]
    [InlineData(false, true)]
    [InlineData(true, false)]
    [InlineData(true, true)]
    public void ShouldUnsubscribeFromValueListWhenViewModelIsDisposed(bool subscribeToCollection, bool subscribeToProperty)
    {
        var sourceList = ImmutableArray.Create(1, 2, 3);
        var a = new BehaviorSubject<IImmutableList<int>>(sourceList);
        var testScheduler = new TestScheduler();
        var vm = new TestViewModel(a, testScheduler);

        if (subscribeToCollection)
			vm.A.CollectionChanged += (_, _) => { };
        if (subscribeToProperty)
			vm.A.PropertyChanged += (_, _) => { };
        testScheduler.Start();

        a.HasObservers.Should().BeTrue();
        vm.Dispose();
        a.HasObservers.Should().BeFalse();
    }
}
