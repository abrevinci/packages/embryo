﻿using System.Linq;

namespace AbreVinci.Embryo.ReactiveUI.Tests.ReactiveViewModelTests;

public class BindListAsViewModels
{
    private class TestChildViewModel : ReactiveViewModel
    {
        public TestChildViewModel(int id, IObservable<string> value, IScheduler testScheduler) : base(testScheduler)
        {
            Id = id;
            Value = BindOneWay(value);
        }

        public int Id { get; }

        // ReSharper disable once MemberCanBePrivate.Local
        // ReSharper disable once UnusedAutoPropertyAccessor.Local
        public string Value { get; }
    }
    
    private class TestViewModel : ReactiveViewModel
    {
        public TestViewModel(IObservable<IImmutableList<int>> ids, Func<int, TestChildViewModel> createChildViewModelForId, IScheduler testScheduler) : base(testScheduler)
        {
            ViewModelsForIds = BindListAsViewModels(ids, createChildViewModelForId, (vm, i) => vm.Id == i);
        }
        
        public IReactiveList<TestChildViewModel> ViewModelsForIds { get; }
    }

    [Fact]
    public void ShouldBeEmptyAfterConstruction()
    {
        var ids = new BehaviorSubject<IImmutableList<int>>(ImmutableArray.Create(0, 1, 2));
        var values = new[]
        {
            new BehaviorSubject<string>("A"),
            new BehaviorSubject<string>("B"),
            new BehaviorSubject<string>("C"),
        };
        var testScheduler = new TestScheduler();

        using var vm = new TestViewModel(ids, id => new TestChildViewModel(id, values[id], testScheduler), testScheduler);

        testScheduler.Start();
        vm.ViewModelsForIds.Should().BeEmpty();
    }

    [Fact]
    public void ShouldHaveCount0AfterContstruction()
    {
	    var ids = new BehaviorSubject<IImmutableList<int>>(ImmutableArray.Create(0, 1, 2));
	    var values = new[]
	    {
		    new BehaviorSubject<string>("A"),
		    new BehaviorSubject<string>("B"),
		    new BehaviorSubject<string>("C"),
	    };
	    var testScheduler = new TestScheduler();

	    using var vm = new TestViewModel(ids, id => new TestChildViewModel(id, values[id], testScheduler), testScheduler);

	    testScheduler.Start();
	    vm.ViewModelsForIds.Count.Should().Be(0);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldSetInitialListWhenSubscribing(bool subscribeToCollection)
    {
        var ids = new BehaviorSubject<IImmutableList<int>>(ImmutableArray.Create(0, 1, 2));
        var values = new[]
        {
            new BehaviorSubject<string>("A"),
            new BehaviorSubject<string>("B"),
            new BehaviorSubject<string>("C"),
        };
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(ids, id => new TestChildViewModel(id, values[id], testScheduler), testScheduler);

        if (subscribeToCollection)
			vm.ViewModelsForIds.CollectionChanged += (_, _) => { };
        else
			vm.ViewModelsForIds.PropertyChanged += (_, _) => { };

        testScheduler.Start();
        vm.ViewModelsForIds.Should().HaveCount(3);
        vm.ViewModelsForIds.ElementAt(0).Id.Should().Be(0);
        vm.ViewModelsForIds.ElementAt(1).Id.Should().Be(1);
        vm.ViewModelsForIds.ElementAt(2).Id.Should().Be(2);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldHaveInitialCountWhenSubscribing(bool subscribeToCollection)
    {
	    var ids = new BehaviorSubject<IImmutableList<int>>(ImmutableArray.Create(0, 1, 2));
	    var values = new[]
	    {
		    new BehaviorSubject<string>("A"),
		    new BehaviorSubject<string>("B"),
		    new BehaviorSubject<string>("C"),
	    };
	    var testScheduler = new TestScheduler();
	    using var vm = new TestViewModel(ids, id => new TestChildViewModel(id, values[id], testScheduler), testScheduler);

	    if (subscribeToCollection)
		    vm.ViewModelsForIds.CollectionChanged += (_, _) => { };
	    else
		    vm.ViewModelsForIds.PropertyChanged += (_, _) => { };

	    testScheduler.Start();
	    vm.ViewModelsForIds.Count.Should().Be(3);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldRaiseCollectionChangedWhenSubscribing(bool subscribeToPropertyBefore)
    {
        var ids = new BehaviorSubject<IImmutableList<int>>(ImmutableArray.Create(0, 1, 2));
        var values = new[]
        {
            new BehaviorSubject<string>("A"),
            new BehaviorSubject<string>("B"),
            new BehaviorSubject<string>("C"),
        };
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(ids, id => new TestChildViewModel(id, values[id], testScheduler), testScheduler);

        var collectionChangedCalls = 0;

        if (subscribeToPropertyBefore)
	        vm.ViewModelsForIds.PropertyChanged += (_, _) => { };
        vm.ViewModelsForIds.CollectionChanged += (_, _) => collectionChangedCalls++; // emits once for each element

        testScheduler.Start();
        collectionChangedCalls.Should().Be(3);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldRaisePropertyChangedWhenSubscribing(bool subscribeToCollectionBefore)
    {
	    var ids = new BehaviorSubject<IImmutableList<int>>(ImmutableArray.Create(0, 1, 2));
	    var values = new[]
	    {
		    new BehaviorSubject<string>("A"),
		    new BehaviorSubject<string>("B"),
		    new BehaviorSubject<string>("C"),
	    };
	    var testScheduler = new TestScheduler();
	    using var vm = new TestViewModel(ids, id => new TestChildViewModel(id, values[id], testScheduler), testScheduler);

        var propertyChangedCalls = 0;

        if (subscribeToCollectionBefore)
	        vm.ViewModelsForIds.CollectionChanged += (_, _) => { };
        vm.ViewModelsForIds.PropertyChanged += (_, _) => propertyChangedCalls++; // emits twice

	    testScheduler.Start();
	    propertyChangedCalls.Should().Be(2);
    }
    
    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldRaiseCollectionChangedWhenEmittingSourceListWithAddedElement(bool subscribeToPropertyBefore)
    {
        var sourceList = ImmutableArray.Create(0, 1);
        var ids = new BehaviorSubject<IImmutableList<int>>(sourceList);
        var values = new[]
        {
            new BehaviorSubject<string>("A"),
            new BehaviorSubject<string>("B"),
            new BehaviorSubject<string>("C"),
        };
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(ids, id => new TestChildViewModel(id, values[id], testScheduler), testScheduler);

        var collectionChangedCallsWithAdd = 0;

        if (subscribeToPropertyBefore)
	        vm.ViewModelsForIds.PropertyChanged += (_, _) => { };
        vm.ViewModelsForIds.CollectionChanged += (_, e) =>  // emits twice (Add VM{ID = 0}, Add VM{ID = 1})
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
                collectionChangedCallsWithAdd++;
        };

        testScheduler.Start();
        ids.OnNext(sourceList.Add(2)); // emits once (Add VM{Id = 2})

        testScheduler.Start();
        collectionChangedCallsWithAdd.Should().Be(3);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldRaisePropertyChangedWhenEmittingSourceListWithAddedElement(bool subscribeToCollectionBefore)
    {
	    var sourceList = ImmutableArray.Create(0, 1);
	    var ids = new BehaviorSubject<IImmutableList<int>>(sourceList);
	    var values = new[]
	    {
		    new BehaviorSubject<string>("A"),
		    new BehaviorSubject<string>("B"),
		    new BehaviorSubject<string>("C"),
	    };
	    var testScheduler = new TestScheduler();
	    using var vm = new TestViewModel(ids, id => new TestChildViewModel(id, values[id], testScheduler), testScheduler);

	    var propertyChangedCalls = 0;

	    if (subscribeToCollectionBefore)
		    vm.ViewModelsForIds.CollectionChanged += (_, _) => { };
        vm.ViewModelsForIds.PropertyChanged += (_, _) => propertyChangedCalls++; // emits twice

	    testScheduler.Start();
	    ids.OnNext(sourceList.Add(2)); // emits once

	    testScheduler.Start();
	    propertyChangedCalls.Should().Be(3);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldRaiseCollectionChangedWhenEmittingSourceListWithRemovedElement(bool subscribeToPropertyBefore)
    {
        var sourceList = ImmutableArray.Create(0, 1, 2);
        var ids = new BehaviorSubject<IImmutableList<int>>(sourceList);
        var values = new[]
        {
            new BehaviorSubject<string>("A"),
            new BehaviorSubject<string>("B"),
            new BehaviorSubject<string>("C"),
        };
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(ids, id => new TestChildViewModel(id, values[id], testScheduler), testScheduler);

        var collectionChangedCallsWithRemove = 0;

        if (subscribeToPropertyBefore)
	        vm.ViewModelsForIds.PropertyChanged += (_, _) => { };
        vm.ViewModelsForIds.CollectionChanged += (_, e) =>  // emits three times (Add VM{ID = 0}, Add VM{ID = 1}, Add VM{Id = 2})
        {
            if (e.Action == NotifyCollectionChangedAction.Remove)
                collectionChangedCallsWithRemove++;
        };

        testScheduler.Start();
        ids.OnNext(sourceList.Remove(1)); // emits once (Remove VM{Id = 1})

        testScheduler.Start();
        collectionChangedCallsWithRemove.Should().Be(1);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldRaisePropertyChangedWhenEmittingSourceListWithRemovedElement(bool subscribeToCollectionBefore)
    {
	    var sourceList = ImmutableArray.Create(0, 1, 2);
	    var ids = new BehaviorSubject<IImmutableList<int>>(sourceList);
	    var values = new[]
	    {
		    new BehaviorSubject<string>("A"),
		    new BehaviorSubject<string>("B"),
		    new BehaviorSubject<string>("C"),
	    };
	    var testScheduler = new TestScheduler();
	    using var vm = new TestViewModel(ids, id => new TestChildViewModel(id, values[id], testScheduler), testScheduler);

	    var propertyChangedCalls = 0;

	    if (subscribeToCollectionBefore)
		    vm.ViewModelsForIds.CollectionChanged += (_, _) => { };
	    vm.ViewModelsForIds.PropertyChanged += (_, e) => propertyChangedCalls++; // emits twice

	    testScheduler.Start();
	    ids.OnNext(sourceList.Remove(1)); // emits once

	    testScheduler.Start();
	    propertyChangedCalls.Should().Be(3);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldNotRaiseCollectionChangedWhenEmittingEqualSourceList(bool subscribeToPropertyBefore)
    {
        var sourceList = ImmutableArray.Create(0, 1, 2);
        var ids = new BehaviorSubject<IImmutableList<int>>(sourceList);
        var values = new[]
        {
            new BehaviorSubject<string>("A"),
            new BehaviorSubject<string>("B"),
            new BehaviorSubject<string>("C"),
        };
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(ids, id => new TestChildViewModel(id, values[id], testScheduler), testScheduler);

        var collectionChangedCalls = 0;

        if (subscribeToPropertyBefore)
	        vm.ViewModelsForIds.PropertyChanged += (_, _) => { };
        vm.ViewModelsForIds.CollectionChanged += (_, _) => collectionChangedCalls++; // emits three times (Add VM{ID = 0}, Add VM{ID = 1}, Add VM{Id = 2})
        
        testScheduler.Start();
        ids.OnNext(ImmutableArray.Create(0, 1, 2)); // does not emit

        testScheduler.Start();
        collectionChangedCalls.Should().Be(3);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldNotRaisePropertyChangedWhenEmittingEqualSourceList(bool subscribeToCollectionBefore)
    {
	    var sourceList = ImmutableArray.Create(0, 1, 2);
	    var ids = new BehaviorSubject<IImmutableList<int>>(sourceList);
	    var values = new[]
	    {
		    new BehaviorSubject<string>("A"),
		    new BehaviorSubject<string>("B"),
		    new BehaviorSubject<string>("C"),
	    };
	    var testScheduler = new TestScheduler();
	    using var vm = new TestViewModel(ids, id => new TestChildViewModel(id, values[id], testScheduler), testScheduler);

	    var propertyChangedCalls = 0;

	    if (subscribeToCollectionBefore)
		    vm.ViewModelsForIds.CollectionChanged += (_, _) => { };
	    vm.ViewModelsForIds.PropertyChanged += (_, _) => propertyChangedCalls++; // emits twice

	    testScheduler.Start();
	    ids.OnNext(ImmutableArray.Create(0, 1, 2)); // does not emit

	    testScheduler.Start();
	    propertyChangedCalls.Should().Be(2);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldRaiseCollectionChangedWhenResubscribing(bool subscribeToPropertyBefore)
    {
        var sourceList = ImmutableArray.Create(0, 1, 2);
        var ids = new BehaviorSubject<IImmutableList<int>>(sourceList);
        var values = new[]
        {
            new BehaviorSubject<string>("A"),
            new BehaviorSubject<string>("B"),
            new BehaviorSubject<string>("C"),
        };
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(ids, id => new TestChildViewModel(id, values[id], testScheduler), testScheduler);

        var collectionChangedCalls = 0;
        void OnPropertyChanged(object? _, PropertyChangedEventArgs e) { }
        void OnCollectionChanged(object? _, NotifyCollectionChangedEventArgs e) => collectionChangedCalls++;

        if (subscribeToPropertyBefore)
	        vm.ViewModelsForIds.PropertyChanged += OnPropertyChanged;
        vm.ViewModelsForIds.CollectionChanged += OnCollectionChanged; // emits three times (Add VM{ID = 0}, Add VM{ID = 1}, Add VM{Id = 2})
        testScheduler.Start();
        vm.ViewModelsForIds.CollectionChanged -= OnCollectionChanged;
        if (subscribeToPropertyBefore)
	        vm.ViewModelsForIds.PropertyChanged -= OnPropertyChanged;

        if (subscribeToPropertyBefore)
	        vm.ViewModelsForIds.PropertyChanged += OnPropertyChanged;
        vm.ViewModelsForIds.CollectionChanged += OnCollectionChanged; // emits three times (Add VM{ID = 0}, Add VM{ID = 1}, Add VM{Id = 2})

        testScheduler.Start();
        collectionChangedCalls.Should().Be(6);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldRaisePropertyChangedWhenResubscribing(bool subscribeToCollectionBefore)
    {
	    var sourceList = ImmutableArray.Create(0, 1, 2);
	    var ids = new BehaviorSubject<IImmutableList<int>>(sourceList);
	    var values = new[]
	    {
		    new BehaviorSubject<string>("A"),
		    new BehaviorSubject<string>("B"),
		    new BehaviorSubject<string>("C"),
	    };
	    var testScheduler = new TestScheduler();
	    using var vm = new TestViewModel(ids, id => new TestChildViewModel(id, values[id], testScheduler), testScheduler);

	    var propertyChangedCalls = 0;
	    void OnPropertyChanged(object? _, PropertyChangedEventArgs e) => propertyChangedCalls++;
        void OnCollectionChanged(object? _, NotifyCollectionChangedEventArgs e) { }

	    if (subscribeToCollectionBefore)
		    vm.ViewModelsForIds.CollectionChanged += OnCollectionChanged;
	    vm.ViewModelsForIds.PropertyChanged += OnPropertyChanged; // emits twice
	    testScheduler.Start();
	    vm.ViewModelsForIds.PropertyChanged -= OnPropertyChanged;
	    if (subscribeToCollectionBefore)
		    vm.ViewModelsForIds.CollectionChanged -= OnCollectionChanged;

	    if (subscribeToCollectionBefore)
		    vm.ViewModelsForIds.CollectionChanged += OnCollectionChanged;
	    vm.ViewModelsForIds.PropertyChanged += OnPropertyChanged; // emits twice

	    testScheduler.Start();
	    propertyChangedCalls.Should().Be(4);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldRaiseCollectionChangedWhenResubscribingToChangedStream(bool subscribeToPropertyBefore)
    {
        var sourceList = ImmutableArray.Create(0, 1);
        var ids = new BehaviorSubject<IImmutableList<int>>(sourceList);
        var values = new[]
        {
            new BehaviorSubject<string>("A"),
            new BehaviorSubject<string>("B"),
            new BehaviorSubject<string>("C"),
        };
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(ids, id => new TestChildViewModel(id, values[id], testScheduler), testScheduler);

        var collectionChangedCalls = 0;
        void OnPropertyChanged(object? _, PropertyChangedEventArgs e) { }
        void OnCollectionChanged(object? _, NotifyCollectionChangedEventArgs e) => collectionChangedCalls++;

        if (subscribeToPropertyBefore)
	        vm.ViewModelsForIds.PropertyChanged += OnPropertyChanged;
        vm.ViewModelsForIds.CollectionChanged += OnCollectionChanged; // emits twice (Add VM{ID = 0}, Add VM{ID = 1})
        testScheduler.Start();
        vm.ViewModelsForIds.CollectionChanged -= OnCollectionChanged;
        if (subscribeToPropertyBefore)
	        vm.ViewModelsForIds.PropertyChanged -= OnPropertyChanged;

        ids.OnNext(sourceList.SetItem(1, 2)); // currently inactive, does not emit

        if (subscribeToPropertyBefore)
	        vm.ViewModelsForIds.PropertyChanged += OnPropertyChanged;
        vm.ViewModelsForIds.CollectionChanged += OnCollectionChanged; // emits twice (Add VM{ID = 0}, Add VM{Id = 2})

        testScheduler.Start();
        collectionChangedCalls.Should().Be(4);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldRaisePropertyChangedWhenResubscribingToChangedStream(bool subscribeToCollectionBefore)
    {
	    var sourceList = ImmutableArray.Create(0, 1);
	    var ids = new BehaviorSubject<IImmutableList<int>>(sourceList);
	    var values = new[]
	    {
		    new BehaviorSubject<string>("A"),
		    new BehaviorSubject<string>("B"),
		    new BehaviorSubject<string>("C"),
	    };
	    var testScheduler = new TestScheduler();
	    using var vm = new TestViewModel(ids, id => new TestChildViewModel(id, values[id], testScheduler), testScheduler);

	    var propertyChangedCalls = 0;
	    void OnPropertyChanged(object? _, PropertyChangedEventArgs e) => propertyChangedCalls++;
        void OnCollectionChanged(object? _, NotifyCollectionChangedEventArgs e) { }

	    if (subscribeToCollectionBefore)
		    vm.ViewModelsForIds.CollectionChanged += OnCollectionChanged;
	    vm.ViewModelsForIds.PropertyChanged += OnPropertyChanged; // emits twice
	    testScheduler.Start();
	    vm.ViewModelsForIds.PropertyChanged -= OnPropertyChanged;
	    if (subscribeToCollectionBefore)
		    vm.ViewModelsForIds.CollectionChanged -= OnCollectionChanged;

	    ids.OnNext(sourceList.SetItem(1, 2)); // currently inactive, does not emit

	    if (subscribeToCollectionBefore)
		    vm.ViewModelsForIds.CollectionChanged += OnCollectionChanged;
	    vm.ViewModelsForIds.PropertyChanged += OnPropertyChanged; // emits twice

	    testScheduler.Start();
	    propertyChangedCalls.Should().Be(4);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldRaiseCollectionChangedWhenResubscribingThenChangingStream(bool subscribeToPropertyBefore)
    {
        var sourceList = ImmutableArray.Create(0, 1);
        var ids = new BehaviorSubject<IImmutableList<int>>(sourceList);
        var values = new[]
        {
            new BehaviorSubject<string>("A"),
            new BehaviorSubject<string>("B"),
            new BehaviorSubject<string>("C"),
        };
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(ids, id => new TestChildViewModel(id, values[id], testScheduler), testScheduler);

        var collectionChangedCalls = 0;
        void OnPropertyChanged(object? _, PropertyChangedEventArgs e) { }
        void OnCollectionChanged(object? _, NotifyCollectionChangedEventArgs e) => collectionChangedCalls++;

        if (subscribeToPropertyBefore)
	        vm.ViewModelsForIds.PropertyChanged += OnPropertyChanged;
        vm.ViewModelsForIds.CollectionChanged += OnCollectionChanged; // emits twice (Add VM{ID = 0}, Add VM{ID = 1})
        testScheduler.Start();
        vm.ViewModelsForIds.CollectionChanged -= OnCollectionChanged;
        if (subscribeToPropertyBefore)
	        vm.ViewModelsForIds.PropertyChanged -= OnPropertyChanged;

        if (subscribeToPropertyBefore)
	        vm.ViewModelsForIds.PropertyChanged += OnPropertyChanged;
        vm.ViewModelsForIds.CollectionChanged += OnCollectionChanged; // emits twice (Add VM{ID = 0}, Add VM{Id = 1})
        
        testScheduler.Start();
        ids.OnNext(sourceList.Add(2)); // emits

        testScheduler.Start();
        collectionChangedCalls.Should().Be(5);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldRaisePropertyChangedWhenResubscribingThenChangingStream(bool subscribeToCollectionBefore)
    {
	    var sourceList = ImmutableArray.Create(0, 1);
	    var ids = new BehaviorSubject<IImmutableList<int>>(sourceList);
	    var values = new[]
	    {
		    new BehaviorSubject<string>("A"),
		    new BehaviorSubject<string>("B"),
		    new BehaviorSubject<string>("C"),
	    };
	    var testScheduler = new TestScheduler();
	    using var vm = new TestViewModel(ids, id => new TestChildViewModel(id, values[id], testScheduler), testScheduler);

	    var propertyChangedCalls = 0;
	    void OnPropertyChanged(object? _, PropertyChangedEventArgs e) => propertyChangedCalls++;
        void OnCollectionChanged(object? _, NotifyCollectionChangedEventArgs e) { }

	    if (subscribeToCollectionBefore)
		    vm.ViewModelsForIds.CollectionChanged += OnCollectionChanged;
	    vm.ViewModelsForIds.PropertyChanged += OnPropertyChanged; // emits twice
	    testScheduler.Start();
	    vm.ViewModelsForIds.PropertyChanged -= OnPropertyChanged;
	    if (subscribeToCollectionBefore)
		    vm.ViewModelsForIds.CollectionChanged -= OnCollectionChanged;

	    if (subscribeToCollectionBefore)
		    vm.ViewModelsForIds.CollectionChanged += OnCollectionChanged;
	    vm.ViewModelsForIds.PropertyChanged += OnPropertyChanged; // emits twice

	    testScheduler.Start();
	    ids.OnNext(sourceList.Add(2)); // emits once

	    testScheduler.Start();
	    propertyChangedCalls.Should().Be(5);
    }

    [Theory]
    [InlineData(false, true)]
    [InlineData(true, false)]
    [InlineData(true, true)]
    public void ShouldUnsubscribeFromValueListWhenViewModelIsDisposed(bool subscribeToCollection, bool subscribeToProperty)
    {
        var sourceList = ImmutableArray.Create(0, 1);
        var ids = new BehaviorSubject<IImmutableList<int>>(sourceList);
        var values = new[]
        {
            new BehaviorSubject<string>("A"),
            new BehaviorSubject<string>("B"),
            new BehaviorSubject<string>("C"),
        };
        var testScheduler = new TestScheduler();
        var vm = new TestViewModel(ids, id => new TestChildViewModel(id, values[id], testScheduler), testScheduler);

        if (subscribeToCollection)
	        vm.ViewModelsForIds.CollectionChanged += (_, _) => { };
        if (subscribeToProperty)
	        vm.ViewModelsForIds.PropertyChanged += (_, _) => { };
        testScheduler.Start();

        ids.HasObservers.Should().BeTrue();
        vm.Dispose();
        ids.HasObservers.Should().BeFalse();
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldUnsubscribeFromChildValuesWhenViewModelIsDisposed(bool subscribeToProperty)
    {
        var sourceList = ImmutableArray.Create(0, 1);
        var ids = new BehaviorSubject<IImmutableList<int>>(sourceList);
        var values = new[]
        {
            new BehaviorSubject<string>("A"),
            new BehaviorSubject<string>("B"),
        };
        var testScheduler = new TestScheduler();
        var vm = new TestViewModel(ids, id => new TestChildViewModel(id, values[id], testScheduler), testScheduler);

        vm.ViewModelsForIds.CollectionChanged += (_, _) => { };
        testScheduler.Start();
        vm.ViewModelsForIds.Should().HaveCount(2);
        vm.ViewModelsForIds.ElementAt(0).PropertyChanged += (_, _) => { };
        vm.ViewModelsForIds.ElementAt(1).PropertyChanged += (_, _) => { };

        if (subscribeToProperty)
	        vm.ViewModelsForIds.PropertyChanged += (_, _) => { };

        values[0].HasObservers.Should().BeTrue();
        values[1].HasObservers.Should().BeTrue();
        vm.Dispose();
        values[0].HasObservers.Should().BeFalse();
        values[1].HasObservers.Should().BeFalse();
    }
}
