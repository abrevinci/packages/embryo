﻿using System.Reactive.Linq;

namespace AbreVinci.Embryo.ReactiveUI.Tests.ReactiveViewModelTests;

public class BindOneWay
{
    private class TestViewModel : ReactiveViewModel
    {
        public TestViewModel(IObservable<int> a, IScheduler testScheduler) : base(testScheduler)
        {
            A = BindOneWay(a);
        }

        public int A { get; }
    }

    private class InheritedViewModel : TestViewModel
    {
        private static readonly Dictionary<int, string> _lookup = new()
        {
            [0] = "Zero",
            [1] = "One",
            [2] = "Two"
        };

        public InheritedViewModel(IObservable<int> a, IObservable<int> b, IScheduler testScheduler) : base(a, testScheduler)
        {
            B = BindOneWay(b.Select(v => _lookup.TryGetValue(v, out var s) ? s : string.Empty));
        }

        public string B { get; }
    }

    [Fact]
    public void ShouldNotSetValueAfterConstruction()
    {
        var a = new BehaviorSubject<int>(11);
        var testScheduler = new TestScheduler();

        using var vm = new TestViewModel(a, testScheduler);

        testScheduler.Start();
        vm.A.Should().Be(default);
    }

    [Fact]
    public void ShouldSetInitialValueWhenSubscribing()
    {
        var a = new BehaviorSubject<int>(11);
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(a, testScheduler);

        vm.PropertyChanged += (_, _) => { };

        testScheduler.Start();
        vm.A.Should().Be(11);
    }

    [Fact]
    public void ShouldRaisePropertyChangedWhenSubscribing()
    {
        var a = new BehaviorSubject<int>(11);
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(a, testScheduler);

        var propertyChangeCallProperties = new List<string>();
        vm.PropertyChanged += (_, e) => propertyChangeCallProperties.Add(e.PropertyName!); // emits once

        testScheduler.Start();
        propertyChangeCallProperties.Should().BeEquivalentTo("A");
    }

    [Fact]
    public void ShouldRaisePropertyChangedWhenEmittingDifferentSourceValues()
    {
        var a = new BehaviorSubject<int>(11);
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(a, testScheduler);

        var propertyChangeCallProperties = new List<string>();
        vm.PropertyChanged += (_, e) => propertyChangeCallProperties.Add(e.PropertyName!); // emits once
        a.OnNext(23); // emits

        testScheduler.Start();
        propertyChangeCallProperties.Should().BeEquivalentTo("A", "A");
    }

    [Fact]
    public void ShouldNotRaisePropertyChangedWhenEmittingEqualSourceValues()
    {
        var a = new BehaviorSubject<int>(11);
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(a, testScheduler);

        var propertyChangeCallProperties = new List<string>();
        vm.PropertyChanged += (_, e) => propertyChangeCallProperties.Add(e.PropertyName!); // emits once
        a.OnNext(11); // same as previous, does not emit

        testScheduler.Start();
        propertyChangeCallProperties.Should().BeEquivalentTo("A");
    }

    [Fact]
    public void ShouldRaisePropertyChangedWhenResubscribing()
    {
        var a = new BehaviorSubject<int>(11);
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(a, testScheduler);

        var propertyChangeCallProperties = new List<string>();
        void OnPropertyChanged(object? _, PropertyChangedEventArgs e) => propertyChangeCallProperties.Add(e.PropertyName!);

        vm.PropertyChanged += OnPropertyChanged; // emits once
        testScheduler.Start();
        vm.PropertyChanged -= OnPropertyChanged;

        vm.PropertyChanged += OnPropertyChanged; // emits once
        testScheduler.Start();

        propertyChangeCallProperties.Should().BeEquivalentTo("A", "A");
    }

    [Fact]
    public void ShouldRaisePropertyChangedWhenResubscribingToChangedStream()
    {
        var a = new BehaviorSubject<int>(11);
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(a, testScheduler);

        var propertyChangeCallProperties = new List<string>();
        void OnPropertyChanged(object? _, PropertyChangedEventArgs e) => propertyChangeCallProperties.Add(e.PropertyName!);

        vm.PropertyChanged += OnPropertyChanged; // emits once
        testScheduler.Start();

        vm.PropertyChanged -= OnPropertyChanged;

        a.OnNext(32); // currently inactive, does not emit

        vm.PropertyChanged += OnPropertyChanged; // emits once
        testScheduler.Start();

        propertyChangeCallProperties.Should().BeEquivalentTo("A", "A");
    }

    [Fact]
    public void ShouldRaisePropertyChangedWhenResubscribingThenChangingStream()
    {
        var a = new BehaviorSubject<int>(11);
        var testScheduler = new TestScheduler();
        using var vm = new TestViewModel(a, testScheduler);

        var propertyChangeCallProperties = new List<string>();
        void OnPropertyChanged(object? _, PropertyChangedEventArgs e) => propertyChangeCallProperties.Add(e.PropertyName!);

        vm.PropertyChanged += OnPropertyChanged; // emits once
        testScheduler.Start();

        vm.PropertyChanged -= OnPropertyChanged;

        vm.PropertyChanged += OnPropertyChanged; // emits once

        a.OnNext(32); // emits

        testScheduler.Start();

        propertyChangeCallProperties.Should().BeEquivalentTo("A", "A", "A");
    }

    [Fact]
    public void ShouldUnsubscribeFromValueWhenViewModelIsDisposed()
    {
        var a = new BehaviorSubject<int>(11);
        var testScheduler = new TestScheduler();
        var vm = new TestViewModel(a, testScheduler);

        vm.PropertyChanged += (_, _) => { };
        testScheduler.Start();

        a.HasObservers.Should().BeTrue();
        vm.Dispose();
        a.HasObservers.Should().BeFalse();
    }

    [Fact]
    public void ShouldSetInitialValuesWhenSubscribingForInheritedViewModel()
    {
        var a = new BehaviorSubject<int>(11);
        var b = new BehaviorSubject<int>(2);
        var testScheduler = new TestScheduler();
        using var vm = new InheritedViewModel(a, b, testScheduler);

        vm.PropertyChanged += (_, _) => { };

        testScheduler.Start();
        vm.A.Should().Be(11);
        vm.B.Should().Be("Two");
    }
}
