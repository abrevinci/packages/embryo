﻿using AbreVinci.Embryo.ReactiveUI.Testing;

namespace AbreVinci.Embryo.ReactiveUI.Tests.Testing.ReactiveTestWindowFactoryTests;

public class CreateWindow
{
	private class TestViewModel : ReactiveViewModel
	{
		public TestViewModel(IObservable<int> a)
		{
			A = BindOneWay(a);
		}

		public int A { get; }
	}

	[Fact]
	public void ShouldCreateAWindowWithAHostedViewModelAsContent()
	{
		var a = new BehaviorSubject<int>(31);
		var viewModelHost = new ReactiveViewModelHost(Scheduler.Immediate, true);
		var testWindowFactory = new ReactiveTestWindowFactory();

		var window = testWindowFactory.CreateWindow("Name", () => viewModelHost.HostViewModel(() => new TestViewModel(a)));

		window.Content.Should().BeOfType<TestViewModel>().Which.A.Should().Be(31);
	}

	[Fact]
	public void ShouldCreateAWindowWithAHostedViewModelThatIsUnsubscribedOnClose()
	{
		var a = new BehaviorSubject<int>(31);
		var viewModelHost = new ReactiveViewModelHost(Scheduler.Immediate, true);
		var testWindowFactory = new ReactiveTestWindowFactory();

		var window = testWindowFactory.CreateWindow("Name", () => viewModelHost.HostViewModel(() => new TestViewModel(a)));

		var viewModel = window.Content.Should().BeOfType<TestViewModel>().Subject;
		window.Close(); // should unsubscribe the view model from a.
		a.OnNext(12); // this should not have any effect.
		viewModel.A.Should().Be(31);
	}

	[Fact]
	public void ShouldCreateAWindowWithoutCallingCallbackWhenNothingIsCalledOnCreatedWindow()
	{
		string? shownWindow = null;
		string? hiddenWindow = null;
		string? closedWindow = null;
		var a = new BehaviorSubject<int>(31);
		var viewModelHost = new ReactiveViewModelHost(Scheduler.Immediate, true);
		var testWindowFactory = new ReactiveTestWindowFactory(w => shownWindow = w, w => hiddenWindow = w, w => closedWindow = w);

		testWindowFactory.CreateWindow("Name", () => viewModelHost.HostViewModel(() => new TestViewModel(a)));

		shownWindow.Should().BeNull();
		hiddenWindow.Should().BeNull();
		closedWindow.Should().BeNull();
	}

	[Fact]
	public void ShouldCreateAWindowYieldingShownCallbacksWhenCallbackIsGivenAndWindowIsShown()
	{
		string? shownWindow = null;
		var a = new BehaviorSubject<int>(31);
		var viewModelHost = new ReactiveViewModelHost(Scheduler.Immediate, true);
		var testWindowFactory = new ReactiveTestWindowFactory(onShow: w => shownWindow = w);

		var window = testWindowFactory.CreateWindow("Name", () => viewModelHost.HostViewModel(() => new TestViewModel(a)));
		window.Show();

		shownWindow.Should().Be("Name");
	}

	[Fact]
	public void ShouldCreateAWindowYieldingHiddenCallbacksWhenCallbackIsGivenAndWindowIsHidden()
	{
		string? hiddenWindow = null;
		var a = new BehaviorSubject<int>(31);
		var viewModelHost = new ReactiveViewModelHost(Scheduler.Immediate, true);
		var testWindowFactory = new ReactiveTestWindowFactory(onHide: w => hiddenWindow = w);

		var window = testWindowFactory.CreateWindow("Name", () => viewModelHost.HostViewModel(() => new TestViewModel(a)));
		window.Hide();

		hiddenWindow.Should().Be("Name");
	}

	[Fact]
	public void ShouldCreateAWindowYieldingClosedCallbacksWhenCallbackIsGivenAndWindowIsClosed()
	{
		string? closedWindow = null;
		var a = new BehaviorSubject<int>(31);
		var viewModelHost = new ReactiveViewModelHost(Scheduler.Immediate, true);
		var testWindowFactory = new ReactiveTestWindowFactory(onClose: w => closedWindow = w);

		var window = testWindowFactory.CreateWindow("Name", () => viewModelHost.HostViewModel(() => new TestViewModel(a)));
		window.Close();

		closedWindow.Should().Be("Name");
	}
}
