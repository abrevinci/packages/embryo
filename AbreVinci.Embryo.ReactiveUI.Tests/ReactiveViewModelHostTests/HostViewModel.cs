﻿using System.Linq;

namespace AbreVinci.Embryo.ReactiveUI.Tests.ReactiveViewModelHostTests;

public class HostViewModel
{
    private class TestChildViewModel : ReactiveViewModel
    {
        public TestChildViewModel(string name, IObservable<int> a, IObservable<string> b)
        {
            Name = name;
            A = BindTwoWay(a, _ => { });
            B = BindOneWay(b);
        }

        public string Name { get; }

        // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Local
        public int A { get; set; }
        public string B { get; }
    }

    private class TestItemViewModel : ReactiveViewModel
    {
        public TestItemViewModel(int id, IObservable<IImmutableList<int>> c)
        {
            Id = id;
            C = BindList(c);
        }

        // ReSharper disable once MemberCanBePrivate.Local
        // ReSharper disable once UnusedAutoPropertyAccessor.Local
        public int Id { get; }

        public IEnumerable<int> C { get; }
    }

    private class TestViewModel : ReactiveViewModel
    {
        public TestViewModel(IObservable<string> name, IObservable<IImmutableList<int>> itemStartValues, Func<string, TestChildViewModel> createChild, Func<int, TestItemViewModel> createItem)
        {
            NamedChild = BindOneWayAsViewModel(name, createChild, (vm, n) => vm.Name == n);
            Child = AddChildViewModel(() => createChild(string.Empty));
            Items = BindListAsViewModels(itemStartValues, createItem, (vm, v) => vm.C.First() == v);
        }

        public TestChildViewModel NamedChild { get; }
        public TestChildViewModel Child { get; }
        public IEnumerable<TestItemViewModel> Items { get; }
    }

    [Fact]
    public void NonHostedViewModelShouldThrowException()
    {
        var name = new BehaviorSubject<string>("name");
        var itemStartValues = new BehaviorSubject<IImmutableList<int>>(ImmutableArray.Create(0, 5, 10));
        var a = new BehaviorSubject<int>(11);
        var b = new BehaviorSubject<string>("hello");
        TestChildViewModel CreateChild(string n) => new(n, a, b);
        TestItemViewModel CreateItem(int id) => new(id, new BehaviorSubject<IImmutableList<int>>(ImmutableArray.Create(id, id + 1, id + 2)));
        Action action = () => new TestViewModel(name, itemStartValues, CreateChild, CreateItem);

        action.Should().Throw<InvalidOperationException>();
    }

    [Fact]
    public void HostedViewModelShouldContainInitialValuesFromSubjectsUponHostCreation()
    {
        var name = new BehaviorSubject<string>("name");
        var itemStartValues = new BehaviorSubject<IImmutableList<int>>(ImmutableArray.Create(0, 5, 10));
        var a = new BehaviorSubject<int>(11);
        var b = new BehaviorSubject<string>("hello");
        TestChildViewModel CreateChild(string n) => new(n, a, b);
        TestItemViewModel CreateItem(int id) => new(id, new BehaviorSubject<IImmutableList<int>>(ImmutableArray.Create(id, id + 1, id + 2)));

        var viewModelHost = new ReactiveViewModelHost(Scheduler.Immediate, true);
        using var testViewModel = viewModelHost.HostViewModel(() => new TestViewModel(name, itemStartValues, CreateChild, CreateItem));

        testViewModel.Should().NotBeNull();
        testViewModel.NamedChild.Should().NotBeNull();
        testViewModel.NamedChild.A.Should().Be(11);
        testViewModel.NamedChild.B.Should().Be("hello");
        testViewModel.NamedChild.Name.Should().Be("name");
        testViewModel.Child.Should().NotBeNull();
        testViewModel.Child.A.Should().Be(11);
        testViewModel.Child.B.Should().Be("hello");
        testViewModel.Child.Name.Should().Be("");
        testViewModel.Items.Should().HaveCount(3);
        testViewModel.Items.ElementAt(0).Should().NotBeNull();
        testViewModel.Items.ElementAt(0).C.Should().BeEquivalentTo(new[] { 0, 1, 2 });
        testViewModel.Items.ElementAt(1).Should().NotBeNull();
        testViewModel.Items.ElementAt(1).C.Should().BeEquivalentTo(new[] { 5, 6, 7 });
        testViewModel.Items.ElementAt(2).Should().NotBeNull();
        testViewModel.Items.ElementAt(2).C.Should().BeEquivalentTo(new[] { 10, 11, 12 });
    }

    [Fact]
    public void HostedViewModelShouldInitViewModelsCreatedAfterInitialConstruction()
    {
        var name = new BehaviorSubject<string>("name");
        var itemStartValues = new BehaviorSubject<IImmutableList<int>>(ImmutableArray.Create(0, 5));
        var a = new BehaviorSubject<int>(11);
        var b = new BehaviorSubject<string>("hello");
        TestChildViewModel CreateChild(string n) => new(n, a, b);
        TestItemViewModel CreateItem(int id) => new(id, new BehaviorSubject<IImmutableList<int>>(ImmutableArray.Create(id, id + 1, id + 2)));

        var viewModelHost = new ReactiveViewModelHost(Scheduler.Immediate, true);
        using var testViewModel = viewModelHost.HostViewModel(() => new TestViewModel(name, itemStartValues, CreateChild, CreateItem));

        itemStartValues.OnNext(itemStartValues.Value.Add(10));

        testViewModel.Should().NotBeNull();
        testViewModel.NamedChild.Should().NotBeNull();
        testViewModel.NamedChild.A.Should().Be(11);
        testViewModel.NamedChild.B.Should().Be("hello");
        testViewModel.NamedChild.Name.Should().Be("name");
        testViewModel.Child.Should().NotBeNull();
        testViewModel.Child.A.Should().Be(11);
        testViewModel.Child.B.Should().Be("hello");
        testViewModel.Child.Name.Should().Be("");
        testViewModel.Items.Should().HaveCount(3);
        testViewModel.Items.ElementAt(0).Should().NotBeNull();
        testViewModel.Items.ElementAt(0).C.Should().BeEquivalentTo(new[] { 0, 1, 2 });
        testViewModel.Items.ElementAt(1).Should().NotBeNull();
        testViewModel.Items.ElementAt(1).C.Should().BeEquivalentTo(new[] { 5, 6, 7 });
        testViewModel.Items.ElementAt(2).Should().NotBeNull();
        testViewModel.Items.ElementAt(2).C.Should().BeEquivalentTo(new[] { 10, 11, 12 });
    }
}
