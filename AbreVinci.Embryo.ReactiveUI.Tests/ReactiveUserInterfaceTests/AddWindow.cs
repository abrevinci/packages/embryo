using AbreVinci.Embryo.ReactiveUI.Testing;

namespace AbreVinci.Embryo.ReactiveUI.Tests.ReactiveUserInterfaceTests;

public class AddWindow
{
	private class TestViewModel : ReactiveViewModel
	{
	}

	public class SimplifiedStateOverride
	{
		private class TestUserInterface : ReactiveUserInterface
		{
			public TestUserInterface(
				IObservable<bool> isOpen,
				Action onClosed,
				Action? onShow = null,
				Action? onHide = null,
				Action? onClose = null) 
				: base(new ReactiveViewModelHost(Scheduler.Immediate, true), new ReactiveTestWindowFactory(_ => onShow?.Invoke(), _ => onHide?.Invoke(), _ => onClose?.Invoke()))
			{
				TestWindow = AddWindow(isOpen, "Name", () => new TestViewModel(), onClosed);
			}

			public IReactiveWindowController<TestViewModel> TestWindow { get; }
		}

		[Theory]
		[InlineData(new[] { true })]
		[InlineData(new[] { false, true, false, true })]
		public void ShouldCreateAWindowControllerThatHasAWindowWhenShown(bool[] emittedIsOpenValues)
		{
			var isOpen = new Subject<bool>();
			using var userInterface = new TestUserInterface(isOpen, () => { });

			foreach (var isOpenValue in emittedIsOpenValues)
			{
				isOpen.OnNext(isOpenValue);
			}

			userInterface.TestWindow.Window.Should().NotBeNull();
		}

		[Theory]
		[InlineData(new[] { true })]
		[InlineData(new[] { false, true, false, true })]
		public void ShouldCreateAWindowControllerThatHasAViewModelWhenShown(bool[] emittedIsOpenValues)
		{
			var isOpen = new Subject<bool>();
			using var userInterface = new TestUserInterface(isOpen, () => { });

			foreach (var isOpenValue in emittedIsOpenValues)
			{
				isOpen.OnNext(isOpenValue);
			}

			userInterface.TestWindow.ViewModel.Should().NotBeNull();
			userInterface.TestWindow.ViewModel.Should().BeOfType<TestViewModel>();
		}

		[Theory]
		[InlineData(new bool[] { })]
		[InlineData(new[] { false })]
		[InlineData(new[] { false, true, false })]
		public void ShouldCreateAWindowControllerThatHasNoWindowWhenClosed(bool[] emittedIsOpenValues)
		{
			var isOpen = new Subject<bool>();
			using var userInterface = new TestUserInterface(isOpen, () => { });

			foreach (var isOpenValue in emittedIsOpenValues)
			{
				isOpen.OnNext(isOpenValue);
			}

			userInterface.TestWindow.Window.Should().BeNull();
		}

		[Theory]
		[InlineData(new bool[]{})]
		[InlineData(new [] { false })]
		[InlineData(new [] { false, true, false })]
		public void ShouldCreateAWindowControllerThatHasNoViewModelWhenClosed(bool[] emittedIsOpenValues)
		{
			var isOpen = new Subject<bool>();
			using var userInterface = new TestUserInterface(isOpen, () => { });

			foreach (var isOpenValue in emittedIsOpenValues)
			{
				isOpen.OnNext(isOpenValue);
			}

			userInterface.TestWindow.ViewModel.Should().BeNull();
		}

		[Theory]
		[InlineData(new[] { true })]
		[InlineData(new[] { false, true, false, true })]
		public void ShouldCreateAWindowControllerThatHasAVisibleStateWhenShown(bool[] emittedIsOpenValues)
		{
			var isOpen = new Subject<bool>();
			using var userInterface = new TestUserInterface(isOpen, () => { });

			foreach (var isOpenValue in emittedIsOpenValues)
			{
				isOpen.OnNext(isOpenValue);
			}

			userInterface.TestWindow.State.Should().Be(ReactiveWindowControllerState.WindowVisible);
		}

		[Theory]
		[InlineData(new bool[] { })]
		[InlineData(new[] { false })]
		[InlineData(new[] { false, true, false })]
		public void ShouldCreateAWindowControllerThatHasAClosedStateWhenClosed(bool[] emittedIsOpenValues)
		{
			var isOpen = new Subject<bool>();
			using var userInterface = new TestUserInterface(isOpen, () => { });

			foreach (var isOpenValue in emittedIsOpenValues)
			{
				isOpen.OnNext(isOpenValue);
			}

			userInterface.TestWindow.State.Should().Be(ReactiveWindowControllerState.WindowClosed);
		}

		[Theory]
		[InlineData(new bool[] { }, 0, 0)]
		[InlineData(new[] { false }, 0, 0)]
		[InlineData(new[] { true }, 1, 0)]
		[InlineData(new[] { false, true, false }, 1, 1)]
		[InlineData(new[] { false, true, false, false }, 1, 1)]
		[InlineData(new[] { true, true, false, true }, 2, 1)]
		[InlineData(new[] { false, true, false, false, true }, 2, 1)]
		public void ShouldCreateAWindowControllerThatCallstheAppropriateCallbacks(bool[] emittedIsOpenValues, int callsToShowCount, int callsToCloseCount)
		{
			var isOpen = new Subject<bool>();
			var callsToShow = 0;
			var callsToHide = 0;
			var callsToClose = 0;
			using var userInterface = new TestUserInterface(isOpen, () => { }, () => callsToShow++, () => callsToHide++, () => callsToClose++);

			foreach (var isOpenValue in emittedIsOpenValues)
			{
				isOpen.OnNext(isOpenValue);
			}

			callsToShow.Should().Be(callsToShowCount);
			callsToHide.Should().Be(0);
			callsToClose.Should().Be(callsToCloseCount);
		}

		[Theory]
		[InlineData(new[] { true })]
		[InlineData(new[] { false, true, false, true })]
		public void ShouldCreateAWindowControllerThatHasNoWindowWhenControlledWindowIsClosedDirectly(bool[] emittedIsOpenValues)
		{
			var isOpen = new Subject<bool>();
			using var userInterface = new TestUserInterface(isOpen, () => { });

			foreach (var isOpenValue in emittedIsOpenValues)
			{
				isOpen.OnNext(isOpenValue);
			}
			userInterface.TestWindow.Window!.Close();

			userInterface.TestWindow.Window.Should().BeNull();
		}

		[Theory]
		[InlineData(new[] { true })]
		[InlineData(new[] { false, true, false, true })]
		public void ShouldCreateAWindowControllerThatCallsOnClosedCallbackWhenControlledWindowIsClosedDirectly(bool[] emittedIsOpenValues)
		{
			var isOpen = new Subject<bool>();
			var didCall = false;
			using var userInterface = new TestUserInterface(isOpen, () => didCall = true);
			foreach (var isOpenValue in emittedIsOpenValues)
			{
				isOpen.OnNext(isOpenValue);
			}

			userInterface.TestWindow.Window!.Close();

			didCall.Should().BeTrue();
		}

		[Theory]
		[InlineData(new bool[] { })]
		[InlineData(new[] { false })]
		[InlineData(new[] { false, true, false })]
		public void ShouldCreateAWindowControllerThatDoesNotCallOnClosedCallbackWhenClosedViaObservable(bool[] emittedIsOpenValues)
		{
			var isOpen = new Subject<bool>();
			var didCall = false;
			using var userInterface = new TestUserInterface(isOpen, () => didCall = true);

			foreach (var isOpenValue in emittedIsOpenValues)
			{
				isOpen.OnNext(isOpenValue);
			}

			didCall.Should().BeFalse();
		}
	}

	public class NoStateOverride
	{
		private class TestUserInterface : ReactiveUserInterface
		{
			public TestUserInterface(
				Action? onShow = null,
				Action? onHide = null,
				Action? onClose = null)
				: base(new ReactiveViewModelHost(Scheduler.Immediate, true), new ReactiveTestWindowFactory(_ => onShow?.Invoke(), _ => onHide?.Invoke(), _ => onClose?.Invoke()))
			{
				TestWindow = AddWindow("Name", () => new TestViewModel());
			}

			public IReactiveWindowController<TestViewModel> TestWindow { get; }
		}

		[Fact]
		public void ShouldCreateAWindowControllerThatHasAViewModel()
		{
			using var userInterface = new TestUserInterface();

			userInterface.TestWindow.ViewModel.Should().NotBeNull();
			userInterface.TestWindow.ViewModel.Should().BeOfType<TestViewModel>();
		}

		[Fact]
		public void ShouldCreateAWindowControllerThatHasAWindow()
		{
			using var userInterface = new TestUserInterface();

			userInterface.TestWindow.Window.Should().NotBeNull();
		}

		[Fact]
		public void ShouldCreateAWindowControllerThatHasAVisibleState()
		{
			using var userInterface = new TestUserInterface();

			userInterface.TestWindow.State.Should().Be(ReactiveWindowControllerState.WindowVisible);
		}

		[Fact]
		public void ShouldCreateAWindowControllerThatCallstheApproperiateCallbacks()
		{
			var callsToShow = 0;
			var callsToHide = 0;
			var callsToClose = 0;
			using var userInterface = new TestUserInterface(() => callsToShow++, () => callsToHide++, () => callsToClose++);

			callsToShow.Should().Be(1);
			callsToHide.Should().Be(0);
			callsToClose.Should().Be(0);
		}
	}

	public class FullStateOverride
	{
		private class TestUserInterface : ReactiveUserInterface
		{
			public TestUserInterface(
				IObservable<ReactiveWindowControllerState> state,
				Action onClosed,
				Action? onShow = null,
				Action? onHide = null,
				Action? onClose = null)
				: base(new ReactiveViewModelHost(Scheduler.Immediate, true), new ReactiveTestWindowFactory(_ => onShow?.Invoke(), _ => onHide?.Invoke(), _ => onClose?.Invoke()))
			{
				TestWindow = AddWindow(state, "Name", () => new TestViewModel(), onClosed);
			}

			public IReactiveWindowController<TestViewModel> TestWindow { get; }
		}

		[Theory]
		[InlineData(new[] { ReactiveWindowControllerState.WindowVisible })]
		[InlineData(new[] { ReactiveWindowControllerState.WindowClosed, ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowHidden, ReactiveWindowControllerState.WindowVisible })]
		public void ShouldCreateAWindowControllerThatHasAWindowWhenShown(ReactiveWindowControllerState[] emittedStateValues)
		{
			var state = new Subject<ReactiveWindowControllerState>();
			using var userInterface = new TestUserInterface(state, () => { });

			foreach (var stateValue in emittedStateValues)
			{
				state.OnNext(stateValue);
			}

			userInterface.TestWindow.Window.Should().NotBeNull();
		}

		[Theory]
		[InlineData(new[] { ReactiveWindowControllerState.WindowVisible })]
		[InlineData(new[] { ReactiveWindowControllerState.WindowClosed, ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowHidden, ReactiveWindowControllerState.WindowVisible })]
		public void ShouldCreateAWindowControllerThatHasAViewModelWhenShown(ReactiveWindowControllerState[] emittedStateValues)
		{
			var state = new Subject<ReactiveWindowControllerState>();
			using var userInterface = new TestUserInterface(state, () => { });

			foreach (var stateValue in emittedStateValues)
			{
				state.OnNext(stateValue);
			}

			userInterface.TestWindow.ViewModel.Should().NotBeNull();
			userInterface.TestWindow.ViewModel.Should().BeOfType<TestViewModel>();
		}

		[Theory]
		[InlineData(new[] { ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowHidden })]
		[InlineData(new[] { ReactiveWindowControllerState.WindowClosed, ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowHidden, ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowHidden })]
		public void ShouldCreateAWindowControllerThatHasAWindowWhenHidden(ReactiveWindowControllerState[] emittedStateValues)
		{
			var state = new Subject<ReactiveWindowControllerState>();
			using var userInterface = new TestUserInterface(state, () => { });

			foreach (var stateValue in emittedStateValues)
			{
				state.OnNext(stateValue);
			}

			userInterface.TestWindow.Window.Should().NotBeNull();
		}

		[Theory]
		[InlineData(new[] { ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowHidden })]
		[InlineData(new[] { ReactiveWindowControllerState.WindowClosed, ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowHidden, ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowHidden })]
		public void ShouldCreateAWindowControllerThatHasAViewModelWhenHidden(ReactiveWindowControllerState[] emittedStateValues)
		{
			var state = new Subject<ReactiveWindowControllerState>();
			using var userInterface = new TestUserInterface(state, () => { });

			foreach (var stateValue in emittedStateValues)
			{
				state.OnNext(stateValue);
			}

			userInterface.TestWindow.ViewModel.Should().NotBeNull();
			userInterface.TestWindow.ViewModel.Should().BeOfType<TestViewModel>();
		}

		[Theory]
		[InlineData(new ReactiveWindowControllerState[] { })]
		[InlineData(new[] { ReactiveWindowControllerState.WindowClosed })]
		[InlineData(new[] { ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowClosed, ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowClosed })]
		public void ShouldCreateAWindowControllerThatHasNoWindowWhenClosed(ReactiveWindowControllerState[] emittedStateValues)
		{
			var state = new Subject<ReactiveWindowControllerState>();
			using var userInterface = new TestUserInterface(state, () => { });

			foreach (var stateValue in emittedStateValues)
			{
				state.OnNext(stateValue);
			}

			userInterface.TestWindow.Window.Should().BeNull();
		}

		[Theory]
		[InlineData(new ReactiveWindowControllerState[] { })]
		[InlineData(new[] { ReactiveWindowControllerState.WindowClosed })]
		[InlineData(new[] { ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowClosed, ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowClosed })]
		public void ShouldCreateAWindowControllerThatHasNoViewModelWhenClosed(ReactiveWindowControllerState[] emittedStateValues)
		{
			var state = new Subject<ReactiveWindowControllerState>();
			using var userInterface = new TestUserInterface(state, () => { });

			foreach (var stateValue in emittedStateValues)
			{
				state.OnNext(stateValue);
			}

			userInterface.TestWindow.ViewModel.Should().BeNull();
		}

		[Theory]
		[InlineData(new[] { ReactiveWindowControllerState.WindowVisible })]
		[InlineData(new[] { ReactiveWindowControllerState.WindowClosed, ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowHidden, ReactiveWindowControllerState.WindowVisible })]
		public void ShouldCreateAWindowControllerThatHasAVisibleStateWhenShown(ReactiveWindowControllerState[] emittedStateValues)
		{
			var state = new Subject<ReactiveWindowControllerState>();
			using var userInterface = new TestUserInterface(state, () => { });

			foreach (var stateValue in emittedStateValues)
			{
				state.OnNext(stateValue);
			}

			userInterface.TestWindow.State.Should().Be(ReactiveWindowControllerState.WindowVisible);
		}

		[Theory]
		[InlineData(new[] { ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowHidden })]
		[InlineData(new[] { ReactiveWindowControllerState.WindowClosed, ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowHidden, ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowHidden })]
		public void ShouldCreateAWindowControllerWithAHiddenStateWhenHidden(ReactiveWindowControllerState[] emittedStateValues)
		{
			var state = new Subject<ReactiveWindowControllerState>();
			using var userInterface = new TestUserInterface(state, () => { });

			foreach (var stateValue in emittedStateValues)
			{
				state.OnNext(stateValue);
			}
			
			userInterface.TestWindow.State.Should().Be(ReactiveWindowControllerState.WindowHidden);
		}

		[Theory]
		[InlineData(new ReactiveWindowControllerState[] { })]
		[InlineData(new[] { ReactiveWindowControllerState.WindowClosed })]
		[InlineData(new[] { ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowClosed, ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowClosed })]
		public void ShouldCreateAWindowControllerThatHasAClosedStateWhenClosed(ReactiveWindowControllerState[] emittedStateValues)
		{
			var state = new Subject<ReactiveWindowControllerState>();
			using var userInterface = new TestUserInterface(state, () => { });

			foreach (var stateValue in emittedStateValues)
			{
				state.OnNext(stateValue);
			}

			userInterface.TestWindow.State.Should().Be(ReactiveWindowControllerState.WindowClosed);
		}

		[Theory]
		[InlineData(new ReactiveWindowControllerState[] { }, 0, 0, 0)]
		[InlineData(new[] { ReactiveWindowControllerState.WindowClosed }, 0, 0, 0)]
		[InlineData(new[] { ReactiveWindowControllerState.WindowVisible }, 1, 0, 0)]
		[InlineData(new[] { ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowHidden }, 1, 1, 0)]
		[InlineData(new[] { ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowHidden, ReactiveWindowControllerState.WindowHidden }, 1, 1, 0)]
		[InlineData(new[] { ReactiveWindowControllerState.WindowClosed, ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowClosed }, 1, 0, 1)]
		[InlineData(new[] { ReactiveWindowControllerState.WindowClosed, ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowClosed, ReactiveWindowControllerState.WindowClosed }, 1, 0, 1)]
		[InlineData(new[] { ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowClosed, ReactiveWindowControllerState.WindowVisible }, 2, 0, 1)]
		[InlineData(new[] { ReactiveWindowControllerState.WindowClosed, ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowHidden, ReactiveWindowControllerState.WindowClosed, ReactiveWindowControllerState.WindowVisible }, 2, 1, 1)]
		public void ShouldCreateAWindowControllerThatCallstheAppropriateCallbacks(ReactiveWindowControllerState[] emittedStateValues, int callsToShowCount, int callsToHideCount, int callsToCloseCount)
		{
			var state = new Subject<ReactiveWindowControllerState>();
			var callsToShow = 0;
			var callsToHide = 0;
			var callsToClose = 0;
			using var userInterface = new TestUserInterface(state, () => { }, () => callsToShow++, () => callsToHide++, () => callsToClose++);

			foreach (var stateValue in emittedStateValues)
			{
				state.OnNext(stateValue);
			}

			callsToShow.Should().Be(callsToShowCount);
			callsToHide.Should().Be(callsToHideCount);
			callsToClose.Should().Be(callsToCloseCount);
		}

		[Theory]
		[InlineData(new[] { ReactiveWindowControllerState.WindowVisible })]
		[InlineData(new[] { ReactiveWindowControllerState.WindowClosed, ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowHidden, ReactiveWindowControllerState.WindowVisible })]
		public void ShouldCreateAWindowControllerThatHasNoWindowWhenControlledWindowIsClosedDirectly(ReactiveWindowControllerState[] emittedStateValues)
		{
			var state = new Subject<ReactiveWindowControllerState>();
			using var userInterface = new TestUserInterface(state, () => { });

			foreach (var stateValue in emittedStateValues)
			{
				state.OnNext(stateValue);
			}
			userInterface.TestWindow.Window!.Close();

			userInterface.TestWindow.Window.Should().BeNull();
		}

		[Theory]
		[InlineData(new[] { ReactiveWindowControllerState.WindowVisible })]
		[InlineData(new[] { ReactiveWindowControllerState.WindowClosed, ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowClosed, ReactiveWindowControllerState.WindowVisible })]
		public void ShouldCreateAWindowControllerThatCallsOnClosedCallbackWhenControlledWindowIsClosedDirectly(ReactiveWindowControllerState[] emittedStateValues)
		{
			var state = new Subject<ReactiveWindowControllerState>();
			var didCall = false;
			using var userInterface = new TestUserInterface(state, () => didCall = true);
			foreach (var stateValue in emittedStateValues)
			{
				state.OnNext(stateValue);
			}

			userInterface.TestWindow.Window!.Close();

			didCall.Should().BeTrue();
		}

		[Theory]
		[InlineData(new ReactiveWindowControllerState[] { })]
		[InlineData(new[] { ReactiveWindowControllerState.WindowClosed })]
		[InlineData(new[] { ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowClosed, ReactiveWindowControllerState.WindowVisible, ReactiveWindowControllerState.WindowClosed })]
		public void ShouldCreateAWindowControllerThatDoesNotCallOnClosedCallbackWhenClosedViaObservable(ReactiveWindowControllerState[] emittedStateValues)
		{
			var state = new Subject<ReactiveWindowControllerState>();
			var didCall = false;
			using var userInterface = new TestUserInterface(state, () => didCall = true);

			foreach (var stateValue in emittedStateValues)
			{
				state.OnNext(stateValue);
			}

			didCall.Should().BeFalse();
		}
	}
}
