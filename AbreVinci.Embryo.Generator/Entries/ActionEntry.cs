﻿namespace AbreVinci.Embryo.Generator.Entries;

internal record ActionEntry(TypeEntry DeclaringType, string Name, ImmutableArray<ParameterEntry> Parameters);
