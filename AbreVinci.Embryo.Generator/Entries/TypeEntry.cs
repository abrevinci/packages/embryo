﻿namespace AbreVinci.Embryo.Generator.Entries;

internal record TypeEntry(string? TypeNamespace, string TypeName)
{
	public ImmutableArray<TypeEntry> RecursiveTypeArguments { get; init; } = ImmutableArray<TypeEntry>.Empty;
	public bool IsArray { get; init; } = false;
	public bool IsNullableValue { get; init; } = false;
	public bool IsNullableReference { get; init; } = false;

	public bool IsNullable => IsNullableValue || IsNullableReference;

	public string TypeFullName => TypeNamespace != null ? $"{TypeNamespace}.{TypeName}" : TypeName;

	public IEnumerable<TypeEntry> DescendantsAndSelf()
	{
		yield return this;
		foreach (var item in RecursiveTypeArguments.SelectMany(x => x.DescendantsAndSelf()))
		{
			yield return item;
		}
	}
}
