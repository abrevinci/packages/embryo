﻿namespace AbreVinci.Embryo.Generator.Entries;

internal record AttributeMarkedSyntaxEntry<TSyntax>(TSyntax Syntax, AttributeSyntax AttributeSyntax, SemanticModel SemanticModel);
