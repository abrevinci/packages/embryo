﻿using AbreVinci.Embryo.Generator.Extensions;

namespace AbreVinci.Embryo.Generator.Entries;

internal record ParameterEntry(TypeEntry Type, string PascalCaseName)
{
	public string CamelCaseName => PascalCaseName.ToCamelCase();
}
