﻿namespace AbreVinci.Embryo.Generator.Entries;

internal record EffectEntry(TypeEntry DeclaringType, ServiceEntry? ServiceEntry, bool IsAsync, bool HasSimpleContext, string Name, ImmutableArray<ParameterEntry> Parameters);
