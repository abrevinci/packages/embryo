﻿using AbreVinci.Embryo.Generator.Extensions;

namespace AbreVinci.Embryo.Generator.Entries;

internal record ServiceEntry(TypeEntry Type, string BaseIdentifier)
{
	public string Postfix { get; init; } = "";

	public string PascalCaseIdentifier => BaseIdentifier.ToPascalCase() + Postfix;
	public string CamelCaseIdentifier => BaseIdentifier.ToCamelCase() + Postfix;
	public string ContextIdentifier => BaseIdentifier.ToPascalCase() + "Context" + Postfix;
}
