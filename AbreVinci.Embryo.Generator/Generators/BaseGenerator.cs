﻿namespace AbreVinci.Embryo.Generator.Generators;

public abstract class BaseGenerator : ISourceGenerator
{
	void ISourceGenerator.Initialize(GeneratorInitializationContext context)
	{
	}

	void ISourceGenerator.Execute(GeneratorExecutionContext context)
	{
		try
		{
			Execute(context);
		}
		catch (Exception exception)
		{
#if DEBUG
			System.Diagnostics.Debugger.Launch();
#endif
			ReportError(context, "E000", "Exception in Generator", exception.ToString(), null);
		}
	}

	protected abstract void Execute(GeneratorExecutionContext context);

	protected void ReportError(GeneratorExecutionContext context, string id, string title, string details, Location? location)
	{
		var descriptor = new DiagnosticDescriptor(id, title, details, GetType().Name, DiagnosticSeverity.Error, true);
		context.ReportDiagnostic(Diagnostic.Create(descriptor, location));
	}
}
