﻿using AbreVinci.Embryo.Generator.Entries;
using AbreVinci.Embryo.Generator.Extensions;

namespace AbreVinci.Embryo.Generator.Generators;

[Generator]
public class CommandTypeGenerator : BaseGenerator
{
	protected override void Execute(GeneratorExecutionContext context)
	{
		var compilation = context.Compilation;

		var commandTypes = compilation.GetCommandTypes().ToList();

		if (commandTypes.Count > 1)
		{
			ReportError(context, "EC001", "Multiple command types", "The command type generator currently only supports a single generated command type.", commandTypes[1].Syntax.GetLocation());
			return;
		}

		if (commandTypes.Count == 0)
			return;

		var commandTypeSyntax = commandTypes[0].Syntax;
		var semanticModel = commandTypes[0].SemanticModel;

		if (semanticModel.GetDeclaredSymbol(commandTypeSyntax) is not ITypeSymbol commandType)
		{
			ReportError(context, "EC002", "Failed to get command type symbol, something is wrong!", "", commandTypeSyntax.GetLocation());
			return;
		}

		var effectContextType = compilation.GetTypeByMetadataName("AbreVinci.Embryo.Core.EffectContext`2");
		
		if (effectContextType == null)
		{
			ReportError(context, "EC003", "Failed to resolve type EffectContext`2, make sure you have referenced AbreVinci.Embryo.Core.", "", null);
			return;
		}

		var effectContextType1 = compilation.GetTypeByMetadataName("AbreVinci.Embryo.Core.EffectContext`1");

		if (effectContextType1 == null)
		{
			ReportError(context, "EC004", "Failed to resolve type EffectContext`1, make sure you have referenced AbreVinci.Embryo.Core.", "", null);
			return;
		}

		var taskType = compilation.GetTypeByMetadataName("System.Threading.Tasks.Task");

		if (taskType == null)
		{
			ReportError(context, "EC005", "Failed to resolve Task type", "", null);
			return;
		}

		try
		{
			var effectEntries = compilation.GetEffectEntries(effectContextType, effectContextType1, taskType).ToList();

			var allTypeEntries = effectEntries
				.SelectMany(e => e.Parameters.Where(p => p.Type != effectContextType.ToTypeEntry() && p.Type != effectContextType1.ToTypeEntry()).SelectMany(p => p.Type.DescendantsAndSelf()))
				.Append(commandType.ToTypeEntry())
				.ToList();

			var source =
$@"{allTypeEntries.GenerateUsings(commandType.ContainingNamespace.ToString(), @"
")}namespace {commandType.ContainingNamespace}
{{
	{commandTypeSyntax.Modifiers} {commandTypeSyntax.Keyword} {commandType.Name}
	{{
		{GenerateCommandTags(effectEntries, @"
		")}

		{GenerateCommandTypes(effectEntries, allTypeEntries, commandType.Name, @"
		")}

		// Prevent external inheritance
		private {commandType.Name}(int _tag)
		{{
			_Tag = _tag;
		}}

		public int _Tag {{ get; }}
	}}
}}
";

			context.AddSource($"{commandType.Name}.g.cs", source);
		}
		catch
		{
			context.AddSource($"{commandType.Name}.g.cs", "");
			throw;
		}
	}

	private string GenerateCommandTags(IEnumerable<EffectEntry> effectEntries, string lineBreak)
	{
		var tags = effectEntries
			.GroupBy(e => e.DeclaringType)
			.OrderBy(g => g.Key.TypeFullName)
			.SelectMany(g => g.OrderBy(e => e.Name))
			.Select((e, i) => $"public const int _{e.Name}Tag = {i + 1};");

		return string.Join(lineBreak, tags);
	}

	private string GenerateCommandTypes(IEnumerable<EffectEntry> effectEntries, ICollection<TypeEntry> allTypeEntries, string commandTypeName, string lineBreak)
	{
		var groups = effectEntries
			.GroupBy(e => e.DeclaringType)
			.OrderBy(g => g.Key.TypeFullName)
			.Select(g =>
			{
				var commands = g
					.OrderBy(e => e.Name)
					.Select(e =>
					{
						var parameters = string.Join(", ", e.Parameters.Select(p => $"{p.Type.Emit(allTypeEntries)} {p.PascalCaseName}"));
						return string.Join(lineBreak, "/// <summary>", $"/// Is handled by <see cref=\"{g.Key.TypeFullName}.{e.Name}\"/>", "/// </summary>", $"public sealed record {e.Name}({parameters}) : {commandTypeName}(_{e.Name}Tag);");
					});

				return string.Join(lineBreak, Enumerable.Repeat($"// {g.Key.TypeFullName}", 1).Concat(commands));
			});

		return string.Join(@"
" + lineBreak, groups);
	}
}
