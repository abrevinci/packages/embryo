﻿using AbreVinci.Embryo.Generator.Entries;
using AbreVinci.Embryo.Generator.Extensions;

namespace AbreVinci.Embryo.Generator.Generators;

[Generator]
public class ReducerGenerator : BaseGenerator
{
	protected override void Execute(GeneratorExecutionContext context)
	{
		var compilation = context.Compilation;

		var reducerEntries = compilation.GetReducerEntries().ToList();

		if (reducerEntries.Count > 1)
		{
			ReportError(context, "ER001", "Multiple reducer types", "The reducer update generator currently only supports a single generated reducer type.", reducerEntries[1].Syntax.GetLocation());
			return;
		}

		if (reducerEntries.Count == 0)
			return;

		var reducerTypeSyntax = reducerEntries[0].Syntax;
		var reducerAttributeSyntax = reducerEntries[0].AttributeSyntax;
		var semanticModel = reducerEntries[0].SemanticModel;

		if (semanticModel.GetDeclaredSymbol(reducerTypeSyntax) is not ITypeSymbol reducerType)
		{
			ReportError(context, "ER002", "Failed to get reducer type symbol, something is wrong!", "", reducerTypeSyntax.GetLocation());
			return;
		}

		if (reducerAttributeSyntax.ArgumentList?.Arguments.Count is not { } count || count < 2 || count > 3)
		{
			ReportError(context, "ER003", "Wrong argument(s)", "The GenerateReducerUpdate attribute should have 2 or 3 type arguments", reducerAttributeSyntax.GetLocation());
			return;
		}

		var stateTypeSyntax = reducerAttributeSyntax.ArgumentList.Arguments[0].DescendantNodesAndSelf().OfType<TypeOfExpressionSyntax>().Select(s => s.Type).FirstOrDefault();
		if (stateTypeSyntax == null || semanticModel.GetTypeInfo(stateTypeSyntax).Type is not { } stateType)
		{
			ReportError(context, "ER004", "Wrong argument (stateType)", "The GenerateReducerUpdate attribute expects it's first argument to be a typeof expression of a valid state type.", reducerAttributeSyntax.GetLocation());
			return;
		}

		var messageTypeSyntax = reducerAttributeSyntax.ArgumentList.Arguments[1].DescendantNodesAndSelf().OfType<TypeOfExpressionSyntax>().Select(s => s.Type).FirstOrDefault();
		if (messageTypeSyntax == null || semanticModel.GetTypeInfo(messageTypeSyntax).Type is not { } messageType)
		{
			ReportError(context, "ER005", "Wrong argument (messageType)", "The GenerateReducerUpdate attribute expects it's second argument to be a typeof expression of a valid message type.", reducerAttributeSyntax.GetLocation());
			return;
		}

		var useCommands = reducerAttributeSyntax.ArgumentList.Arguments.Count == 3;
		
		var commandTypeSyntax = useCommands ? reducerAttributeSyntax.ArgumentList.Arguments[2].DescendantNodesAndSelf().OfType<TypeOfExpressionSyntax>().Select(s => s.Type).FirstOrDefault() : null;
		var commandType = commandTypeSyntax != null ? semanticModel.GetTypeInfo(commandTypeSyntax).Type : null;
		if (useCommands && (commandTypeSyntax == null || commandType == null))
		{
			ReportError(context, "ER006", "Wrong argument (commandType)", "The GenerateReducerUpdate attribute expects it's third argument (if given) to be a typeof expression of a valid command type.", reducerAttributeSyntax.GetLocation());
			return;
		}

		var actionContextType = compilation.GetTypeByMetadataName("AbreVinci.Embryo.Core.ActionContext`2");
		
		if (commandType != null && actionContextType == null)
		{
			ReportError(context, "ER007", "Failed to resolve type ActionContext`2, make sure you have referenced AbreVinci.Embryo.Core.", "", null);
			return;
		}

		try
		{
			var expectedStateOrContextType = commandType != null ? actionContextType!.Construct(stateType, commandType) : stateType;
			var actionEntries = compilation.GetActionEntries(expectedStateOrContextType).ToList();

			var allTypeEntries = actionEntries
				.Select(e => e.DeclaringType)
				.Append(reducerType.ToTypeEntry())
				.Append(stateType.ToTypeEntry(stateTypeSyntax))
				.Append(messageType.ToTypeEntry(messageTypeSyntax))
				.Concat(commandType != null ? new[] { commandType.ToTypeEntry(commandTypeSyntax), actionContextType!.ToTypeEntry() } : Enumerable.Empty<TypeEntry>())
				.ToList();

			var stateOrContextName = commandType != null ? "context" : "state";
			
			var stateOrContextType = commandType != null ? 
				$"{actionContextType!.Emit(allTypeEntries)}<{stateType.Emit(allTypeEntries)}, {commandType.Emit(allTypeEntries)}>" : 
				$"{stateType.Emit(allTypeEntries)}";

			var source =
$@"{allTypeEntries.GenerateUsings(reducerType.ContainingNamespace.ToString(), @"
")}namespace {reducerType.ContainingNamespace}
{{
	{reducerTypeSyntax.Modifiers} {reducerTypeSyntax.Keyword} {reducerType.Name}
	{{
		public static {stateOrContextType} Update(this {stateOrContextType} {stateOrContextName}, {messageTypeSyntax} message)
		{{
			return message._Tag switch
			{{
				{GenerateSwitchCases(actionEntries, messageType.Emit(allTypeEntries), stateOrContextName, @"
				")}
			}};
		}}
	}}
}}
"; 
		
			context.AddSource($"{reducerType.Name}.g.cs", source);
		}
		catch
		{
			context.AddSource($"{reducerType.Name}.g.cs", "");
			throw;
		}
	}

	private static string GenerateSwitchCases(IEnumerable<ActionEntry> actionEntries, string messageType, string stateOrContextName, string lineBreak)
	{
		var groups = actionEntries
			.GroupBy(e => e.DeclaringType)
			.OrderBy(g => g.Key.TypeFullName)
			.Select(g =>
			{
				var cases = g
					.OrderBy(e => e.Name)
					.Select(e =>
					{
						if (e.Parameters.Length > 0)
						{
							var args = e.Parameters.Select(p => "m." + p.PascalCaseName);
							return $"{messageType}._{e.Name}Tag when ({messageType}.{e.Name})message is var m => {stateOrContextName}.{e.Name}({string.Join(", ", args)}),";
						}

						return $"{messageType}._{e.Name}Tag => {stateOrContextName}.{e.Name}(),";
					});

				return string.Join(lineBreak, Enumerable.Repeat($"// {g.Key.TypeFullName}", 1).Concat(cases));
			})
			.Append($"_ => {stateOrContextName}");

		return string.Join(@"
" + lineBreak, groups);
	}
}
