using AbreVinci.Embryo.Generator.Entries;
using AbreVinci.Embryo.Generator.Extensions;

namespace AbreVinci.Embryo.Generator.Generators;

[Generator]
public class MessageTypeGenerator : BaseGenerator
{
	protected override void Execute(GeneratorExecutionContext context)
	{
		var compilation = context.Compilation;

		var messageTypes = compilation.GetMessageTypes().ToList();

		if (messageTypes.Count > 1)
		{
			ReportError(context, "EM001", "Multiple message types", "The message type generator currently only supports a single generated message type.", messageTypes[1].Syntax.GetLocation());
			return;
		}

		if (messageTypes.Count == 0)
			return;

		var messageTypeSyntax = messageTypes[0].Syntax;
		var semanticModel = messageTypes[0].SemanticModel;

		if (semanticModel.GetDeclaredSymbol(messageTypeSyntax) is not ITypeSymbol messageType)
		{
			ReportError(context, "EM002", "Failed to get message type symbol, something is wrong!", "", messageTypeSyntax.GetLocation());
			return;
		}

		try
		{
			var actionEntries = compilation.GetActionEntries().ToList();

			var allTypeEntries = actionEntries
				.SelectMany(e => e.Parameters.SelectMany(p => p.Type.DescendantsAndSelf()))
				.Append(messageType.ToTypeEntry())
				.ToList();
			
			var source =
$@"{allTypeEntries.GenerateUsings(messageType.ContainingNamespace.ToString(), @"
")}namespace {messageType.ContainingNamespace}
{{
	{messageTypeSyntax.Modifiers} {messageTypeSyntax.Keyword} {messageType.Name}
	{{
		{GenerateMessageTags(actionEntries, @"
		")}

		{GenerateMessageTypes(actionEntries, allTypeEntries, messageType.Name, @"
		")}

		// Prevent external inheritance
		private {messageType.Name}(int _tag)
		{{
			_Tag = _tag;
		}}

		public int _Tag {{ get; }}
	}}
}}
";

			context.AddSource($"{messageType.Name}.g.cs", source);
		}
		catch
		{
			context.AddSource($"{messageType.Name}.g.cs", "");
			throw;
		}
	}

	private string GenerateMessageTags(IEnumerable<ActionEntry> actionEntries, string lineBreak)
	{
		var tags = actionEntries
			.GroupBy(e => e.DeclaringType)
			.OrderBy(g => g.Key.TypeFullName)
			.SelectMany(g => g.OrderBy(e => e.Name))
			.Select((e, i) => $"public const int _{e.Name}Tag = {i + 1};");

		return string.Join(lineBreak, tags);
	}
	
	private string GenerateMessageTypes(IEnumerable<ActionEntry> actionEntries, ICollection<TypeEntry> allTypeEntries, string messageTypeName, string lineBreak)
	{
		var groups = actionEntries
			.GroupBy(e => e.DeclaringType)
			.OrderBy(g => g.Key.TypeFullName)
			.Select(g =>
			{
				var messages = g
					.OrderBy(e => e.Name)
					.Select(e =>
					{
						var parameters = string.Join(", ", e.Parameters.Select(p => $"{p.Type.Emit(allTypeEntries)} {p.PascalCaseName}"));
						return string.Join(lineBreak, "/// <summary>", $"/// Is handled by <see cref=\"{g.Key.TypeFullName}.{e.Name}\"/>", "/// </summary>", $"public sealed record {e.Name}({parameters}) : {messageTypeName}(_{e.Name}Tag);");
					});

				return string.Join(lineBreak, Enumerable.Repeat($"/// ${g.Key.TypeFullName}", 1).Concat(messages));
			});

		return string.Join(@"
" + lineBreak, groups);
	}
}
