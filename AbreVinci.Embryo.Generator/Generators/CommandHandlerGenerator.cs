﻿using AbreVinci.Embryo.Generator.Entries;
using AbreVinci.Embryo.Generator.Extensions;

namespace AbreVinci.Embryo.Generator.Generators;

[Generator]
public class CommandHandlerGenerator : BaseGenerator
{
	protected override void Execute(GeneratorExecutionContext context)
	{
		var compilation = context.Compilation;

		var commandHandlerEntries = compilation.GetCommandHandlerEntries().ToList();

		if (commandHandlerEntries.Count > 1)
		{
			ReportError(context, "EH001", "Multiple command handler types", "The command handler generator currently only supports a single generated command handler type.", commandHandlerEntries[1].Syntax.GetLocation());
			return;
		}

		if (commandHandlerEntries.Count == 0)
			return;

		var commandHandlerTypeSyntax = commandHandlerEntries[0].Syntax;
		var commandHandlerAttributeSyntax = commandHandlerEntries[0].AttributeSyntax;
		var semanticModel = commandHandlerEntries[0].SemanticModel;

		if (semanticModel.GetDeclaredSymbol(commandHandlerTypeSyntax) is not ITypeSymbol commandHandlerType)
		{
			ReportError(context, "EH002", "Failed to get command handler type symbol, something is wrong!", "", commandHandlerTypeSyntax.GetLocation());
			return;
		}

		if (commandHandlerAttributeSyntax.ArgumentList?.Arguments.Count != 2)
		{
			ReportError(context, "EH003", "Wrong argument(s)", "The GenerateCommandHandler attribute should have 2 type arguments", commandHandlerAttributeSyntax.GetLocation());
			return;
		}

		var commandTypeSyntax = commandHandlerAttributeSyntax.ArgumentList.Arguments[0].DescendantNodesAndSelf().OfType<TypeOfExpressionSyntax>().Select(s => s.Type).FirstOrDefault();
		if (commandTypeSyntax == null || semanticModel.GetTypeInfo(commandTypeSyntax).Type is not { } commandType)
		{
			ReportError(context, "EH004", "Wrong argument (commandType)", "The GenerateCommandHandler attribute expects it's first argument to be a typeof expression of a valid command type.", commandHandlerAttributeSyntax.GetLocation());
			return;
		}

		var messageTypeSyntax = commandHandlerAttributeSyntax.ArgumentList.Arguments[1].DescendantNodesAndSelf().OfType<TypeOfExpressionSyntax>().Select(s => s.Type).FirstOrDefault();
		if (messageTypeSyntax == null || semanticModel.GetTypeInfo(messageTypeSyntax).Type is not { } messageType)
		{
			ReportError(context, "EH005", "Wrong argument (messageType)", "The GenerateCommandHandler attribute expects it's second argument to be a typeof expression of a valid message type.", commandHandlerAttributeSyntax.GetLocation());
			return;
		}

		var effectContextType = compilation.GetTypeByMetadataName("AbreVinci.Embryo.Core.EffectContext`2");

		if (effectContextType == null)
		{
			ReportError(context, "EH004", "Failed to resolve type EffectContext`2, make sure you have referenced AbreVinci.Embryo.Core.", "", null);
			return;
		}

		var effectContextType1 = compilation.GetTypeByMetadataName("AbreVinci.Embryo.Core.EffectContext`1");

		if (effectContextType1 == null)
		{
			ReportError(context, "EH005", "Failed to resolve type EffectContext`1, make sure you have referenced AbreVinci.Embryo.Core.", "", null);
			return;
		}

		var taskType = compilation.GetTypeByMetadataName("System.Threading.Tasks.Task");

		if (taskType == null)
		{
			ReportError(context, "EH006", "Failed to resolve Task type", "", null);
			return;
		}

		var actionType = compilation.GetTypeByMetadataName("System.Action");
		var actionType1 = compilation.GetTypeByMetadataName("System.Action`1");

		if (actionType == null || actionType1 == null)
		{
			ReportError(context, "EH007", "Failed to resolve Action type", "", null);
			return;
		}

		var disposableType = compilation.GetTypeByMetadataName("System.IDisposable");

		if (disposableType == null)
		{
			ReportError(context, "EH008", "Failed to resolve IDisposable type", "", null);
			return;
		}

		var observableType = compilation.GetTypeByMetadataName("System.IObservable`1");

		if (observableType == null)
		{
			ReportError(context, "EH009", "Failed to resolve IObservable`1 type", "", null);
			return;
		}

		try
		{
			var effectEntries = compilation.GetEffectEntries(effectContextType, effectContextType1, taskType).ToList();
			
			var serviceEntries = effectEntries
				.Where(e => e.ServiceEntry != null)
				.Select(e => e.ServiceEntry!)
				.Distinct()
				.GroupBy(e => e.PascalCaseIdentifier)
				.SelectMany(g => g.Count() == 1 ? g : g.Select((e, i) => e with { Postfix = $"__{i + 1}" }))
				.OrderBy(e => e.CamelCaseIdentifier)
				.ToList();

			var allTypeEntries = effectEntries
				.Select(e => e.DeclaringType)
				.Append(commandType.ToTypeEntry(commandTypeSyntax))
				.Append(messageType.ToTypeEntry(messageTypeSyntax))
				.Append(taskType.ToTypeEntry())
				.Append(actionType.ToTypeEntry())
				.Append(actionType1.ToTypeEntry())
				.Append(disposableType.ToTypeEntry())
				.Append(observableType.ToTypeEntry())
				.Append(effectContextType1.ToTypeEntry())
				.Concat(serviceEntries.Any() ? serviceEntries.Select(e => e.Type).Append(effectContextType.ToTypeEntry()) : Enumerable.Empty<TypeEntry>())
				.ToList();

			var source =
$@"{allTypeEntries.GenerateUsings(commandHandlerType.ContainingNamespace.ToString(), @"
")}namespace {commandHandlerType.ContainingNamespace}
{{
	{commandHandlerTypeSyntax.Modifiers} {commandHandlerTypeSyntax.Keyword} {commandHandlerType.Name} : {disposableType.Emit(allTypeEntries)}
	{{
		private readonly {disposableType.Emit(allTypeEntries)} _subscription;

		public {commandHandlerType.Name}(
			{observableType.Emit(allTypeEntries)}<{commandType.Emit(allTypeEntries)}> commands,
			{actionType1.Emit(allTypeEntries)}<{messageType.Emit(allTypeEntries)}> dispatch{GenerateServiceArguments(serviceEntries, allTypeEntries, @",
			")})
		{{
			Context = new {effectContextType1.Emit(allTypeEntries)}<{messageType.Emit(allTypeEntries)}>(dispatch);{(serviceEntries.Any() ? @"
			" : "")}{GenerateEffectContextAssignments(serviceEntries, allTypeEntries, effectContextType.Emit(allTypeEntries), messageType.Emit(allTypeEntries), @"
			")}

			_subscription = commands.Subscribe(command => Handle(command));
		}}

		protected {effectContextType1.Emit(allTypeEntries)}<{messageType.Emit(allTypeEntries)}> Context {{ get; }}{(serviceEntries.Any() ? @"
		" : "")}{GenerateEffectContextProperties(serviceEntries, allTypeEntries, effectContextType.Emit(allTypeEntries), messageType.Emit(allTypeEntries), @"
		")}

		protected virtual {taskType.Emit(allTypeEntries)} Handle({commandType.Emit(allTypeEntries)} command)
		{{
			return command._Tag switch
			{{
				{GenerateEffectHandlers(effectEntries, serviceEntries, allTypeEntries, commandType.Emit(allTypeEntries), taskType.Emit(allTypeEntries), @"
				")}
			}};
		}}

		protected static {taskType.Emit(allTypeEntries)} WrapAsAsync({actionType.Emit(allTypeEntries)} action)
		{{
			action();
			return {taskType.Emit(allTypeEntries)}.CompletedTask;
		}}

		public void Dispose()
		{{
			_subscription.Dispose();
		}}
	}}
}}
";

			context.AddSource($"{commandHandlerType.Name}.g.cs", source);
		}
		catch
		{
			context.AddSource($"{commandHandlerType.Name}.g.cs", "");
			throw;
		}
	}

	private string GenerateServiceArguments(IEnumerable<ServiceEntry> serviceEntries, ICollection<TypeEntry> allTypeEntries, string joiner)
	{
		var arguments = string.Join(joiner, serviceEntries.Select(e => $"{e.Type.Emit(allTypeEntries)} {e.CamelCaseIdentifier}"));
		return arguments.Length == 0 ? string.Empty : $"{joiner}{arguments}";
	}

	private string GenerateEffectContextAssignments(IEnumerable<ServiceEntry> serviceEntries, ICollection<TypeEntry> allTypeEntries, string effectContextType, string messageType, string lineBreak)
	{
		return string.Join(lineBreak, serviceEntries.Select(e => $"{e.ContextIdentifier} = new {effectContextType}<{e.Type.Emit(allTypeEntries)}, {messageType}>({e.CamelCaseIdentifier}, dispatch);"));
	}

	private string GenerateEffectContextProperties(IEnumerable<ServiceEntry> serviceEntries, ICollection<TypeEntry> allTypeEntries, string effectContextType, string messageType, string lineBreak)
	{
		return string.Join(lineBreak, serviceEntries.Select(e => $"protected {effectContextType}<{e.Type.Emit(allTypeEntries)}, {messageType}> {e.ContextIdentifier} {{ get; }}"));
	}

	private string GenerateEffectHandlers(IEnumerable<EffectEntry> effectEntries, IEnumerable<ServiceEntry> allServiceEntries, ICollection<TypeEntry> allTypeEntries, string commandType, string taskType, string lineBreak)
	{
		var groups = effectEntries
			.GroupBy(e => e.DeclaringType)
			.OrderBy(g => g.Key.TypeFullName)
			.Select(g =>
			{
				var cases = g
					.OrderBy(e => e.Name)
					.Select(e =>
					{
						var wrapperStart = e.IsAsync ? "" : "WrapAsAsync(() => ";
						var wrapperEnd = e.IsAsync ? "" : ")";

						var serviceEntry = e.ServiceEntry != null ? allServiceEntries.Single(s => s.Type == e.ServiceEntry.Type) : null;
						var context = serviceEntry != null ?
							serviceEntry.ContextIdentifier :
							e.HasSimpleContext ?
								"Context" :
								null;

						var args = string.Join(", ", e.Parameters.Select(p => "c." + p.PascalCaseName));
						if (args.Length > 0)
						{
							args = context != null ? string.Join(", ", context, args) : args;
							return $"{commandType}._{e.Name}Tag when ({commandType}.{e.Name})command is var c => {wrapperStart}{e.DeclaringType.Emit(allTypeEntries)}.{e.Name}({args}){wrapperEnd},";
						}

						args = context ?? args;
						return $"{commandType}._{e.Name}Tag => {wrapperStart}{e.DeclaringType.Emit(allTypeEntries)}.{e.Name}({args}){wrapperEnd},";
					});

				return string.Join(lineBreak, Enumerable.Repeat($"// {g.Key.TypeFullName}", 1).Concat(cases));
			})
			.Append($"_ => {taskType}.CompletedTask");

		return string.Join(@"
" + lineBreak, groups);
	}
}
