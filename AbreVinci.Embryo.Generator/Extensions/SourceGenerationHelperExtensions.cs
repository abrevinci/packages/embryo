﻿using AbreVinci.Embryo.Generator.Entries;

namespace AbreVinci.Embryo.Generator.Extensions;

internal static class SourceGenerationHelperExtensions
{
	public static string GenerateUsings(this ICollection<TypeEntry> allTypeEntries, string fileNamespace, string lineBreak)
	{
		var usings = allTypeEntries
			.Select(e => new TypeEntry(e.TypeNamespace, e.TypeName))
			.Distinct()
			.Where(e => e.TypeNamespace != null && e.TypeNamespace != fileNamespace && !fileNamespace.StartsWith($"{e.TypeNamespace}."))
			.GroupBy(e => e.TypeName)
			.SelectMany(g => g.Count() == 1 ? g.Where(e => e.TypeNamespace != "<global namespace>").Select(e => e.TypeNamespace) : Enumerable.Empty<string>())
			.Distinct()
			.OrderBy(n => n)
			.Select(n => $"using {n};")
			.ToList();

		var str = usings.Any() ? $"{string.Join(lineBreak, usings)}{lineBreak}{lineBreak}" : string.Empty;

		if (allTypeEntries.Any(t => t.IsNullableReference))
			str += $"#nullable enable{lineBreak}";

		return str;
	}
	
	public static string Emit(this TypeEntry entry, ICollection<TypeEntry> allTypeEntries)
	{
		var (typeNamespace, typeName) = entry;

		var entryGroup = allTypeEntries
			.Select(e => new TypeEntry(e.TypeNamespace, e.TypeName))
			.Distinct()
			.GroupBy(e => e.TypeName)
			.FirstOrDefault(g => g.Key == typeName);
		
		typeName += entry.RecursiveTypeArguments.Any() ? $"<{string.Join(", ", entry.RecursiveTypeArguments.Select(e => e.Emit(allTypeEntries)))}>" : "";
		typeName += entry.IsNullable ? "?" : "";
		typeName += entry.IsArray ? "[]" : "";

		if (typeNamespace == null || entryGroup != null && entryGroup.Count() == 1)
		{
			return typeName;
		}

		return $"{typeNamespace}.{typeName}";
	}

	public static string Emit(this ITypeSymbol type, ICollection<TypeEntry> allTypeEntries)
	{
		return type.ToTypeEntry().Emit(allTypeEntries);
	}
}
