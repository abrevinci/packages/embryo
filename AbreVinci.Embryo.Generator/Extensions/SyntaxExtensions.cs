﻿using AbreVinci.Embryo.Generator.Entries;

namespace AbreVinci.Embryo.Generator.Extensions;

internal static class SyntaxExtensions
{
	public static bool IsPartial(this MemberDeclarationSyntax syntax)
	{
		return syntax.Modifiers.Any(m => m.ToString() == "partial");
	}

	public static bool IsStaticMemberMethod(this MethodDeclarationSyntax syntax, SemanticModel semanticModel)
	{
		return syntax.Parent is TypeDeclarationSyntax parent &&
		       syntax.Modifiers.Any(m => m.ToString() == "static") &&
		       semanticModel.GetDeclaredSymbol(parent) != null;
	}

	public static bool IsExtensionMethod(this MethodDeclarationSyntax syntax, SemanticModel semanticModel)
	{
		return syntax.IsStaticMemberMethod(semanticModel) &&
		       syntax.ParameterList.Parameters.FirstOrDefault()?.Modifiers.ToString().Contains("this") == true;
	}

	public static bool AreParametersValid(this MethodDeclarationSyntax syntax, SemanticModel semanticModel)
	{
		return syntax.ParameterList.Parameters.All(p => 
			p.Type != null &&
            !string.IsNullOrWhiteSpace(p.Identifier.ToString()) &&
            semanticModel.GetTypeInfo(p.Type).Type is { } type &&
            type.ContainingNamespace?.IsGlobalNamespace != true);
	}

	public static IEnumerable<ParameterEntry> GetParameterEntries(this MethodDeclarationSyntax syntax, SemanticModel semanticModel)
	{
		return syntax.ParameterList.Parameters
			.Select(p => new ParameterEntry(semanticModel.GetTypeInfo(p.Type!).Type!.ToTypeEntry(p.Type), p.Identifier.ToString().ToPascalCase()));
	}

	public static TypeEntry ToTypeEntry(this ITypeSymbol type, TypeSyntax? syntax = null)
	{
		var displayString = type.ToDisplayString(SymbolDisplayFormat.MinimallyQualifiedFormat);
		var fullDisplayString = type.ToDisplayString(SymbolDisplayFormat.FullyQualifiedFormat);
		
		if (type is IArrayTypeSymbol arrayType)
			return arrayType.ElementType.ToTypeEntry() with { IsArray = true };

		if (syntax is NullableTypeSyntax nullableTypeSyntax && type is INamedTypeSymbol nullableType)
			return nullableType.Name == "Nullable" ?
				nullableType.TypeArguments[0].ToTypeEntry(nullableTypeSyntax.ElementType) with { IsNullableValue = true } :
				nullableType.ToTypeEntry(nullableTypeSyntax.ElementType) with { IsNullableReference = true };

		if (fullDisplayString == displayString)
			return new TypeEntry(null, displayString);

		if (type is INamedTypeSymbol { IsGenericType: true } namedType && namedType.TypeArguments.Any())
		{
			var isGenericInstance = !SymbolEqualityComparer.Default.Equals(namedType, namedType.OriginalDefinition);
			var typeArguments = isGenericInstance ? namedType.TypeArguments.Select(t => t.ToTypeEntry()).ToImmutableArray() : ImmutableArray<TypeEntry>.Empty;
			return new TypeEntry(type.ContainingNamespace.ToString(), type.Name) { RecursiveTypeArguments = typeArguments };
		}

		return new TypeEntry(type.ContainingNamespace?.ToString(), displayString);
	}

	public static bool IsChainableExtensionMethod(this MethodDeclarationSyntax syntax, ITypeSymbol? expectedChainType, SemanticModel semanticModel)
	{
		if (syntax.IsExtensionMethod(semanticModel) && 
		   syntax.ReturnType.ToString() == syntax.ParameterList.Parameters[0].Type!.ToString() &&
		   semanticModel.GetTypeInfo(syntax.ParameterList.Parameters[0].Type!).Type is { } chainType)
		{
			return expectedChainType == null || chainType.IsAssignableFrom(expectedChainType, true);
		}

		return false;
	}
	
	public static bool IsAsync(this MethodDeclarationSyntax syntax, INamedTypeSymbol taskType, SemanticModel semanticModel)
	{
		var returnType = semanticModel.GetTypeInfo(syntax.ReturnType).Type;
		return returnType != null && taskType.IsAssignableFrom(returnType);
	}

	public static string GenerateIdentifier(this ITypeSymbol type)
	{
		return type.TypeKind == TypeKind.Interface && type.Name.StartsWith("I") && type.Name.Length >= 2 ?
			type.Name.Substring(1) :
			type.Name;
	}

	public static bool TryGetAsGenericInstanceSymbol(this TypeSyntax? syntax, INamedTypeSymbol genericTypeSymbol, SemanticModel semanticModel, [MaybeNullWhen(false)] out INamedTypeSymbol genericInstanceTypeSymbol)
	{
		if (syntax is GenericNameSyntax genericNameSyntax &&
		    semanticModel.GetTypeInfo(genericNameSyntax).Type is INamedTypeSymbol candidate &&
		    candidate.OriginalDefinition.IsAssignableFrom(genericTypeSymbol, true))
		{
			genericInstanceTypeSymbol = candidate;
			return true;
		}
		
		if (syntax is IdentifierNameSyntax identifierNameSyntax &&
		    semanticModel.GetSymbolInfo(identifierNameSyntax).Symbol is INamedTypeSymbol candidate2 &&
		    candidate2.OriginalDefinition.IsAssignableFrom(genericTypeSymbol, true))
		{
			genericInstanceTypeSymbol = candidate2;
			return true;
		}

		genericInstanceTypeSymbol = default!;
		return false;
	}

	public static AttributeSyntax? FindAttributeOfType(this MemberDeclarationSyntax syntax, INamedTypeSymbol attributeType, SemanticModel semanticModel)
	{
		return syntax
			.AttributeLists
			.SelectMany(list => list.Attributes)
			.FirstOrDefault(attribute => semanticModel.GetTypeInfo(attribute).Type is { } t && attributeType.IsAssignableFrom(t, true));
	}
	
	public static bool IsAssignableFrom(this ITypeSymbol targetType, ITypeSymbol sourceType, bool exactMatch = false)
	{
		if (exactMatch)
		{
			return SymbolEqualityComparer.Default.Equals(sourceType, targetType);
		}

		var currentType = sourceType;
		while (currentType != null)
		{
			if (SymbolEqualityComparer.Default.Equals(currentType, targetType))
			{
				return true;
			}

			if (targetType.TypeKind == TypeKind.Interface)
			{
				return currentType.AllInterfaces.Any(i => SymbolEqualityComparer.Default.Equals(i, targetType));
			}

			currentType = currentType.BaseType;
		}

		return false;
	}
}
