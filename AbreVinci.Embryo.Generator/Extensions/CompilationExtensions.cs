﻿using AbreVinci.Embryo.Generator.Entries;

namespace AbreVinci.Embryo.Generator.Extensions;

internal static class CompilationExtensions
{
	public static IEnumerable<ActionEntry> GetActionEntries(this Compilation compilation, ITypeSymbol? expectedStateOrContextType = null)
	{
		return compilation
			.GetAttributeMarkedSyntaxEntries<MethodDeclarationSyntax>("ActionAttribute", e => 
				e.Syntax.IsChainableExtensionMethod(expectedStateOrContextType, e.SemanticModel) && 
				e.Syntax.AreParametersValid(e.SemanticModel))
			.Select(e =>
			{
				var (syntax, _, semanticModel) = e;
				var parent = (ITypeSymbol)semanticModel.GetDeclaredSymbol(syntax.Parent!)!;
				var parameters = syntax.GetParameterEntries(semanticModel).Skip(1).ToImmutableArray();
				return new ActionEntry(parent.ToTypeEntry(), syntax.Identifier.ToString(), parameters);
			});
	}

	public static IEnumerable<EffectEntry> GetEffectEntries(this Compilation compilation, INamedTypeSymbol effectContextType, INamedTypeSymbol effectContextType1, INamedTypeSymbol taskType)
	{
		return compilation
			.GetAttributeMarkedSyntaxEntries<MethodDeclarationSyntax>("EffectAttribute", e => 
				e.Syntax.IsStaticMemberMethod(e.SemanticModel) && 
				e.Syntax.AreParametersValid(e.SemanticModel))
			.Select(e =>
			{
				var (syntax, _, semanticModel) = e;
				var parent = (ITypeSymbol)semanticModel.GetDeclaredSymbol(syntax.Parent!)!;

				ServiceEntry? serviceEntry = null;
				var skipFirstParameter = false;
				if (syntax.ParameterList.Parameters.FirstOrDefault()?.Type is { } typeSyntax)
				{
					if (typeSyntax.TryGetAsGenericInstanceSymbol(effectContextType, semanticModel, out var type))
						serviceEntry = new ServiceEntry(type.TypeArguments[0].ToTypeEntry(), type.TypeArguments[0].GenerateIdentifier().ToPascalCase());

					skipFirstParameter = 
						serviceEntry != null ||
						serviceEntry == null && typeSyntax.TryGetAsGenericInstanceSymbol(effectContextType1, semanticModel, out _);
				}

				var parameters = syntax.GetParameterEntries(semanticModel).Skip(skipFirstParameter ? 1 : 0).ToImmutableArray();
				return new EffectEntry(parent.ToTypeEntry(), serviceEntry, syntax.IsAsync(taskType, semanticModel), serviceEntry == null && skipFirstParameter, syntax.Identifier.ToString(), parameters);
			});
	}

	public static IEnumerable<AttributeMarkedSyntaxEntry<ClassDeclarationSyntax>> GetReducerEntries(this Compilation compilation)
	{
		return compilation.GetAttributeMarkedSyntaxEntries<ClassDeclarationSyntax>("GenerateReducerUpdateAttribute", e => e.Syntax.IsPartial());
	}

	public static IEnumerable<AttributeMarkedSyntaxEntry<ClassDeclarationSyntax>> GetCommandHandlerEntries(this Compilation compilation)
	{
		return compilation.GetAttributeMarkedSyntaxEntries<ClassDeclarationSyntax>("GenerateCommandHandlerAttribute", e => e.Syntax.IsPartial());
	}
	
	public static IEnumerable<AttributeMarkedSyntaxEntry<RecordDeclarationSyntax>> GetMessageTypes(this Compilation compilation)
	{
		return compilation.GetAttributeMarkedSyntaxEntries<RecordDeclarationSyntax>("GenerateMessageTypeAttribute", e => e.Syntax.IsPartial());
	}

	public static IEnumerable<AttributeMarkedSyntaxEntry<RecordDeclarationSyntax>> GetCommandTypes(this Compilation compilation)
	{
		return compilation.GetAttributeMarkedSyntaxEntries<RecordDeclarationSyntax>("GenerateCommandTypeAttribute", e => e.Syntax.IsPartial());
	}

	private static IEnumerable<AttributeMarkedSyntaxEntry<TSyntax>> GetAttributeMarkedSyntaxEntries<TSyntax>(this Compilation compilation, string attributeTypeName, Func<AttributeMarkedSyntaxEntry<TSyntax>, bool> filter)
		where TSyntax : MemberDeclarationSyntax
	{
		var attributeType = compilation.GetTypeByMetadataName($"AbreVinci.Embryo.Generator.Attributes.{attributeTypeName}")!;

		return compilation.SyntaxTrees
			.SelectMany(syntaxTree =>
			{
				var semanticModel = compilation.GetSemanticModel(syntaxTree);

				return syntaxTree
					.GetRoot()
					.DescendantNodesAndSelf()
					.OfType<TSyntax>()
					.Where(s => s.FindAttributeOfType(attributeType, semanticModel) != null)
					.Select(s => new AttributeMarkedSyntaxEntry<TSyntax>(s, s.FindAttributeOfType(attributeType, semanticModel)!, semanticModel))
					.Where(filter);
			});
	}
}
