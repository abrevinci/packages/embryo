﻿namespace AbreVinci.Embryo.Generator.Extensions;

internal static class IdentifierExtensions
{
	public static string ToPascalCase(this string identifier)
	{
		return identifier.Length > 0 ? identifier.Substring(0, 1).ToUpper() + identifier.Substring(1) : identifier;
	}

	public static string ToCamelCase(this string identifier)
	{
		return identifier.Length > 0 ? identifier.Substring(0, 1).ToLower() + identifier.Substring(1) : identifier;
	}
}
