﻿using AbreVinci.Embryo.WPF;

namespace AbreVinci.Embryo.Debugging.WPF;

/// <summary>
/// View resource dictionary for the embryo debugger views.
/// </summary>
public class DebuggerViewResourceDictionary : ViewResourceDictionary
{
	/// <summary>
	/// Construct the embryo debugger view resource dictionary.
	/// </summary>
	public DebuggerViewResourceDictionary() : base("pack://application:,,,/AbreVinci.Embryo.Debugging.WPF;component/Views")
	{
	}
}
