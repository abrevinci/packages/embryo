﻿using AbreVinci.Embryo.ReactiveUI;
using Embryowpf.ViewModels;

namespace Embryowpf;

public class UserInterface : ReactiveUserInterface
{
	public UserInterface(
		ViewModelFactory viewModelFactory,
		IReactiveViewModelHost viewModelHost,
		IReactiveWindowFactory windowFactory) : base(viewModelHost, windowFactory)
	{
		MainWindow = AddWindow(nameof(MainWindow), viewModelFactory.CreateMainWindowViewModel);
	}

	public IReactiveWindowController<MainWindowViewModel> MainWindow { get; }
}
