﻿using AbreVinci.Embryo.App;
using AbreVinci.Embryo.ReactiveUI;
using AbreVinci.Embryo.WPF;
using Embryowpf.Core;
#if (Counter)
#if (Commands)
using Embryowpf.Services;
#endif
#endif

namespace Embryowpf;

public static class EntryPoint
{
	[STAThread]
	public static void Main(string[] commandLineArguments)
	{
		var application = new Application();
		var scheduler = new UiScheduler(application.Dispatcher);

#if (Commands)
		var program = Program.Create<State, Message, Command>(Reducer.Init, Reducer.Update, scheduler);
#else
		var program = Program.Create<State, Message>(Reducer.Init, Reducer.Update, scheduler);
#endif

		var viewModelFactory = new ViewModelFactory(program.States, program.DispatchMessage);
		var viewModelHost = new ReactiveViewModelHost(scheduler, false);
		var windowFactory = new WindowFactory();
		using var userInterface = new UserInterface(viewModelFactory, viewModelHost, windowFactory);

#if (Commands)
#if (Counter)
		var persistanceService = new PersistanceService();
		using var commandHandler = new CommandHandler(program.Commands, program.DispatchMessage, persistanceService);
#else
		using var commandHandler = new CommandHandler(program.Commands, program.DispatchMessage);
#endif
#endif
		application.Run();
	}
}
