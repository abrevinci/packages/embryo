﻿using Embryowpf.Core;
using Embryowpf.ViewModels;

namespace Embryowpf;

public class ViewModelFactory
{
	private readonly IObservable<State> _state;
	private readonly Action<Message> _dispatch;

	public ViewModelFactory(IObservable<State> state, Action<Message> dispatch)
	{
		_state = state;
		_dispatch = dispatch;
	}

	public MainWindowViewModel CreateMainWindowViewModel()
	{
#if (Counter)
#if (Commands)
		var counter = _state.Select(s => s.Counter);
		var canSave = _state.Select(s => !s.IsSaving);
		return new MainWindowViewModel(counter, canSave, _dispatch);
#else
		var counter = _state.Select(s => s.Counter);
		return new MainWindowViewModel(counter, _dispatch);
#endif
#else
		return new MainWindowViewModel();
#endif
	}
}
