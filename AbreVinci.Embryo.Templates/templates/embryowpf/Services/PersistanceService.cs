﻿#if (Counter && Commands)
using Embryowpf.Core.Services;

namespace Embryowpf.Services;

public class PersistanceService : IPersistanceService
{
	public Task SaveAsync(int value)
	{
#if (Framework == "net472")
		File.WriteAllText("counter.txt", $"Counter = {value}");
		return Task.CompletedTask;
#else
		return File.WriteAllTextAsync("counter.txt", $"Counter = {value}");
#endif
	}
}
#endif
