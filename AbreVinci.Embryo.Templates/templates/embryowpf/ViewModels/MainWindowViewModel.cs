﻿using AbreVinci.Embryo.ReactiveUI;
#if (Counter)
using Embryowpf.Core;

namespace Embryowpf.ViewModels;

public class MainWindowViewModel : ReactiveViewModel
#if (Fody)
{
#else
{
	private readonly IReactiveOneWayBinding<int> _counter;

#endif
#if (Commands)
	public MainWindowViewModel(IObservable<int> counter, IObservable<bool> canSave, Action<Message> dispatch)
#else
	public MainWindowViewModel(IObservable<int> counter, Action<Message> dispatch)
#endif
	{
#if (Fody)
		Counter = BindOneWay(counter);
#else
		_counter = CreateOneWayBinding(counter, nameof(Counter));
#endif

		Increment = BindCommand(() => dispatch(new Message.Increment()));
#if (Commands)
		Save = BindCommand(() => dispatch(new Message.Save()), canSave);
#endif
	}

#if (Fody)
	public int Counter { get; }
#else
	public int Counter => _counter.Value;
#endif

	public ICommand Increment { get; }
#if (Commands)
	public ICommand Save { get; }
#endif
}
#else

namespace Embryowpf.ViewModels;

public class MainWindowViewModel : ReactiveViewModel
{
}
#endif
