﻿#if (Commands)
#if (Generator)
using AbreVinci.Embryo.Generator.Attributes;
#else
using AbreVinci.Embryo.Core;
#if (Counter)
using Embryowpf.Core.Services;
#endif
#endif

namespace Embryowpf.Core;

#if (Generator)
[GenerateCommandHandler(typeof(Command), typeof(Message))]
public partial class CommandHandler
{
}
#else
#if (Counter)
public class CommandHandler : IDisposable
{
	private readonly IDisposable _subscription;

	public CommandHandler(IObservable<Command> commands, Action<Message> dispatch, IPersistanceService persistanceService)
	{
		Context = new EffectContext<Message>(dispatch);
		PersistanceServiceContext = new EffectContext<IPersistanceService, Message>(persistanceService, dispatch);

		_subscription = commands.Subscribe(command => ExecuteAsync(command));
	}

	private EffectContext<Message> Context { get; }
	private EffectContext<IPersistanceService, Message> PersistanceServiceContext { get; }

	private Task ExecuteAsync(Command command)
	{
		return command switch
		{
			Command.SaveAsync(var value) => Effects.SaveAsync(PersistanceServiceContext, value),
			_ => Task.CompletedTask
		};
	}

	private Task WrapAsAsync(Action action)
	{
		action();
		return Task.CompletedTask;
	}

	public void Dispose()
	{
		_subscription.Dispose();
	}
}
#else
public class CommandHandler : IDisposable
{
	private readonly IDisposable _subscription;

	public CommandHandler(IObservable<Command> commands, Action<Message> dispatch)
	{
		Context = new EffectContext<Message>(dispatch);

		_subscription = commands.Subscribe(command => ExecuteAsync(command));
	}

	private EffectContext<Message> Context { get; }

	private Task ExecuteAsync(Command command)
	{
		return command switch
		{
			// Todo: Add your switch cases here
			_ => Task.CompletedTask
		};
	}

	private Task WrapAsAsync(Action action)
	{
		action();
		return Task.CompletedTask;
	}

	public void Dispose()
	{
		_subscription.Dispose();
	}
}
#endif
#endif
#endif
