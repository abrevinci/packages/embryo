﻿#if (Generator)
using AbreVinci.Embryo.Generator.Attributes;

namespace Embryowpf.Core;
#else
namespace Embryowpf.Core;
#endif

#if (Commands)
#if (Generator)
[GenerateReducerUpdate(typeof(State), typeof(Message), typeof(Command))]
public static partial class Reducer
{
	public static ActionContext Init() => new (State.InitialState);
}
#else
public static class Reducer
{
	public static ActionContext Init() => new (State.InitialState);

	public static ActionContext Update(this ActionContext context, Message message)
	{
		return message switch
		{
#if (Counter)
			Message.Increment => context.Increment(),
			Message.Save => context.Save(),
			Message.OnSaveComplete => context.OnSaveComplete(),
#else
			// Todo: Add your switch cases here
#endif
			_ => context
		};
	}
}
#endif
#else
#if (Generator)
[GenerateReducerUpdate(typeof(State), typeof(Message))]
public static partial class Reducer
{
	public static State Init() => State.InitialState;
}
#else
public static class Reducer
{
	public static State Init() => State.InitialState;

	public static State Update(this State state, Message message)
	{
		return message switch
		{
#if (Counter)
			Message.Increment => state.Increment(),
#else
			// Todo: Add your switch cases here
#endif
			_ => state
		};
	}
}
#endif
#endif
