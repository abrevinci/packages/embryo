﻿#if (Generator)
using AbreVinci.Embryo.Generator.Attributes;

namespace Embryowpf.Core;
#else
namespace Embryowpf.Core;
#endif

public static class Actions
{
#if (Counter)
#if (Commands)
#if (Generator)
	[Action]
#endif
	public static ActionContext Increment(this ActionContext context)
	{
		var state = context.State;
		var newState = state with
		{
			Counter = state.Counter + 1
		};
		return context.WithState(newState);
	}

#if (Generator)
	[Action]
#endif
	public static ActionContext Save(this ActionContext context)
	{
		var state = context.State;
		var newState = state with
		{
			IsSaving = true
		};
		return context.WithState(newState).WithCommand(new Command.SaveAsync(state.Counter));
	}

#if (Generator)
	[Action]
#endif
	public static ActionContext OnSaveComplete(this ActionContext context)
	{
		var state = context.State;
		var newState = state with
		{
			IsSaving = false
		};
		return context.WithState(newState);
	}
#else
#if (Generator)
	[Action]
#endif
	public static State Increment(this State state)
	{
		var newState = state with
		{
			Counter = state.Counter + 1
		};
		return newState;
	}
#endif
#endif
}
