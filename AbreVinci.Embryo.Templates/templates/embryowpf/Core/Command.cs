﻿#if (Commands)
#if (Generator)
using AbreVinci.Embryo.Generator.Attributes;

namespace Embryowpf.Core;
#else
namespace Embryowpf.Core;
#endif

#if (Generator)
[GenerateCommandType]
public abstract partial record Command;
#else
public abstract record Command
{
#if (Counter)
	public sealed record SaveAsync(int Value) : Command;

#endif
	// prevent external inheritance
	private Command() { }
}
#endif
#endif
