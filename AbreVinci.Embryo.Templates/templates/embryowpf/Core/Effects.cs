﻿#if (Commands)
#if (Counter)
using AbreVinci.Embryo.Core;
#if (Generator)
using AbreVinci.Embryo.Generator.Attributes;
#endif
using Embryowpf.Core.Services;

namespace Embryowpf.Core;
#else
namespace Embryowpf.Core;
#endif

public static class Effects
{
#if (Counter)
#if (Generator)
	[Effect]
#endif
	public static Task SaveAsync(EffectContext<IPersistanceService, Message> context, int value)
	{
		return context.ExecuteAsync(
			service => service.SaveAsync(value),
			createOnCompletionMessage: () => new Message.OnSaveComplete());
	}
#endif
}
#endif
