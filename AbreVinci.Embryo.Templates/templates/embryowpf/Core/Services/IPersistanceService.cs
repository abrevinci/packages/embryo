﻿#if (Counter && Commands)
namespace Embryowpf.Core.Services;

public interface IPersistanceService
{
	Task SaveAsync(int value);
}
#endif
