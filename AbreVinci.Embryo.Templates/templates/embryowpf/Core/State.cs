﻿namespace Embryowpf.Core;

#if (Counter)
#if (Commands)
public record State(int Counter, bool IsSaving)
{
	public static readonly State InitialState = new(0, false);
}
#else
public record State(int Counter)
{
	public static readonly State InitialState = new(0);
}
#endif
#else
public record State
{
	public static readonly State InitialState = new();
}
#endif
