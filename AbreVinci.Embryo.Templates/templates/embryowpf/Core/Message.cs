﻿#if (Generator)
using AbreVinci.Embryo.Generator.Attributes;

namespace Embryowpf.Core;
#else
namespace Embryowpf.Core;
#endif

#if (Generator)
[GenerateMessageType]
public abstract partial record Message;
#else
public abstract record Message
{
#if (Counter)
	public sealed record Increment : Message;
#if (Commands)
	public sealed record Save : Message;
	public sealed record OnSaveComplete : Message;
#endif

#endif
	// prevent external inheritance
	private Message() {}
}
#endif
