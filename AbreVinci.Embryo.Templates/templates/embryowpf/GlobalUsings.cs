﻿global using System;
#if (Counter)
#if (Commands)
global using System.IO;
#endif
#endif
global using System.Reactive.Concurrency;
#if (Counter)
global using System.Reactive.Linq;
#endif
#if (Commands)
global using System.Threading.Tasks;
#endif
global using System.Windows;
#if (Counter)
global using System.Windows.Input;
#endif

#if (Commands)
global using ActionContext = AbreVinci.Embryo.Core.ActionContext<Embryowpf.Core.State, Embryowpf.Core.Command>;
#endif

#if (Framework == "net472")
// ReSharper disable once CheckNamespace
namespace System.Runtime.CompilerServices
{
	internal static class IsExternalInit { }
}
#endif
 