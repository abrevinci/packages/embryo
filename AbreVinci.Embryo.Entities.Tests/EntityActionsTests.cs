﻿using AbreVinci.Embryo.Core;

namespace AbreVinci.Embryo.Entities.Tests;

public class EntityActionsTests
{
	private record AppState(EntityState<int, string, string> Entities);
	// ReSharper disable once NotAccessedPositionalProperty.Local
	private record AppCommand(EntityCommand Command);

    [Fact]
    public void UpdateEntityState_ShouldNotChangeStateButYieldLoadCommandWhenPassedLoadMessage()
    {
        var message = new EntityMessage.Load();
        var entityState = new EntityState<int, string, string>();
        var state = new AppState(entityState);
        var context = new ActionContext<AppState, AppCommand>(state);

        var (newState, commands) = context.UpdateEntityState(message, s => s.Entities, (s, e) => s with { Entities = e }, c => new AppCommand(c));

        newState.Should().BeSameAs(state);
        commands.Should().ContainSingle().Which.Should().Be(new AppCommand(new EntityCommand.Load()));
    }

    [Fact]
    public void UpdateEntityState_ShouldUpdateEntityStateButNotYieldAnyCommandsWhenPassedOnLoadStartedMessage()
    {
        var command = new EntityCommand.Load();
        var message = new EntityMessage.OnLoadStarted(command);
        var entityState = new EntityState<int, string, string>();
        var state = new AppState(entityState);
        var context = new ActionContext<AppState, AppCommand>(state);

        var (newState, commands) = context.UpdateEntityState(message, s => s.Entities, (s, e) => s with { Entities = e }, c => new AppCommand(c));

        newState.Entities.Should().BeEquivalentTo(entityState.OnCommandStarted(command));
        commands.Should().BeEmpty();
    }

    [Fact]
    public void UpdateEntityState_ShouldUpdateEntityStateButNotYieldAnyCommandsWhenPassedOnLoadSucceededMessage()
    {
        var command = new EntityCommand.Load();
        var message = new EntityMessage.OnLoadSucceeded<int, string>(command, ImmutableArray.Create((1, "One")));
        var entityState = new EntityState<int, string, string>().OnCommandStarted(command);
        var state = new AppState(entityState);
        var context = new ActionContext<AppState, AppCommand>(state);

        var (newState, commands) = context.UpdateEntityState(message, s => s.Entities, (s, e) => s with { Entities = e }, c => new AppCommand(c));

        newState.Entities.Should().BeEquivalentTo(entityState.OnLoadSucceeded(command, ImmutableArray.Create((1, "One"))));
        commands.Should().BeEmpty();
    }

	[Fact]
	public void UpdateEntityState_ShouldUpdateEntityStateButNotYieldAnyCommandsWhenPassedOnLoadFailedMessage()
	{
		var command = new EntityCommand.Load();
		var message = new EntityMessage.OnLoadFailed(command, new Exception("Bad bad panda"));
		var entityState = new EntityState<int, string, string>().OnCommandStarted(command);
		var state = new AppState(entityState);
		var context = new ActionContext<AppState, AppCommand>(state);

		var (newState, commands) = context.UpdateEntityState(message, s => s.Entities, (s, e) => s with { Entities = e }, c => new AppCommand(c));

		newState.Entities.Should().BeEquivalentTo(entityState.OnCommandFailed(command, new Exception("Bad bad panda")));
		commands.Should().BeEmpty();
	}

	[Fact]
	public void UpdateEntityState_ShouldNotChangeStateButYieldCreateCommandWhenPassedCreateMessage()
	{
		var message = new EntityMessage.Create<string>("Input");
		var entityState = new EntityState<int, string, string>();
		var state = new AppState(entityState);
		var context = new ActionContext<AppState, AppCommand>(state);

		var (newState, commands) = context.UpdateEntityState(message, s => s.Entities, (s, e) => s with { Entities = e }, c => new AppCommand(c));

		newState.Should().BeSameAs(state);
		commands.Should().ContainSingle().Which.Should().Be(new AppCommand(new EntityCommand.Create<string>("Input")));
	}

	[Fact]
	public void UpdateEntityState_ShouldUpdateEntityStateButNotYieldAnyCommandsWhenPassedOnCreateStartedMessage()
	{
		var command = new EntityCommand.Create<string>("Input");
		var message = new EntityMessage.OnCreateStarted<string>(command);
		var entityState = new EntityState<int, string, string>();
		var state = new AppState(entityState);
		var context = new ActionContext<AppState, AppCommand>(state);

		var (newState, commands) = context.UpdateEntityState(message, s => s.Entities, (s, e) => s with { Entities = e }, c => new AppCommand(c));

		newState.Entities.Should().BeEquivalentTo(entityState.OnCommandStarted(command));
		commands.Should().BeEmpty();
	}

	[Fact]
	public void UpdateEntityState_ShouldUpdateEntityStateButNotYieldAnyCommandsWhenPassedOnCreateSucceededMessage()
	{
		var command = new EntityCommand.Create<string>("Input");
		var message = new EntityMessage.OnCreateSucceeded<int, string, string>(command, 1, "One");
		var entityState = new EntityState<int, string, string>().OnCommandStarted(command);
		var state = new AppState(entityState);
		var context = new ActionContext<AppState, AppCommand>(state);

		var (newState, commands) = context.UpdateEntityState(message, s => s.Entities, (s, e) => s with { Entities = e }, c => new AppCommand(c));

		newState.Entities.Should().BeEquivalentTo(entityState.OnCreateSucceeded(command, 1, "One"));
		commands.Should().BeEmpty();
	}

	[Fact]
	public void UpdateEntityState_ShouldUpdateEntityStateButNotYieldAnyCommandsWhenPassedOnCreateFailedMessage()
	{
		var command = new EntityCommand.Create<string>("Input");
		var message = new EntityMessage.OnCreateFailed<string>(command, new Exception("Bad bad panda"));
		var entityState = new EntityState<int, string, string>().OnCommandStarted(command);
		var state = new AppState(entityState);
		var context = new ActionContext<AppState, AppCommand>(state);

		var (newState, commands) = context.UpdateEntityState(message, s => s.Entities, (s, e) => s with { Entities = e }, c => new AppCommand(c));

		newState.Entities.Should().BeEquivalentTo(entityState.OnCommandFailed(command, new Exception("Bad bad panda")));
		commands.Should().BeEmpty();
	}

	[Fact]
	public void UpdateEntityState_ShouldNotChangeStateButYieldCreateCommandWhenPassedUpdateMessage()
	{
		var createCommand = new EntityCommand.Create<string>("One");
		var message = new EntityMessage.Update<int, string>(1, "Two");
		var entityState = new EntityState<int, string, string>()
			.OnCommandStarted(createCommand)
			.OnCreateSucceeded(createCommand, 1, "One");
		var state = new AppState(entityState);
		var context = new ActionContext<AppState, AppCommand>(state);

		var (newState, commands) = context.UpdateEntityState(message, s => s.Entities, (s, e) => s with { Entities = e }, c => new AppCommand(c));

		newState.Should().BeSameAs(state);
		commands.Should().ContainSingle().Which.Should().Be(new AppCommand(new EntityCommand.Update<int, string>(1, "Two")));
	}

	[Fact]
	public void UpdateEntityState_ShouldUpdateEntityStateButNotYieldAnyCommandsWhenPassedOnUpdateStartedMessage()
	{
		var createCommand = new EntityCommand.Create<string>("One");
		var command = new EntityCommand.Update<int, string>(1, "Two");
		var message = new EntityMessage.OnUpdateStarted<int, string>(command);
		var entityState = new EntityState<int, string, string>()
			.OnCommandStarted(createCommand)
			.OnCreateSucceeded(createCommand, 1, "One");
		var state = new AppState(entityState);
		var context = new ActionContext<AppState, AppCommand>(state);

		var (newState, commands) = context.UpdateEntityState(message, s => s.Entities, (s, e) => s with { Entities = e }, c => new AppCommand(c));

		newState.Entities.Should().BeEquivalentTo(entityState.OnCommandStarted(command));
		commands.Should().BeEmpty();
	}

	[Fact]
	public void UpdateEntityState_ShouldUpdateEntityStateButNotYieldAnyCommandsWhenPassedOnUpdateSucceededMessage()
	{
		var createCommand = new EntityCommand.Create<string>("One");
		var command = new EntityCommand.Update<int, string>(1, "Two");
		var message = new EntityMessage.OnUpdateSucceeded<int, string>(command, "Two");
		var entityState = new EntityState<int, string, string>()
			.OnCommandStarted(createCommand)
			.OnCreateSucceeded(createCommand, 1, "One")
			.OnCommandStarted(command);
		var state = new AppState(entityState);
		var context = new ActionContext<AppState, AppCommand>(state);

		var (newState, commands) = context.UpdateEntityState(message, s => s.Entities, (s, e) => s with { Entities = e }, c => new AppCommand(c));

		newState.Entities.Should().BeEquivalentTo(entityState.OnUpdateSucceeded(command, "Two"));
		commands.Should().BeEmpty();
	}

	[Fact]
	public void UpdateEntityState_ShouldUpdateEntityStateButNotYieldAnyCommandsWhenPassedOnUpdateFailedMessage()
	{
		var createCommand = new EntityCommand.Create<string>("One");
		var command = new EntityCommand.Update<int, string>(1, "Two");
		var message = new EntityMessage.OnUpdateFailed<int, string>(command, new Exception("Bad bad panda"));
		var entityState = new EntityState<int, string, string>()
			.OnCommandStarted(createCommand)
			.OnCreateSucceeded(createCommand, 1, "One")
			.OnCommandStarted(command);
		var state = new AppState(entityState);
		var context = new ActionContext<AppState, AppCommand>(state);

		var (newState, commands) = context.UpdateEntityState(message, s => s.Entities, (s, e) => s with { Entities = e }, c => new AppCommand(c));

		newState.Entities.Should().BeEquivalentTo(entityState.OnCommandFailed(command, new Exception("Bad bad panda")));
		commands.Should().BeEmpty();
	}

	[Fact]
	public void UpdateEntityState_ShouldNotChangeStateButYieldCreateCommandWhenPassedDeleteMessage()
	{
		var createCommand = new EntityCommand.Create<string>("One");
		var message = new EntityMessage.Delete<int>(1);
		var entityState = new EntityState<int, string, string>()
			.OnCommandStarted(createCommand)
			.OnCreateSucceeded(createCommand, 1, "One");
		var state = new AppState(entityState);
		var context = new ActionContext<AppState, AppCommand>(state);

		var (newState, commands) = context.UpdateEntityState(message, s => s.Entities, (s, e) => s with { Entities = e }, c => new AppCommand(c));

		newState.Should().BeSameAs(state);
		commands.Should().ContainSingle().Which.Should().Be(new AppCommand(new EntityCommand.Delete<int>(1)));
	}

	[Fact]
	public void UpdateEntityState_ShouldUpdateEntityStateButNotYieldAnyCommandsWhenPassedOnDeleteStartedMessage()
	{
		var createCommand = new EntityCommand.Create<string>("One");
		var command = new EntityCommand.Delete<int>(1);
		var message = new EntityMessage.OnDeleteStarted<int>(command);
		var entityState = new EntityState<int, string, string>()
			.OnCommandStarted(createCommand)
			.OnCreateSucceeded(createCommand, 1, "One");
		var state = new AppState(entityState);
		var context = new ActionContext<AppState, AppCommand>(state);

		var (newState, commands) = context.UpdateEntityState(message, s => s.Entities, (s, e) => s with { Entities = e }, c => new AppCommand(c));

		newState.Entities.Should().BeEquivalentTo(entityState.OnCommandStarted(command));
		commands.Should().BeEmpty();
	}

	[Fact]
	public void UpdateEntityState_ShouldUpdateEntityStateButNotYieldAnyCommandsWhenPassedOnDeleteSucceededMessage()
	{
		var createCommand = new EntityCommand.Create<string>("One");
		var command = new EntityCommand.Delete<int>(1);
		var message = new EntityMessage.OnDeleteSucceeded<int>(command);
		var entityState = new EntityState<int, string, string>()
			.OnCommandStarted(createCommand)
			.OnCreateSucceeded(createCommand, 1, "One")
			.OnCommandStarted(command);
		var state = new AppState(entityState);
		var context = new ActionContext<AppState, AppCommand>(state);

		var (newState, commands) = context.UpdateEntityState(message, s => s.Entities, (s, e) => s with { Entities = e }, c => new AppCommand(c));

		newState.Entities.Should().BeEquivalentTo(entityState.OnDeleteSucceeded(command));
		commands.Should().BeEmpty();
	}

	[Fact]
	public void UpdateEntityState_ShouldUpdateEntityStateButNotYieldAnyCommandsWhenPassedOnDeleteFailedMessage()
	{
		var createCommand = new EntityCommand.Create<string>("One");
		var command = new EntityCommand.Delete<int>(1);
		var message = new EntityMessage.OnDeleteFailed<int>(command, new Exception("Bad bad panda"));
		var entityState = new EntityState<int, string, string>()
			.OnCommandStarted(createCommand)
			.OnCreateSucceeded(createCommand, 1, "One")
			.OnCommandStarted(command);
		var state = new AppState(entityState);
		var context = new ActionContext<AppState, AppCommand>(state);

		var (newState, commands) = context.UpdateEntityState(message, s => s.Entities, (s, e) => s with { Entities = e }, c => new AppCommand(c));

		newState.Entities.Should().BeEquivalentTo(entityState.OnCommandFailed(command, new Exception("Bad bad panda")));
		commands.Should().BeEmpty();
	}

	[Fact]
	public void UpdateEntityState_ShouldUpdateEntityStateButNotYieldAnyCommandsWhenPassedClearAllErrorsMessage()
	{
		var command = new EntityCommand.Create<string>("Input");
		var message = new EntityMessage.ClearAllErrors();
		var entityState = new EntityState<int, string, string>()
			.OnCommandStarted(command)
			.OnCommandFailed(command, new Exception());
		var state = new AppState(entityState);
		var context = new ActionContext<AppState, AppCommand>(state);

		var (newState, commands) = context.UpdateEntityState(message, s => s.Entities, (s, e) => s with { Entities = e }, c => new AppCommand(c));

		newState.Entities.Should().BeEquivalentTo(entityState.ClearAllErrors());
		commands.Should().BeEmpty();
	}
}
