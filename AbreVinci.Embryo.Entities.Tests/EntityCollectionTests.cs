﻿namespace AbreVinci.Embryo.Entities.Tests;

public class EntityCollectionTests
{
    [Fact]
    public void ShouldBeEmptyWithDefaultConstructor()
    {
        var collection = new EntityCollection<int, string>();

        collection.Entities.Should().BeEmpty();
        collection.Ids.Should().BeEmpty();
    }

    [Fact]
    public void Count_ShouldBe0WhenEmpty()
    {
	    var collection = new EntityCollection<int, string>();

	    collection.Count.Should().Be(0);
    }

    [Fact]
    public void Count_ShouldReflectTheNumberOfEntities()
    {
	    var collection = new EntityCollection<int, string>()
		    .AddRange(new[] { (1, "One"), (2, "Two") });

	    collection.Count.Should().Be(2);
    }

    [Fact]
    public void Add_ShouldNotModifyExistingCollection()
    {
        var collection = new EntityCollection<int, string>();

        _ = collection.Add(1, "One");

        collection.Entities.Should().BeEmpty();
        collection.Ids.Should().BeEmpty();
    }

    [Fact]
    public void Add_ShouldReturnANewCollectionWithTheGivenEntityForTheGivenId()
    {
        var collection = new EntityCollection<int, string>();

        var newCollection = collection.Add(1, "One");

        newCollection.Entities.Should().HaveCount(1);
        newCollection.Entities.Should().ContainKey(1).WhoseValue.Should().Be("One");
        newCollection.Ids.Should().ContainSingle().Which.Should().Be(1);
    }

    [Fact]
    public void AddRange_ShouldNotModifyExistingCollection()
    {
        var collection = new EntityCollection<int, string>();

        _ = collection.AddRange(new[] { (1, "One"), (2, "Two") });

        collection.Entities.Should().BeEmpty();
        collection.Ids.Should().BeEmpty();
    }

    [Fact]
    public void AddRange_ShouldReturnANewCollectionWithTheGivenEntitiesForTheirRespectiveIds()
    {
        var collection = new EntityCollection<int, string>();

        var newCollection = collection.AddRange(new[] { (1, "One"), (2, "Two") });

        newCollection.Entities.Should().HaveCount(2);
        newCollection.Entities.Should().ContainKey(1).WhoseValue.Should().Be("One");
        newCollection.Entities.Should().ContainKey(2).WhoseValue.Should().Be("Two");
        newCollection.Ids.Should().HaveCount(2);
        newCollection.Ids.Should().HaveElementAt(0, 1);
        newCollection.Ids.Should().HaveElementAt(1, 2);
    }

    [Fact]
    public void Remove_ShouldNotModifyExistingCollection()
    {
        var collection = new EntityCollection<int, string>().Add(1, "One");

        _ = collection.Remove(1);

        collection.Entities.Should().HaveCount(1);
        collection.Entities.Should().ContainKey(1).WhoseValue.Should().Be("One");
        collection.Ids.Should().ContainSingle().Which.Should().Be(1);
    }

    [Fact]
    public void Remove_ShouldReturnANewCollectionWithTheGivenIdRemoved()
    {
        var collection = new EntityCollection<int, string>().Add(1, "One");

        var newCollection = collection.Remove(1);

        newCollection.Entities.Should().BeEmpty();
        newCollection.Ids.Should().BeEmpty();
    }

    [Fact]
    public void Set_ShouldNotModifyExistingCollection()
    {
        var collection = new EntityCollection<int, string>().Add(1, "One");

        _ = collection.Set(1, "Modified");

        collection.Entities.Should().HaveCount(1);
        collection.Entities.Should().ContainKey(1).WhoseValue.Should().Be("One");
        collection.Ids.Should().ContainSingle().Which.Should().Be(1);
    }

    [Fact]
    public void Set_ShouldReturnANewCollectionWithTheGivenEntitySetForTheGivenId()
    {
        var collection = new EntityCollection<int, string>().Add(1, "One");

        var newCollection = collection.Set(1, "Modified");

        newCollection.Entities.Should().HaveCount(1);
        newCollection.Entities.Should().ContainKey(1).WhoseValue.Should().Be("Modified");
        newCollection.Ids.Should().ContainSingle().Which.Should().Be(1);
    }

    [Fact]
    public void Update_ShouldNotModifyExistingCollection()
    {
	    var collection = new EntityCollection<int, string>().Add(1, "One");

	    _ = collection.Update(1, v => v + "Updated");

	    collection.Entities.Should().HaveCount(1);
	    collection.Entities.Should().ContainKey(1).WhoseValue.Should().Be("One");
	    collection.Ids.Should().ContainSingle().Which.Should().Be(1);
    }

    [Fact]
    public void Update_ShouldReturnANewCollectionWithTheGivenEntityUpdatedForTheGivenId()
    {
	    var collection = new EntityCollection<int, string>().Add(1, "One");

	    var newCollection = collection.Update(1, v => v + "Updated");

	    newCollection.Entities.Should().HaveCount(1);
	    newCollection.Entities.Should().ContainKey(1).WhoseValue.Should().Be("OneUpdated");
	    newCollection.Ids.Should().ContainSingle().Which.Should().Be(1);
    }

    [Fact]
    public void TryGet_ShouldReturnFalseWhenEntityDoesNotExist()
    {
	    var collection = new EntityCollection<int, string>().Add(1, "One");

	    var exists = collection.TryGet(5, out _);

	    exists.Should().BeFalse();
    }

    [Fact]
    public void TryGet_ShouldOutputDefaultWhenEntityDoesNotExist()
    {
	    var collection = new EntityCollection<int, string>().Add(1, "One");

	    collection.TryGet(5, out var entity);

	    entity.Should().Be(default);
    }

    [Fact]
    public void TryGet_ShouldReturnTrueWhenEntityExists()
    {
	    var collection = new EntityCollection<int, string>().Add(1, "One");

	    var exists = collection.TryGet(1, out _);

	    exists.Should().BeTrue();
    }

    [Fact]
    public void TryGet_ShouldOutputEntityWhenEntityExists()
    {
	    var collection = new EntityCollection<int, string>().Add(1, "One");

	    collection.TryGet(1, out var entity);

	    entity.Should().Be("One");
    }
}
