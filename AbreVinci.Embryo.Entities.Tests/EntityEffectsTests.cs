﻿using AbreVinci.Embryo.Core;

namespace AbreVinci.Embryo.Entities.Tests;

public class EntityEffectsTests
{
	// ReSharper disable once NotAccessedPositionalProperty.Local
	private record AppMessage(EntityMessage Message);

    [Fact]
    public async Task HandleEntityCommandAsync_ShouldDispatchOnLoadStartedMessageWhenPassedLoadCommand()
    {
        var command = new EntityCommand.Load();
        var entityServiceMock = new Mock<IEntityService<int, string, string>>();
        entityServiceMock.Setup(s => s.LoadAsync()).ReturnsAsync(ImmutableArray.Create((1, "One"), (2, "Two")));
        var dispatchedMessages = new List<AppMessage>();
        var context = new EffectContext<IEntityService<int, string, string>, AppMessage>(entityServiceMock.Object, dispatchedMessages.Add);

        await context.HandleEntityCommandAsync(command, m => new AppMessage(m));

        dispatchedMessages.Should().HaveCount(2);
        dispatchedMessages[0].Should().Be(new AppMessage(new EntityMessage.OnLoadStarted(command)));
    }

    [Fact]
    public async Task HandleEntityCommandAsync_ShouldDispatchOnLoadSucceededMessageWhenPassedLoadCommandAndLoadSucceeds()
    {
        var command = new EntityCommand.Load();
        var entityServiceMock = new Mock<IEntityService<int, string, string>>();
        var loadedEntities = ImmutableArray.Create((1, "One"), (2, "Two"));
        entityServiceMock.Setup(s => s.LoadAsync()).ReturnsAsync(loadedEntities);
        var dispatchedMessages = new List<AppMessage>();
        var context = new EffectContext<IEntityService<int, string, string>, AppMessage>(entityServiceMock.Object, dispatchedMessages.Add);

        await context.HandleEntityCommandAsync(command, m => new AppMessage(m));

        dispatchedMessages.Should().HaveCount(2);
        dispatchedMessages[1].Should().Be(new AppMessage(new EntityMessage.OnLoadSucceeded<int, string>(command, loadedEntities)));
    }

    [Fact]
    public async Task HandleEntityCommandAsync_ShouldDispatchOnLoadFailedMessageWhenPassedLoadCommandAndLoadFails()
    {
        var command = new EntityCommand.Load();
        var entityServiceMock = new Mock<IEntityService<int, string, string>>();
        var exception = new Exception("Bad bad panda");
        entityServiceMock.Setup(s => s.LoadAsync()).Throws(exception);
        var dispatchedMessages = new List<AppMessage>();
        var context = new EffectContext<IEntityService<int, string, string>, AppMessage>(entityServiceMock.Object, dispatchedMessages.Add);

        await context.HandleEntityCommandAsync(command, m => new AppMessage(m));

        dispatchedMessages.Should().HaveCount(2);
        dispatchedMessages[1].Should().Be(new AppMessage(new EntityMessage.OnLoadFailed(command, exception)));
    }

    [Fact]
    public async Task HandleEntityCommandAsync_ShouldDispatchOnCreateStartedMessageWhenPassedCreateCommand()
    {
        var command = new EntityCommand.Create<string>("Input");
        var entityServiceMock = new Mock<IEntityService<int, string, string>>();
        entityServiceMock.Setup(s => s.CreateAsync("Input")).ReturnsAsync((1, "One"));
        var dispatchedMessages = new List<AppMessage>();
        var context = new EffectContext<IEntityService<int, string, string>, AppMessage>(entityServiceMock.Object, dispatchedMessages.Add);

        await context.HandleEntityCommandAsync(command, m => new AppMessage(m));

        dispatchedMessages.Should().HaveCount(2);
        dispatchedMessages[0].Should().Be(new AppMessage(new EntityMessage.OnCreateStarted<string>(command)));
    }

    [Fact]
    public async Task HandleEntityCommandAsync_ShouldDispatchOnCreateSucceededMessageWhenPassedCreateCommandAndCreateSucceeds()
    {
        var command = new EntityCommand.Create<string>("Input");
        var entityServiceMock = new Mock<IEntityService<int, string, string>>();
        entityServiceMock.Setup(s => s.CreateAsync("Input")).ReturnsAsync((1, "One"));
        var dispatchedMessages = new List<AppMessage>();
        var context = new EffectContext<IEntityService<int, string, string>, AppMessage>(entityServiceMock.Object, dispatchedMessages.Add);

        await context.HandleEntityCommandAsync(command, m => new AppMessage(m));

        dispatchedMessages.Should().HaveCount(2);
        dispatchedMessages[1].Should().Be(new AppMessage(new EntityMessage.OnCreateSucceeded<int, string, string>(command, 1, "One")));
    }

    [Fact]
    public async Task HandleEntityCommandAsync_ShouldDispatchOnCreateFailedMessageWhenPassedCreateCommandAndCreateFails()
    {
        var command = new EntityCommand.Create<string>("Input");
        var entityServiceMock = new Mock<IEntityService<int, string, string>>();
        var exception = new Exception("Bad bad panda");
        entityServiceMock.Setup(s => s.CreateAsync("Input")).Throws(exception);
        var dispatchedMessages = new List<AppMessage>();
        var context = new EffectContext<IEntityService<int, string, string>, AppMessage>(entityServiceMock.Object, dispatchedMessages.Add);

        await context.HandleEntityCommandAsync(command, m => new AppMessage(m));

        dispatchedMessages.Should().HaveCount(2);
        dispatchedMessages[1].Should().Be(new AppMessage(new EntityMessage.OnCreateFailed<string>(command, exception)));
    }

    [Fact]
    public async Task HandleEntityCommandAsync_ShouldDispatchOnUpdateStartedMessageWhenPassedUpdateCommand()
    {
        var command = new EntityCommand.Update<int, string>(1, "Two");
        var entityServiceMock = new Mock<IEntityService<int, string, string>>();
        entityServiceMock.Setup(s => s.UpdateAsync(1, "Two")).ReturnsAsync("Two");
        var dispatchedMessages = new List<AppMessage>();
        var context = new EffectContext<IEntityService<int, string, string>, AppMessage>(entityServiceMock.Object, dispatchedMessages.Add);

        await context.HandleEntityCommandAsync(command, m => new AppMessage(m));

        dispatchedMessages.Should().HaveCount(2);
        dispatchedMessages[0].Should().Be(new AppMessage(new EntityMessage.OnUpdateStarted<int, string>(command)));
    }

    [Fact]
    public async Task HandleEntityCommandAsync_ShouldDispatchOnUpdateSucceededMessageWhenPassedUpdateCommandAndUpdateSucceeds()
    {
        var command = new EntityCommand.Update<int, string>(1, "Two");
        var entityServiceMock = new Mock<IEntityService<int, string, string>>();
        entityServiceMock.Setup(s => s.UpdateAsync(1, "Two")).ReturnsAsync("Two");
        var dispatchedMessages = new List<AppMessage>();
        var context = new EffectContext<IEntityService<int, string, string>, AppMessage>(entityServiceMock.Object, dispatchedMessages.Add);

        await context.HandleEntityCommandAsync(command, m => new AppMessage(m));

        dispatchedMessages.Should().HaveCount(2);
        dispatchedMessages[1].Should().Be(new AppMessage(new EntityMessage.OnUpdateSucceeded<int, string>(command, "Two")));
    }

    [Fact]
    public async Task HandleEntityCommandAsync_ShouldDispatchOnUpdateFailedMessageWhenPassedUpdateCommandAndUpdateFails()
    {
        var command = new EntityCommand.Update<int, string>(1, "Two");
        var entityServiceMock = new Mock<IEntityService<int, string, string>>();
        var exception = new Exception("Bad bad panda");
        entityServiceMock.Setup(s => s.UpdateAsync(1, "Two")).Throws(exception);
        var dispatchedMessages = new List<AppMessage>();
        var context = new EffectContext<IEntityService<int, string, string>, AppMessage>(entityServiceMock.Object, dispatchedMessages.Add);

        await context.HandleEntityCommandAsync(command, m => new AppMessage(m));

        dispatchedMessages.Should().HaveCount(2);
        dispatchedMessages[1].Should().Be(new AppMessage(new EntityMessage.OnUpdateFailed<int, string>(command, exception)));
    }

    [Fact]
    public async Task HandleEntityCommandAsync_ShouldDispatchOnDeleteStartedMessageWhenPassedDeleteCommand()
    {
        var command = new EntityCommand.Delete<int>(1);
        var entityServiceMock = new Mock<IEntityService<int, string, string>>();
        entityServiceMock.Setup(s => s.DeleteAsync(1)).Returns(Task.CompletedTask);
        var dispatchedMessages = new List<AppMessage>();
        var context = new EffectContext<IEntityService<int, string, string>, AppMessage>(entityServiceMock.Object, dispatchedMessages.Add);

        await context.HandleEntityCommandAsync(command, m => new AppMessage(m));

        dispatchedMessages.Should().HaveCount(2);
        dispatchedMessages[0].Should().Be(new AppMessage(new EntityMessage.OnDeleteStarted<int>(command)));
    }

    [Fact]
    public async Task HandleEntityCommandAsync_ShouldDispatchOnDeleteSucceededMessageWhenPassedDeleteCommandAndDeleteSucceeds()
    {
        var command = new EntityCommand.Delete<int>(1);
        var entityServiceMock = new Mock<IEntityService<int, string, string>>();
        entityServiceMock.Setup(s => s.DeleteAsync(1)).Returns(Task.CompletedTask);
        var dispatchedMessages = new List<AppMessage>();
        var context = new EffectContext<IEntityService<int, string, string>, AppMessage>(entityServiceMock.Object, dispatchedMessages.Add);

        await context.HandleEntityCommandAsync(command, m => new AppMessage(m));

        dispatchedMessages.Should().HaveCount(2);
        dispatchedMessages[1].Should().Be(new AppMessage(new EntityMessage.OnDeleteSucceeded<int>(command)));
    }

    [Fact]
    public async Task HandleEntityCommandAsync_ShouldDispatchOnDeleteFailedMessageWhenPassedDeleteCommandAndDeleteFails()
    {
        var command = new EntityCommand.Delete<int>(1);
        var entityServiceMock = new Mock<IEntityService<int, string, string>>();
        var exception = new Exception("Bad bad panda");
        entityServiceMock.Setup(s => s.DeleteAsync(1)).Throws(exception);
        var dispatchedMessages = new List<AppMessage>();
        var context = new EffectContext<IEntityService<int, string, string>, AppMessage>(entityServiceMock.Object, dispatchedMessages.Add);

        await context.HandleEntityCommandAsync(command, m => new AppMessage(m));

        dispatchedMessages.Should().HaveCount(2);
        dispatchedMessages[1].Should().Be(new AppMessage(new EntityMessage.OnDeleteFailed<int>(command, exception)));
    }
}
