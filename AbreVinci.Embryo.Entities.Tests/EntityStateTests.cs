﻿namespace AbreVinci.Embryo.Entities.Tests;

public class EntityStateTests
{
    [Fact]
    public void ShouldBeEmptyWithDefaultConstructor()
    {
        var entityState = new EntityState<int, string, string>();

        entityState.Collection.Entities.Should().BeEmpty();
        entityState.Collection.Ids.Should().BeEmpty();
        entityState.ActiveCommands.Should().BeEmpty();
        entityState.CommandErrors.Should().BeEmpty();
    }

    [Fact]
    public void OnCommandStarted_ShouldNotModifyExistingState()
    {
        var entityState = new EntityState<int, string, string>();

        _ = entityState.OnCommandStarted(new EntityCommand.Load());

        entityState.Collection.Entities.Should().BeEmpty();
        entityState.Collection.Ids.Should().BeEmpty();
        entityState.ActiveCommands.Should().BeEmpty();
        entityState.CommandErrors.Should().BeEmpty();
    }

    [Fact]
    public void OnCommandStarted_ShouldAddTheCommandToActiveCommands()
    {
        var entityState = new EntityState<int, string, string>();
        var command = new EntityCommand.Load();

        var newEntityState = entityState.OnCommandStarted(command);

        newEntityState.Collection.Should().BeSameAs(entityState.Collection);
        newEntityState.ActiveCommands.Should().ContainSingle().Which.Should().BeSameAs(command);
        newEntityState.CommandErrors.Should().BeSameAs(entityState.CommandErrors);
    }

    [Fact]
    public void OnLoadSucceeded_ShouldNotModifyExistingState()
    {
        var command = new EntityCommand.Load();
        var entityState = new EntityState<int, string, string>().OnCommandStarted(command);

        _ = entityState.OnLoadSucceeded(command, ImmutableList.Create((1, "One"), (2, "Two")));

        entityState.Collection.Entities.Should().BeEmpty();
        entityState.Collection.Ids.Should().BeEmpty();
        entityState.ActiveCommands.Should().ContainSingle().Which.Should().BeSameAs(command);
        entityState.CommandErrors.Should().BeEmpty();
    }

    [Fact]
    public void OnLoadSucceeded_ShouldReturnANewStateWithTheGivenEntitiesAddedToCollectionAndCommandRemovedFromActiveCommands()
    {
        var command = new EntityCommand.Load();
        var entityState = new EntityState<int, string, string>().OnCommandStarted(command);

        var newEntityState = entityState.OnLoadSucceeded(command, ImmutableList.Create((1, "One"), (2, "Two")));

        newEntityState.Collection.Entities.Should().HaveCount(2);
        newEntityState.Collection.Entities.Should().ContainKey(1).WhoseValue.Should().Be("One");
        newEntityState.Collection.Entities.Should().ContainKey(2).WhoseValue.Should().Be("Two");
        newEntityState.Collection.Ids.Should().HaveCount(2);
        newEntityState.Collection.Ids.Should().HaveElementAt(0, 1);
        newEntityState.Collection.Ids.Should().HaveElementAt(1, 2);
        newEntityState.ActiveCommands.Should().BeEmpty();
        newEntityState.CommandErrors.Should().BeSameAs(entityState.CommandErrors);
    }

    [Fact]
    public void OnCreateSucceeded_ShouldNotModifyExistingState()
    {
        var command = new EntityCommand.Create<string>("Input");
        var entityState = new EntityState<int, string, string>().OnCommandStarted(command);

        _ = entityState.OnCreateSucceeded(command, 1, "One");

        entityState.Collection.Entities.Should().BeEmpty();
        entityState.Collection.Ids.Should().BeEmpty();
        entityState.ActiveCommands.Should().ContainSingle().Which.Should().BeSameAs(command);
        entityState.CommandErrors.Should().BeEmpty();
    }

    [Fact]
    public void OnCreateSucceeded_ShouldReturnANewStateWithTheGivenEntityAddedToCollectionAndCommandRemovedFromActiveCommands()
    {
        var command = new EntityCommand.Create<string>("Input");
        var entityState = new EntityState<int, string, string>().OnCommandStarted(command);

        var newEntityState = entityState.OnCreateSucceeded(command, 1, "One");

        newEntityState.Collection.Entities.Should().HaveCount(1);
        newEntityState.Collection.Entities.Should().ContainKey(1).WhoseValue.Should().Be("One");
        newEntityState.Collection.Ids.Should().ContainSingle().Which.Should().Be(1);
        newEntityState.ActiveCommands.Should().BeEmpty();
        newEntityState.CommandErrors.Should().BeSameAs(entityState.CommandErrors);
    }

    [Fact]
    public void OnUpdateSucceeded_ShouldNotModifyExistingState()
    {
        var createCommand = new EntityCommand.Create<string>("One");
        var command = new EntityCommand.Update<int, string>(1, "Two");
        var entityState = new EntityState<int, string, string>()
            .OnCommandStarted(createCommand)
            .OnCreateSucceeded(createCommand, 1, "One")
            .OnCommandStarted(command);

        _ = entityState.OnUpdateSucceeded(command, "One");

        entityState.Collection.Entities.Should().HaveCount(1);
        entityState.Collection.Entities.Should().ContainKey(1).WhoseValue.Should().Be("One");
        entityState.Collection.Ids.Should().ContainSingle().Which.Should().Be(1);
        entityState.ActiveCommands.Should().ContainSingle().Which.Should().BeSameAs(command);
        entityState.CommandErrors.Should().BeEmpty();
    }

    [Fact]
    public void OnUpdateSucceeded_ShouldReturnANewStateWithTheGivenEntityUpdatedInCollectionAndCommandRemovedFromActiveCommands()
    {
        var createCommand = new EntityCommand.Create<string>("One");
        var command = new EntityCommand.Update<int, string>(1, "Two");
        var entityState = new EntityState<int, string, string>()
            .OnCommandStarted(createCommand)
            .OnCreateSucceeded(createCommand, 1, "One")
            .OnCommandStarted(command);

        var newEntityState = entityState.OnUpdateSucceeded(command, "Two");

        newEntityState.Collection.Entities.Should().HaveCount(1);
        newEntityState.Collection.Entities.Should().ContainKey(1).WhoseValue.Should().Be("Two");
        newEntityState.Collection.Ids.Should().ContainSingle().Which.Should().Be(1);
        newEntityState.ActiveCommands.Should().BeEmpty();
        newEntityState.CommandErrors.Should().BeSameAs(entityState.CommandErrors);
    }

    [Fact]
    public void OnDeleteSucceeded_ShouldNotModifyExistingState()
    {
        var createCommand = new EntityCommand.Create<string>("One");
        var command = new EntityCommand.Delete<int>(1);
        var entityState = new EntityState<int, string, string>()
            .OnCommandStarted(createCommand)
            .OnCreateSucceeded(createCommand, 1, "One")
            .OnCommandStarted(command);

        _ = entityState.OnDeleteSucceeded(command);

        entityState.Collection.Entities.Should().HaveCount(1);
        entityState.Collection.Entities.Should().ContainKey(1).WhoseValue.Should().Be("One");
        entityState.Collection.Ids.Should().ContainSingle().Which.Should().Be(1);
        entityState.ActiveCommands.Should().ContainSingle().Which.Should().BeSameAs(command);
        entityState.CommandErrors.Should().BeEmpty();
    }

    [Fact]
    public void OnDeleteSucceeded_ShouldReturnANewStateWithTheGivenIdRemovedFromCollectionAndCommandRemovedFromActiveCommands()
    {
        var createCommand = new EntityCommand.Create<string>("One");
        var command = new EntityCommand.Delete<int>(1);
        var entityState = new EntityState<int, string, string>()
            .OnCommandStarted(createCommand)
            .OnCreateSucceeded(createCommand, 1, "One")
            .OnCommandStarted(command);

        var newEntityState = entityState.OnDeleteSucceeded(command);

        newEntityState.Collection.Entities.Should().BeEmpty();
        newEntityState.Collection.Ids.Should().BeEmpty();
        newEntityState.ActiveCommands.Should().BeEmpty();
        newEntityState.CommandErrors.Should().BeSameAs(entityState.CommandErrors);
    }

    [Fact]
    public void OnCommandFailed_ShouldNotModifyExistingState()
    {
        var command = new EntityCommand.Load();
        var exception = new Exception("Bad bad panda");
        var entityState = new EntityState<int, string, string>().OnCommandStarted(command);

        _ = entityState.OnCommandFailed(command, exception);

        entityState.Collection.Entities.Should().BeEmpty();
        entityState.Collection.Ids.Should().BeEmpty();
        entityState.ActiveCommands.Should().ContainSingle().Which.Should().BeSameAs(command);
        entityState.CommandErrors.Should().BeEmpty();
    }

    [Fact]
    public void OnCommandFailed_ShouldReturnANewStateWithErrorAddedToCommandErrorsAndCommandRemovedFromActiveCommands()
    {
        var command = new EntityCommand.Load();
        var exception = new Exception("Bad bad panda");
        var entityState = new EntityState<int, string, string>().OnCommandStarted(command);

        var newEntityState = entityState.OnCommandFailed(command, exception);

        newEntityState.Collection.Should().BeSameAs(entityState.Collection);
        newEntityState.ActiveCommands.Should().BeEmpty();
        newEntityState.CommandErrors.Should().ContainSingle().Which.command.Should().BeSameAs(command);
        newEntityState.CommandErrors.Should().ContainSingle().Which.exception.Should().BeSameAs(exception);
    }

    [Fact]
    public void ClearAllErrors_ShouldNotModifyExistingState()
    {
        var command = new EntityCommand.Load();
        var exception = new Exception("Bad bad panda");
        var entityState = new EntityState<int, string, string>()
            .OnCommandStarted(command)
            .OnCommandFailed(command, exception);

        _ = entityState.ClearAllErrors();

        entityState.Collection.Should().BeSameAs(entityState.Collection);
        entityState.ActiveCommands.Should().BeEmpty();
        entityState.CommandErrors.Should().ContainSingle().Which.command.Should().BeSameAs(command);
        entityState.CommandErrors.Should().ContainSingle().Which.exception.Should().BeSameAs(exception);
    }

    [Fact]
    public void ClearAllErrors_ShouldReturnANewStateWithCommandErrorsCleared()
    {
        var command = new EntityCommand.Load();
        var exception = new Exception("Bad bad panda");
        var entityState = new EntityState<int, string, string>()
            .OnCommandStarted(command)
            .OnCommandFailed(command, exception);

        var newEntityState = entityState.ClearAllErrors();

        newEntityState.Collection.Should().BeSameAs(entityState.Collection);
        newEntityState.ActiveCommands.Should().BeEmpty();
        newEntityState.CommandErrors.Should().BeEmpty();
    }
}
