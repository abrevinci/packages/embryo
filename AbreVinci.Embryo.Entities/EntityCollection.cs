﻿namespace AbreVinci.Embryo.Entities;

/// <summary>
/// Common data type used for entities.
/// </summary>
/// <typeparam name="TId">The entity id type.</typeparam>
/// <typeparam name="TEntity">The entity type.</typeparam>
/// <param name="Entities">An immutable dictionary with entities by id.</param>
/// <param name="Ids">An immutable list/array of ids (to maintain an ordering which the dictionary does not provide).</param>
public record EntityCollection<TId, TEntity>(
    IImmutableDictionary<TId, TEntity> Entities,
    IImmutableList<TId> Ids) where TId : notnull
{
    /// <summary>
    /// And empty entity collection.
    /// </summary>
	public static readonly EntityCollection<TId, TEntity> Empty = new();

    /// <summary>
    /// An empty entity collection.
    /// </summary>
    public EntityCollection() : this(
        ImmutableDictionary<TId, TEntity>.Empty,
        ImmutableArray<TId>.Empty)
    { }

    /// <summary>
    /// The number of entities in the collection.
    /// </summary>
    public int Count => Entities.Count;

    /// <summary>
    /// Adds the given <paramref name="entity"/> added at <paramref name="id"/>.
    /// </summary>
    /// <param name="id">The id of the entity to add</param>
    /// <param name="entity">The entity to add</param>
    /// <returns>A new entity collection with the entity added.</returns>
    [Pure]
    public EntityCollection<TId, TEntity> Add(TId id, TEntity entity)
    {
        return this with
        {
            Entities = Entities.Add(id, entity),
            Ids = Ids.Add(id)
        };
    }

    /// <summary>
    /// Adds the given <paramref name="entities"/> at their respective ids.
    /// </summary>
    /// <param name="entities">The ids and entities to add.</param>
    /// <returns>A new entity collection with the entities added.</returns>
    [Pure]
    public EntityCollection<TId, TEntity> AddRange(IEnumerable<(TId id, TEntity entity)> entities)
    {
	    var entityCollection = entities as ICollection<(TId id, TEntity entity)> ?? entities.ToList();
	    return this with
        {
            Entities = Entities.AddRange(entityCollection.Select(pair => new KeyValuePair<TId, TEntity>(pair.id, pair.entity))),
            Ids = Ids.AddRange(entityCollection.Select(pair => pair.id))
        };
    }

    /// <summary>
    /// Removes the entity with the given <paramref name="id"/>.
    /// </summary>
    /// <param name="id">The id of the entity to remove.</param>
    /// <returns>A new entity collection with the entity removed.</returns>
    [Pure]
    public EntityCollection<TId, TEntity> Remove(TId id)
    {
        return this with
        {
            Entities = Entities.Remove(id),
            Ids = Ids.Remove(id)
        };
    }

    /// <summary>
    /// Sets the entity with the given <paramref name="id"/> to <paramref name="entity"/>.
    /// Does nothing if the id does not exist.
    /// </summary>
    /// <param name="id">The id of the entity to set.</param>
    /// <param name="entity">The entity to set.</param>
    /// <returns>A new entity collection with the entity replaced, or this if no changes were made.</returns>
    [Pure]
    public EntityCollection<TId, TEntity> Set(TId id, TEntity entity)
    {
        if (Entities.ContainsKey(id))
        {
            return this with
            {
                Entities = Entities.SetItem(id, entity)
            };
        }
        return this;
    }

    /// <summary>
    /// Updates the entity with the given <paramref name="id"/> using the transform function <paramref name="update"/>.
    /// Does nothing if the id does not exist.
    /// </summary>
    /// <param name="id">The id of the entity to update.</param>
    /// <param name="update">The entity transform function to use for computing the updated entity.</param>
    /// <returns>A new entity collection with the entity updated, or this if no changes were made.</returns>
    [Pure]
    public EntityCollection<TId, TEntity> Update(TId id, Func<TEntity, TEntity> update)
    {
	    if (Entities.TryGetValue(id, out var entity))
	    {
		    var newEntity = update(entity);
		    if (!Equals(newEntity, entity))
		    {
			    return this with
			    {
				    Entities = Entities.SetItem(id, newEntity)
			    };
		    }
	    }
	    return this;
    }

    /// <summary>
    /// Try to get the entity with the specified <paramref name="id"/>, shorthand for Entities.TryGetValue(id, out entity).
    /// </summary>
    /// <param name="id">The id of the entity to get.</param>
    /// <param name="entity">Is set to retrieved entity if found, default otherwise.</param>
    /// <returns>True if the entity is found, false otherwise.</returns>
    [Pure]
#if NET5_0
    public bool TryGet(TId id, [System.Diagnostics.CodeAnalysis.MaybeNullWhen(false)] out TEntity entity)
#else
    public bool TryGet(TId id, out TEntity entity)
#endif
    {
	    if (Entities.TryGetValue(id, out var e))
	    {
		    entity = e;
		    return true;
	    }
	    else
	    {
		    entity = default!;
		    return false;
	    }
    }
}
