﻿using AbreVinci.Embryo.Core;

namespace AbreVinci.Embryo.Entities;

/// <summary>
/// Utility actions for working with entities. Use within your own actions and/or reducers.
/// </summary>
public static class EntityActions
{
	/// <summary>
	/// Update the entity state based on the given <see cref="EntityMessage"/>. This acts like a sub-reducer.
	/// </summary>
	/// <typeparam name="TAppState">The program state type.</typeparam>
	/// <typeparam name="TAppCommand">The program command type.</typeparam>
	/// <typeparam name="TId">The entity id type.</typeparam>
	/// <typeparam name="TEntity">The entity type.</typeparam>
	/// <typeparam name="TEntityInput">The entity input type.</typeparam>
	/// <param name="context">The action context to act on.</param>
	/// <param name="message">The entity message to receive.</param>
	/// <param name="getEntityState">Should get the entity state from the application state.</param>
	/// <param name="setEntityState">Should return a new application state with the given entity state set.</param>
	/// <param name="wrapCommand">Should create an application command based on an <see cref="EntityCommand"/>.</param>
	/// <returns>A new, potentially updated action context.</returns>
	[Pure]
	public static ActionContext<TAppState, TAppCommand> UpdateEntityState<TAppState, TAppCommand, TId, TEntity, TEntityInput>(
		this ActionContext<TAppState, TAppCommand> context,
        EntityMessage message,
        Func<TAppState, EntityState<TId, TEntity, TEntityInput>> getEntityState,
		Func<TAppState, EntityState<TId, TEntity, TEntityInput>, TAppState> setEntityState,
		Func<EntityCommand, TAppCommand> wrapCommand) where TId : notnull
	{
		return message switch
		{
            EntityMessage.Load => context.WithCommand(wrapCommand(new EntityCommand.Load())),
            EntityMessage.OnLoadStarted(var command) => context.UpdateEntityState(getEntityState, setEntityState, s => s.OnCommandStarted(command)),
            EntityMessage.OnLoadSucceeded<TId, TEntity>(var command, var loadedEntities) => context.UpdateEntityState(getEntityState, setEntityState, s => s.OnLoadSucceeded(command, loadedEntities)),
            EntityMessage.OnLoadFailed(var command, var exception) => context.UpdateEntityState(getEntityState, setEntityState, s => s.OnCommandFailed(command, exception)),

            EntityMessage.Create<TEntityInput>(var input) => context.WithCommand(wrapCommand(new EntityCommand.Create<TEntityInput>(input))),
            EntityMessage.OnCreateStarted<TEntityInput>(var command) => context.UpdateEntityState(getEntityState, setEntityState, s => s.OnCommandStarted(command)),
            EntityMessage.OnCreateSucceeded<TId, TEntity, TEntityInput>(var command, var id, var entity) => context.UpdateEntityState(getEntityState, setEntityState, s => s.OnCreateSucceeded(command, id, entity)),
            EntityMessage.OnCreateFailed<TEntityInput>(var command, var exception) => context.UpdateEntityState(getEntityState, setEntityState, s => s.OnCommandFailed(command, exception)),

            EntityMessage.Update<TId, TEntity>(var id, var entity) => context.WithCommand(wrapCommand(new EntityCommand.Update<TId, TEntity>(id, entity))),
            EntityMessage.OnUpdateStarted<TId, TEntity>(var command) => context.UpdateEntityState(getEntityState, setEntityState, s => s.OnCommandStarted(command)),
            EntityMessage.OnUpdateSucceeded<TId, TEntity>(var command, var entity) => context.UpdateEntityState(getEntityState, setEntityState, s => s.OnUpdateSucceeded(command, entity)),
            EntityMessage.OnUpdateFailed<TId, TEntity>(var command, var exception) => context.UpdateEntityState(getEntityState, setEntityState, s => s.OnCommandFailed(command, exception)),

            EntityMessage.Delete<TId>(var id) => context.WithCommand(wrapCommand(new EntityCommand.Delete<TId>(id))),
            EntityMessage.OnDeleteStarted<TId>(var command) => context.UpdateEntityState(getEntityState, setEntityState, s => s.OnCommandStarted(command)),
            EntityMessage.OnDeleteSucceeded<TId>(var command) => context.UpdateEntityState(getEntityState, setEntityState, s => s.OnDeleteSucceeded(command)),
            EntityMessage.OnDeleteFailed<TId>(var command, var exception) => context.UpdateEntityState(getEntityState, setEntityState, s => s.OnCommandFailed(command, exception)),

            EntityMessage.ClearAllErrors => context.UpdateEntityState(getEntityState, setEntityState, s => s.ClearAllErrors()),

            _ => context
		};
	}
	
	[Pure]
	private static ActionContext<TAppState, TAppCommand> UpdateEntityState<TAppState, TAppCommand, TId, TEntity, TEntityInput>(
		this ActionContext<TAppState, TAppCommand> context,
		Func<TAppState, EntityState<TId, TEntity, TEntityInput>> getEntityState,
		Func<TAppState, EntityState<TId, TEntity, TEntityInput>, TAppState> setEntityState,
		Func<EntityState<TId, TEntity, TEntityInput>, EntityState<TId, TEntity, TEntityInput>> update) where TId : notnull
	{
		var state = context.State;
		var entityState = getEntityState(state);
		var newEntityState = update(entityState);
		if (newEntityState != entityState)
		{
			var newState = setEntityState(state, newEntityState);
			return context.WithState(newState);
		}

		return context;
	}
}
