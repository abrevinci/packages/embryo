﻿namespace AbreVinci.Embryo.Entities;

/// <summary>
/// Entity messages for use with <see cref="EntityState{TId, TEntity, TEntityInput}"/>.
/// </summary>
public abstract record EntityMessage
{
    /// <summary>
    /// Request a <see cref="AbreVinci.Embryo.Entities.EntityCommand.Load"/> command to be dispatched.
    /// </summary>
    public record Load : EntityMessage;

    /// <summary>
    /// Dispatched when the <see cref="AbreVinci.Embryo.Entities.EntityCommand.Load"/> command has started executing.
    /// </summary>
    /// <param name="Command">The command instance that started executing.</param>
    public record OnLoadStarted(EntityCommand.Load Command) : EntityMessage;

    /// <summary>
    /// Dispatched when the <see cref="AbreVinci.Embryo.Entities.EntityCommand.Load"/> command has succeeded and returned <paramref name="Entities"/>.
    /// </summary>
    /// <param name="Command">The command instance that succeeded executing.</param>
    /// <param name="Entities">The loaded entities together with their ids.</param>
    public record OnLoadSucceeded<TId, TEntity>(EntityCommand.Load Command, IImmutableList<(TId id, TEntity entity)> Entities) : EntityMessage where TId : notnull;

    /// <summary>
    /// Dispatched when the <see cref="AbreVinci.Embryo.Entities.EntityCommand.Load"/> command has failed with an exception.
    /// </summary>
    /// <param name="Command">The command instance that failed executing.</param>
    /// <param name="Exception">The exception.</param>
    public record OnLoadFailed(EntityCommand.Load Command, Exception Exception) : EntityMessage;

    /// <summary>
    /// Requests a <see cref="AbreVinci.Embryo.Entities.EntityCommand.Create{TEntityInput}"/> command to be dispatched with the given <paramref name="Input"/>.
    /// </summary>
    /// <typeparam name="TEntityInput">The entity input type.</typeparam>
    /// <param name="Input">The entity creation input.</param>
    public record Create<TEntityInput>(TEntityInput Input) : EntityMessage;

    /// <summary>
    /// Dispatched when the <see cref="AbreVinci.Embryo.Entities.EntityCommand.Create{TEntityInput}"/> command has started executing.
    /// </summary>
    /// <typeparam name="TEntityInput">The entity input type.</typeparam>
    /// <param name="Command">The command instance that started executing.</param>
    public record OnCreateStarted<TEntityInput>(EntityCommand.Create<TEntityInput> Command) : EntityMessage;

    /// <summary>
    /// Dispatched when the <see cref="AbreVinci.Embryo.Entities.EntityCommand.Create{TEntityInput}"/> command has succeeded and returned <paramref name="Id"/> and <paramref name="Entity"/>.
    /// </summary>
    /// <typeparam name="TId">The entity id type.</typeparam>
    /// <typeparam name="TEntity">The entity type.</typeparam>
    /// <typeparam name="TEntityInput">The entity input type.</typeparam>
    /// <param name="Command">The command instance that succeeded executing.</param>
    /// <param name="Id">The id of the created entity.</param>
    /// <param name="Entity">The created entity.</param>
    public record OnCreateSucceeded<TId, TEntity, TEntityInput>(EntityCommand.Create<TEntityInput> Command, TId Id, TEntity Entity) : EntityMessage;

    /// <summary>
    /// Dispatched when the <see cref="AbreVinci.Embryo.Entities.EntityCommand.Create{TEntityInput}"/> command has failed with an exception.
    /// </summary>
    /// <typeparam name="TEntityInput">The entity input type.</typeparam>
    /// <param name="Command">The command instance that failed executing.</param>
    /// <param name="Exception">The exception.</param>
    public record OnCreateFailed<TEntityInput>(EntityCommand.Create<TEntityInput> Command, Exception Exception) : EntityMessage;

    /// <summary>
    /// Request a <see cref="AbreVinci.Embryo.Entities.EntityCommand.Update{TId, TEntity}"/> command to be dispatched with the given <paramref name="Id"/> and <paramref name="Entity"/>.
    /// </summary>
    /// <typeparam name="TId">The entity id type.</typeparam>
    /// <typeparam name="TEntity">The entity type.</typeparam>
    /// <param name="Id">The id of the entity to update.</param>
    /// <param name="Entity">The requested new entity.</param>
    public record Update<TId, TEntity>(TId Id, TEntity Entity) : EntityMessage where TId : notnull;

    /// <summary>
    /// Dispatched when the <see cref="AbreVinci.Embryo.Entities.EntityCommand.Update{TId, TEntity}"/> command has started executing.
    /// </summary>
    /// <typeparam name="TId">The entity id type.</typeparam>
    /// <typeparam name="TEntity">The entity type.</typeparam>
    /// <param name="Command">The command instance that started executing.</param>
    public record OnUpdateStarted<TId, TEntity>(EntityCommand.Update<TId, TEntity> Command) : EntityMessage where TId : notnull;

    /// <summary>
    /// Dispatched when the <see cref="AbreVinci.Embryo.Entities.EntityCommand.Update{TId, TEntity}"/> command has succeeded and returned <paramref name="Entity"/>.
    /// </summary>
    /// <typeparam name="TId">The entity id type.</typeparam>
    /// <typeparam name="TEntity">The entity type.</typeparam>
    /// <param name="Command">The command instance that succeeded executing.</param>
    /// <param name="Entity">The updated entity.</param>
    public record OnUpdateSucceeded<TId, TEntity>(EntityCommand.Update<TId, TEntity> Command, TEntity Entity) : EntityMessage where TId : notnull;

    /// <summary>
    /// Dispatched when the <see cref="AbreVinci.Embryo.Entities.EntityCommand.Update{TId, TEntity}"/> command has failed with an exception.
    /// </summary>
    /// <typeparam name="TId">The entity id type.</typeparam>
    /// <typeparam name="TEntity">The entity type.</typeparam>
    /// <param name="Command">The command instance that failed executing.</param>
    /// <param name="Exception">The exception.</param>
    public record OnUpdateFailed<TId, TEntity>(EntityCommand.Update<TId, TEntity> Command, Exception Exception) : EntityMessage where TId : notnull;

    /// <summary>
    /// Request a <see cref="AbreVinci.Embryo.Entities.EntityCommand.Delete{TId}"/> command to be dispatched with the given <paramref name="Id"/>.
    /// </summary>
    /// <typeparam name="TId">The entity id type.</typeparam>
    /// <param name="Id">The id of the entity to delete.</param>
    public record Delete<TId>(TId Id) : EntityMessage where TId : notnull;

    /// <summary>
    /// Dispatched when the <see cref="AbreVinci.Embryo.Entities.EntityCommand.Delete{TId}"/> command has started executing.
    /// </summary>
    /// <typeparam name="TId">The entity id type.</typeparam>
    /// <param name="Command">The command instance that started executing.</param>
    public record OnDeleteStarted<TId>(EntityCommand.Delete<TId> Command) : EntityMessage where TId : notnull;

    /// <summary>
    /// Dispatched when the <see cref="AbreVinci.Embryo.Entities.EntityCommand.Delete{TId}"/> command has succeeded.
    /// </summary>
    /// <typeparam name="TId">The entity id type.</typeparam>
    /// <param name="Command">The command instance that succeeded executing.</param>
    public record OnDeleteSucceeded<TId>(EntityCommand.Delete<TId> Command) : EntityMessage where TId : notnull;

    /// <summary>
    /// Dispatched when the <see cref="AbreVinci.Embryo.Entities.EntityCommand.Delete{TId}"/> command has failed with an exception.
    /// </summary>
    /// <typeparam name="TId">The entity id type.</typeparam>
    /// <param name="Command">The command instance that failed executing.</param>
    /// <param name="Exception">The exception.</param>
    public record OnDeleteFailed<TId>(EntityCommand.Delete<TId> Command, Exception Exception) : EntityMessage where TId : notnull;
	
    /// <summary>
    /// Clear <see cref="AbreVinci.Embryo.Entities.EntityState{TId, TEntity, TEntityInput}.CommandErrors"/>.
    /// </summary>
    public record ClearAllErrors : EntityMessage;
}
