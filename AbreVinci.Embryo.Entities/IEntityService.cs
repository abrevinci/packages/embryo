﻿namespace AbreVinci.Embryo.Entities;

/// <summary>
/// Entity service interface backing the <see cref="EntityEffects"/>.
/// Implement this in your own code. Your code should throw exceptions in case of errors. 
/// The exceptions will end up in <see cref="EntityState{TId, TEntity, TEntityInput}.CommandErrors"/>.
/// </summary>
/// <typeparam name="TId">The entity id type.</typeparam>
/// <typeparam name="TEntity">The entity type.</typeparam>
/// <typeparam name="TEntityInput">The entity input type (used when creating entities).</typeparam>
public interface IEntityService<TId, TEntity, in TEntityInput> where TId : notnull
{
    /// <summary>
    /// Load entities.
    /// </summary>
    /// <returns>Am awaitable task with the loaded entities and their ids.</returns>
    Task<IImmutableList<(TId id, TEntity entity)>> LoadAsync();

    /// <summary>
    /// Create a new entity from an entity input.
    /// </summary>
    /// <param name="input">The input to use when creating the entity.</param>
    /// <returns>Am awaitable task with the entity id and entity.</returns>
    Task<(TId id, TEntity entity)> CreateAsync(TEntityInput input);

    /// <summary>
    /// Update the entity with the given id.
    /// </summary>
    /// <param name="id">The id of the entity to update.</param>
    /// <param name="entity">The requested entity state to update to.</param>
    /// <returns>Am awaitable task with the updated entity.</returns>
    Task<TEntity> UpdateAsync(TId id, TEntity entity);

    /// <summary>
    /// Delete the entity with the given id.
    /// </summary>
    /// <param name="id">The id of the entity to delete.</param>
    /// <returns>An awaitable task.</returns>
    Task DeleteAsync(TId id);
}
