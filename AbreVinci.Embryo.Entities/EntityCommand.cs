﻿namespace AbreVinci.Embryo.Entities;

/// <summary>
/// Entity commands for use with <see cref="EntityState{TId, TEntity, TEntityInput}"/>.
/// </summary>
public abstract record EntityCommand
{
    /// <summary>
    /// Request a load of entities.
    /// </summary>
    public record Load : EntityCommand;

    /// <summary>
    /// Request entity creation.
    /// </summary>
    /// <typeparam name="TEntityInput">The entity input type.</typeparam>
    /// <param name="Input">The input to use when creating the entity.</param>
    public record Create<TEntityInput>(TEntityInput Input) : EntityCommand;

    /// <summary>
    /// Request entity update.
    /// </summary>
    /// <typeparam name="TId">The entity id type.</typeparam>
    /// <typeparam name="TEntity">The entity type.</typeparam>
    /// <param name="Id">The id of the entity to update.</param>
    /// <param name="Entity">The requested new entity.</param>
    public record Update<TId, TEntity>(TId Id, TEntity Entity) : EntityCommand where TId : notnull;

    /// <summary>
    /// Request entity removal.
    /// </summary>
    /// <typeparam name="TId">The entity id type.</typeparam>
    /// <param name="Id">The id of the entity to remove.</param>
    public record Delete<TId>(TId Id) : EntityCommand where TId : notnull;
}
