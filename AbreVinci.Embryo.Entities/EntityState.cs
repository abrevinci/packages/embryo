﻿namespace AbreVinci.Embryo.Entities;

/// <summary>
/// Entity state complete with command tracking and error state. Use together with <see cref="EntityActions"/> 
/// and <see cref="EntityEffects"/>.
/// </summary>
/// <typeparam name="TId">The entity id type.</typeparam>
/// <typeparam name="TEntity">The entity type.</typeparam>
/// <typeparam name="TEntityInput">The entity input type (used when creating entities).</typeparam>
/// <param name="Collection">An <see cref="EntityCollection{TId, TEntity}"/> instance.</param>
/// <param name="ActiveCommands">A list of active commands that have been started but not yet succeeded or failed.</param>
/// <param name="CommandErrors">A list of all errors that have occurred during command handling.</param>
public record EntityState<TId, TEntity, TEntityInput>(
    EntityCollection<TId, TEntity> Collection,
    IImmutableList<EntityCommand> ActiveCommands,
    IImmutableList<(EntityCommand command, Exception exception)> CommandErrors) where TId : notnull
{
    /// <summary>
    /// Creates an empty entity state.
    /// </summary>
    public EntityState() : this(
        new EntityCollection<TId, TEntity>(), 
        ImmutableList<EntityCommand>.Empty, 
        ImmutableList<(EntityCommand command, Exception exception)>.Empty) 
    { }

    /// <summary>
    /// Registers a command as active in <see cref="ActiveCommands"/>.
    /// </summary>
    /// <param name="command">The command to register.</param>
    /// <returns>An updated entity state.</returns>
    [Pure]
    public EntityState<TId, TEntity, TEntityInput> OnCommandStarted(EntityCommand command)
    {
        return this with
        {
            ActiveCommands = ActiveCommands.Add(command)
        };
    }

    /// <summary>
    /// Makes sure incoming loaded entities are added to the <see cref="Collection"/> and that the command is removed from <see cref="ActiveCommands"/>.
    /// </summary>
    /// <param name="command">The command to remove.</param>
    /// <param name="loadedEntities">The loaded entities to add.</param>
    /// <returns>An updated entity state.</returns>
    [Pure]
    public EntityState<TId, TEntity, TEntityInput> OnLoadSucceeded(EntityCommand.Load command, IImmutableList<(TId, TEntity)> loadedEntities)
    {
        return this with
        {
            Collection = Collection.AddRange(loadedEntities),
            ActiveCommands = ActiveCommands.Remove(command, ReferenceComparer.Instance)
        };
    }

    /// <summary>
    /// Makes sure an created <paramref name="entity"/> with <paramref name="id"/> is added to the <see cref="Collection"/> and that the command is removed from <see cref="ActiveCommands"/>.
    /// </summary>
    /// <param name="command">The command to remove.</param>
    /// <param name="id">The id to add an entity for.</param>
    /// <param name="entity">The entity to add.</param>
    /// <returns>An updated entity state.</returns>
    [Pure]
    public EntityState<TId, TEntity, TEntityInput> OnCreateSucceeded(EntityCommand.Create<TEntityInput> command, TId id, TEntity entity)
    {
        return this with
        {
            Collection = Collection.Add(id, entity),
            ActiveCommands = ActiveCommands.Remove(command, ReferenceComparer.Instance)
        };
    }

    /// <summary>
    /// Makes sure an updated <paramref name="entity"/> is replaced in the <see cref="Collection"/> and that the command is removed from <see cref="ActiveCommands"/>.
    /// </summary>
    /// <param name="command">The command to remove.</param>
    /// <param name="entity">The entity to replace with.</param>
    /// <returns>An updated entity state.</returns>
    [Pure]
    public EntityState<TId, TEntity, TEntityInput> OnUpdateSucceeded(EntityCommand.Update<TId, TEntity> command, TEntity entity)
    {
        return this with
        {
            Collection = Collection.Set(command.Id, entity),
            ActiveCommands = ActiveCommands.Remove(command, ReferenceComparer.Instance)
        };
    }

    /// <summary>
    /// Makes sure a deleted entity gets removed from the <see cref="Collection"/> and that the command is removed from <see cref="ActiveCommands"/>.
    /// </summary>
    /// <param name="command">The command to remove.</param>
    /// <returns>An updated entity state.</returns>
    [Pure]
    public EntityState<TId, TEntity, TEntityInput> OnDeleteSucceeded(EntityCommand.Delete<TId> command)
    {
        return this with
        {
            Collection = Collection.Remove(command.Id),
            ActiveCommands = ActiveCommands.Remove(command, ReferenceComparer.Instance)
        };
    }

    /// <summary>
    /// Resolves a command as failed and registers it with its exception in <see cref="CommandErrors"/> while removing it from <see cref="ActiveCommands"/>.
    /// </summary>
    /// <param name="command">The failed command.</param>
    /// <param name="exception">The exception that was thrown.</param>
    /// <returns>An updated entity state.</returns>
    [Pure]
    public EntityState<TId, TEntity, TEntityInput> OnCommandFailed(EntityCommand command, Exception exception)
    {
        return this with
        {
            CommandErrors = CommandErrors.Add((command, exception)),
            ActiveCommands = ActiveCommands.Remove(command, ReferenceComparer.Instance)
        };
    }

    /// <summary>
    /// Removes all errors from <see cref="CommandErrors"/>.
    /// </summary>
    /// <returns>An updated entity state.</returns>
    [Pure]
    public EntityState<TId, TEntity, TEntityInput> ClearAllErrors()
    {
        return this with
        {
            CommandErrors = CommandErrors.Clear()
        };
    }

    internal class ReferenceComparer : IEqualityComparer<EntityCommand>
    {
        public static readonly ReferenceComparer Instance = new();

        public bool Equals(EntityCommand? command1, EntityCommand? command2)
        {
            return ReferenceEquals(command1, command2);
        }

        public int GetHashCode(EntityCommand command)
        {
            return RuntimeHelpers.GetHashCode(command);
        }
    }
}
