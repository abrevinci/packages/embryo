﻿using AbreVinci.Embryo.Core;

namespace AbreVinci.Embryo.Entities;

/// <summary>
/// Utility effects for working with entities. Use within your own effects and/or command handlers.
/// </summary>
public static class EntityEffects
{
	/// <summary>
	/// Handles the given entity command.
	/// </summary>
	/// <typeparam name="TAppMessage">The program message type.</typeparam>
	/// <typeparam name="TId">The entity id type.</typeparam>
	/// <typeparam name="TEntity">The entity type.</typeparam>
	/// <typeparam name="TEntityInput">The entity input type.</typeparam>
	/// <param name="context">The effect context to use.</param>
	/// <param name="command">The command to handle.</param>
	/// <param name="wrapMessage">Should create an application command based on an <see cref="EntityCommand"/>.</param>
	/// <returns>An awaitable task.</returns>
	public static Task HandleEntityCommandAsync<TAppMessage, TId, TEntity, TEntityInput>(
		this EffectContext<IEntityService<TId, TEntity, TEntityInput>, TAppMessage> context, 
		EntityCommand command, 
		Func<EntityMessage, TAppMessage> wrapMessage) where TId : notnull
	{
		return command switch
		{
			EntityCommand.Load load => 
				context.ExecuteAsync(
					service => service.LoadAsync(),
					createOnStartMessage: () => wrapMessage(new EntityMessage.OnLoadStarted(load)),
					createOnCompletionMessage: entities => wrapMessage(new EntityMessage.OnLoadSucceeded<TId, TEntity>(load, entities)),
					createOnExceptionMessage: exception => wrapMessage(new EntityMessage.OnLoadFailed(load, exception))),

            EntityCommand.Create<TEntityInput> create =>
                context.ExecuteAsync(
	                service => service.CreateAsync(create.Input),
	                createOnStartMessage: () => wrapMessage(new EntityMessage.OnCreateStarted<TEntityInput>(create)),
	                createOnCompletionMessage: idAndEntity => wrapMessage(new EntityMessage.OnCreateSucceeded<TId, TEntity, TEntityInput>(create, idAndEntity.id, idAndEntity.entity)),
	                createOnExceptionMessage: exception => wrapMessage(new EntityMessage.OnCreateFailed<TEntityInput>(create, exception))),

            EntityCommand.Update<TId, TEntity> update =>
				context.ExecuteAsync(
					service => service.UpdateAsync(update.Id, update.Entity),
					createOnStartMessage: () => wrapMessage(new EntityMessage.OnUpdateStarted<TId, TEntity>(update)),
					createOnCompletionMessage: entity => wrapMessage(new EntityMessage.OnUpdateSucceeded<TId, TEntity>(update, entity)),
					createOnExceptionMessage: exception => wrapMessage(new EntityMessage.OnUpdateFailed<TId, TEntity>(update, exception))),

            EntityCommand.Delete<TId> delete =>
                context.ExecuteAsync(
	                service => service.DeleteAsync(delete.Id),
	                createOnStartMessage: () => wrapMessage(new EntityMessage.OnDeleteStarted<TId>(delete)),
	                createOnCompletionMessage: () => wrapMessage(new EntityMessage.OnDeleteSucceeded<TId>(delete)),
	                createOnExceptionMessage: exception => wrapMessage(new EntityMessage.OnDeleteFailed<TId>(delete, exception))),

			_ => Task.CompletedTask
        };
	}
}
