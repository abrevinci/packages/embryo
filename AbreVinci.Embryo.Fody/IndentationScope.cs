﻿namespace AbreVinci.Embryo.Fody;

internal class IndentationScope : IDisposable
{
	private readonly IIndentable _indentable;

	public IndentationScope(IIndentable indentable)
	{
		_indentable = indentable;
		_indentable.Indentation++;
	}

	public void Dispose()
	{
		_indentable.Indentation--;
	}
}
