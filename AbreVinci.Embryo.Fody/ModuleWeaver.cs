﻿using Fody;
using Mono.Cecil;
using Mono.Cecil.Cil;
using Mono.Cecil.Rocks;
using Mono.Collections.Generic;
using FieldAttributes = Mono.Cecil.FieldAttributes;

namespace AbreVinci.Embryo.Fody;

public class ModuleWeaver : IndentableModuleWeaver
{
	private record Binding(Instruction CallInstruction, Instruction AssignInstruction, PropertyDefinition Property);
	private record ViewModelBinding(Instruction CallInstruction, Instruction AssignInstruction, PropertyDefinition Property, TypeReference ValueType);

	public override void Execute()
	{
		var embryoRef = ModuleDefinition.AssemblyReferences.FirstOrDefault(a => a.Name == "AbreVinci.Embryo.ReactiveUI");
		if (embryoRef == null)
		{
			WriteWarning("Could not find an assembly reference to AbreVinci.Embryo.ReactiveUI. You likely haven't added any code using Embryo to your project yet.");
			return;
		}
		var embryo = ModuleDefinition.AssemblyResolver.Resolve(embryoRef).MainModule;

		WriteInfo("Detecting reactive view models...");
		using var _ = new IndentationScope(this);

		var reactiveViewModel = embryo.GetType("AbreVinci.Embryo.ReactiveUI.ReactiveViewModel");
		foreach (var viewModel in ModuleDefinition.Types.SelectMany(t => t.NestedTypes.Append(t)).Where(t => IsReactiveViewModel(t, reactiveViewModel)))
		{
			WriteInfo("Detected view model: " + viewModel.FullName);
			using var i1 = new IndentationScope(this);

			var (ctorInstructions, ctorProcessor) = GetConstructorInstructions(viewModel);
			ctorProcessor.Body.SimplifyMacros();
			SetupOneWayBindings(embryo, viewModel, reactiveViewModel, ctorInstructions, ctorProcessor);
			SetupTwoWayBindings(embryo, viewModel, reactiveViewModel, ctorInstructions, ctorProcessor);
			SetupOneWayAsViewModelBindings(embryo, viewModel, reactiveViewModel, ctorInstructions, ctorProcessor);
			ctorProcessor.Body.OptimizeMacros();
		}
	}

	public override IEnumerable<string> GetAssembliesForScanning()
	{
		yield break;
	}

	private bool IsReactiveViewModel(TypeDefinition type, TypeDefinition reactiveViewModel)
    {
		var baseType = type.BaseType?.Resolve();
		if (baseType == reactiveViewModel)
			return true;

		while (baseType is { } t)
        {
			baseType = t.BaseType?.Resolve();
			if (baseType == reactiveViewModel)
				return true;
		}
		return false;
    }

	private void SetupOneWayAsViewModelBindings(ModuleDefinition embryo, TypeDefinition viewModel, TypeDefinition reactiveViewModel, Collection<Instruction> ctorInstructions, ILProcessor ctorProcessor)
    {
		WriteInfo($"Detecting one-way as view model bindings for {viewModel.Name}...");
		using var _1 = new IndentationScope(this);

		var reactiveOneWayBinding = embryo.GetType("AbreVinci.Embryo.ReactiveUI.IReactiveOneWayBinding`1");
		if (reactiveOneWayBinding == null)
        {
			throw new WeavingException("Failed to load binding type from AbreVinci.Embryo");
        }

		var bindOneWayAsViewModel = reactiveViewModel.Methods.FirstOrDefault(m => m.Name == "BindOneWayAsViewModel");
		if (bindOneWayAsViewModel == null)
        {
			throw new WeavingException("Failed to load bind to method for one way as view model bindings");
		}

		var createOneWayAsViewModelBinding = ModuleDefinition.ImportReference(reactiveViewModel.Methods.FirstOrDefault(m => m.Name == "CreateOneWayAsViewModelBinding"));
		if (createOneWayAsViewModelBinding == null)
		{
			throw new WeavingException("Failed to load create binding method for one way as view model bindings");
		}

		var getBindingValue = ModuleDefinition.ImportReference(reactiveOneWayBinding.Methods.First(m => m.Name == "get_Value"));
		if (getBindingValue == null)
		{
			throw new WeavingException("Failed to load value getter for one way as view model reactive property");
		}

		WriteInfo("Trying to detect one way as view model bindings in constructor...");
		using var _2 = new IndentationScope(this);

		var bindings = GetOneWayAsViewModelBindings(viewModel, ctorInstructions, bindOneWayAsViewModel, reactiveViewModel);

		foreach (var binding in bindings)
        {
			// add field
			var fieldName = $"<{binding.Property.Name}>k__BindingField";
			var fieldType = viewModel.Module.ImportReference(reactiveOneWayBinding).MakeGenericInstanceType(binding.Property.PropertyType);
			var field = new FieldDefinition(fieldName, FieldAttributes.Private | FieldAttributes.InitOnly, fieldType);
			WriteInfo($"Adding field {field}");
			viewModel.Fields.Add(field);

			// updating constructor to use CreateOneWayBinding and assign to binding field
			WriteInfo("Updating constructor");
			var createOneWayBindingInstance = new GenericInstanceMethod(createOneWayAsViewModelBinding) { GenericArguments = { binding.ValueType, binding.Property.PropertyType } };
			var loadStrInstruction = ctorProcessor.Create(OpCodes.Ldstr, binding.Property.Name);
			var callInstruction = ctorProcessor.Create(OpCodes.Call, createOneWayBindingInstance);
			var assignInstruction = ctorProcessor.Create(OpCodes.Stfld, field);
			ReplaceBranchTargets(ctorInstructions, new() { [binding.CallInstruction] = loadStrInstruction, [binding.AssignInstruction] = assignInstruction });
			ctorProcessor.InsertBefore(binding.CallInstruction, loadStrInstruction);
			ctorProcessor.Replace(binding.CallInstruction, callInstruction);
			ctorProcessor.Replace(binding.AssignInstruction, assignInstruction);

			// modify property getter
			WriteInfo($"Populating getter for {binding.Property.Name}");
			var getPropertyBindingValue = getBindingValue.MakeHostInstanceGeneric(binding.Property.PropertyType);
			var getInstructions = binding.Property.GetMethod.Body.Instructions;
			getInstructions.Clear();
			getInstructions.Add(Instruction.Create(OpCodes.Ldarg_0)); // this
			getInstructions.Add(Instruction.Create(OpCodes.Ldfld, field)); // this.{fieldName}
			getInstructions.Add(Instruction.Create(OpCodes.Callvirt, getPropertyBindingValue)); // this.{fieldName}.get_Value()
			getInstructions.Add(Instruction.Create(OpCodes.Ret)); // ret

			// remove backing field
			viewModel.Fields.Remove(viewModel.Fields.Single(f => f.Name == $"<{binding.Property.Name}>k__BackingField"));
		}
	}

	private void ReplaceBranchTargets(Collection<Instruction> instructions, Dictionary<Instruction, Instruction> oldToNewTargetLookup)
	{
		foreach (var instruction in instructions)
		{
			if (instruction.Operand is Instruction target)
			{
				WriteInfo("Found targetted instruction: " + target);
				if (oldToNewTargetLookup.TryGetValue(target, out var newTarget))
				{
					instruction.Operand = newTarget;
				}
			}
			else if (instruction.Operand is Instruction[] inss)
			{
				if (inss.Any(oldToNewTargetLookup.ContainsKey))
				{
					WriteError("Found unsupported instruction: " + instruction);
					WriteError("We need your help in implementing support for this. Please contact AbreVinci.Embryo package owner with your repro case!");
				}
			}
		}
	}

	private void SetupOneWayBindings(ModuleDefinition embryo, TypeDefinition viewModel, TypeDefinition reactiveViewModel, Collection<Instruction> ctorInstructions, ILProcessor ctorProcessor)
	{
		WriteInfo($"Detecting one-way bindings for {viewModel.Name}...");
		using var _1 = new IndentationScope(this);

		var reactiveOneWayBinding = embryo.GetType("AbreVinci.Embryo.ReactiveUI.IReactiveOneWayBinding`1");
		if (reactiveOneWayBinding == null)
		{
			throw new WeavingException("Failed to load binding type from AbreVinci.Embryo");
		}

		var bindOneWay = reactiveViewModel.Methods.FirstOrDefault(m => m.Name == "BindOneWay");
		if (bindOneWay == null)
		{
			throw new WeavingException("Failed to load bind to method for one way bindings");
		}

		var createOneWayBinding = ModuleDefinition.ImportReference(reactiveViewModel.Methods.FirstOrDefault(m => m.Name == "CreateOneWayBinding"));
		if (createOneWayBinding == null)
		{
			throw new WeavingException("Failed to load create binding method for one way bindings");
		}

		var getBindingValue = ModuleDefinition.ImportReference(reactiveOneWayBinding.Methods.First(m => m.Name == "get_Value"));
		if (getBindingValue == null)
		{
			throw new WeavingException("Failed to load value getter for one way reactive property");
		}
			
		WriteInfo("Trying to detect one way bindings in constructor...");
		using var _2 = new IndentationScope(this);

		var bindings = GetOneWayBindings(viewModel, ctorInstructions, bindOneWay, reactiveViewModel);

		foreach (var binding in bindings)
		{
			// add field
			var fieldName = $"<{binding.Property.Name}>k__BindingField";
			var fieldType = viewModel.Module.ImportReference(reactiveOneWayBinding).MakeGenericInstanceType(binding.Property.PropertyType);
			var field = new FieldDefinition(fieldName, FieldAttributes.Private|FieldAttributes.InitOnly, fieldType);
			WriteInfo($"Adding field {field}");
			viewModel.Fields.Add(field);

			// updating constructor to use CreateOneWayBinding and assign to binding field
			WriteInfo("Updating constructor");
			var createOneWayBindingInstance = new GenericInstanceMethod(createOneWayBinding) { GenericArguments = { binding.Property.PropertyType } };
			var loadStrInstruction = ctorProcessor.Create(OpCodes.Ldstr, binding.Property.Name);
			var callInstruction = ctorProcessor.Create(OpCodes.Call, createOneWayBindingInstance);
			var assignInstruction = ctorProcessor.Create(OpCodes.Stfld, field);
			ReplaceBranchTargets(ctorInstructions, new() { [binding.CallInstruction] = loadStrInstruction, [binding.AssignInstruction] = assignInstruction });
			ctorProcessor.InsertBefore(binding.CallInstruction, loadStrInstruction);            
			ctorProcessor.Replace(binding.CallInstruction, callInstruction);
			ctorProcessor.Replace(binding.AssignInstruction, assignInstruction);

            // modify property getter
            WriteInfo($"Populating getter for {binding.Property.Name}");
            var getPropertyBindingValue = getBindingValue.MakeHostInstanceGeneric(binding.Property.PropertyType);
            var getInstructions = binding.Property.GetMethod.Body.Instructions;
            getInstructions.Clear();
            getInstructions.Add(Instruction.Create(OpCodes.Ldarg_0)); // this
            getInstructions.Add(Instruction.Create(OpCodes.Ldfld, field)); // this.{fieldName}
            getInstructions.Add(Instruction.Create(OpCodes.Callvirt, getPropertyBindingValue)); // this.{fieldName}.get_Value()
            getInstructions.Add(Instruction.Create(OpCodes.Ret)); // ret

            // remove backing field
            viewModel.Fields.Remove(viewModel.Fields.Single(f => f.Name == $"<{binding.Property.Name}>k__BackingField"));
        }
	}

	private (Collection<Instruction>, ILProcessor) GetConstructorInstructions(TypeDefinition viewModel)
	{
		if (viewModel.GetConstructors().Count(c => !c.IsStatic) > 1)
		{
			throw new WeavingException($"Detected multiple constructors for {viewModel.Name}. Please make sure there is only one!");
		}

		var constructor = viewModel.GetConstructors().FirstOrDefault(c => !c.IsStatic);
		if (constructor == null)
		{
			throw new WeavingException($"Did not detect any constructor for {viewModel.Name}. Please make sure there is one!");
		}

		var instructions = constructor.Body.Instructions;
		WriteDebug("Constructor instructions:");
		foreach (var instruction in instructions)
		{
			WriteDebug(instruction.ToString());
		}

		return (instructions, constructor.Body.GetILProcessor());
	}

	private List<ViewModelBinding> GetOneWayAsViewModelBindings(TypeDefinition viewModel, Collection<Instruction> instructions, MethodDefinition bindOneWayAsViewModel, TypeDefinition reactiveViewModel)
	{
		var bindings = new List<ViewModelBinding>();

		for (var i = 0; i < instructions.Count; i++)
		{
			var instruction = instructions[i];
			if (instruction.OpCode.Code == Code.Call &&
				instruction.Operand is GenericInstanceMethod callee &&
				callee.DeclaringType.Resolve() == reactiveViewModel &&
				callee.Resolve() == bindOneWayAsViewModel &&
				callee.GenericArguments.Count == 2)
			{
				WriteDebug(instruction.ToString());
				WriteInfo($"Detected one way as view model binding call at IL_{instruction.Offset:x4}.");
				WriteInfo($"Value type: {callee.GenericArguments[0].FullName}");
				WriteInfo($"View model type: {callee.GenericArguments[1].FullName}");
				WriteInfo("Trying to detect binding property...");

                var next = instructions[i + 1];
                if (next.OpCode.Code == Code.Stfld &&
                    next.Operand is FieldDefinition backingField &&
                    backingField.Name.EndsWith(">k__BackingField"))
                {
                    var propertyName = backingField.Name.Substring(1, backingField.Name.Length - "<>k__BackingField".Length);
                    var property = viewModel.Properties.FirstOrDefault(p => p.Name == propertyName);
                    if (property == null)
                    {
                        throw new WeavingException("Found assigned to backing field without property.");
                    }

                    if (property.SetMethod != null)
                    {
                        WriteError($"Property {viewModel.FullName}.{property.Name} has a set method but is used with a one-way as view model binding.");
                    }
                    else if (property.GetMethod == null)
                    {
                        WriteError($"Property {viewModel.FullName}.{property.Name} has no get method.");
                    }
					else
                    {
                        WriteInfo($"Detected bound property {property.PropertyType.Name} {propertyName}");
                        bindings.Add(new ViewModelBinding(instruction, next, property, callee.GenericArguments[0]));
                    }
                }
                else if (next.OpCode.Code == Code.Call &&
                            next.Operand is MethodDefinition setter &&
                            setter.Name.StartsWith("set_"))
                {
                    var propertyName = setter.Name.Substring("set_".Length);

                    var property = viewModel.Properties.FirstOrDefault(p => p.Name == propertyName);
                    if (property == null)
                    {
                        throw new WeavingException("Found setter without property.");
                    }

                    if (viewModel.Fields.All(f => f.Name != $"<{propertyName}>k__BackingField"))
                    {
                        WriteError($"Property {viewModel.FullName}.{property.Name} is not an auto property.");
                    }
                    else if (property.SetMethod != null)
                    {
                        WriteError($"Property {viewModel.FullName}.{property.Name} has a set method but is used with a one-way as view model binding.");
                    }
                    else if (property.GetMethod == null)
                    {
                        WriteError($"Property {viewModel.FullName}.{property.Name} has no get method.");
                    }
					else
                    {
                        WriteInfo($"Detected bound property {property.PropertyType.Name} {propertyName}");
						bindings.Add(new ViewModelBinding(instruction, next, property, callee.GenericArguments[0]));
					}
                }
                else
                {
	                WriteError($"Not able to detect property for binding inside {viewModel.FullName}. Make sure that you have assigned the return value of the BindOneWayAsViewModel method to an auto property with a getter");
				}
            }
        }

        return bindings;
	}

	private List<Binding> GetOneWayBindings(TypeDefinition viewModel, Collection<Instruction> instructions, MethodDefinition bindOneWay, TypeDefinition reactiveViewModel)
	{
		var bindings = new List<Binding>();

		for (var i = 0; i < instructions.Count; i++)
		{
			var instruction = instructions[i];
			if (instruction.OpCode.Code == Code.Call &&
				instruction.Operand is GenericInstanceMethod callee &&
				callee.DeclaringType.Resolve() == reactiveViewModel &&
				callee.Resolve() == bindOneWay &&
				callee.GenericArguments.Count == 1)
			{
				WriteDebug(instruction.ToString());
				WriteInfo($"Detected one way binding call at IL_{instruction.Offset:x4}.");
				WriteInfo($"Value type: {callee.GenericArguments[0].FullName}");
				WriteInfo("Trying to detect binding property...");

				var next = instructions[i + 1];
				if (next.OpCode.Code == Code.Stfld &&
					next.Operand is FieldDefinition backingField &&
					backingField.Name.EndsWith(">k__BackingField"))
				{
					var propertyName = backingField.Name.Substring(1, backingField.Name.Length - "<>k__BackingField".Length);
					var property = viewModel.Properties.FirstOrDefault(p => p.Name == propertyName);
					if (property == null)
					{
						throw new WeavingException("Found assigned to backing field without property.");
					}

					if (property.SetMethod != null)
					{
						WriteError($"Property {viewModel.FullName}.{property.Name} has a set method but is used with a one-way binding.");
					}
					else if (property.GetMethod == null)
					{
						WriteError($"Property {viewModel.FullName}.{property.Name} has no get method.");
					}
					else
					{
						WriteInfo($"Detected bound property {property.PropertyType.Name} {propertyName}");
						bindings.Add(new Binding(instruction, next, property));
					}
				}
				else if (next.OpCode.Code == Code.Call &&
							next.Operand is MethodDefinition setter &&
							setter.Name.StartsWith("set_"))
				{
					var propertyName = setter.Name.Substring("set_".Length);

					var property = viewModel.Properties.FirstOrDefault(p => p.Name == propertyName);
					if (property == null)
					{
						throw new WeavingException("Found setter without property.");
					}

					if (viewModel.Fields.All(f => f.Name != $"<{propertyName}>k__BackingField"))
					{
						WriteError($"Property {viewModel.FullName}.{property.Name} is not an auto property.");
					}
					else if (property.SetMethod != null)
					{
						WriteError($"Property {viewModel.FullName}.{property.Name} has a set method but is used with a one-way binding.");
					}
					else if (property.GetMethod == null)
					{
						WriteError($"Property {viewModel.FullName}.{property.Name} has no get method.");
					}
					else
					{
						WriteInfo($"Detected bound property {property.PropertyType.Name} {propertyName}");
						bindings.Add(new Binding(instruction, next, property));
					}
				}
				else
				{
					WriteError($"Not able to detect property for binding inside {viewModel.FullName}. Make sure that you have assigned the return value of the BindOneWay method to an auto property with a getter");
				}
			}
		}

		return bindings;
	}
	
	private void SetupTwoWayBindings(ModuleDefinition embryo, TypeDefinition viewModel, TypeDefinition reactiveViewModel, Collection<Instruction> ctorInstructions, ILProcessor ctorProcessor)
	{
		WriteInfo($"Detecting one-way bindings for {viewModel.Name}...");
		using var i1 = new IndentationScope(this);

		var reactiveTwoWayBinding = embryo.GetType("AbreVinci.Embryo.ReactiveUI.IReactiveTwoWayBinding`1");
		if (reactiveTwoWayBinding == null)
		{
			throw new WeavingException("Failed to load binding type from AbreVinci.Embryo");
		}

		var bindTwoWay = reactiveViewModel.Methods.FirstOrDefault(m => m.Name == "BindTwoWay");
		if (bindTwoWay == null)
		{
			throw new WeavingException("Failed to load bind to method for two way bindings");
		}

		var createTwoWayBinding = ModuleDefinition.ImportReference(reactiveViewModel.Methods.FirstOrDefault(m => m.Name == "CreateTwoWayBinding"));
		if (createTwoWayBinding == null)
		{
			throw new WeavingException("Failed to load create binding method for two way bindings");
		}
			
		var getBindingValue = ModuleDefinition.ImportReference(reactiveTwoWayBinding.Methods.FirstOrDefault(m => m.Name == "get_Value"));
		var setBindingValue = ModuleDefinition.ImportReference(reactiveTwoWayBinding.Methods.FirstOrDefault(m => m.Name == "set_Value"));
		if (getBindingValue == null || setBindingValue == null)
		{
			throw new WeavingException("Failed to load value getter and setter for two way reactive property");
		}

		WriteInfo("Trying to detect two way bindings in constructor...");
		using var i2 = new IndentationScope(this);

		var bindings = GetTwoWayBindings(viewModel, ctorInstructions, bindTwoWay, reactiveViewModel);

		foreach (var binding in bindings)
		{
			// add field
			var fieldName = $"<{binding.Property.Name}>k__BindingField";
			var fieldType = viewModel.Module.ImportReference(reactiveTwoWayBinding).MakeGenericInstanceType(binding.Property.PropertyType);
			var field = new FieldDefinition(fieldName, FieldAttributes.Private | FieldAttributes.InitOnly, fieldType);
			WriteInfo($"Adding field {field}");
			viewModel.Fields.Add(field);

			// updating constructor to use CreateOneWayBinding and assign to binding field
			WriteInfo("Updating constructor");
			var createTwoWayBindingInstance = new GenericInstanceMethod(createTwoWayBinding) { GenericArguments = { binding.Property.PropertyType } };
			var loadStrInstruction = ctorProcessor.Create(OpCodes.Ldstr, binding.Property.Name);
			var callInstruction = ctorProcessor.Create(OpCodes.Call, createTwoWayBindingInstance);
			var assignInstruction = ctorProcessor.Create(OpCodes.Stfld, field);
			ReplaceBranchTargets(ctorInstructions, new() { [binding.CallInstruction] = loadStrInstruction, [binding.AssignInstruction] = assignInstruction });
			ctorProcessor.InsertBefore(binding.CallInstruction, loadStrInstruction);
			ctorProcessor.Replace(binding.CallInstruction, callInstruction);
			ctorProcessor.Replace(binding.AssignInstruction, assignInstruction);
				
			// modify property getter
			WriteInfo($"Populating getter for {binding.Property.Name}");
			var getPropertyBindingValue = getBindingValue.MakeHostInstanceGeneric(binding.Property.PropertyType);
			var getInstructions = binding.Property.GetMethod.Body.Instructions;
			getInstructions.Clear();
			getInstructions.Add(Instruction.Create(OpCodes.Ldarg_0)); // this
			getInstructions.Add(Instruction.Create(OpCodes.Ldfld, field)); // this.{fieldName}
			getInstructions.Add(Instruction.Create(OpCodes.Callvirt, getPropertyBindingValue)); // this.{fieldName}.get_Value()
			getInstructions.Add(Instruction.Create(OpCodes.Ret)); // ret

			// modify property setter
			WriteInfo($"Populating setter for {binding.Property.Name}");
			var setPropertyBindingValue = setBindingValue.MakeHostInstanceGeneric(binding.Property.PropertyType);
			var setInstructions = binding.Property.SetMethod.Body.Instructions;
			setInstructions.Clear();
			setInstructions.Add(Instruction.Create(OpCodes.Ldarg_0)); // this
			setInstructions.Add(Instruction.Create(OpCodes.Ldfld, field)); // this.{fieldName}
			setInstructions.Add(Instruction.Create(OpCodes.Ldarg_1)); // value
			setInstructions.Add(Instruction.Create(OpCodes.Callvirt, setPropertyBindingValue)); // this.{fieldName}.set_Value(value)
			setInstructions.Add(Instruction.Create(OpCodes.Nop));
			setInstructions.Add(Instruction.Create(OpCodes.Ret)); // ret

			// remove backing field
			viewModel.Fields.Remove(viewModel.Fields.Single(f => f.Name == $"<{binding.Property.Name}>k__BackingField"));
		}
	}

	private List<Binding> GetTwoWayBindings(TypeDefinition viewModel, Collection<Instruction> instructions, MethodDefinition bindTwoWay, TypeDefinition reactiveViewModel)
	{
		var bindings = new List<Binding>();

		for (var i = 0; i < instructions.Count; i++)
		{
			var instruction = instructions[i];
			if (instruction.OpCode.Code == Code.Call &&
				instruction.Operand is GenericInstanceMethod callee &&
				callee.DeclaringType.Resolve() == reactiveViewModel &&
				callee.Resolve() == bindTwoWay &&
				callee.GenericArguments.Count == 1)
			{
				WriteDebug(instruction.ToString());
				WriteInfo($"Detected two way binding call at IL_{instruction.Offset:x4}.");
				WriteInfo($"Value type: {callee.GenericArguments[0].FullName}");
				WriteInfo("Trying to detect binding property...");

				var next = instructions[i + 1];
				if (next.OpCode.Code == Code.Stfld &&
					next.Operand is FieldDefinition backingField &&
					backingField.Name.EndsWith(">k__BackingField"))
				{
					var propertyName = backingField.Name.Substring(1, backingField.Name.Length - "<>k__BackingField".Length);
					var property = viewModel.Properties.FirstOrDefault(p => p.Name == propertyName);
					if (property == null)
					{
						throw new WeavingException("Found assigned to backing field without property.");
					}

					if (property.SetMethod is not { IsPublic: true })
					{
						WriteError($"Property {viewModel.FullName}.{property.Name} has no public set method but is used with a two-way binding.");
					}
					else if (property.GetMethod == null)
					{
						WriteError($"Property {viewModel.FullName}.{property.Name} has no get method.");
					}
					else
					{
						WriteInfo($"Detected bound property {property.PropertyType.Name} {propertyName}");
						bindings.Add(new Binding(instruction, next, property));
					}
				}
				else if (next.OpCode.Code == Code.Call &&
							next.Operand is MethodDefinition setter &&
							setter.Name.StartsWith("set_"))
				{
					var propertyName = setter.Name.Substring("set_".Length);

					var property = viewModel.Properties.FirstOrDefault(p => p.Name == propertyName);
					if (property == null)
					{
						throw new WeavingException("Found setter without property.");
					}

					if (viewModel.Fields.All(f => f.Name != $"<{propertyName}>k__BackingField"))
					{
						WriteError($"Property {viewModel.FullName}.{property.Name} is not an auto property.");
					}
					else if (property.SetMethod is not { IsPublic: true })
					{
						WriteError($"Property {viewModel.FullName}.{property.Name} has no public set method but is used with a two-way binding.");
					}
					else if (property.GetMethod == null)
					{
						WriteError($"Property {viewModel.FullName}.{property.Name} has no get method.");
					}
					else
					{
						WriteInfo($"Detected bound property {property.PropertyType.Name} {propertyName}");
						bindings.Add(new Binding(instruction, next, property));
					}
				}
				else
				{
					WriteError($"Not able to detect property for binding inside {viewModel.FullName}. Make sure that you have assigned the return value of the BindTwoWay method to an auto property with a setter and a getter.");
				}
			}
		}

		return bindings;
	}
}
