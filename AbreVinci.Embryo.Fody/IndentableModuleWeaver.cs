﻿using Fody;

namespace AbreVinci.Embryo.Fody;

public abstract class IndentableModuleWeaver : BaseModuleWeaver, IIndentable
{
	public int Indentation { get; set; }
	public string IndentationSpace => string.Join("", Enumerable.Repeat("  ", Indentation));

	public new void WriteDebug(string message)
	{
		base.WriteDebug(IndentationSpace + message);
	}

	public new void WriteInfo(string message)
	{
		base.WriteInfo(IndentationSpace + message);
	}

	public new void WriteWarning(string message)
	{
		base.WriteWarning(IndentationSpace + message);
	}

	public new void WriteError(string message)
	{
		base.WriteError(IndentationSpace + message);
	}
}
