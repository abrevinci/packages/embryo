﻿namespace AbreVinci.Embryo.Fody;

internal interface IIndentable
{
	int Indentation { get; set; }
}
