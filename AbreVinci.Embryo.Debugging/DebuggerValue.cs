﻿namespace AbreVinci.Embryo.Debugging;

/// <summary>
/// Debugger value base class.
/// </summary>
public abstract record DebuggerValue
{
	/// <summary>
	/// Represents a single value (not a collection).
	/// </summary>
	/// <param name="Type">The type of the value.</param>
	/// <param name="Value">The serialized value.</param>
	public sealed record Single(string Type, string Value) : DebuggerValue;

	/// <summary>
	/// Represents a compound value (a collection).
	/// </summary>
	/// <param name="Type">The type of the collection.</param>
	/// <param name="ValueKeys">The keys of the collection.</param>
	/// <param name="Values">The values of the collection by their key.</param>
	public sealed record Compound(string Type, IImmutableList<string> ValueKeys, IImmutableDictionary<string, DebuggerValue?> Values) : DebuggerValue;

	// Prevent further inheritance
	private DebuggerValue() {}
}
