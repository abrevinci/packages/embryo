﻿namespace AbreVinci.Embryo.Debugging;

/// <summary>
/// Settings for configuring LogDebugger.
/// </summary>
public class LogDebuggerSettings
{
	/// <summary>
	/// The number of spaces to use for a single level of indentation (default 2).
	/// </summary>
	public int IndentationSpaces { get; init; } = 2;

	/// <summary>
	/// The newline string to use for line breaks (default "\n").
	/// </summary>
	public string NewLine { get; init; } = "\n";
}
