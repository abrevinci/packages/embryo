﻿using AbreVinci.Embryo.ReactiveUI;

namespace AbreVinci.Embryo.Debugging.ViewModels;

/// <summary>
/// View model for listing a particular debugger frame.
/// </summary>
public class DebuggerFrameItemViewModel : ReactiveViewModel
{
	private readonly IReactiveOneWayBinding<DebuggerValueViewModel> _messageBinding;

	/// <summary>
	/// Constructor.
	/// </summary>
	/// <param name="id">The frame id.</param>
	/// <param name="frame">An observable stream of the frame.</param>
	/// <param name="createDebuggerValue">Factory for creating debugger value view model for message.</param>
	public DebuggerFrameItemViewModel(
		int id, 
		IObservable<DebuggerFrame> frame,
		Func<int, string, DebuggerValue?, DebuggerValueViewModel> createDebuggerValue)
	{
		Id = id;
		_messageBinding = CreateOneWayAsViewModelBinding(frame.Select(f => new {f.Message}), m => createDebuggerValue(id, "ItemMessage", m.Message), (_, _) => true, nameof(Message));
	}

	/// <summary>
	/// The id of the frame.
	/// </summary>
	public int Id { get; }

	/// <summary>
	/// The message.
	/// </summary>
	public DebuggerValueViewModel Message => _messageBinding.Value;
}