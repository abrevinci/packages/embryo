﻿namespace AbreVinci.Embryo.Debugging.ViewModels;

/// <summary>
/// View model for a single (non collection) debugger value.
/// </summary>
public class SingleDebuggerValueViewModel : DebuggerValueViewModel
{
	/// <summary>
	/// Constructor.
	/// </summary>
	/// <param name="frameId">The frame in which this value exists.</param>
	/// <param name="path">The path of this value.</param>
	/// <param name="value">The value.</param>
	public SingleDebuggerValueViewModel(int frameId, string path, DebuggerValue.Single? value) : base(frameId, path, value)
	{
		Value = value?.Value;
	}

	/// <summary>
	/// The string representation of the value or null if value is null.
	/// </summary>
	public string? Value { get; }
}
