﻿namespace AbreVinci.Embryo.Debugging.ViewModels;

/// <summary>
/// View model factory for <see cref="DebuggerViewModel"/>.
/// </summary>
public class DebuggerViewModelFactory
{
	private readonly IDebuggerController _debuggerController;

	/// <summary>
	/// Constructor.
	/// </summary>
	/// <param name="debuggerController">The debugger.</param>
	public DebuggerViewModelFactory(IDebuggerController debuggerController)
	{
		_debuggerController = debuggerController;
	}

	/// <summary>
	/// Creates a debugger view model for this instance´s current debugger.
	/// </summary>
	/// <returns>The created debugger view model.</returns>
	public DebuggerViewModel CreateDebuggerViewModel()
	{
		IObservable<DebuggerState> state = _debuggerController.State;
		IObservable<DebuggerFrame> SelectFrame(int frameId) => state.Select(s => s.Frames.TryGetValue(frameId, out var frame) ? frame : null).Where(v => v != null).Select(v => v!);
		IObservable<bool> SelectIsExpanded(int frameId, string path) => state.Select(s => s.Frames.TryGetValue(frameId, out var frame) && frame.ExpandedPaths.Contains(path));
		DebuggerValueViewModel CreateDebuggerValue(int frameId, string path, DebuggerValue? debuggerValue) => debuggerValue switch
		{
			DebuggerValue.Single single => new SingleDebuggerValueViewModel(frameId, path, single),
			DebuggerValue.Compound compound => new CompoundDebuggerValueViewModel(frameId, path, compound, SelectIsExpanded(frameId, path), v => _debuggerController.SetExpanded(frameId, path, v), CreateDebuggerValue),
			_ => new SingleDebuggerValueViewModel(frameId, path, null)
		};
		DebuggerFrameItemViewModel CreateDebuggerFrameItem(int id) => new(id, SelectFrame(id), CreateDebuggerValue);
		DebuggerFrameInspectionViewModel CreateDebuggerFrameInspection(int id) => new(id, SelectFrame(id), CreateDebuggerValue);
		return new(_debuggerController.State, _debuggerController.GoToFrame, CreateDebuggerFrameItem, CreateDebuggerFrameInspection);
	}
}
