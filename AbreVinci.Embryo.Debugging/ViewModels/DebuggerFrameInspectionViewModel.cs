﻿using AbreVinci.Embryo.ReactiveUI;

namespace AbreVinci.Embryo.Debugging.ViewModels;

/// <summary>
/// View model for inspecting a particular debugger frame.
/// </summary>
public class DebuggerFrameInspectionViewModel : ReactiveViewModel
{
	private readonly IReactiveOneWayBinding<DebuggerValueViewModel> _beforeStateBinding;
	private readonly IReactiveOneWayBinding<DebuggerValueViewModel> _afterStateBinding;
	private readonly IReactiveOneWayBinding<DebuggerValueViewModel> _messageBinding;
	private readonly IReactiveOneWayBinding<DebuggerValueViewModel> _commandsBinding;

	/// <summary>
	/// Constructor.
	/// </summary>
	/// <param name="id">The frame id.</param>
	/// <param name="frame">An observable stream of the frame.</param>
	/// <param name="createDebuggerValue">Factory for creating debugger value view models.</param>
	public DebuggerFrameInspectionViewModel(
		int id,
		IObservable<DebuggerFrame> frame,
		Func<int, string, DebuggerValue?, DebuggerValueViewModel> createDebuggerValue)
	{
		Id = id;
		_beforeStateBinding = CreateOneWayAsViewModelBinding(frame.Select(f => new{f.BeforeState}), s => AddChildViewModel(() => createDebuggerValue(id, "BeforeState", s.BeforeState)), (_, _) => true, nameof(BeforeState));
		_afterStateBinding = CreateOneWayAsViewModelBinding(frame.Select(f => f.AfterState), s => AddChildViewModel(() => createDebuggerValue(id, "AfterState", s)), (_, _) => true, nameof(AfterState));
		_messageBinding = CreateOneWayAsViewModelBinding(frame.Select(f => new{f.Message}), m => AddChildViewModel(() => createDebuggerValue(id, "Message", m.Message)), (_, _) => true, nameof(Message));
		_commandsBinding = CreateOneWayAsViewModelBinding(frame.Select(f => f.Commands), c => AddChildViewModel(() => createDebuggerValue(id, "Commands", c)), (_, _) => true, nameof(Commands));
	}

	/// <summary>
	/// The id of the frame.
	/// </summary>
	public int Id { get; }

	/// <summary>
	/// The before state.
	/// </summary>
	public DebuggerValueViewModel BeforeState => _beforeStateBinding.Value;

	/// <summary>
	/// The after state.
	/// </summary>
	public DebuggerValueViewModel AfterState => _afterStateBinding.Value;

	/// <summary>
	/// The message.
	/// </summary>
	public DebuggerValueViewModel Message => _messageBinding.Value;

	/// <summary>
	/// The commands.
	/// </summary>
	public DebuggerValueViewModel Commands => _commandsBinding.Value;
}
