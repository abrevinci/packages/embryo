﻿using AbreVinci.Embryo.ReactiveUI;

namespace AbreVinci.Embryo.Debugging.ViewModels;

/// <summary>
/// View model for a compound (collection/object) debugger value.
/// </summary>
public class CompoundDebuggerValueViewModel : DebuggerValueViewModel
{
	private readonly IReactiveTwoWayBinding<bool> _isExpandedBinding;

	/// <summary>
	/// Constructor.
	/// </summary>
	/// <param name="frameId">The frame in which this value exists.</param>
	/// <param name="path">The path of this value.</param>
	/// <param name="value">The value.</param>
	/// <param name="isExpanded">An observable stream indicating if the value is expanded or not.</param>
	/// <param name="setExpanded">Callback for setting if the value should be expanded or not.</param>
	/// <param name="createDebuggerValue">Factory for creating child value view models.</param>
	public CompoundDebuggerValueViewModel(
		int frameId,
		string path, 
		DebuggerValue.Compound value,
		IObservable<bool> isExpanded,
		Action<bool> setExpanded,
		Func<int, string, DebuggerValue?, DebuggerValueViewModel> createDebuggerValue) : base(frameId, path, value)
	{
		_isExpandedBinding = CreateTwoWayBinding(isExpanded, setExpanded, nameof(IsExpanded));
		Values = value.ValueKeys.Select(key => AddChildViewModel(() => createDebuggerValue(frameId, $"{path}/{key}", value.Values[key]))).ToImmutableArray();
	}

	/// <summary>
	/// Gets or set whether or not this value is expanded.
	/// </summary>
	public bool IsExpanded { get => _isExpandedBinding.Value; set => _isExpandedBinding.Value = value; }
	
	/// <summary>
	/// Retrieves any child values.
	/// </summary>
	public IImmutableList<DebuggerValueViewModel> Values { get; }
}
