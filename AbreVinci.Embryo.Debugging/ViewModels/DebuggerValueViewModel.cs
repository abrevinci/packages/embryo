﻿using AbreVinci.Embryo.ReactiveUI;

namespace AbreVinci.Embryo.Debugging.ViewModels;

/// <summary>
/// Base class for debugger value view models.
/// </summary>
public abstract class DebuggerValueViewModel : ReactiveViewModel
{
	/// <summary>
	/// Constructor.
	/// </summary>
	/// <param name="frameId">The frame in which this value exists.</param>
	/// <param name="path">The path of this value.</param>
	/// <param name="value">The value.</param>
	protected DebuggerValueViewModel(int frameId, string path, DebuggerValue? value)
	{
		FrameId = frameId;
		Path = path;
		var pathComponents = path.Split('/');
		KeyText = pathComponents.Length > 1 ? $"{pathComponents.Last()}: " : string.Empty;
		Type = value switch
		{
			DebuggerValue.Single(var type, _) => type,
			DebuggerValue.Compound(var type, _, _) => type,
			_ => null
		};
	}
	
	/// <summary>
	/// The frame id in which this value exists.
	/// </summary>
	public int FrameId { get; }

	/// <summary>
	/// The path of this value.
	/// </summary>
	public string Path { get; }

	/// <summary>
	/// The key text of this value (format is either "someKey: " or "")
	/// </summary>
	public string KeyText { get; }

	/// <summary>
	/// The type of this value or null if the value is null.
	/// </summary>
	public string? Type { get; }
}
