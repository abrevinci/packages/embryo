﻿using AbreVinci.Embryo.ReactiveUI;

namespace AbreVinci.Embryo.Debugging.ViewModels;

/// <summary>
/// View model for a debugger.
/// </summary>
public class DebuggerViewModel : ReactiveViewModel
{
	private readonly IReactiveTwoWayBinding<int> _currentDebuggerFrameIdBinding;
	private readonly IReactiveOneWayBinding<DebuggerFrameInspectionViewModel> _debuggerFrameInspectionBinding;
	private readonly IReactiveOneWayBinding<bool> _isPausedBinding;

	/// <summary>
	/// Constructor.
	/// </summary>
	/// <param name="state">An observable stream of the debugger´s state.</param>
	/// <param name="goToFrame">A callback to go to the given frame.</param>
	/// <param name="createDebuggerFrameItem">Factory for creating debugger frame item view models.</param>
	/// <param name="createDebuggerFrameInspection">Factory for creating debugger frame inspection view models.</param>
	public DebuggerViewModel(
		IObservable<DebuggerState> state,
		Action<int> goToFrame,
		Func<int, DebuggerFrameItemViewModel> createDebuggerFrameItem,
		Func<int, DebuggerFrameInspectionViewModel> createDebuggerFrameInspection)
	{
		DebuggerFrameItems = BindListAsViewModels(state.Select(s => s.FrameIds), createDebuggerFrameItem, (vm, id) => vm.Id == id);
		_currentDebuggerFrameIdBinding = CreateTwoWayBinding(state.Select(s => s.CurrentFrameId), goToFrame, nameof(CurrentDebuggerFrameId));
		_debuggerFrameInspectionBinding = CreateOneWayAsViewModelBinding(state.Select(s => s.CurrentFrameId), createDebuggerFrameInspection, (vm, id) => vm.Id == id, nameof(DebuggerFrameInspection));
		_isPausedBinding = CreateOneWayBinding(state.Select(s => s.IsPaused), nameof(IsPaused));
	}

	/// <summary>
	/// All debugger frame items.
	/// </summary>
	public IEnumerable<DebuggerFrameItemViewModel> DebuggerFrameItems { get; }

	/// <summary>
	/// Gets or sets the currently selected frame id.
	/// </summary>
	public int CurrentDebuggerFrameId { get => _currentDebuggerFrameIdBinding.Value; set => _currentDebuggerFrameIdBinding.Value = value; }
	
	/// <summary>
	/// Inspection for the current debugger frame.
	/// </summary>
	public DebuggerFrameInspectionViewModel DebuggerFrameInspection => _debuggerFrameInspectionBinding.Value;
	
	/// <summary>
	/// Whether or not the debugger is paused.
	/// </summary>
	public bool IsPaused => _isPausedBinding.Value;
}
