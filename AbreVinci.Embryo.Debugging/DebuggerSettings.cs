﻿namespace AbreVinci.Embryo.Debugging;

/// <summary>
/// Settings for <see cref="Debugger{TState, TMessage, TCommand}"/>.
/// </summary>
public class DebuggerSettings
{
	/// <summary>
	/// The maximum size of the frame stack. If this value is reached, the oldest frames will be removed.
	/// </summary>
	public int MaxHistorySize { get; init; } = int.MaxValue;
}
