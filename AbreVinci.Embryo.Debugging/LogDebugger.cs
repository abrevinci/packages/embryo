﻿using AbreVinci.Embryo.App;

namespace AbreVinci.Embryo.Debugging;

/// <summary>
/// A logging debugger capable of writing to a string output.
/// </summary>
/// <typeparam name="TState">The program state type.</typeparam>
/// <typeparam name="TMessage">The program message type.</typeparam>
/// <typeparam name="TCommand">The program command type.</typeparam>
public class LogDebugger<TState, TMessage, TCommand> : IDebugger<TState, TMessage, TCommand>
	where TState : notnull
	where TMessage : notnull
	where TCommand : notnull
{
	private readonly LogDebuggerSettings _settings;
	private readonly Action<string> _write;
	private readonly Type[] _typesForStringRepresentation;
	
	private int _frameCounter;

	/// <summary>
	/// Constructor.
	/// </summary>
	/// <param name="settings">Settings for configuring this debugger instance.</param>
	/// <param name="write">Callback abstracting the actual output operation (could be Debug.Write or log file or something else).</param>
	/// <param name="typesForStringRepresentation">Types of non-primitive values that should be displayed by calling ToString() instead of serializing properties, for instance Guid and DateTime.</param>
	public LogDebugger(LogDebuggerSettings settings, Action<string> write, params Type[] typesForStringRepresentation)
	{
		_settings = settings;
		_write = write;
		_typesForStringRepresentation = typesForStringRepresentation;

		_frameCounter = 1;
	}

	/// <summary>
	/// Always false, this debugger can not time travel.
	/// </summary>
	public bool IsPaused => false;

	/// <summary>
	/// Called by the program when the program's init function has been called.
	/// </summary>
	/// <param name="state">The initial state.</param>
	/// <param name="commands">The initial commands.</param>
	public void OnInit(TState state, IImmutableList<TCommand> commands)
	{
		var nestingSpaces = "".PadLeft(_settings.IndentationSpaces);
		_write($"{_frameCounter++}: Init:{_settings.NewLine}");
		_write($"{nestingSpaces}AfterState:{_settings.NewLine}{state.ToDebuggerValue(_typesForStringRepresentation).Dump(_settings.IndentationSpaces, _settings.NewLine, 2)}");
		_write($"{nestingSpaces}Commands:{_settings.NewLine}{commands.ToDebuggerValue(_typesForStringRepresentation).Dump(_settings.IndentationSpaces, _settings.NewLine, 2)}");
	}

	/// <summary>
	/// Called by the program when the program's update function has been called.
	/// </summary>
	/// <param name="stateBefore">The state before the update function was called.</param>
	/// <param name="message">The message for which update was called.</param>
	/// <param name="stateAfter">The state after the update function was called.</param>
	/// <param name="commands">Any commands emitted by the update function.</param>
	public void OnUpdate(TState stateBefore, TMessage message, TState stateAfter, IImmutableList<TCommand> commands)
	{
		var nestingSpaces = "".PadLeft(_settings.IndentationSpaces);
		_write($"{_frameCounter++}: Update:{_settings.NewLine}");
		_write($"{nestingSpaces}BeforeState:{_settings.NewLine}{stateBefore.ToDebuggerValue(_typesForStringRepresentation).Dump(_settings.IndentationSpaces, _settings.NewLine, 2)}");
		_write($"{nestingSpaces}Message:{_settings.NewLine}{message.ToDebuggerValue(_typesForStringRepresentation).Dump(_settings.IndentationSpaces, _settings.NewLine, 2)}");
		_write($"{nestingSpaces}AfterState:{_settings.NewLine}{stateAfter.ToDebuggerValue(_typesForStringRepresentation).Dump(_settings.IndentationSpaces, _settings.NewLine, 2)}");
		_write($"{nestingSpaces}Commands:{_settings.NewLine}{commands.ToDebuggerValue(_typesForStringRepresentation).Dump(_settings.IndentationSpaces, _settings.NewLine, 2)}");
	}

	/// <summary>
	/// Subscribed to by the program but never emits anything since this is not a time travel debugger.
	/// </summary>
	/// <param name="observer">The observer to subscribe.</param>
	/// <returns>A disposable instance representing the subscription.</returns>
	public IDisposable Subscribe(IObserver<TState> observer)
	{
		return Observable.Never<TState>().Subscribe(observer);
	}
}

/// <summary>
/// Command-less logging debugger for use with programs that do not use commands.
/// </summary>
/// <typeparam name="TState">The program state type.</typeparam>
/// <typeparam name="TMessage">The program message type.</typeparam>
public class LogDebugger<TState, TMessage> : LogDebugger<TState, TMessage, object>, IDebugger<TState, TMessage>
	where TState : notnull
	where TMessage : notnull
{
	/// <summary>
	/// Constructor for time travel debugger.
	/// </summary>
	/// <param name="settings">Settings for configuring this debugger instance.</param>
	/// <param name="write">Callback abstracting the actual output operation (could be Debug.Write or log file or something else).</param>
	/// <param name="typesForStringRepresentation">Types of non-primitive values that should be displayed by calling ToString() instead of serializing properties, for instance Guid and DateTime.</param>
	public LogDebugger(LogDebuggerSettings settings, Action<string> write, params Type[] typesForStringRepresentation) 
		: base(settings, write, typesForStringRepresentation)
	{
	}

	/// <summary>
	/// Called by the program when the program's init function has been called.
	/// </summary>
	/// <param name="state">The initial state.</param>
	public void OnInit(TState state)
	{
		OnInit(state, ImmutableArray<object>.Empty);
	}

	/// <summary>
	/// Called by the program when the program's update function has been called.
	/// </summary>
	/// <param name="stateBefore">The state before the update function was called.</param>
	/// <param name="message">The message for which update was called.</param>
	/// <param name="stateAfter">The state after the update function was called.</param>
	public void OnUpdate(TState stateBefore, TMessage message, TState stateAfter)
	{
		OnUpdate(stateBefore, message, stateAfter, ImmutableArray<object>.Empty);
	}
}
