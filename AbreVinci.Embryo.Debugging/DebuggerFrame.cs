﻿namespace AbreVinci.Embryo.Debugging;

/// <summary>
/// A debugger frame representing a single init or update call inside the program.
/// </summary>
/// <param name="DebugState">The actual state at the end of the frame.</param>
/// <param name="BeforeState">A representation of the before state, if any.</param>
/// <param name="AfterState">A representation of the after state.</param>
/// <param name="Message">A representation of the message.</param>
/// <param name="Commands">A representation of the commands.</param>
/// <param name="ExpandedPaths">Any expanded value paths.</param>
public record DebuggerFrame(
	object DebugState,
	DebuggerValue? BeforeState,
	DebuggerValue AfterState,
	DebuggerValue? Message,
	DebuggerValue Commands,
	IImmutableSet<string> ExpandedPaths);
