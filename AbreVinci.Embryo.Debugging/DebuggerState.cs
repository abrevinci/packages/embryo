﻿namespace AbreVinci.Embryo.Debugging;

/// <summary>
/// The state of a <see cref="Debugger{TState, TMessage, TCommand}"/>.
/// </summary>
/// <param name="Frames">The debugger frames (by id).</param>
/// <param name="FrameIds">The frame ids.</param>
/// <param name="CurrentFrameId">The selected frame id.</param>
/// <param name="NextId">The id that will be used for the next added frame.</param>
public record DebuggerState(
	IImmutableDictionary<int, DebuggerFrame> Frames,
	IImmutableList<int> FrameIds,
	int CurrentFrameId,
	int NextId)
{
	/// <summary>
	/// Determines if the debugger is to be considered paused or not (looking at anything but the last frame).
	/// </summary>
	public bool IsPaused => CurrentFrameId < NextId - 1;
}
