﻿global using System;
global using System.Collections;
global using System.Collections.Generic;
global using System.Collections.Immutable;
global using System.ComponentModel;
global using System.Diagnostics;
global using System.Linq;
global using System.Reactive.Linq;
global using System.Reactive.Subjects;

// ReSharper disable once CheckNamespace
namespace System.Runtime.CompilerServices
{
	internal static class IsExternalInit { }
}
