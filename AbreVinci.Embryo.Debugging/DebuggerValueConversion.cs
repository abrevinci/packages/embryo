﻿using System.Globalization;
using System.Text;

namespace AbreVinci.Embryo.Debugging;

/// <summary>
/// Contains extension methods for converting to/from DebuggerValue.
/// </summary>
public static class DebuggerValueConversion
{
	/// <summary>
	/// Recursively converts the given object into a debugger value.
	/// </summary>
	/// <param name="obj">The object to convert.</param>
	/// <param name="typesForStringRepresentation">Types that should have ToString called instead of serializing properties (such as Guid and DateTime).</param>
	/// <returns>The computed DebuggerValue.</returns>
	public static DebuggerValue ToDebuggerValue(this object obj, params Type[] typesForStringRepresentation)
	{
		return obj switch
		{
			string str => new DebuggerValue.Single("string", $"\"{str}\""),

			float f => new DebuggerValue.Single("float", f.ToString(CultureInfo.InvariantCulture)),
			double d => new DebuggerValue.Single("double", d.ToString(CultureInfo.InvariantCulture)),

			_ when obj.GetType().IsPrimitive => new DebuggerValue.Single(obj.GetType().GetTypeName(), obj.ToString()!),

			IDictionary dictionary => 
				dictionary.Keys.Count > 0 ?
					new DebuggerValue.Compound(
						dictionary.GetType().GetTypeName(), 
						dictionary.Keys.Cast<object>().Select(key => key.ToString()!).ToImmutableArray(), 
						dictionary.Keys.Cast<object>().ToImmutableDictionary(key => key.ToString() ?? string.Empty, key => dictionary[key]?.ToDebuggerValue(typesForStringRepresentation))) :
					new DebuggerValue.Single(
						dictionary.GetType().GetTypeName(), "{}"),
			
			IEnumerable enumerable => 
				enumerable.Cast<object?>().ToList() is IList<object?> { Count: > 0 } list ?
					new DebuggerValue.Compound(
						enumerable.GetType().GetTypeName(), 
						list.Select((_, i) => i.ToString()).ToImmutableArray(),
						list.Select((v, i) => (k: i.ToString(), v: v?.ToDebuggerValue(typesForStringRepresentation))).ToImmutableDictionary(pair => pair.k, pair => pair.v)) :
					new DebuggerValue.Single(enumerable.GetType().GetTypeName(), "[]"),

			_ when typesForStringRepresentation.Contains(obj.GetType()) => new DebuggerValue.Single(obj.GetType().GetTypeName(), obj.ToString()!),

			_ =>
				TypeDescriptor.GetProperties(obj).Cast<PropertyDescriptor>().ToList() is IList<PropertyDescriptor> { Count: > 0 } list ?
					new DebuggerValue.Compound(
						obj.GetType().GetTypeName(),
						list.Select(p => p.Name).ToImmutableArray(),
						list.ToImmutableDictionary(p => p.Name, p => p.GetValue(obj)?.ToDebuggerValue(typesForStringRepresentation))) :
					new DebuggerValue.Single(obj.GetType().GetTypeName(), "{}"),
		};
	}

	private static string GetTypeName(this Type type)
	{
		if (type.IsPrimitive)
		{
			if (type == typeof(bool))
				return "bool";
			if (type == typeof(byte))
				return "byte";
			if (type == typeof(sbyte))
				return "sbyte";
			if (type == typeof(char))
				return "char";
			if (type == typeof(short))
				return "short";
			if (type == typeof(ushort))
				return "ushort";
			if (type == typeof(int))
				return "int";
			if (type == typeof(uint))
				return "uint";
			if (type == typeof(long))
				return "long";
			if (type == typeof(ulong))
				return "ulong";
			if (type == typeof(float))
				return "float";
			if (type == typeof(double))
				return "double";
			if (type == typeof(decimal))
				return "decimal";
		}
		if (type == typeof(string))
			return "string";
		if (type.GenericTypeArguments.Any())
		{
			return type.Name.Split('`')[0] + "<" + string.Join(", ", type.GenericTypeArguments.Select(GetTypeName)) + ">";
		}

		return type.Name;
	}

	/// <summary>
	/// Dumps the given debuggerValue to a string (for logging purposes).
	/// </summary>
	/// <param name="debuggerValue">The value to convert.</param>
	/// <param name="indentationSpaces">The number of spaces to use for indenting (default 2).</param>
	/// <param name="newLine">The new line sequence to use (default "\n").</param>
	/// <param name="startNestingLevel">The nesting level to start at (default 0).</param>
	/// <returns>The generated string.</returns>
	public static string Dump(this DebuggerValue? debuggerValue, int indentationSpaces = 2, string newLine = "\n", int startNestingLevel = 0)
	{
		var builder = new StringBuilder();

		void DumpImpl(DebuggerValue? value, string key, int nestingLevel)
		{
			var nestingSpaces = "".PadLeft(nestingLevel * indentationSpaces);

			if (value == null)
			{
				builder.Append($"{nestingSpaces}{key}null;{newLine}");
			}
			else if (value is DebuggerValue.Single single)
			{
				builder.Append($"{nestingSpaces}{key}{single.Type}: {single.Value};{newLine}");
			}
			else if (value is DebuggerValue.Compound compound)
			{
				if (compound.Values.Count > 0)
				{
					builder.Append($"{nestingSpaces}{key}{compound.Type}:{newLine}");
					builder.Append($"{nestingSpaces}{{{newLine}");
					foreach (var k in compound.ValueKeys)
					{
						DumpImpl(compound.Values[k], $"{k}: ", nestingLevel + 1);
					}
					builder.Append($"{nestingSpaces}}}{newLine}");
				}
				else
				{
					builder.Append($"{nestingSpaces}{key}{compound.Type}: {{}}{newLine}");
				}
			}
		}

		DumpImpl(debuggerValue, "", startNestingLevel);
		return builder.ToString();
	}
}
