﻿namespace AbreVinci.Embryo.Debugging;

/// <summary>
/// Interface for a debugger to implement if wanting to take advantage of the debugger view.
/// </summary>
public interface IDebuggerController
{
	/// <summary>
	/// An observable stream of the debugger´s state.
	/// </summary>
	IObservable<DebuggerState> State { get; }

	/// <summary>
	/// Navigate the debugger to the given frame.
	/// </summary>
	/// <param name="frameId">The id of the frame to navigate to.</param>
	void GoToFrame(int frameId);

	/// <summary>
	/// Control the expansion state of the value with the given path inside the frame with the given id.
	/// </summary>
	/// <param name="frameId">The frame inside which the value exists.</param>
	/// <param name="path">The path of the value.</param>
	/// <param name="isExpanded">Whether or not the value should be expanded.</param>
	void SetExpanded(int frameId, string path, bool isExpanded);
}
