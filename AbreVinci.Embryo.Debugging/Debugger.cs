﻿using AbreVinci.Embryo.App;

namespace AbreVinci.Embryo.Debugging;

/// <summary>
/// Default (time travel) debugger with view model and view support (views are in AbreVinci.Embryo.WPF).
/// </summary>
/// <typeparam name="TState">The program state type.</typeparam>
/// <typeparam name="TMessage">The program message type.</typeparam>
/// <typeparam name="TCommand">The program command type.</typeparam>
public class Debugger<TState, TMessage, TCommand> : IDebugger<TState, TMessage, TCommand>, IDebuggerController
	where TState : notnull
	where TMessage : notnull
	where TCommand : notnull
{
	private readonly DebuggerSettings _settings;
	private readonly Type[] _typesForStringRepresentation;
	private readonly BehaviorSubject<DebuggerState> _debuggerState;
	private readonly ISubject<TState> _debugState;

	/// <summary>
	/// Constructor for time travel debugger.
	/// </summary>
	/// <param name="settings">Settings to use for this debugger instance.</param>
	/// <param name="typesForStringRepresentation">Types of non-primitive values that should be displayed by calling ToString() instead of serializing properties, for instance Guid and DateTime.</param>
	public Debugger(DebuggerSettings settings, params Type[] typesForStringRepresentation)
	{
		_settings = settings;
		_typesForStringRepresentation = typesForStringRepresentation;
		_debuggerState = new BehaviorSubject<DebuggerState>(
			new DebuggerState(
				Frames: ImmutableDictionary<int, DebuggerFrame>.Empty,
				FrameIds: ImmutableArray<int>.Empty,
				CurrentFrameId: 0,
				NextId: 1));
		_debugState = new Subject<TState>();
	}

	/// <summary>
	/// An observable stream of the debugger´s state.
	/// </summary>
	public IObservable<DebuggerState> State => _debuggerState;

	/// <summary>
	/// Whether or not the debugger is paused (when looking at earlier states).
	/// </summary>
	public bool IsPaused => _debuggerState.Value.IsPaused;

	/// <summary>
	/// Called by the program when the program's init function has been called.
	/// </summary>
	/// <param name="state">The initial state.</param>
	/// <param name="commands">The initial commands.</param>
	public void OnInit(TState state, IImmutableList<TCommand> commands)
	{
		var frame = new DebuggerFrame(
			DebugState: state,
			BeforeState: null,
			AfterState: state.ToDebuggerValue(_typesForStringRepresentation),
			Message: null,
			Commands: commands.ToDebuggerValue(_typesForStringRepresentation),
			ExpandedPaths: ImmutableHashSet<string>.Empty);
		_debuggerState.OnNext(AddFrame(_debuggerState.Value, frame, _settings.MaxHistorySize));
	}

	/// <summary>
	/// Called by the program when the program's update function has been called.
	/// </summary>
	/// <param name="stateBefore">The state before the update function was called.</param>
	/// <param name="message">The message for which update was called.</param>
	/// <param name="stateAfter">The state after the update function was called.</param>
	/// <param name="commands">Any commands emitted by the update function.</param>
	public void OnUpdate(TState stateBefore, TMessage message, TState stateAfter, IImmutableList<TCommand> commands)
	{
		var frame = new DebuggerFrame(
			DebugState: stateAfter,
			BeforeState: stateBefore.ToDebuggerValue(_typesForStringRepresentation),
			AfterState: stateAfter.ToDebuggerValue(_typesForStringRepresentation),
			Message: message.ToDebuggerValue(_typesForStringRepresentation),
			Commands: commands.ToDebuggerValue(_typesForStringRepresentation),
			ExpandedPaths: ImmutableHashSet<string>.Empty);
		_debuggerState.OnNext(AddFrame(_debuggerState.Value, frame, _settings.MaxHistorySize));
	}

	/// <summary>
	/// Subscribed to by the program in order to allow the debugger to emit (old) states.
	/// </summary>
	/// <param name="observer">The observer to subscribe.</param>
	/// <returns>A disposable instance representing the subscription.</returns>
	public IDisposable Subscribe(IObserver<TState> observer)
	{
		return _debugState.Subscribe(observer);
	}

	/// <summary>
	/// Navigate the debugger to the given frame.
	/// </summary>
	/// <param name="frameId">The id of the frame to navigate to.</param>
	public void GoToFrame(int frameId)
	{
		if (_debuggerState.Value.Frames.TryGetValue(frameId, out var frame))
		{
			_debuggerState.OnNext(_debuggerState.Value with
			{
				CurrentFrameId = frameId
			});
			_debugState.OnNext((TState)frame.DebugState);
		}
	}

	/// <summary>
	/// Control the expansion state of the value with the given path inside the frame with the given id.
	/// </summary>
	/// <param name="frameId">The frame inside which the value exists.</param>
	/// <param name="path">The path of the value.</param>
	/// <param name="isExpanded">Whether or not the value should be expanded.</param>
	public void SetExpanded(int frameId, string path, bool isExpanded)
	{
		_debuggerState.OnNext(SetExpanded(_debuggerState.Value, frameId, path, isExpanded));
	}

	private static DebuggerState AddFrame(DebuggerState state, DebuggerFrame frame, int maxHistorySize)
	{
		var newFrames = state.Frames.Add(state.NextId, frame);
		var newFrameIds = state.FrameIds.Add(state.NextId);
		while (newFrames.Count > maxHistorySize)
		{
			newFrames = newFrames.Remove(newFrameIds[0]);
			newFrameIds = newFrameIds.RemoveAt(0);
		}
		var newState = state with
		{
			Frames = newFrames,
			FrameIds = newFrameIds,
			CurrentFrameId = state.NextId,
			NextId = state.NextId + 1
		};
		return newState;
	}
	
	private static DebuggerState SetExpanded(DebuggerState state, int frameId, string path, bool isExpanded)
	{
		if (state.Frames.TryGetValue(frameId, out var frame))
		{
			var newState = state with
			{
				Frames = state.Frames.SetItem(frameId, frame with
				{
					ExpandedPaths = isExpanded ? frame.ExpandedPaths.Add(path) : frame.ExpandedPaths.Remove(path)
				})
			};
			return newState;
		}
		return state;
	}
}

/// <summary>
/// Command-less (time travel) debugger for use with programs that do not use commands.
/// </summary>
/// <typeparam name="TState">The program state type.</typeparam>
/// <typeparam name="TMessage">The program message type.</typeparam>
public class Debugger<TState, TMessage> : Debugger<TState, TMessage, object>, IDebugger<TState, TMessage>
	where TState : notnull
	where TMessage : notnull
{
	/// <summary>
	/// Constructor for time travel debugger.
	/// </summary>
	/// <param name="settings">Settings to use for this debugger instance.</param>
	/// <param name="typesForStringRepresentation">Types of non-primitive values that should be displayed by calling ToString() instead of serializing properties, for instance Guid and DateTime.</param>
	public Debugger(DebuggerSettings settings, params Type[] typesForStringRepresentation) : base(settings, typesForStringRepresentation)
	{
	}

	/// <summary>
	/// Called by the program when the program's init function has been called.
	/// </summary>
	/// <param name="state">The initial state.</param>
	public void OnInit(TState state)
	{
		OnInit(state, ImmutableArray<object>.Empty);
	}

	/// <summary>
	/// Called by the program when the program's update function has been called.
	/// </summary>
	/// <param name="stateBefore">The state before the update function was called.</param>
	/// <param name="message">The message for which update was called.</param>
	/// <param name="stateAfter">The state after the update function was called.</param>
	public void OnUpdate(TState stateBefore, TMessage message, TState stateAfter)
	{
		OnUpdate(stateBefore, message, stateAfter, ImmutableArray<object>.Empty);
	}
}
