﻿namespace AbreVinci.Embryo.WPF;

/// <summary>
/// Utility to easily load data template type views from resource dictionaries.
/// </summary>
/// <remarks>This does not work if you are using user controls as views.</remarks>
public class ViewResourceDictionary : ResourceDictionary, IDataTemplateProvider
{
	private readonly string _viewBaseUri;

	/// <summary>
	/// Constructor
	/// </summary>
	/// <param name="viewBaseUri">The base pack url where view .xaml resource dictionaries are located.</param>
	public ViewResourceDictionary(string viewBaseUri = "pack://application:,,,/Views")
	{
		_viewBaseUri = viewBaseUri;
	}

	/// <summary>
	/// Tries to resolve the given data template. This will look in a file with the given name and .xaml extension 
	/// and try to find a data template with the name as the key.
	/// </summary>
	/// <param name="name">The name of the file (excluding .xaml) and data template key.</param>
	/// <returns>The data template if it was successfully located.</returns>
	/// <exception cref="System.Windows.ResourceReferenceKeyNotFoundException">When the data template could not be located.</exception>
	public DataTemplate GetDataTemplate(string name)
	{
		try
		{
			var uri = new Uri($"{_viewBaseUri}/{name}.xaml");
			MergedDictionaries.Add(new ResourceDictionary { Source = uri });
			if (this[name] is DataTemplate dataTemplate)
				return dataTemplate;
		}
		catch
        {
			// Ignored
        }

		throw new ResourceReferenceKeyNotFoundException($"Data template {name} could not be located.", name);
	}
}
