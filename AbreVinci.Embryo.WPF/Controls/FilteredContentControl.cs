﻿namespace AbreVinci.Embryo.WPF.Controls;

/// <summary>
/// Use like a ContentControl. Allows the control to be hidden when it's content is null irrespective of the current ContentTemplate.
/// </summary>
public class FilteredContentControl : HeaderedContentControl
{
	static FilteredContentControl()
	{
		DefaultStyleKeyProperty.OverrideMetadata(typeof(FilteredContentControl), new FrameworkPropertyMetadata(typeof(FilteredContentControl)));
	}

	/// <summary>
	/// Overrides the default OnContentChanged method adding logic to automatically hide the view when the content is null.
	/// </summary>
	/// <param name="oldContent">The previous content.</param>
	/// <param name="newContent">The new content.</param>
	protected override void OnContentChanged(object? oldContent, object? newContent)
	{
		base.OnContentChanged(oldContent, newContent);

		if (newContent == null)
		{
			ClearValue(HeaderTemplateProperty);
			ClearValue(HeaderProperty);
		}
		else
		{
			SetBinding(HeaderTemplateProperty, new Binding("ContentTemplate") { Source = this });
			SetBinding(HeaderProperty, new Binding("Content") { Source = this });
		}
	}
}
