﻿namespace AbreVinci.Embryo.WPF.Controls;

/// <summary>
/// Allows you to use the <see cref="CurrentRoute"/> to select which child <see cref="RouterItem"/> should be active. 
/// All other routes will not be bound (which differs from a TabControl). Using this control instead allows you to keep
/// view models that are not currently visible in an inactive state meaning they will be unsubscribed from their respective
/// observable streams and not incur any overhead.
/// This is an alternative to using a content control with different view models and relying on data template selection. This
/// allows an extra degree of control and explicitness.
/// </summary>
public class RouterControl : HeaderedItemsControl
{
    /// <summary>
    /// Definition for the <see cref="CurrentRoute"/> property.
    /// </summary>
    public static readonly DependencyProperty CurrentRouteProperty = DependencyProperty.Register("CurrentRoute", typeof(object), typeof(RouterControl), new FrameworkPropertyMetadata(OnCurrentRouteChanged));

    static RouterControl()
    {
        DefaultStyleKeyProperty.OverrideMetadata(typeof(RouterControl), new FrameworkPropertyMetadata(typeof(RouterControl)));
    }

    /// <summary>
    /// Any value that uniquely identifies a route from a <see cref="RouterItem"/>. Common types of values are strings, numbers or enums.
    /// If the current value for the current route is not found, no route will be active.
    /// </summary>
    public object CurrentRoute
    {
        get => GetValue(CurrentRouteProperty);
        set => SetValue(CurrentRouteProperty, value);
    }

    private static void OnCurrentRouteChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        var router = (RouterControl)d;
        router.Navigate();
    }

    /// <summary>
    /// Reacts to changes in the collection of registered routes by updating the active route.
    /// </summary>
    /// <param name="e">The event args telling what changed.</param>
    protected override void OnItemsChanged(NotifyCollectionChangedEventArgs e)
    {
        base.OnItemsChanged(e);
        Navigate();
    }

    private void Navigate()
    {
        var route = Items.OfType<RouterItem>().FirstOrDefault(v => Equals(v.Route, CurrentRoute));
        if (route == null)
        {
            ClearValue(HeaderTemplateProperty);
            ClearValue(HeaderProperty);
        }
        else
        {
            SetBinding(HeaderTemplateProperty, new Binding("ContentTemplate") { Source = route });
            SetBinding(HeaderProperty, new Binding("Content") { Source = route });
        }
    }
}

/// <summary>
/// To be used together with <see cref="RouterControl"/> to define what shall be displayed for a particular route.
/// </summary>
public class RouterItem : FrameworkElement
{
    /// <summary>
    /// Definition of the <see cref="Route"/> property.
    /// </summary>
    public static readonly DependencyProperty RouteProperty = DependencyProperty.Register("Route", typeof(object), typeof(RouterItem));

    /// <summary>
    /// Definition of the <see cref="Content"/> property.
    /// </summary>
    public static readonly DependencyProperty ContentProperty = DependencyProperty.Register("Content", typeof(object), typeof(RouterItem));

    /// <summary>
    /// Definition of the <see cref="ContentTemplate"/> property.
    /// </summary>
    public static readonly DependencyProperty ContentTemplateProperty = DependencyProperty.Register("ContentTemplate", typeof(DataTemplate), typeof(RouterItem));

    /// <summary>
    /// The route value for which the given <see cref="Content"/> should be active. It will be active if and only if 
    /// this value matches the parent <see cref="RouterControl"/>'s <see cref="RouterControl.CurrentRoute"/> property.
    /// </summary>
    public object Route
    {
        get => GetValue(RouteProperty);
        set => SetValue(RouteProperty, value);
    }

    /// <summary>
    /// The content (view model) to be activated and displayed once this route is active.
    /// </summary>
    public object Content
    {
        get => GetValue(ContentProperty);
        set => SetValue(ContentProperty, value);
    }

    /// <summary>
    /// The data template that should be used to display the <see cref="Content"/>. 
    /// If automatic type base data template selection is used, this can be omitted.
    /// </summary>
    public DataTemplate ContentTemplate
    {
        get => (DataTemplate)GetValue(ContentTemplateProperty);
        set => SetValue(ContentTemplateProperty, value);
    }
}
