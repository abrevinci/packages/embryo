﻿namespace AbreVinci.Embryo.WPF.Controls;

/// <summary>
/// A content control that will only be active for the given <see cref="Feature"/>.
/// </summary>
public class ActivateForFeatureControl : HeaderedContentControl
{
	/// <summary>
	/// Set this to the feature for which this control should be active.
	/// </summary>
	public static readonly DependencyProperty FeatureProperty =
		DependencyProperty.Register(nameof(Feature), typeof(object), typeof(ActivateForFeatureControl),
			new FrameworkPropertyMetadata(OnFeatureChanged));

	static ActivateForFeatureControl()
	{
		DefaultStyleKeyProperty.OverrideMetadata(typeof(ActivateForFeatureControl), new FrameworkPropertyMetadata(typeof(ActivateForFeatureControl)));
	}

	/// <summary>
	/// Set this to the feature for which this control should be active.
	/// </summary>
	public object? Feature
	{
		get => GetValue(FeatureProperty);
		set => SetValue(FeatureProperty, value);
	}

	private static void OnFeatureChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
	{
		if (d is ActivateForFeatureControl control)
		{
			control.Update();
		}
	}

	internal void OnFeatureToggleChanged(object? sender, ViewFeatureToggleChangedEventArgs e)
	{
		if (e.Feature == Feature)
		{
			Update();
		}
	}
	
	private void Update()
	{
		if (ViewFeatureToggles.GetContext(this) is { } context && Feature is { } feature)
		{
			if (context.IsFeatureEnabled(feature))
			{
				SetBinding(HeaderTemplateProperty, new Binding("ContentTemplate") { Source = this });
				SetBinding(HeaderProperty, new Binding("Content") { Source = this });
			}
			else
			{
				ClearValue(HeaderTemplateProperty);
				ClearValue(HeaderProperty);
			}
		}
	}
}
