﻿using System.Windows.Controls.Primitives;
using Microsoft.Xaml.Behaviors;

namespace AbreVinci.Embryo.WPF.Behaviors;

/// <summary>
/// Behavior that can be used to bind a value to a slider and not update the binding while dragging.
/// </summary>
public class SliderNoDragBindingBehavior : Behavior<Slider>
{
	/// <summary>
	/// The value dependency property.
	/// </summary>
	public static readonly DependencyProperty ValueProperty = 
		DependencyProperty.Register(nameof(Value), typeof(double), typeof(SliderNoDragBindingBehavior), 
			new PropertyMetadata(0.0, OnValuePropertyChanged));

	/// <summary>
	/// The value property that will not update while the slider thumb is being dragged.
	/// </summary>
	public double Value
	{
		get => (double)GetValue(ValueProperty);
		set => SetValue(ValueProperty, value);
	}

	private bool _isDragging;

	/// <summary>
	/// Called when this behavior is attached to a slider.
	/// </summary>
	protected override void OnAttached()
	{
		DragStartedEventHandler dragStarted = OnDragStarted;
		AssociatedObject.AddHandler(Thumb.DragStartedEvent, dragStarted);
		DragCompletedEventHandler dragCompleted = OnDragCompleted;
		AssociatedObject.AddHandler(Thumb.DragCompletedEvent, dragCompleted);

		AssociatedObject.ValueChanged += OnValueChanged;

		AssociatedObject.Value = Value;
	}

	/// <summary>
	/// Call when this behavior is detached from a slider.
	/// </summary>
	protected override void OnDetaching()
	{
		AssociatedObject.ValueChanged -= OnValueChanged;

		DragCompletedEventHandler dragCompleted = OnDragCompleted;
		AssociatedObject.RemoveHandler(Thumb.DragCompletedEvent, dragCompleted);
		DragStartedEventHandler dragStarted = OnDragStarted;
		AssociatedObject.RemoveHandler(Thumb.DragStartedEvent, dragStarted);
	}

	private static void OnValuePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
	{
		var behavior = (SliderNoDragBindingBehavior)d;
		if (behavior.AssociatedObject != null)
			behavior.AssociatedObject.Value = behavior.Value;
	}

	private void OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
	{
		if (!_isDragging)
			Value = e.NewValue;
	}

	private void OnDragStarted(object sender, DragStartedEventArgs e)
	{
		_isDragging = true;
	}

	private void OnDragCompleted(object sender, DragCompletedEventArgs e)
	{
		_isDragging = false;
		Value = AssociatedObject.Value;
	}
}
