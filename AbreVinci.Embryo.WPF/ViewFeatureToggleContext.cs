﻿using AbreVinci.Embryo.App;
using AbreVinci.Embryo.WPF.Controls;

namespace AbreVinci.Embryo.WPF;

/// <summary>
/// A context to be set to a view hierarchy using <see cref="ViewFeatureToggles.SetContext(DependencyObject, IViewFeatureToggleContext?)"/>.
/// It allows <see cref="ActivateForFeatureControl"/> and <see cref="DeactivateForFeatureControl"/> to check feature toggle states.
/// </summary>
/// <typeparam name="TFeatureToggle">The type of feature toggle used by the application.</typeparam>
public class ViewFeatureToggleContext<TFeatureToggle> : IViewFeatureToggleContext where TFeatureToggle : notnull
{
	private readonly IFeatureToggleDictionary<TFeatureToggle> _featureToggleDictionary;

	/// <summary>
	/// Constructor.
	/// </summary>
	/// <param name="featureToggleDictionary">An underlying feature toggle dictionary that this class acts as an adapter for.</param>
	public ViewFeatureToggleContext(IFeatureToggleDictionary<TFeatureToggle> featureToggleDictionary)
	{
		_featureToggleDictionary = featureToggleDictionary;
		_featureToggleDictionary.FeatureToggleChanged += feature =>
			FeatureToggleChanged?.Invoke(this, new ViewFeatureToggleChangedEventArgs(feature));
	}

	/// <summary>
	/// Check if the given feature is enabled or not.
	/// </summary>
	/// <param name="feature">The feature to check.</param>
	/// <returns>True if the feature is enabled, false otherwise.</returns>
	public bool IsFeatureEnabled(object feature) => _featureToggleDictionary.IsFeatureEnabled((TFeatureToggle)feature);

	/// <summary>
	/// Notify the view that the feature status has changed.
	/// </summary>
	public event EventHandler<ViewFeatureToggleChangedEventArgs>? FeatureToggleChanged;
}
