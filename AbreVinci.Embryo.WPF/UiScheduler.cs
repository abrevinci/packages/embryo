﻿namespace AbreVinci.Embryo.WPF;

/// <summary>
/// Use this to schedule things on the UI thread.
/// </summary>
public class UiScheduler : SynchronizationContextScheduler
{
	/// <summary>
	/// Constructor
	/// </summary>
	/// <param name="dispatcher">The dispatcher to execute on.</param>
	public UiScheduler(Dispatcher dispatcher) : base(new DispatcherSynchronizationContext(dispatcher))
	{
	}
}
