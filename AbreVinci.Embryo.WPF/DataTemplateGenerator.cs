﻿namespace AbreVinci.Embryo.WPF;

/// <summary>
/// A data template provider capable of generating data templates from types user controls based on the control type name.
/// </summary>
public class DataTemplateGenerator : IDataTemplateProvider
{
	private readonly Type[] _viewTypes;

	/// <summary>
	/// Instantiate the generator and provide the types it needs to support.
	/// </summary>
	/// <param name="viewTypes">The user control types that this instance is capable of generating data tempates for.</param>
	public DataTemplateGenerator(params Type[] viewTypes)
	{
		_viewTypes = viewTypes;
	}

	/// <summary>
	/// Tries to find a view type with the given name and create a data template for it.
	/// </summary>
	/// <param name="name">The type name of the user control to wrap in a data template.</param>
	/// <returns>The generated data template.</returns>
	/// <exception cref="System.Windows.ResourceReferenceKeyNotFoundException">When the view type could not be located.</exception>
	public DataTemplate GetDataTemplate(string name)
	{
		try
		{
			if (_viewTypes.FirstOrDefault(t => t.Name == name) is { } type)
			{
				var template = new DataTemplate
				{
					VisualTree = new FrameworkElementFactory(type)
				};
				return template;
			}
		}
		catch
		{
			// Ignored
		}

		throw new ResourceReferenceKeyNotFoundException($"Data template for view {name} could not be created.", name);
	}
}
