﻿using AbreVinci.Embryo.ReactiveUI;

namespace AbreVinci.Embryo.WPF;

/// <summary>
/// window for use with window controllers inside user interfaces.
/// </summary>
public class ReactiveWindow : Window, IReactiveWindow
{
	/// <summary>
	/// This is what gets called on the dispatcher when the window controller determines that the window should be shown.
	/// By default this calls <see cref="System.Windows.Window.Show"/>.
	/// </summary>
	protected virtual void ShowImpl()
	{
		Show();
	}

	/// <summary>
	/// This is what gets called on the dispatcher when the window controller determines that the window should be hidden.
	/// By default this calls <see cref="System.Windows.Window.Hide"/>.
	/// </summary>
	protected virtual void HideImpl()
	{
		Hide();
	}

	/// <summary>
	/// This is what gets called on the dispatcher when the window controller determines that the window should be closed.
	/// By default this calls <see cref="System.Windows.Window.Close"/>.
	/// </summary>
	protected virtual void CloseImpl()
	{
		Close();
	}

	void IReactiveWindow.Show()
	{
		Dispatcher.BeginInvoke(ShowImpl);
	}

	void IReactiveWindow.Hide()
	{
		Dispatcher.BeginInvoke(HideImpl);
	}

	void IReactiveWindow.Close()
	{
		Dispatcher.BeginInvoke(CloseImpl);
	}
}
