﻿using AbreVinci.Embryo.WPF.Controls;

namespace AbreVinci.Embryo.WPF;

/// <summary>
/// Contains the attached property <see cref="ContextProperty"/> for providing an <see cref="IViewFeatureToggleContext"/> to views.
/// </summary>
public static class ViewFeatureToggles
{
	/// <summary>
	/// Inherited context property to be set on parent view (usually the window) and can be retrieved from any child view.
	/// </summary>
	public static readonly DependencyProperty ContextProperty =
		DependencyProperty.RegisterAttached("Context", typeof(IViewFeatureToggleContext), typeof(ActivateForFeatureControl),
			new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.Inherits, OnContextChanged));

	/// <summary>
	/// Gets the active context for the given dependency object (view).
	/// </summary>
	/// <param name="d">The dependency object for which to get the context.</param>
	/// <returns>A <see cref="IViewFeatureToggleContext"/>, if any.</returns>
	public static IViewFeatureToggleContext? GetContext(DependencyObject d) => (IViewFeatureToggleContext?)d.GetValue(ContextProperty);

	/// <summary>
	/// Sets the active context for the given dependency object (view).
	/// </summary>
	/// <param name="d">The dependency object for which to set the context.</param>
	/// <param name="value">The <see cref="IViewFeatureToggleContext"/> to set, if any.</param>
	public static void SetContext(DependencyObject d, IViewFeatureToggleContext? value) => d.SetValue(ContextProperty, value);

	private static void OnContextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
	{
		if (d is ActivateForFeatureControl activateControl)
		{
			if (e.OldValue is IViewFeatureToggleContext oldContext)
			{
				oldContext.FeatureToggleChanged -= activateControl.OnFeatureToggleChanged;
			}
			if (e.NewValue is IViewFeatureToggleContext newContext)
			{
				newContext.FeatureToggleChanged += activateControl.OnFeatureToggleChanged;
			}
		}
		if (d is DeactivateForFeatureControl deactivateControl)
		{
			if (e.OldValue is IViewFeatureToggleContext oldContext)
			{
				oldContext.FeatureToggleChanged -= deactivateControl.OnFeatureToggleChanged;
			}
			if (e.NewValue is IViewFeatureToggleContext newContext)
			{
				newContext.FeatureToggleChanged += deactivateControl.OnFeatureToggleChanged;
			}
		}
	}
}
