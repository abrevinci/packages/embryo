﻿[assembly: ThemeInfo(ResourceDictionaryLocation.None, ResourceDictionaryLocation.SourceAssembly)]
[assembly: XmlnsPrefix("http://schemas.abrevinci.com/em/", "em")]
[assembly: XmlnsDefinition("http://schemas.abrevinci.com/em/", "AbreVinci.Embryo.WPF.Behaviors")]
[assembly: XmlnsDefinition("http://schemas.abrevinci.com/em/", "AbreVinci.Embryo.WPF.Controls")]
