﻿namespace AbreVinci.Embryo.WPF;

/// <summary>
/// Contract interface for a locator of data templates.
/// </summary>
public interface IDataTemplateProvider
{
	/// <summary>
	/// Tries to get the given data template.
	/// </summary>
	/// <param name="name">The name of the data template to find.</param>
	/// <returns>The data template if successful, throws otherwise.</returns>
	DataTemplate GetDataTemplate(string name);
}
