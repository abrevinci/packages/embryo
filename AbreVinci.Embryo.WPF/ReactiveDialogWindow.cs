﻿namespace AbreVinci.Embryo.WPF;

/// <summary>
/// Dialog window for use with window controllers inside user interfaces.
/// </summary>
public class ReactiveDialogWindow : ReactiveWindow
{
	/// <summary>
	/// Overrides <see cref="ReactiveWindow.ShowImpl"/> to call ShowDialog.
	/// </summary>
	protected override void ShowImpl()
	{
		ShowDialog();
	}
}
