﻿using AbreVinci.Embryo.WPF.Controls;

namespace AbreVinci.Embryo.WPF;

/// <summary>
/// A context to be set to a view hierarchy using <see cref="ViewFeatureToggles.SetContext(DependencyObject, IViewFeatureToggleContext?)"/>.
/// It allows <see cref="ActivateForFeatureControl"/> and <see cref="DeactivateForFeatureControl"/> to check feature toggle states.
/// </summary>
public interface IViewFeatureToggleContext
{
	/// <summary>
	/// Check if the given feature is enabled or not.
	/// </summary>
	/// <param name="feature">The feature to check.</param>
	/// <returns>True if the feature is enabled, false otherwise.</returns>
	bool IsFeatureEnabled(object feature);

	/// <summary>
	/// Notify the view that the feature status has changed.
	/// </summary>
	event EventHandler<ViewFeatureToggleChangedEventArgs> FeatureToggleChanged;
}
