﻿using System.Collections.Generic;
using AbreVinci.Embryo.ReactiveUI;

namespace AbreVinci.Embryo.WPF;

/// <summary>
/// Reactive window factory for creating WPF windows based on <see cref="ReactiveWindow"/>.
/// </summary>
public abstract class ReactiveWindowFactory : IReactiveWindowFactory
{
	private readonly IDataTemplateProvider _dataTemplateProvider;
	private readonly IDataTemplateProvider _debuggerDataTemplateProvider;
	private readonly Dictionary<string, Func<ReactiveWindow>> _factories;

	/// <summary>
	/// Constructor allowing to override the data template provider being used.
	/// </summary>
	/// <param name="dataTemplateProvider">The data template provider to use (normally a <see cref="ViewResourceDictionary"/>).</param>
	/// <param name="debuggerDataTemplateProvider">An optional data template provider to use for a debugger window. When null, <paramref name="dataTemplateProvider"/> will be used for debugger windows as well.</param>
	protected ReactiveWindowFactory(IDataTemplateProvider dataTemplateProvider, IDataTemplateProvider? debuggerDataTemplateProvider)
	{
		_dataTemplateProvider = dataTemplateProvider;
		_debuggerDataTemplateProvider = debuggerDataTemplateProvider ?? _dataTemplateProvider;
		_factories = new Dictionary<string, Func<ReactiveWindow>>();
	}

	/// <summary>
	/// Constructor allowing to override the data template provider being used.
	/// </summary>
	/// <param name="dataTemplateProvider">The data template provider to use (normally a <see cref="ViewResourceDictionary"/>).</param>
	protected ReactiveWindowFactory(IDataTemplateProvider dataTemplateProvider) : this(dataTemplateProvider, dataTemplateProvider)
	{
		_dataTemplateProvider = dataTemplateProvider;
		_factories = new Dictionary<string, Func<ReactiveWindow>>();
	}

	/// <summary>
	/// Default constructor using a default <see cref="ViewResourceDictionary"/> instance for the <see cref="IDataTemplateProvider"/> of this factory.
	/// </summary>
	protected ReactiveWindowFactory() : this(new ViewResourceDictionary(), new ViewResourceDictionary())
	{
	}

	/// <summary>
	/// Register a window factory function for a particular window name.
	/// </summary>
	/// <typeparam name="TWindow">The type of the window to create (must be a <see cref="ReactiveWindow"/> or a subclass thereof).</typeparam>
	/// <param name="windowName">The name of the window as referenced in <see cref="CreateWindow{TViewModel}"/>.</param>
	/// <param name="dataTemplateName">The name of the data template to get using the data template provider.</param>
	/// <param name="createWindow">A window factory function allowing customized window setup, however the Content and ContentTemplate properties are reserved as they will be overwritten.</param>
	/// <exception cref="System.InvalidOperationException">When a window with the specified name already exists.</exception>
	/// <example>
	///	[!code-csharp[Example](../../DocSnippets/WPF/ReactiveWindowFactoryBase/RegisterWindow/WindowFactory.cs#Snippet)]
	/// </example>
	protected void RegisterWindow<TWindow>(string windowName, string dataTemplateName, Func<TWindow> createWindow) where TWindow : ReactiveWindow
	{
		if (_factories.ContainsKey(windowName))
			throw new InvalidOperationException();

		var dataTemplate = _dataTemplateProvider.GetDataTemplate(dataTemplateName);
		
		_factories.Add(
			windowName,
			() =>
			{
				var window = createWindow();
				window.ContentTemplate = dataTemplate;
				return window;
			});
	}

	/// <summary>
	/// Register a window factory function for a particular window name using the default debugger view data template.
	/// </summary>
	/// <typeparam name="TWindow">The type of the window to create (must be a <see cref="ReactiveWindow"/> or a subclass thereof).</typeparam>
	/// <param name="windowName">The name of the window as referenced in <see cref="CreateWindow{TViewModel}"/>.</param>
	/// <param name="createWindow">A window factory function allowing customized window setup, however the Content and ContentTemplate properties are reserved as they will be overwritten.</param>
	/// <exception cref="System.InvalidOperationException">When a window with the specified name already exists.</exception>
	protected void RegisterDebuggerWindow<TWindow>(string windowName, Func<TWindow> createWindow) where TWindow : ReactiveWindow
	{
		if (_factories.ContainsKey(windowName))
			throw new InvalidOperationException();

		var dataTemplate = _debuggerDataTemplateProvider.GetDataTemplate("DebuggerView");

		_factories.Add(
			windowName,
			() =>
			{
				var window = createWindow();
				window.ContentTemplate = dataTemplate;
				return window;
			});
	}

	/// <summary>
	/// Creates a window to be used with a window controller.
	/// </summary>
	/// <typeparam name="TViewModel">The type of view model to assign to the window.</typeparam>
	/// <param name="windowName">The name of the window as registered using <see cref="RegisterWindow{TWindow}"/> or <see cref="RegisterDebuggerWindow{TWindow}"/>.</param>
	/// <param name="createViewModel">A factory function for the view model to be assigned to the created window.</param>
	/// <returns>The created window.</returns>
	/// <exception cref="System.InvalidOperationException">When a window with the given name has not been registered.</exception>
	public IReactiveWindow CreateWindow<TViewModel>(string windowName, Func<TViewModel> createViewModel) where TViewModel : notnull
	{
		if (!_factories.TryGetValue(windowName, out var factory))
			throw new InvalidOperationException();

		var window = factory();
		window.Content = createViewModel();
		window.Closed += (_, _) => (window.Content as IDisposable)?.Dispose();
		return window;
	}
}
