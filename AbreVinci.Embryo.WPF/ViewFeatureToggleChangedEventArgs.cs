﻿namespace AbreVinci.Embryo.WPF;

/// <summary>
/// Event args for <see cref="IViewFeatureToggleContext.FeatureToggleChanged"/>.
/// </summary>
public class ViewFeatureToggleChangedEventArgs : EventArgs
{
	/// <summary>
	/// Constructor.
	/// </summary>
	/// <param name="feature">The feature that has changed.</param>
	public ViewFeatureToggleChangedEventArgs(object feature)
	{
		Feature = feature;
	}

	/// <summary>
	/// The feature that has changed.
	/// </summary>
	public object Feature { get; }
}
