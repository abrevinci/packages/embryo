﻿using AbreVinci.Embryo.Core;
using AbreVinci.Embryo.Generator.Attributes;

namespace DocSnippets.Core.ActionContext.WithState;
	
#region Snippet
using ActionContext = ActionContext<State, Command>;

public static class Actions
{
	[Action]
	public static ActionContext SetValue(this ActionContext context, int value)
	{
		var state = context.State;
		var newState = state with
		{
			Value = value
		};
		return context.WithState(newState);
	}
}
#endregion

public record State(int Value);
public record Command;
