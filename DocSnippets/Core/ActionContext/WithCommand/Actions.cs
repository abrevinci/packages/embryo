﻿using AbreVinci.Embryo.Core;
using AbreVinci.Embryo.Generator.Attributes;

namespace DocSnippets.Core.ActionContext.WithCommand;
	
#region Snippet
using ActionContext = ActionContext<State, Command>;

public static class Actions
{
	[Action]
	public static ActionContext Save(this ActionContext context)
	{
		var state = context.State;
		return context.WithCommand(new Command.Save(state.Value));
	}
}
#endregion

public record State(int Value);

public record Command
{
	public record Save(int Value) : Command;
}
