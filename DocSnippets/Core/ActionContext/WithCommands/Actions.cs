﻿using AbreVinci.Embryo.Core;
using AbreVinci.Embryo.Generator.Attributes;

namespace DocSnippets.Core.ActionContext.WithCommands;
	
#region Snippet
using ActionContext = ActionContext<State, Command>;

public static class Actions
{
	[Action]
	public static ActionContext OnLoggedIn(this ActionContext context, User user)
	{
		var state = context.State;
		var newState = state with
		{
			User = user
		};
		return context
			.WithState(newState)
			.WithCommands(new Command.FetchSharedData(), new Command.FetchUserData(user));
	}
}
#endregion

public record User(string Token);

public record State(User User);

public record Command
{
	public record FetchSharedData : Command;
	public record FetchUserData(User User) : Command;
}
