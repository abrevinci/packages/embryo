﻿using AbreVinci.Embryo.Core;
using AbreVinci.Embryo.Generator.Attributes;

namespace DocSnippets.Core.EffectContext.Dispatch;

#region Snippet
public static class Effects
{
	[Effect]
	public static async Task DelayDispatchAsync(
		EffectContext<Message> context, 
		Message message, 
		TimeSpan delay)
	{
		await Task.Delay(delay);
		context.Dispatch(message);
	}
}
#endregion
