﻿using AbreVinci.Embryo.Core;
using AbreVinci.Embryo.Generator.Attributes;

namespace DocSnippets.Core.EffectContext.Dispatch;

#region Snippet
using PersistanceEffectContext = EffectContext<IPersistanceService, Message>;

public static class PeristanceEffects
{
	[Effect]
	public static async Task SaveAsync(PersistanceEffectContext context, Data data)
	{
		try
		{
			await context.Service.SaveAsync(data);
			context.Dispatch(new Message.OnSaveCompleted());
		}
		catch
		{
			context.Dispatch(new Message.OnSaveFailed());
		}
	}
}
#endregion

public record Data;

public record Message
{
	public record OnSaveCompleted : Message;
	public record OnSaveFailed : Message;
}

public interface IPersistanceService
{
	Task SaveAsync(Data data);
}
