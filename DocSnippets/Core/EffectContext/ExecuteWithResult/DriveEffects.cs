﻿using System.Collections.Immutable;
using System.IO;
using AbreVinci.Embryo.Core;
using AbreVinci.Embryo.Generator.Attributes;

namespace DocSnippets.Core.EffectContext.ExecuteWithResult;

#region Snippet
public static class DriveEffects
{
	[Effect]
	public static void DetectDrives(this EffectContext<Message> context)
	{
		context.Execute(
			() => DriveInfo.GetDrives().Select(d => d.Name).ToImmutableArray(),
			createOnCompletionMessage: drives => new Message.OnDrivesDetected(drives));
	}
}
#endregion
