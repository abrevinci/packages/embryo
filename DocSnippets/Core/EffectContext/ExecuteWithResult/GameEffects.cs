﻿using System.Collections.Immutable;
using AbreVinci.Embryo.Core;
using AbreVinci.Embryo.Generator.Attributes;

namespace DocSnippets.Core.EffectContext.ExecuteWithResult;

#region Snippet
using GameEffectContext = EffectContext<IGameService, Message>;

public static class GameEffects
{
	[Effect]
	public static void CreateNewGame(GameEffectContext context)
	{
		context.Execute(
			service => service.CreateNewGame(),
			createOnCompletionMessage: game => new Message.OnNewGameCreated(game));
	}
}
#endregion

public record GameState;

public record Message
{
	public record OnNewGameCreated(GameState GameState) : Message;
	public record OnDrivesDetected(IImmutableList<string> DriveLetters) : Message;
}

public interface IGameService
{
	GameState CreateNewGame();
}
