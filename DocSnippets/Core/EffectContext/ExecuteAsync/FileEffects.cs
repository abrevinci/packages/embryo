﻿using System.IO;
using AbreVinci.Embryo.Core;
using AbreVinci.Embryo.Generator.Attributes;

namespace DocSnippets.Core.EffectContext.ExecuteAsync;

#region Snippet
public static class FileEffects
{
	[Effect]
	public static Task WriteAllTextAsync(
		EffectContext<Message> context, 
		string filePath, 
		string text)
	{
		return context.ExecuteAsync(
			() => File.WriteAllTextAsync(filePath, text),
			createOnCompletionMessage: () => new Message.OnTextWritten(filePath));
	}
}
#endregion
