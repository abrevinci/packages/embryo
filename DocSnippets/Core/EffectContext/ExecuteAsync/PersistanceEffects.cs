﻿using AbreVinci.Embryo.Core;
using AbreVinci.Embryo.Generator.Attributes;

namespace DocSnippets.Core.EffectContext.ExecuteAsync;

#region Snippet
using PersistanceEffectContext = EffectContext<IPersistanceService, Message>;

public static class PersistanceEffects
{
	[Effect]
	public static Task SaveAsync(PersistanceEffectContext context, Data data)
	{
		return context.ExecuteAsync(
			service => service.SaveAsync(data),
			createOnStartMessage: () => new Message.OnSaveStarted(),
			createOnCompletionMessage: () => new Message.OnSaveCompleted(),
			createOnExceptionMessage: exception => new Message.OnSaveFailed(exception));
	}
}
#endregion

public record Data;

public record Message
{
	public record OnSaveStarted : Message;
	public record OnSaveCompleted : Message;
	public record OnSaveFailed(Exception Exception) : Message;

	public record OnTextWritten(string FilePath) : Message;
}

public interface IPersistanceService
{
	Task SaveAsync(Data data);
}
