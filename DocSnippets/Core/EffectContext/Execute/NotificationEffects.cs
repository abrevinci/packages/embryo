﻿using AbreVinci.Embryo.Core;
using AbreVinci.Embryo.Generator.Attributes;

namespace DocSnippets.Core.EffectContext.Execute;
	
#region Snippet
using NotificationEffectContext = EffectContext<INotificationService, Message>;

public static class NotificationEffects
{
	[Effect]
	public static void ShowNotification(NotificationEffectContext context, string message)
	{
		context.Execute(
			service => service.ShowNotification(message),
			createOnCompletionMessage: () => new Message.OnShowNotificationSucceeded(),
			createOnExceptionMessage: exception => new Message.OnException(exception));
	}
}
#endregion

public record Data;

public record Message
{
	public record OnShowNotificationSucceeded : Message;
	public record OnException(Exception Exception) : Message;
}

public interface INotificationService
{
	void ShowNotification(string message);
}
