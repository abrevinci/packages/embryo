﻿using System.Diagnostics;
using AbreVinci.Embryo.Core;
using AbreVinci.Embryo.Generator.Attributes;

namespace DocSnippets.Core.EffectContext.Execute;
	
#region Snippet
public static class BrowseEffects
{
	[Effect]
	public static void OpenInBrowser(EffectContext<Message> context, string url)
	{
		context.Execute(
			() => Process.Start(url),
			createOnExceptionMessage: exception => new Message.OnException(exception));
	}
}
#endregion
