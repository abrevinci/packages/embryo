﻿using System.IO;
using AbreVinci.Embryo.Core;
using AbreVinci.Embryo.Generator.Attributes;

namespace DocSnippets.Core.EffectContext.ExecuteAsyncWithResult;

#region Snippet
public static class FileEffects
{
	[Effect]
	public static Task ReadAllTextAsync(EffectContext<Message> context, string filePath)
	{
		return context.ExecuteAsync(
			() => File.ReadAllTextAsync(filePath),
			createOnCompletionMessage: text => new Message.OnTextRead(filePath, text));
	}
}
#endregion
