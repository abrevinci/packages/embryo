﻿using AbreVinci.Embryo.Core;
using AbreVinci.Embryo.Generator.Attributes;

namespace DocSnippets.Core.EffectContext.ExecuteAsyncWithResult;

#region Snippet
using PersistanceEffectContext = EffectContext<IPersistanceService, Message>;

public static class PersistanceEffects
{
	[Effect]
	public static Task LoadAsync(PersistanceEffectContext context)
	{
		return context.ExecuteAsync(
			service => service.LoadAsync(),
			createOnStartMessage: () => new Message.OnLoadStarted(),
			createOnCompletionMessage: data => new Message.OnLoadCompleted(data),
			createOnExceptionMessage: exception => new Message.OnLoadFailed(exception));
	}
}
#endregion

public record Data;

public record Message
{
	public record OnLoadStarted : Message;
	public record OnLoadCompleted(Data data) : Message;
	public record OnLoadFailed(Exception Exception) : Message;

	public record OnTextRead(string FilePath, string Text) : Message;
}

public interface IPersistanceService
{
	Task<Data> LoadAsync();
}
