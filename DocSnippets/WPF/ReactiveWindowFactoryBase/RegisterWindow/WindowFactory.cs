﻿using System.Windows;
using AbreVinci.Embryo.ReactiveUI;
using AbreVinci.Embryo.WPF;

namespace DocSnippets.WPF.ReactiveWindowFactoryBase.RegisterWindow;

#region Snippet
public class WindowFactory : ReactiveWindowFactory
{
	public WindowFactory()
	{
		RegisterWindow(
			nameof(UserInterface.MainWindow), 
			"MainWindowView", 
			CreateMainWindow);
	}

	private ReactiveWindow CreateMainWindow()
	{
		return new ReactiveWindow
		{
			Title = "Main Window",
			Width = 640,
			Height = 480,
			WindowStartupLocation = WindowStartupLocation.CenterScreen
		};
	}
}
#endregion

public class MainWindowViewModel : ReactiveViewModel
{
}

public class UserInterface : ReactiveUserInterface
{
	public UserInterface(
		IReactiveViewModelHost viewModelHost, 
		IReactiveWindowFactory windowFactory) 
		: base(viewModelHost, windowFactory)
	{
		MainWindow = AddWindow(nameof(MainWindow), () => new MainWindowViewModel());
	}

	public IReactiveWindowController<MainWindowViewModel> MainWindow { get; }
}
