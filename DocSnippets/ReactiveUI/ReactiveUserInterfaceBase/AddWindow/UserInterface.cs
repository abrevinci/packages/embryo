﻿using AbreVinci.Embryo.ReactiveUI;

namespace DocSnippets.ReactiveUI.ReactiveUserInterfaceBase.AddWindow;

#region Snippet
public class UserInterface : ReactiveUserInterface
{
	public UserInterface(
		ViewModelFactory viewModelFactory,
		IReactiveViewModelHost viewModelHost, 
		IReactiveWindowFactory windowFactory) 
		: base(viewModelHost, windowFactory)
	{
		MainWindow = AddWindow(
			nameof(MainWindow), 
			viewModelFactory.CreateMainWindowViewModel);
	}

	public IReactiveWindowController<MainWindowViewModel> MainWindow { get; }
}
#endregion

public class MainWindowViewModel : ReactiveViewModel
{
}

public class ViewModelFactory
{
	public MainWindowViewModel CreateMainWindowViewModel()
	{
		return new MainWindowViewModel();
	}
}
