﻿using AbreVinci.Embryo.ReactiveUI;

namespace DocSnippets.ReactiveUI.ReactiveUserInterfaceBase.AddWindowWithIsOpen;

#region Snippet
public class UserInterface : ReactiveUserInterface
{
	public UserInterface(
		IObservable<bool> isMainWindowOpen,
		Action<Message> dispatch,
		ViewModelFactory viewModelFactory,
		IReactiveViewModelHost viewModelHost,
		IReactiveWindowFactory windowFactory)
		: base(viewModelHost, windowFactory)
	{
		MainWindow = AddWindow(
			isMainWindowOpen,
			nameof(MainWindow),
			viewModelFactory.CreateMainWindowViewModel,
			() => dispatch(new Message.OnMainWindowClosed()));
	}

	public IReactiveWindowController<MainWindowViewModel> MainWindow { get; }
}
#endregion

public record Message
{
	public record OnMainWindowClosed : Message;
}

public class MainWindowViewModel : ReactiveViewModel
{
}

public class ViewModelFactory
{
	public MainWindowViewModel CreateMainWindowViewModel()
	{
		return new MainWindowViewModel();
	}
}