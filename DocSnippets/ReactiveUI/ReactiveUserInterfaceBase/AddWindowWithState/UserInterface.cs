﻿using AbreVinci.Embryo.ReactiveUI;

namespace DocSnippets.ReactiveUI.ReactiveUserInterfaceBase.AddWindowWithState;

#region Snippet
public class UserInterface : ReactiveUserInterface
{
	public UserInterface(
		IObservable<ReactiveWindowControllerState> mainWindowState,
		Action<Message> dispatch,
		ViewModelFactory viewModelFactory,
		IReactiveViewModelHost viewModelHost,
		IReactiveWindowFactory windowFactory)
		: base(viewModelHost, windowFactory)
	{
		MainWindow = AddWindow(
			mainWindowState,
			nameof(MainWindow),
			viewModelFactory.CreateMainWindowViewModel,
			() => dispatch(new Message.OnMainWindowClosed()));
	}

	public IReactiveWindowController<MainWindowViewModel> MainWindow { get; }
}
#endregion

public record Message
{
	public record OnMainWindowClosed : Message;
}

public class MainWindowViewModel : ReactiveViewModel
{
}

public class ViewModelFactory
{
	public MainWindowViewModel CreateMainWindowViewModel()
	{
		return new MainWindowViewModel();
	}
}
