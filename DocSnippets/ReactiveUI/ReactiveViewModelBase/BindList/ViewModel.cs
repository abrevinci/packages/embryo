﻿using System.Collections.Immutable;
using AbreVinci.Embryo.ReactiveUI;

namespace DocSnippets.ReactiveUI.ReactiveViewModelBase.BindList;

#region Snippet
public class ViewModel : ReactiveViewModel
{
	public ViewModel(IObservable<IImmutableList<string>> names)
	{
		Names = BindList(names);
	}

	public IEnumerable<string> Names { get; }
}
#endregion
