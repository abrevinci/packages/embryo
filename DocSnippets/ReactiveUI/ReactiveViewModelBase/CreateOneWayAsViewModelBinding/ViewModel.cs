﻿using AbreVinci.Embryo.ReactiveUI;

namespace DocSnippets.ReactiveUI.ReactiveViewModelBase.CreateOneWayAsViewModelBinding;

#region Snippet
public class ViewModel : ReactiveViewModel
{
	private readonly IReactiveOneWayBinding<MovieViewModel> _selectedMovie;

	public ViewModel(
		IObservable<MovieId> selectedMovie, 
		Func<MovieId, MovieViewModel> createMovie)
	{
		_selectedMovie = CreateOneWayAsViewModelBinding(
			selectedMovie,
			createMovie,
			(vm, id) => vm.Id == id,
			nameof(SelectedMovie));
	}

	public MovieViewModel SelectedMovie => _selectedMovie.Value;
}
#endregion

public class MovieViewModel : ReactiveViewModel
{
	public MovieViewModel(MovieId id)
	{
		Id = id;
	}

	public MovieId Id { get; }
}

public record struct MovieId;
