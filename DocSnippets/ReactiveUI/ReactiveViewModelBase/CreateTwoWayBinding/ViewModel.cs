﻿using AbreVinci.Embryo.ReactiveUI;

namespace DocSnippets.ReactiveUI.ReactiveViewModelBase.CreateTwoWayBinding;

#region Snippet
public class ViewModel : ReactiveViewModel
{
	private readonly IReactiveTwoWayBinding<int> _value;

	public ViewModel(IObservable<int> value, Action<Message> dispatch)
	{
		_value = CreateTwoWayBinding(
			value, 
			v => dispatch(new Message.SetValue(v)), 
			nameof(Value));
	}

	public int Value { get => _value.Value; set => _value.Value = value; }
}
#endregion

public record Message
{
	public record SetValue(int Value) : Message;
}
