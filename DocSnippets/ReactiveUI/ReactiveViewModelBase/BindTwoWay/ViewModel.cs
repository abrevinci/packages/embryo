﻿using AbreVinci.Embryo.ReactiveUI;

namespace DocSnippets.ReactiveUI.ReactiveViewModelBase.BindTwoWay;

#region Snippet
public class ViewModel : ReactiveViewModel
{
	public ViewModel(IObservable<int> value, Action<Message> dispatch)
	{
		// This requires the use of AbreVinci.Embryo.Fody package!
		Value = BindTwoWay(value, v => dispatch(new Message.SetValue(v)));
	}

	public int Value { get; set; }
}
#endregion

public record Message
{
	public record SetValue(int Value) : Message;
}
