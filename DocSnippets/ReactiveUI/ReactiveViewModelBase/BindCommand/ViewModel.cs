﻿using System.Windows.Input;
using AbreVinci.Embryo.ReactiveUI;

namespace DocSnippets.ReactiveUI.ReactiveViewModelBase.BindCommand;

#region Snippet
public class ViewModel : ReactiveViewModel
{
	public ViewModel(IObservable<bool> canSave, Action<Message> dispatch)
	{
		Save = BindCommand(() => dispatch(new Message.Save()), canSave);
		Cancel = BindCommand(() => dispatch(new Message.Cancel()));
	}

	public ICommand Save { get; }
	public ICommand Cancel { get; }
}
#endregion

public record Message
{
	public record Save : Message;
	public record Cancel : Message;
}
