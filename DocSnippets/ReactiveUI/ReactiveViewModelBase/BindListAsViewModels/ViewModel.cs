﻿using System.Collections.Immutable;
using AbreVinci.Embryo.ReactiveUI;

namespace DocSnippets.ReactiveUI.ReactiveViewModelBase.BindListAsViewModels;

#region Snippet
public class ViewModel : ReactiveViewModel
{
	public ViewModel(
		IObservable<IImmutableList<MovieId>> movieIds,
		Func<MovieId, MovieViewModel> createMovie)
	{
		Movies = BindListAsViewModels(movieIds, createMovie, (vm, id) => vm.Id == id);
	}

	public IEnumerable<MovieViewModel> Movies { get; }
}
#endregion

public class MovieViewModel : ReactiveViewModel
{
	public MovieViewModel(MovieId id)
	{
		Id = id;
	}

	public MovieId Id { get; }
}

public record struct MovieId;
