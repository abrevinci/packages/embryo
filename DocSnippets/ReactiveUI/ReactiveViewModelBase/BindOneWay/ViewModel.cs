﻿using AbreVinci.Embryo.ReactiveUI;

namespace DocSnippets.ReactiveUI.ReactiveViewModelBase.BindOneWay;

#region Snippet
public class ViewModel : ReactiveViewModel
{
	public ViewModel(IObservable<int> value)
	{
		// This requires the use of AbreVinci.Embryo.Fody package!
		Value = BindOneWay(value);
	}

	public int Value { get; }
}
#endregion
