﻿using AbreVinci.Embryo.ReactiveUI;

namespace DocSnippets.ReactiveUI.ReactiveViewModelBase.CreateOneWayBinding;

#region Snippet
public class ViewModel : ReactiveViewModel
{
	private readonly IReactiveOneWayBinding<int> _value;

	public ViewModel(IObservable<int> value)
	{
		_value = CreateOneWayBinding(value, nameof(Value));
	}

	public int Value => _value.Value;
}
#endregion
