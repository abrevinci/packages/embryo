﻿using System.Windows.Input;
using AbreVinci.Embryo.ReactiveUI;

namespace DocSnippets.ReactiveUI.ReactiveViewModelBase.BindCommandWithParam;

#region Snippet
public class ViewModel : ReactiveViewModel
{
	public ViewModel(
		IObservable<bool> canSelectPrivateOption,
		Action<Message> dispatch)
	{
		SelectPublicOption = BindCommand<Option>(
			o => dispatch(new Message.SelectPublicOption(o)));

		SelectPrivateOption = BindCommand<Option>(
			o => dispatch(new Message.SelectPrivateOption(o)),
			canSelectPrivateOption);
	}

	public ICommand SelectPublicOption { get; }
	public ICommand SelectPrivateOption { get; }
}
#endregion

public enum Option { }

public record Message
{
	public record SelectPublicOption(Option Option) : Message;
	public record SelectPrivateOption(Option Option) : Message;
}
