﻿using AbreVinci.Embryo.ReactiveUI;

namespace DocSnippets.ReactiveUI.ReactiveViewModelBase.AddChildViewModel;

#region Snippet
public class ViewModel : ReactiveViewModel
{
	public ViewModel(Func<ChildViewModel> createChild)
	{
		// AddChildViewModel makes sure that the child view model
		// is properly hooked up and disposed.
		Child = AddChildViewModel(createChild);
	}

	public ChildViewModel Child { get; }
}
#endregion

public class ChildViewModel : ReactiveViewModel
{
}
