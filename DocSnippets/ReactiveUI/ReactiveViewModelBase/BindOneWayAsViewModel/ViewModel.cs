﻿using AbreVinci.Embryo.ReactiveUI;

namespace DocSnippets.ReactiveUI.ReactiveViewModelBase.BindOneWayAsViewModel;

#region Snippet
public class ViewModel : ReactiveViewModel
{
	public ViewModel(
		IObservable<MovieId> selectedMovie, 
		Func<MovieId, MovieViewModel> createMovie)
	{
		// This requires the use of AbreVinci.Embryo.Fody package!
		SelectedMovie = BindOneWayAsViewModel(
			selectedMovie, 
			createMovie, 
			(vm, id) => vm.Id == id);
	}

	public MovieViewModel SelectedMovie { get; }
}
#endregion

public class MovieViewModel : ReactiveViewModel
{
	public MovieViewModel(MovieId id)
	{
		Id = id;
	}

	public MovieId Id { get; }
}

public record struct MovieId;
