﻿using AbreVinci.Embryo.Debugging.ViewModels;
using AbreVinci.Embryo.ReactiveUI;

namespace AbreVinci.Embryo.Debugging.Tests.DebuggerViewModelTests;

public class FrameSelection
{
	public record TestState;
	public record TestMessage;
	public record TestCommand;

	[Fact]
	public void ViewModelShouldBeAbleToSelectFrameToInspect()
	{
		var debugger = new Debugger<TestState, TestMessage, TestCommand>(new DebuggerSettings());
		debugger.OnInit(new TestState(), ImmutableArray<TestCommand>.Empty);
		debugger.OnUpdate(new TestState(), new TestMessage(), new TestState(), ImmutableArray<TestCommand>.Empty);
		debugger.OnUpdate(new TestState(), new TestMessage(), new TestState(), ImmutableArray<TestCommand>.Empty);
		var viewModelHost = new ReactiveViewModelHost(Scheduler.Immediate, true);
		var viewModel = viewModelHost.HostViewModel(() => new DebuggerViewModelFactory(debugger).CreateDebuggerViewModel());
		viewModel.CurrentDebuggerFrameId.Should().Be(3);
		viewModel.DebuggerFrameInspection.Id.Should().Be(3);

		viewModel.CurrentDebuggerFrameId = 1;

		viewModel.CurrentDebuggerFrameId.Should().Be(1);
		viewModel.DebuggerFrameInspection.Id.Should().Be(1);
	}
}
