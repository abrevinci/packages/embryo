﻿using AbreVinci.Embryo.Debugging.ViewModels;
using AbreVinci.Embryo.ReactiveUI;

namespace AbreVinci.Embryo.Debugging.Tests.DebuggerViewModelTests;

public class PauseIndicator
{
	public record TestState;
	public record TestMessage;
	public record TestCommand;

	[Fact]
	public void ViewModelShouldNotIndicatePauseWhenOnLastFrame()
	{
		var debugger = new Debugger<TestState, TestMessage, TestCommand>(new DebuggerSettings());
		debugger.OnInit(new TestState(), ImmutableArray<TestCommand>.Empty);
		debugger.OnUpdate(new TestState(), new TestMessage(), new TestState(), ImmutableArray<TestCommand>.Empty);
		debugger.OnUpdate(new TestState(), new TestMessage(), new TestState(), ImmutableArray<TestCommand>.Empty);
		var viewModelHost = new ReactiveViewModelHost(Scheduler.Immediate, true);
		var viewModel = viewModelHost.HostViewModel(() => new DebuggerViewModelFactory(debugger).CreateDebuggerViewModel());

		viewModel.IsPaused.Should().BeFalse();
	}

	[Fact]
	public void ViewModelShouldIndicatePauseWhenNotOnLastFrame()
	{
		var debugger = new Debugger<TestState, TestMessage, TestCommand>(new DebuggerSettings());
		debugger.OnInit(new TestState(), ImmutableArray<TestCommand>.Empty);
		debugger.OnUpdate(new TestState(), new TestMessage(), new TestState(), ImmutableArray<TestCommand>.Empty);
		debugger.OnUpdate(new TestState(), new TestMessage(), new TestState(), ImmutableArray<TestCommand>.Empty);
		var viewModelHost = new ReactiveViewModelHost(Scheduler.Immediate, true);
		var viewModel = viewModelHost.HostViewModel(() => new DebuggerViewModelFactory(debugger).CreateDebuggerViewModel());
		viewModel.CurrentDebuggerFrameId = 1;

		viewModel.IsPaused.Should().BeTrue();
	}
}
