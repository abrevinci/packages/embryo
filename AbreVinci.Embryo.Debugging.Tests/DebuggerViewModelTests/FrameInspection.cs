﻿using AbreVinci.Embryo.Debugging.ViewModels;
using AbreVinci.Embryo.ReactiveUI;

namespace AbreVinci.Embryo.Debugging.Tests.DebuggerViewModelTests;

public class FrameInspection
{
	public record TestState(int Value);
	public record TestMessage;
	public record TestCommand;

	[Fact]
	public void ViewModelShouldShowAfterStateAndCommandsForInitFrame()
	{
		var debugger = new Debugger<TestState, TestMessage, TestCommand>(new DebuggerSettings());
		debugger.OnInit(new TestState(1), ImmutableArray<TestCommand>.Empty);
		debugger.OnUpdate(new TestState(1), new TestMessage(), new TestState(2), ImmutableArray<TestCommand>.Empty);
		debugger.OnUpdate(new TestState(2), new TestMessage(), new TestState(3), ImmutableArray<TestCommand>.Empty);
		var viewModelHost = new ReactiveViewModelHost(Scheduler.Immediate, true);
		var viewModel = viewModelHost.HostViewModel(() => new DebuggerViewModelFactory(debugger).CreateDebuggerViewModel());
		
		viewModel.CurrentDebuggerFrameId = 1;

		viewModel.DebuggerFrameInspection.Id.Should().Be(1);
		viewModel.DebuggerFrameInspection.Message.Should().BeOfType<SingleDebuggerValueViewModel>().Which.Should().BeEquivalentTo(
			new
			{
				FrameId = 1,
				Path = "Message",
				KeyText = "",
				Type = (string?)null,
				Value = (string?)null
			});
		viewModel.DebuggerFrameInspection.BeforeState.Should().BeOfType<SingleDebuggerValueViewModel>().Which.Should().BeEquivalentTo(
			new
			{
				FrameId = 1,
				Path = "BeforeState",
				KeyText = "",
				Type = (string?)null,
				Value = (string?)null
			});
		viewModel.DebuggerFrameInspection.AfterState.Should().BeOfType<CompoundDebuggerValueViewModel>().Which.Should().BeEquivalentTo(
			new
			{
				FrameId = 1,
				Path = "AfterState",
				KeyText = "",
				Type = "TestState",
				IsExpanded = false,
				Values = new[]
				{
					new
					{
						FrameId = 1,
						Path = "AfterState/Value",
						KeyText = "Value: ",
						Type = "int",
						Value = "1"
					}
				}
			});
		viewModel.DebuggerFrameInspection.Commands.Should().BeOfType<SingleDebuggerValueViewModel>().Which.Should().BeEquivalentTo(
			new
			{
				FrameId = 1,
				Path = "Commands",
				KeyText = "",
				Type = "ImmutableArray<TestCommand>",
				Value = "[]"
			});
	}

	[Fact]
	public void ViewModelShouldShowMessageAndBeforeStateAndAfterStateAndCommandsForUpdateFrame()
	{
		var debugger = new Debugger<TestState, TestMessage, TestCommand>(new DebuggerSettings());
		debugger.OnInit(new TestState(1), ImmutableArray<TestCommand>.Empty);
		debugger.OnUpdate(new TestState(1), new TestMessage(), new TestState(2), ImmutableArray<TestCommand>.Empty);
		debugger.OnUpdate(new TestState(2), new TestMessage(), new TestState(3), ImmutableArray<TestCommand>.Empty);
		var viewModelHost = new ReactiveViewModelHost(Scheduler.Immediate, true);
		var viewModel = viewModelHost.HostViewModel(() => new DebuggerViewModelFactory(debugger).CreateDebuggerViewModel());

		viewModel.CurrentDebuggerFrameId = 2;

		viewModel.DebuggerFrameInspection.Id.Should().Be(2);
		viewModel.DebuggerFrameInspection.Message.Should().BeOfType<SingleDebuggerValueViewModel>().Which.Should().BeEquivalentTo(
			new
			{
				FrameId = 2,
				Path = "Message",
				KeyText = "",
				Type = "TestMessage",
				Value = "{}"
			});
		viewModel.DebuggerFrameInspection.BeforeState.Should().BeOfType<CompoundDebuggerValueViewModel>().Which.Should().BeEquivalentTo(
			new
			{
				FrameId = 2,
				Path = "BeforeState",
				KeyText = "",
				Type = "TestState",
				IsExpanded = false,
				Values = new[]
				{
					new
					{
						FrameId = 2,
						Path = "BeforeState/Value",
						KeyText = "Value: ",
						Type = "int",
						Value = "1"
					}
				}
			});
		viewModel.DebuggerFrameInspection.AfterState.Should().BeOfType<CompoundDebuggerValueViewModel>().Which.Should().BeEquivalentTo(
			new
			{
				FrameId = 2,
				Path = "AfterState",
				KeyText = "",
				Type = "TestState",
				IsExpanded = false,
				Values = new[]
				{
					new
					{
						FrameId = 2,
						Path = "AfterState/Value",
						KeyText = "Value: ",
						Type = "int",
						Value = "2"
					}
				}
			});
		viewModel.DebuggerFrameInspection.Commands.Should().BeOfType<SingleDebuggerValueViewModel>().Which.Should().BeEquivalentTo(
			new
			{
				FrameId = 2,
				Path = "Commands",
				KeyText = "",
				Type = "ImmutableArray<TestCommand>",
				Value = "[]"
			});
	}
}
