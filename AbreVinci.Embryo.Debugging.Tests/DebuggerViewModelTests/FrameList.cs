﻿using System.Linq;
using AbreVinci.Embryo.Debugging.ViewModels;
using AbreVinci.Embryo.ReactiveUI;

namespace AbreVinci.Embryo.Debugging.Tests.DebuggerViewModelTests;

public class FrameList
{
	public record TestState;
	public record TestMessage(int Value);
	public record TestCommand;

	[Fact]
	public void ViewModelShouldListAllFrames()
	{
		var debugger = new Debugger<TestState, TestMessage, TestCommand>(new DebuggerSettings());
		debugger.OnInit(new TestState(), ImmutableArray<TestCommand>.Empty);
		debugger.OnUpdate(new TestState(), new TestMessage(1), new TestState(), ImmutableArray<TestCommand>.Empty);
		debugger.OnUpdate(new TestState(), new TestMessage(2), new TestState(), ImmutableArray<TestCommand>.Empty);
		var viewModelHost = new ReactiveViewModelHost(Scheduler.Immediate, true);
		var viewModel = viewModelHost.HostViewModel(() => new DebuggerViewModelFactory(debugger).CreateDebuggerViewModel());

		var listedFrames = viewModel.DebuggerFrameItems.ToList();

		listedFrames.Should().HaveCount(3);

		listedFrames[0].Id.Should().Be(1);
		listedFrames[0].Message.Should().BeOfType<SingleDebuggerValueViewModel>().Which.Should().BeEquivalentTo(
			new
			{
				FrameId = 1, 
				Path = "ItemMessage", 
				KeyText = "", 
				Type = (string?)null, 
				Value = (string?)null
			});

		listedFrames[1].Id.Should().Be(2);
		listedFrames[1].Message.Should().BeOfType<CompoundDebuggerValueViewModel>().Which.Should().BeEquivalentTo(
			new
			{
				FrameId = 2, 
				Path = "ItemMessage", 
				KeyText = "", 
				Type = "TestMessage", 
				IsExpanded = false,
				Values = new[]
				{
					new
					{
						FrameId = 2, 
						Path = "ItemMessage/Value", 
						KeyText = "Value: ", 
						Type = "int", 
						Value = "1"
					}
				}
			});

		listedFrames[2].Id.Should().Be(3);
		listedFrames[2].Message.Should().BeOfType<CompoundDebuggerValueViewModel>().Which.Should().BeEquivalentTo(
			new
			{
				FrameId = 3,
				Path = "ItemMessage",
				KeyText = "",
				Type = "TestMessage",
				IsExpanded = false,
				Values = new[]
				{
					new
					{
						FrameId = 3,
						Path = "ItemMessage/Value",
						KeyText = "Value: ",
						Type = "int",
						Value = "2"
					}
				}
			});
	}
}
