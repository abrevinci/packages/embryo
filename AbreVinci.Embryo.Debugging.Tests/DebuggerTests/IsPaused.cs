﻿namespace AbreVinci.Embryo.Debugging.Tests.DebuggerTests;

public class IsPaused
{
	private record TestState;
	private record TestMessage;
	private record TestCommand;

	[Fact]
	public void ShouldBeTrueWhenCurrentFrameIsNotLatestFrame()
	{
		var debugger = new Debugger<TestState, TestMessage, TestCommand>(new DebuggerSettings());
		debugger.OnInit(new TestState(), ImmutableArray<TestCommand>.Empty);
		debugger.OnUpdate(new TestState(), new TestMessage(), new TestState(), ImmutableArray<TestCommand>.Empty);
		debugger.GoToFrame(1);

		var isPaused = debugger.IsPaused;

		isPaused.Should().BeTrue();
	}

	[Fact]
	public void ShouldBeFalseWhenCurrentFrameIsLatestFrameAfterAddingFrame()
	{
		var debugger = new Debugger<TestState, TestMessage, TestCommand>(new DebuggerSettings());
		debugger.OnInit(new TestState(), ImmutableArray<TestCommand>.Empty);
		debugger.OnUpdate(new TestState(), new TestMessage(), new TestState(), ImmutableArray<TestCommand>.Empty);
		
		var isPaused = debugger.IsPaused;

		isPaused.Should().BeFalse();
	}

	[Fact]
	public void ShouldBeFalseWhenCurrentFrameIsLatestFrameAfterGoingBackToIt()
	{
		var debugger = new Debugger<TestState, TestMessage, TestCommand>(new DebuggerSettings());
		debugger.OnInit(new TestState(), ImmutableArray<TestCommand>.Empty);
		debugger.OnUpdate(new TestState(), new TestMessage(), new TestState(), ImmutableArray<TestCommand>.Empty);
		debugger.GoToFrame(1);
		debugger.GoToFrame(2);

		var isPaused = debugger.IsPaused;

		isPaused.Should().BeFalse();
	}
}