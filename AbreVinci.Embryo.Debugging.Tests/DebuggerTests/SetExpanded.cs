﻿namespace AbreVinci.Embryo.Debugging.Tests.DebuggerTests;

public class SetExpanded
{
	public record TestState(int Value);
	public record TestMessage;
	public record TestCommand;

	[Fact]
	public void ShouldNotChangeAnythingIfFrameDoesNotExist()
	{
		var debugger = new Debugger<TestState, TestMessage, TestCommand>(new DebuggerSettings());

		var states = new List<DebuggerState>();
		debugger.State.Subscribe(states.Add);
		debugger.SetExpanded(1, "Message", true);

		states.Should().HaveCount(2);
		states[0].Should().Be(states[1]);
	}

	[Fact]
	public void ShouldAddPathToExpandedPathsOfGivenFrameWhenItExistsAndIsExpandedIsTrue()
	{
		var debugger = new Debugger<TestState, TestMessage, TestCommand>(new DebuggerSettings());
		debugger.OnInit(new TestState(1), ImmutableArray<TestCommand>.Empty);
		debugger.OnUpdate(new TestState(1), new TestMessage(), new TestState(2), ImmutableArray<TestCommand>.Empty);

		DebuggerState? state = null;
		debugger.State.Subscribe(s => state = s);
		debugger.SetExpanded(2, "Message", true);

		state?.Frames.Should().ContainKey(2).WhoseValue.ExpandedPaths.Should().ContainSingle().Which.Should().Be("Message");
	}

	[Fact]
	public void ShouldRemovePathFromExpandedPathsOfGivenFrameWhenItExistsAndIsExpandedIsFalse()
	{
		var debugger = new Debugger<TestState, TestMessage, TestCommand>(new DebuggerSettings());
		debugger.OnInit(new TestState(1), ImmutableArray<TestCommand>.Empty);
		debugger.OnUpdate(new TestState(1), new TestMessage(), new TestState(2), ImmutableArray<TestCommand>.Empty);
		debugger.SetExpanded(2, "Message", true);

		DebuggerState? state = null;
		debugger.State.Subscribe(s => state = s);
		debugger.SetExpanded(2, "Message", false);

		state?.Frames.Should().ContainKey(2).WhoseValue.ExpandedPaths.Should().BeEmpty();
	}
}
