﻿namespace AbreVinci.Embryo.Debugging.Tests.DebuggerTests;

public class GoToFrame
{
	// ReSharper disable once NotAccessedPositionalProperty.Local
	private record TestState(int Value);
	private record TestMessage;
	private record TestCommand;

	[Fact]
	public void ShouldNotSetCurrentFrameIdWhenTheGivenFrameIdDoesNotExist()
	{
		var debugger = new Debugger<TestState, TestMessage, TestCommand>(new DebuggerSettings());
		debugger.OnInit(new TestState(1), ImmutableArray<TestCommand>.Empty);

		DebuggerState? state = null;
		debugger.State.Subscribe(s => state = s);
		debugger.GoToFrame(3);

		state?.CurrentFrameId.Should().Be(1);
	}

	[Fact]
	public void ShouldSetCurrentFrameIdWhenTheGivenFrameIdExists()
	{
		var debugger = new Debugger<TestState, TestMessage, TestCommand>(new DebuggerSettings());
		debugger.OnInit(new TestState(1), ImmutableArray<TestCommand>.Empty);
		debugger.OnUpdate(new TestState(1), new TestMessage(), new TestState(2), ImmutableArray<TestCommand>.Empty);

		DebuggerState? state = null;
		debugger.State.Subscribe(s => state = s);
		debugger.GoToFrame(1);

		state?.CurrentFrameId.Should().Be(1);
	}

	[Fact]
	public void ShouldNotEmitAnyStateWhenTheFrameDoesNotExist()
	{
		var debugger = new Debugger<TestState, TestMessage, TestCommand>(new DebuggerSettings());
		debugger.OnInit(new TestState(1), ImmutableArray<TestCommand>.Empty);

		TestState? state = null;
		debugger.Subscribe(s => state = s);
		debugger.GoToFrame(2);

		state?.Should().BeNull();
	}

	[Fact]
	public void ShouldEmitTheStateOfTheGivenFrameWhenItExists()
	{
		var debugger = new Debugger<TestState, TestMessage, TestCommand>(new DebuggerSettings());
		debugger.OnInit(new TestState(1), ImmutableArray<TestCommand>.Empty);
		debugger.OnUpdate(new TestState(1), new TestMessage(), new TestState(2), ImmutableArray<TestCommand>.Empty);

		TestState? state = null;
		debugger.Subscribe(s => state = s);
		debugger.GoToFrame(1);

		state?.Should().Be(new TestState(1));
	}
}
