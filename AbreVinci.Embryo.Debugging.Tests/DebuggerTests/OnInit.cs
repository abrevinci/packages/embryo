﻿namespace AbreVinci.Embryo.Debugging.Tests.DebuggerTests;

public class OnInit
{
	public record TestState(int Value);
	public record TestMessage;
	public record TestCommand;

	[Fact]
	public void ShouldAddAnInitFrameToTheState()
	{
		var debugger = new Debugger<TestState, TestMessage, TestCommand>(new DebuggerSettings());

		DebuggerState? state = null;
		debugger.State.Subscribe(s => state = s);
		debugger.OnInit(new TestState(1), ImmutableArray.Create(new TestCommand()));

		state?.FrameIds.Should().ContainSingle().Which.Should().Be(1);
		state?.Frames.Should().ContainKey(1).WhoseValue.DebugState.Should().Be(new TestState(1));
	}

	[Fact]
	public void ShouldRemoveFirstFrameIfMaxFrameCountIsReached()
	{
		var debugger = new Debugger<TestState, TestMessage, TestCommand>(new DebuggerSettings{MaxHistorySize = 1});
		debugger.OnInit(new TestState(1), ImmutableArray.Create(new TestCommand()));

		DebuggerState? state = null;
		debugger.State.Subscribe(s => state = s);
		debugger.OnInit(new TestState(2), ImmutableArray.Create(new TestCommand()));

		state?.FrameIds.Should().ContainSingle().Which.Should().Be(2);
		state?.Frames.Should().ContainKey(2).WhoseValue.DebugState.Should().Be(new TestState(2));
	}

	[Fact]
	public void ShouldSetCurrentFrameIdToAddedFrameId()
	{
		var debugger = new Debugger<TestState, TestMessage, TestCommand>(new DebuggerSettings());

		DebuggerState? state = null;
		debugger.State.Subscribe(s => state = s);
		debugger.OnInit(new TestState(1), ImmutableArray.Create(new TestCommand()));

		state?.CurrentFrameId.Should().Be(1);
	}
}
