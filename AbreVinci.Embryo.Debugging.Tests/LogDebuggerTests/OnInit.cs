﻿using System.Text;

namespace AbreVinci.Embryo.Debugging.Tests.LogDebuggerTests;

public class OnInit
{
	// ReSharper disable once NotAccessedPositionalProperty.Local
	private record TestState(int Value);
	private record TestMessage;
	private record TestCommand;

	[Fact]
	public void ShouldOutputAfterStateAndCommands()
	{
		var stringBuilder = new StringBuilder();
		var logDebugger = new LogDebugger<TestState, TestMessage, TestCommand>(new LogDebuggerSettings(), str => stringBuilder.Append(str));

		logDebugger.OnInit(new TestState(1), ImmutableArray.Create(new TestCommand()));

		stringBuilder.ToString().Should().Be("1: Init:\n  AfterState:\n    TestState:\n    {\n      Value: int: 1;\n    }\n  Commands:\n    ImmutableArray<TestCommand>:\n    {\n      0: TestCommand: {};\n    }\n");
	}
}
