﻿using System.Text;

namespace AbreVinci.Embryo.Debugging.Tests.LogDebuggerTests;

public class OnUpdate
{
	// ReSharper disable once NotAccessedPositionalProperty.Local
	private record TestState(int Value);
	private record TestMessage;
	private record TestCommand;

	[Fact]
	public void ShouldOutputAfterStateAndCommands()
	{
		var stringBuilder = new StringBuilder();
		var logDebugger = new LogDebugger<TestState, TestMessage, TestCommand>(new LogDebuggerSettings(), str => stringBuilder.Append(str));

		logDebugger.OnUpdate(new TestState(1), new TestMessage(), new TestState(2), ImmutableArray.Create(new TestCommand()));

		stringBuilder.ToString().Should().Be("1: Update:\n  BeforeState:\n    TestState:\n    {\n      Value: int: 1;\n    }\n  Message:\n    TestMessage: {};\n  AfterState:\n    TestState:\n    {\n      Value: int: 2;\n    }\n  Commands:\n    ImmutableArray<TestCommand>:\n    {\n      0: TestCommand: {};\n    }\n");
	}
}
