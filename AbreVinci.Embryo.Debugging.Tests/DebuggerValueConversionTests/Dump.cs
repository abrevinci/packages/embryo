﻿namespace AbreVinci.Embryo.Debugging.Tests.DebuggerValueConversionTests;

public class Dump
{
	[Theory]
	[InlineData("")]
	[InlineData("\n")]
	[InlineData("\r\n")]
	public void ShouldConvertNullValueCorrectly(string newLine)
	{
		DebuggerValue? debuggerValue = null;

		var converted = debuggerValue.Dump(newLine: newLine);

		converted.Should().Be($"null;{newLine}");
	}

	[Theory]
	[InlineData("")]
	[InlineData("\n")]
	[InlineData("\r\n")]
	public void ShouldConvertSingleValueCorrectly(string newLine)
	{
		var debuggerValue = new DebuggerValue.Single("SomeType", "SomeValue");

		var converted = debuggerValue.Dump(newLine: newLine);

		converted.Should().Be($"SomeType: SomeValue;{newLine}");
	}

	[Theory]
	[InlineData("")]
	[InlineData("\n")]
	[InlineData("\r\n")]
	public void ShouldConvertEmptyCompoundValueCorrectly(string newLine)
	{
		var debuggerValue = new DebuggerValue.Compound(
			"SomeRootType", 
			ImmutableArray<string>.Empty,
			ImmutableDictionary<string, DebuggerValue?>.Empty);

		var converted = debuggerValue.Dump(newLine: newLine);

		converted.Should().Be($"SomeRootType: {{}}{newLine}");
	}

	[Theory]
	[InlineData(0, "")]
	[InlineData(0, "\n")]
	[InlineData(0, "\r\n")]
	[InlineData(2, "")]
	[InlineData(2, "\n")]
	[InlineData(2, "\r\n")]
	public void ShouldConvertCompoundValueCorrectly(int indentationSpaces, string newLine)
	{
		var nestingSpaces = "".PadLeft(indentationSpaces);
		var debuggerValue = new DebuggerValue.Compound(
			"SomeRootType",
			ImmutableArray.Create("Key1", "Key2"),
			ImmutableDictionary<string, DebuggerValue?>.Empty
				.Add("Key1", new DebuggerValue.Single("SomeType", "SomeValue"))
				.Add("Key2", null));

		var converted = debuggerValue.Dump(indentationSpaces, newLine);

		converted.Should().Be($"SomeRootType:{newLine}{{{newLine}{nestingSpaces}Key1: SomeType: SomeValue;{newLine}{nestingSpaces}Key2: null;{newLine}}}{newLine}");
	}

	[Theory]
	[InlineData(0, "")]
	[InlineData(0, "\n")]
	[InlineData(0, "\r\n")]
	[InlineData(2, "")]
	[InlineData(2, "\n")]
	[InlineData(2, "\r\n")]
	public void ShouldConvertNestedCompountValueCorrectly(int indentationSpaces, string newLine)
	{
		var nestingSpaces = "".PadLeft(indentationSpaces);
		var debuggerValue = new DebuggerValue.Compound(
			"SomeRootType",
			ImmutableArray.Create("Key1", "Key2", "Key3"),
			ImmutableDictionary<string, DebuggerValue?>.Empty
				.Add("Key1", new DebuggerValue.Single("SomeType", "SomeValue"))
				.Add("Key2", null)
				.Add("Key3", new DebuggerValue.Compound(
					"SomeCompoundType", 
					ImmutableArray.Create("0"), 
					ImmutableDictionary<string, DebuggerValue?>.Empty
						.Add("0", new DebuggerValue.Single("SomeOtherType", "SomeOtherValue")))));

		var converted = debuggerValue.Dump(indentationSpaces, newLine);

		converted.Should().Be($"SomeRootType:{newLine}{{{newLine}{nestingSpaces}Key1: SomeType: SomeValue;{newLine}{nestingSpaces}Key2: null;{newLine}{nestingSpaces}Key3: SomeCompoundType:{newLine}{nestingSpaces}{{{newLine}{nestingSpaces}{nestingSpaces}0: SomeOtherType: SomeOtherValue;{newLine}{nestingSpaces}}}{newLine}}}{newLine}");
	}
}
