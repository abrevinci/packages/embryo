﻿namespace AbreVinci.Embryo.Debugging.Tests.DebuggerValueConversionTests;

public class ToDebuggerValue
{
	public record struct EntityId(Guid Id);

	public record Entity(EntityId Id, IImmutableList<string> Values, string? CurrentValue)
	{
		public static readonly Entity Entity1 = new(
			new EntityId(Guid.Parse("2A8E6F7C-46A9-4440-BA19-5240C6C6A396")),
			ImmutableArray.Create("V1", "V2"), 
			"V2");

		public static readonly Entity Entity2 = new(
			new EntityId(Guid.Parse("ABCDEF12-46A9-4440-BA19-5240C6C6A396")),
			ImmutableArray<string>.Empty,
			null);
	}

	public record TestState(int Int, float Float, IImmutableDictionary<EntityId, Entity> Entities);

	[Fact]
	public void ShouldConvertComplexValue()
	{
		var state = new TestState(
			2,
			4.5f,
			ImmutableDictionary<EntityId, Entity>.Empty.Add(Entity.Entity1.Id, Entity.Entity1).Add(Entity.Entity2.Id, Entity.Entity2));

		var expectedEntity1IdDebuggerValue = new
		{
			Type = "EntityId",
			ValueKeys = new[]
			{
				"Id"
			},
			Values = new Dictionary<string, object?>
			{
				["Id"] = new
				{
					Type = "Guid",
					Value = Entity.Entity1.Id.Id.ToString()
				}
			}
		};

		var expectedEntity2IdDebuggerValue = new
		{
			Type = "EntityId", 
			ValueKeys = new[]
			{
				"Id"
			},
			Values = new Dictionary<string, object?>
			{
				["Id"] = new
				{
					Type = "Guid", 
					Value = Entity.Entity2.Id.Id.ToString()
				}
			}
		};

		var expectedEntity1ValuesDebuggerValue = new
		{
			Type = "ImmutableArray<string>",
			ValueKeys = new[]
			{
				"0",
				"1"
			},
			Values = new Dictionary<string, object?>
			{
				["0"] = new
				{
					Type = "string",
					Value = "\"V1\""
				},
				["1"] = new
				{
					Type = "string",
					Value = "\"V2\""
				}
			}
		};

		var expectedEntity2ValuesDebuggerValue = new
		{
			Type = "ImmutableArray<string>",
			Value = "[]"
		};

		var expectedEntity1CurrentValueDebuggerValue = new 
		{
			Type = "string",
			Value = "\"V2\""
		};
		
		object? expectedEntity2CurrentValueDebuggerValue = null;

		var expectedEntity1DebuggerValue = new 
		{
			Type = "Entity",
			ValueKeys = new[]
			{
				"Id", 
				"Values", 
				"CurrentValue"
			},
			Values = new Dictionary<string, object?> 
			{
				["Id"] = expectedEntity1IdDebuggerValue,
				["Values"] = expectedEntity1ValuesDebuggerValue,
				["CurrentValue"] = expectedEntity1CurrentValueDebuggerValue
			}
		};

		var expectedEntity2DebuggerValue = new
		{
			Type = "Entity",
			ValueKeys = new[]
			{
				"Id",
				"Values",
				"CurrentValue"
			},
			Values = new Dictionary<string, object?>
			{
				["Id"] = expectedEntity2IdDebuggerValue,
				["Values"] = expectedEntity2ValuesDebuggerValue,
				["CurrentValue"] = expectedEntity2CurrentValueDebuggerValue
			}
		};
		
		var expectedEntitiesDebuggerValue = new 
		{
			Type = "ImmutableDictionary<EntityId, Entity>",
			ValueKeys = new[]
			{
				Entity.Entity1.Id.ToString(), 
				Entity.Entity2.Id.ToString()
			},
			Values = new Dictionary<string, object?>
			{
				[Entity.Entity1.Id.ToString()] = expectedEntity1DebuggerValue,
				[Entity.Entity2.Id.ToString()] = expectedEntity2DebuggerValue
			}
		};

		var expectedIntDebuggerValue = new 
		{
			Type = "int", 
			Value = "2"
		};

		var expectedFloatDebuggerValue = new 
		{
			Type = "float", 
			Value = "4.5"
		};

		var expectedDebuggerValue = new
		{
			Type = "TestState",
			ValueKeys = new[]
			{
				"Int", 
				"Float", 
				"Entities"
			},
			Values = new Dictionary<string, object?>
			{
				["Int"] = expectedIntDebuggerValue,
				["Float"] = expectedFloatDebuggerValue,
				["Entities"] = expectedEntitiesDebuggerValue
			}
		};

		var debuggerValue = state.ToDebuggerValue(typeof(Guid));

		debuggerValue.Should().BeEquivalentTo(expectedDebuggerValue);
	}
}
