﻿using System;

namespace AbreVinci.Embryo.Generator.Tests;

public abstract class GeneratorTestFixture
{
	protected string? GetGeneratedText(Compilation compilation, string fileName)
	{
		var newFile = compilation.SyntaxTrees
			.FirstOrDefault(x => Path.GetFileName(x.FilePath).EndsWith(fileName));

		return newFile?.GetText().ToString();
	}

	protected Compilation CreateCompilation(params string[] sources)
	{
		var trustedAssembliesPaths = ((string)AppContext.GetData("TRUSTED_PLATFORM_ASSEMBLIES")!).Split(Path.PathSeparator);
		var neededAssemblies = new[]
		{
			"AbreVinci.Embryo.Core",
			"AbreVinci.Embryo.Generator.Attributes",
			"System.Collections.Immutable",
			"System.Private.CoreLib",
			"System.Reactive",
			"System.Runtime",
			"mscorlib",
			"netstandard"
		};
		var references = trustedAssembliesPaths
			.Where(p => neededAssemblies.Contains(Path.GetFileNameWithoutExtension(p)))
			.Select(p => MetadataReference.CreateFromFile(p))
			.ToList();

		return CSharpCompilation.Create("compilation",
			sources.Select((source, i) => CSharpSyntaxTree.ParseText(source, new CSharpParseOptions(LanguageVersion.CSharp10), $"Source_{i + 1}.cs")).ToArray(),
			references,
			new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary, nullableContextOptions: NullableContextOptions.Enable));
	}

	protected Compilation RunGenerators(Compilation compilation, params ISourceGenerator[] generators)
	{
		CSharpGeneratorDriver.Create(generators).RunGeneratorsAndUpdateCompilation(compilation, out var newCompilation, out _);
		return newCompilation;
	}
}
