﻿using AbreVinci.Embryo.Generator.Generators;

namespace AbreVinci.Embryo.Generator.Tests;

public class ReducerGeneratorTests : GeneratorTestFixture
{
	[Fact]
	public void ShouldGenerateReducerInMarkedClassWhenNotUsingCommands()
	{
		var stateSource =
@"namespace App.Core
{
	public record State(int Counter);
}
";

		var commandSource =
@"namespace App.Core
{
	public record Command;
}
";
		
		var messageSource =
@"namespace App.Core
{
	public record Message(int _Tag)
	{
		public const int _SetValueTag = 1;
		public const int _IncrementTag = 2;
		public const int _ResetTag = 3;

		// App.Core.Actions.Actions
		public record SetValue(int Value) : Message(_SetValueTag);

		// App.Core.Actions.CounterActions
		public record Increment(int ByValue) : Message(_IncrementTag);
		public record Reset() : Message(_ResetTag);
	}
}
";

		var reducerSource =
@"using AbreVinci.Embryo.Generator.Attributes;

namespace App.Core
{
	[GenerateReducerUpdate(typeof(State), typeof(Message))]
	public static partial class Reducer
	{
	}
}
";

		var actionsSource =
@"using AbreVinci.Embryo.Generator.Attributes;

namespace App.Core.Actions
{
	public static class CounterActions
	{
		[Action]
		public static State Reset(this State state)
		{
			return state with { Counter = 0 };
		}

		[Action]
		public static State Increment(this State state, int byValue)
		{
			return state with { Counter = state.Counter + byValue };
		}
	}
}
";

		var actionsSource2 =
@"using AbreVinci.Embryo.Generator.Attributes;

namespace App.Core.Actions
{
	public static class Actions
	{
		[Action]
		public static State SetValue(this State state, int value)
		{
			return state with { Counter = value };
		}
	}
}
";

		var compilation = CreateCompilation(stateSource, commandSource, messageSource, reducerSource, actionsSource, actionsSource2);
		var newCompilation = RunGenerators(compilation, new ReducerGenerator());

		newCompilation.GetDiagnostics().Should().BeEmpty();

		var generatedText = GetGeneratedText(newCompilation, "Reducer.g.cs");

		generatedText.Should().Be(
@"using App.Core.Actions;

namespace App.Core
{
	public static partial class Reducer
	{
		public static State Update(this State state, Message message)
		{
			return message._Tag switch
			{
				// App.Core.Actions.Actions
				Message._SetValueTag when (Message.SetValue)message is var m => state.SetValue(m.Value),

				// App.Core.Actions.CounterActions
				Message._IncrementTag when (Message.Increment)message is var m => state.Increment(m.ByValue),
				Message._ResetTag => state.Reset(),

				_ => state
			};
		}
	}
}
");
	}

	[Fact]
	public void ShouldGenerateReducerInMarkedClassWhenUsingCommands()
	{
		var stateSource =
@"namespace App.Core
{
	public record State(int Counter);
}
";

		var commandSource =
@"namespace App.Core
{
	public record Command;
}
";

		var messageSource =
@"namespace App.Core
{
	public record Message(int _Tag)
	{
		public const int _SetValueTag = 1;
		public const int _IncrementTag = 2;
		public const int _ResetTag = 3;

		// Actions
		public record SetValue(int Value) : Message(_SetValueTag);

		// CounterActions
		public record Increment(int ByValue) : Message(_IncrementTag);
		public record Reset() : Message(_ResetTag);
	}
}
";

		var reducerSource =
@"using AbreVinci.Embryo.Generator.Attributes;

namespace App.Core
{
	[GenerateReducerUpdate(typeof(State), typeof(Message), typeof(Command))]
	public static partial class Reducer
	{
	}
}
";

		var actionsSource =
@"using AbreVinci.Embryo.Core;
using AbreVinci.Embryo.Generator.Attributes;

namespace App.Core.Actions
{
	public static class CounterActions
	{
		[Action]
		public static ActionContext<State, Command> Reset(this ActionContext<State, Command> context)
		{
			var state = context.State;
			var newState = state with { Counter = 0 };
			return context.WithState(newState);
		}

		[Action]
		public static ActionContext<State, Command> Increment(this ActionContext<State, Command> context, int byValue)
		{
			var state = context.State;
			var newState = state with { Counter = state.Counter + byValue };
			return context.WithState(newState);
		}
	}
}
";

		var actionsSource2 =
@"using AbreVinci.Embryo.Core;
using AbreVinci.Embryo.Generator.Attributes;

namespace App.Core.Actions
{
	public static class Actions
	{
		[Action]
		public static ActionContext<State, Command> SetValue(this ActionContext<State, Command> context, int value)
		{
			var state = context.State;
			var newState = state with { Counter = value };
			return context.WithState(newState);
		}
	}
}
";

		var compilation = CreateCompilation(stateSource, commandSource, messageSource, reducerSource, actionsSource, actionsSource2);
		var newCompilation = RunGenerators(compilation, new ReducerGenerator());

		newCompilation.GetDiagnostics().Should().BeEmpty();

		var generatedText = GetGeneratedText(newCompilation, "Reducer.g.cs");

		generatedText.Should().Be(
@"using AbreVinci.Embryo.Core;
using App.Core.Actions;

namespace App.Core
{
	public static partial class Reducer
	{
		public static ActionContext<State, Command> Update(this ActionContext<State, Command> context, Message message)
		{
			return message._Tag switch
			{
				// App.Core.Actions.Actions
				Message._SetValueTag when (Message.SetValue)message is var m => context.SetValue(m.Value),

				// App.Core.Actions.CounterActions
				Message._IncrementTag when (Message.Increment)message is var m => context.Increment(m.ByValue),
				Message._ResetTag => context.Reset(),

				_ => context
			};
		}
	}
}
");
	}
}
