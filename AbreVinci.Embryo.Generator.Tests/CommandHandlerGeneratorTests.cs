﻿using AbreVinci.Embryo.Generator.Generators;

namespace AbreVinci.Embryo.Generator.Tests;

public class CommandHandlerGeneratorTests : GeneratorTestFixture
{
	[Fact]
	public void ShouldGenerateCommandHandlerInMarkedClassWhenServicesAreUsed()
	{
		var stateSource =
@"namespace App.Core
{
	public record State(int Counter);
}
";

		var messageSource =
@"namespace App.Core
{
	public record Message
	{
		public record LoadComplete(int Value) : Message;
		public record LoadFailed : Message;
		public record SaveComplete : Message;
		public record SaveFailed : Message;
		public record SetCounter(int Value) : Message;
	}
}
";

		var commandSource =
@"using System;

namespace App.Core
{
	public record Command(int _Tag)
	{
		public const int _DelayDispatchTag = 1;
		public const int _ExitTag = 2;
		public const int _FirstRecordTag = 3;
		public const int _LoadAsyncTag = 4;
		public const int _RandomizeTag = 5;
		public const int _SaveAsyncTag = 6;
		public const int _HaveFunTag = 7;
		public const int _HaveFunConcreteTag = 8;
		public const int _SecondRecordTag = 9;

		// App.Core.Effects.CounterEffects
		public record DelayDispatch(TimeSpan Delay, Message Message) : Command(_DelayDispatchTag);
		public record Exit(int Code) : Command(_ExitTag);
		public record FirstRecord(Record1Namespace.Record FirstRecordValue) : Command(_FirstRecordTag);
		public record LoadAsync() : Command(_LoadAsyncTag);
		public record Randomize() : Command(_RandomizeTag);
		public record SaveAsync(int Value) : Command(_SaveAsyncTag);

		// App.Core.Effects.MoreCounterEffects
		public record HaveFun() : Command(_HaveFunTag);
		public record HaveFunConcrete() : Command(_HaveFunConcreteTag);
		public record SecondRecord(Record2Namespace.Record SecondRecordValue) : Command(_SecondRecordTag);
	}
}
";

		var persistanceService1Source =
@"using System.Threading.Tasks;

namespace App.Services
{
	public interface IPersistanceService 
	{
		Task<int> LoadAsync();
		Task SaveAsync(int value);
	}
}
";

		var persistanceService2Source =
@"namespace App.Services2
{
	public interface IPersistanceService 
	{
		void HaveFun();
	}

	public class PersistanceService : IPersistanceService
	{
		public void HaveFun() {}
	}
}
";

		var record1Source =
@"namespace App.Record1Namespace
{
	public record Record;
}
";

		var record2Source =
@"namespace App.Record2Namespace
{
	public record Record;
}
";

		var effects1Source =
@"using AbreVinci.Embryo.Generator.Attributes;
using AbreVinci.Embryo.Core;
using System;
using System.Threading.Tasks;
using App.Services;
using App.Record1Namespace;

namespace App.Core.Effects
{
	using PersistanceContext = EffectContext<IPersistanceService, Message>;

	public static class CounterEffects
	{
		[Effect]
		public static async Task LoadAsync(PersistanceContext context)
		{
			await context.ExecuteAsync(
				service => service.LoadAsync(), 
				createOnCompletionMessage: result => new Message.LoadComplete(result),
				createOnExceptionMessage: _ => new Message.LoadFailed());
		}

		[Effect]
		public static async Task SaveAsync(PersistanceContext context, int value)
		{
			await context.ExecuteAsync(
				service => service.SaveAsync(value),
				createOnCompletionMessage: () => new Message.SaveComplete(),
				createOnExceptionMessage: _ => new Message.SaveFailed());
		}

		[Effect]
		public static void Randomize(EffectContext<Random, Message> context)
		{
			context.Execute(
				random => random.Next(),
				createOnCompletionMessage: result => new Message.SetCounter(result));
		}

		[Effect]
		public static void Exit(int code)
		{
			Environment.Exit(code);
		}

		[Effect]
		public static async Task DelayDispatch(EffectContext<Message> context, TimeSpan delay, Message message)
		{
			await Task.Delay(delay);
			context.Dispatch(message);
		}

		[Effect]
		public static void FirstRecord(Record firstRecordValue)
		{
		}
	}
}
";

		var effects2Source =
@"using AbreVinci.Embryo.Generator.Attributes;
using AbreVinci.Embryo.Core;
using App.Record2Namespace;
using App.Services2;

namespace App.Core.Effects
{
	public static class MoreCounterEffects
	{
		[Effect]
		public static void SecondRecord(Record secondRecordValue)
		{
		}

		[Effect]
		public static void HaveFun(EffectContext<IPersistanceService, Message> context)
		{
			context.Service.HaveFun();
		}

		[Effect]
		public static void HaveFunConcrete(EffectContext<PersistanceService, Message> context)
		{
			context.Service.HaveFun();
		}
	}
}
";

		var commandHandlerSource =
@"using AbreVinci.Embryo.Generator.Attributes;

namespace App.Core
{
	[GenerateCommandHandler(typeof(Command), typeof(Message))]
	public partial class CommandHandler
	{
	}
}
";

		var compilation = CreateCompilation(stateSource, messageSource, commandSource, persistanceService1Source, persistanceService2Source, record1Source, record2Source, effects1Source, effects2Source, commandHandlerSource);
		var newCompilation = RunGenerators(compilation, new CommandHandlerGenerator());
		
		newCompilation.GetDiagnostics().Should().BeEmpty();

		var generatedText = GetGeneratedText(newCompilation, "CommandHandler.g.cs");

		generatedText.Should().Be(
@"using AbreVinci.Embryo.Core;
using App.Core.Effects;
using App.Services2;
using System;
using System.Threading.Tasks;

namespace App.Core
{
	public partial class CommandHandler : IDisposable
	{
		private readonly IDisposable _subscription;

		public CommandHandler(
			IObservable<Command> commands,
			Action<Message> dispatch,
			App.Services.IPersistanceService persistanceService__1,
			App.Services2.IPersistanceService persistanceService__2,
			PersistanceService persistanceService__3,
			Random random)
		{
			Context = new EffectContext<Message>(dispatch);
			PersistanceServiceContext__1 = new EffectContext<App.Services.IPersistanceService, Message>(persistanceService__1, dispatch);
			PersistanceServiceContext__2 = new EffectContext<App.Services2.IPersistanceService, Message>(persistanceService__2, dispatch);
			PersistanceServiceContext__3 = new EffectContext<PersistanceService, Message>(persistanceService__3, dispatch);
			RandomContext = new EffectContext<Random, Message>(random, dispatch);

			_subscription = commands.Subscribe(command => Handle(command));
		}

		protected EffectContext<Message> Context { get; }
		protected EffectContext<App.Services.IPersistanceService, Message> PersistanceServiceContext__1 { get; }
		protected EffectContext<App.Services2.IPersistanceService, Message> PersistanceServiceContext__2 { get; }
		protected EffectContext<PersistanceService, Message> PersistanceServiceContext__3 { get; }
		protected EffectContext<Random, Message> RandomContext { get; }

		protected virtual Task Handle(Command command)
		{
			return command._Tag switch
			{
				// App.Core.Effects.CounterEffects
				Command._DelayDispatchTag when (Command.DelayDispatch)command is var c => CounterEffects.DelayDispatch(Context, c.Delay, c.Message),
				Command._ExitTag when (Command.Exit)command is var c => WrapAsAsync(() => CounterEffects.Exit(c.Code)),
				Command._FirstRecordTag when (Command.FirstRecord)command is var c => WrapAsAsync(() => CounterEffects.FirstRecord(c.FirstRecordValue)),
				Command._LoadAsyncTag => CounterEffects.LoadAsync(PersistanceServiceContext__1),
				Command._RandomizeTag => WrapAsAsync(() => CounterEffects.Randomize(RandomContext)),
				Command._SaveAsyncTag when (Command.SaveAsync)command is var c => CounterEffects.SaveAsync(PersistanceServiceContext__1, c.Value),

				// App.Core.Effects.MoreCounterEffects
				Command._HaveFunTag => WrapAsAsync(() => MoreCounterEffects.HaveFun(PersistanceServiceContext__2)),
				Command._HaveFunConcreteTag => WrapAsAsync(() => MoreCounterEffects.HaveFunConcrete(PersistanceServiceContext__3)),
				Command._SecondRecordTag when (Command.SecondRecord)command is var c => WrapAsAsync(() => MoreCounterEffects.SecondRecord(c.SecondRecordValue)),

				_ => Task.CompletedTask
			};
		}

		protected static Task WrapAsAsync(Action action)
		{
			action();
			return Task.CompletedTask;
		}

		public void Dispose()
		{
			_subscription.Dispose();
		}
	}
}
");
	}

	[Fact]
	public void ShouldGenerateCommandHandlerInMarkedClassWhenNoServicesAreUsed()
	{
		var stateSource =
@"namespace App.Core
{
	public record State(int Counter);
}
";

		var messageSource =
@"namespace App.Core
{
	public record Message
	{
		public record LoadComplete(int Value) : Message;
		public record LoadFailed : Message;
		public record SaveComplete : Message;
		public record SaveFailed : Message;
		public record SetCounter(int Value) : Message;
	}
}
";

		var commandSource =
@"using System;

namespace App.Core
{
	public record Command(int _Tag)
	{
		public const int _DelayDispatchTag = 1;
		public const int _ExitTag = 2;

		public record DelayDispatch(TimeSpan Delay, Message Message) : Command(_DelayDispatchTag);
		public record Exit(int Code) : Command(_ExitTag);
	}
}
";
		
		var effectsSource =
@"using AbreVinci.Embryo.Generator.Attributes;
using AbreVinci.Embryo.Core;
using System;
using System.Threading.Tasks;

namespace App.Core.Effects
{
	public static partial class CounterEffects
	{
		[Effect]
		public static void Exit(int code)
		{
			Environment.Exit(code);
		}

		[Effect]
		public static async Task DelayDispatch(EffectContext<Message> context, TimeSpan delay, Message message)
		{
			await Task.Delay(delay);
			context.Dispatch(message);
		}
	}
}
";
		
		var commandHandlerSource =
@"using AbreVinci.Embryo.Generator.Attributes;

namespace App.Core
{
	[GenerateCommandHandler(typeof(Command), typeof(Message))]
	public partial class CommandHandler
	{
	}
}
";

		var compilation = CreateCompilation(stateSource, messageSource, commandSource, effectsSource, commandHandlerSource);
		var newCompilation = RunGenerators(compilation, new CommandHandlerGenerator());
		
		newCompilation.GetDiagnostics().Should().BeEmpty();

		var generatedText = GetGeneratedText(newCompilation, "CommandHandler.g.cs");

		generatedText.Should().Be(
@"using AbreVinci.Embryo.Core;
using App.Core.Effects;
using System;
using System.Threading.Tasks;

namespace App.Core
{
	public partial class CommandHandler : IDisposable
	{
		private readonly IDisposable _subscription;

		public CommandHandler(
			IObservable<Command> commands,
			Action<Message> dispatch)
		{
			Context = new EffectContext<Message>(dispatch);

			_subscription = commands.Subscribe(command => Handle(command));
		}

		protected EffectContext<Message> Context { get; }

		protected virtual Task Handle(Command command)
		{
			return command._Tag switch
			{
				// App.Core.Effects.CounterEffects
				Command._DelayDispatchTag when (Command.DelayDispatch)command is var c => CounterEffects.DelayDispatch(Context, c.Delay, c.Message),
				Command._ExitTag when (Command.Exit)command is var c => WrapAsAsync(() => CounterEffects.Exit(c.Code)),

				_ => Task.CompletedTask
			};
		}

		protected static Task WrapAsAsync(Action action)
		{
			action();
			return Task.CompletedTask;
		}

		public void Dispose()
		{
			_subscription.Dispose();
		}
	}
}
");
	}
}
