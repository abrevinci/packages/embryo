﻿using AbreVinci.Embryo.Generator.Generators;

namespace AbreVinci.Embryo.Generator.Tests;

public class CommandTypeGeneratorTests : GeneratorTestFixture
{
	[Fact]
	public void ShouldGenerateCommandTypesForMarkedEffects()
	{
		var stateSource =
@"namespace App.Core
{
	public record State(int Counter);
}
";

		var messageSource =
@"namespace App.Core
{
	public record Message
	{
		public record LoadComplete(int Value) : Message;
		public record LoadFailed : Message;
		public record SaveComplete : Message;
		public record SaveFailed : Message;
		public record SetCounter(int Value) : Message;
	}
}
";

		var commandSource = 
@"using AbreVinci.Embryo.Generator.Attributes;

namespace App.Core
{
	[GenerateCommandType]
	public partial record Command;
}
";

		var persistanceServiceSource =
@"using System.Threading.Tasks;

namespace App.Services
{
	public interface IPersistanceService 
	{
		Task<int> LoadAsync();
		Task SaveAsync(int value);
	}
}
";

		var persistanceService2Source =
@"namespace App.Services2
{
	public interface IPersistanceService 
	{
		void HaveFun();
	}

	public class PersistanceService : IPersistanceService
	{
		public void HaveFun() {}
	}
}
";

		var record1Source =
@"namespace App.Record1Namespace
{
	public record Record;
}
";

		var record2Source =
@"namespace App.Record2Namespace
{
	public record Record;
}
";

		var effects1Source =
@"using AbreVinci.Embryo.Generator.Attributes;
using AbreVinci.Embryo.Core;
using System;
using System.Threading.Tasks;
using App.Services;
using App.Record1Namespace;

namespace App.Core.Effects
{
	using PersistanceContext = EffectContext<IPersistanceService, Message>;

	public static class CounterEffects
	{
		[Effect]
		public static async Task LoadAsync(PersistanceContext context)
		{
			await context.ExecuteAsync(
				service => service.LoadAsync(), 
				createOnCompletionMessage: result => new Message.LoadComplete(result),
				createOnExceptionMessage: _ => new Message.LoadFailed());
		}

		[Effect]
		public static async Task SaveAsync(PersistanceContext context, int value)
		{
			await context.ExecuteAsync(
				service => service.SaveAsync(value),
				createOnCompletionMessage: () => new Message.SaveComplete(),
				createOnExceptionMessage: _ => new Message.SaveFailed());
		}

		[Effect]
		public static void Randomize(EffectContext<Random, Message> context)
		{
			context.Execute(
				random => random.Next(),
				createOnCompletionMessage: result => new Message.SetCounter(result));
		}

		[Effect]
		public static void Exit(int code)
		{
			Environment.Exit(code);
		}

		[Effect]
		public static async Task DelayDispatch(EffectContext<Message> context, TimeSpan delay, Message message)
		{
			await Task.Delay(delay);
			context.Dispatch(message);
		}

		[Effect]
		public static void FirstRecord(Record firstRecordValue)
		{
		}
	}
}
";

		var effects2Source =
@"using AbreVinci.Embryo.Generator.Attributes;
using AbreVinci.Embryo.Core;
using App.Services2;
using App.Record2Namespace;

namespace App.Core.Effects
{
	public static class MoreCounterEffects
	{
		[Effect]
		public static void SecondRecord(Record secondRecordValue)
		{
		}

		[Effect]
		public static void HaveFun(EffectContext<IPersistanceService, Message> context)
		{
			context.Service.HaveFun();
		}

		[Effect]
		public static void HaveFunConcrete(EffectContext<PersistanceService, Message> context)
		{
			context.Service.HaveFun();
		}
	}
}
";

		var compilation = CreateCompilation(stateSource, messageSource, commandSource, persistanceServiceSource, persistanceService2Source, record1Source, record2Source, effects1Source, effects2Source);
		var newCompilation = RunGenerators(compilation, new CommandTypeGenerator());

		newCompilation.GetDiagnostics().Should().BeEmpty();

		var generatedText = GetGeneratedText(newCompilation, "Command.g.cs");

		generatedText.Should().Be(
@"using System;

namespace App.Core
{
	public partial record Command
	{
		public const int _DelayDispatchTag = 1;
		public const int _ExitTag = 2;
		public const int _FirstRecordTag = 3;
		public const int _LoadAsyncTag = 4;
		public const int _RandomizeTag = 5;
		public const int _SaveAsyncTag = 6;
		public const int _HaveFunTag = 7;
		public const int _HaveFunConcreteTag = 8;
		public const int _SecondRecordTag = 9;

		// App.Core.Effects.CounterEffects
		/// <summary>
		/// Is handled by <see cref=""App.Core.Effects.CounterEffects.DelayDispatch""/>
		/// </summary>
		public sealed record DelayDispatch(TimeSpan Delay, Message Message) : Command(_DelayDispatchTag);
		/// <summary>
		/// Is handled by <see cref=""App.Core.Effects.CounterEffects.Exit""/>
		/// </summary>
		public sealed record Exit(int Code) : Command(_ExitTag);
		/// <summary>
		/// Is handled by <see cref=""App.Core.Effects.CounterEffects.FirstRecord""/>
		/// </summary>
		public sealed record FirstRecord(App.Record1Namespace.Record FirstRecordValue) : Command(_FirstRecordTag);
		/// <summary>
		/// Is handled by <see cref=""App.Core.Effects.CounterEffects.LoadAsync""/>
		/// </summary>
		public sealed record LoadAsync() : Command(_LoadAsyncTag);
		/// <summary>
		/// Is handled by <see cref=""App.Core.Effects.CounterEffects.Randomize""/>
		/// </summary>
		public sealed record Randomize() : Command(_RandomizeTag);
		/// <summary>
		/// Is handled by <see cref=""App.Core.Effects.CounterEffects.SaveAsync""/>
		/// </summary>
		public sealed record SaveAsync(int Value) : Command(_SaveAsyncTag);

		// App.Core.Effects.MoreCounterEffects
		/// <summary>
		/// Is handled by <see cref=""App.Core.Effects.MoreCounterEffects.HaveFun""/>
		/// </summary>
		public sealed record HaveFun() : Command(_HaveFunTag);
		/// <summary>
		/// Is handled by <see cref=""App.Core.Effects.MoreCounterEffects.HaveFunConcrete""/>
		/// </summary>
		public sealed record HaveFunConcrete() : Command(_HaveFunConcreteTag);
		/// <summary>
		/// Is handled by <see cref=""App.Core.Effects.MoreCounterEffects.SecondRecord""/>
		/// </summary>
		public sealed record SecondRecord(App.Record2Namespace.Record SecondRecordValue) : Command(_SecondRecordTag);

		// Prevent external inheritance
		private Command(int _tag)
		{
			_Tag = _tag;
		}

		public int _Tag { get; }
	}
}
");
	}
}
