﻿using AbreVinci.Embryo.Generator.Generators;

namespace AbreVinci.Embryo.Generator.Tests;

public class MessageTypeGeneratorTests : GeneratorTestFixture
{
	[Fact]
	public void ShouldGenerateMessageTypesForMarkedActions()
	{
		var stateSource = 
@"namespace App.Core
{
	public record State(int Counter);
}
";

		var messageSource = 
@"using AbreVinci.Embryo.Generator.Attributes;

namespace App.Core
{
	[GenerateMessageType]
	public partial record Message;
}
";

		var record1Source =
@"namespace App.Record1Namespace
{
	public record Record;
}
";

		var record2Source =
@"namespace App.Record2Namespace
{
	public record Record;
}
";

		var actionsSource =
@"using AbreVinci.Embryo.Generator.Attributes;
using App.Record1Namespace;

namespace App.Core.Actions
{
	public static class CounterActions
	{
		[Action]
		public static State Reset(this State state)
		{
			return state with { Counter = 0 };
		}

		[Action]
		public static State Increment(this State state, int byValue)
		{
			return state with { Counter = state.Counter + byValue };
		}

		[Action]
		public static State FirstRecord(this State state, Record firstRecordValue)
		{
			return state;
		}
	}
}
";

		var actionsSource2 =
@"using AbreVinci.Embryo.Generator.Attributes;
using App.Record2Namespace;
using System.Collections.Immutable;
using System;

namespace App.Core.Actions
{
	public static class Actions
	{
		[Action]
		public static State SetValue(this State state, int value)
		{
			return state with { Counter = value };
		}

		[Action]
		public static State SecondRecord(this State state, Record secondRecordValue)
		{
			return state;
		}

		[Action]
		public static State Generic(this State state, IImmutableDictionary<string, IImmutableList<Message>> messages)
		{
			return state;
		}

		[Action]
		public static State Nullable(this State state, int? value, TimeSpan? duration, Message? message)
		{
			return state;
		}

		[Action]
		public static State Array(this State state, int[] values, Record[] records)
		{
			return state;
		}
	}
}
";

		var compilation = CreateCompilation(stateSource, messageSource, record1Source, record2Source, actionsSource, actionsSource2);
		var newCompilation = RunGenerators(compilation, new MessageTypeGenerator());

		newCompilation.GetDiagnostics().Should().BeEmpty();

		var generatedText = GetGeneratedText(newCompilation, "Message.g.cs");

		generatedText.Should().Be(
@"using System;
using System.Collections.Immutable;

#nullable enable
namespace App.Core
{
	public partial record Message
	{
		public const int _ArrayTag = 1;
		public const int _GenericTag = 2;
		public const int _NullableTag = 3;
		public const int _SecondRecordTag = 4;
		public const int _SetValueTag = 5;
		public const int _FirstRecordTag = 6;
		public const int _IncrementTag = 7;
		public const int _ResetTag = 8;

		/// $App.Core.Actions.Actions
		/// <summary>
		/// Is handled by <see cref=""App.Core.Actions.Actions.Array""/>
		/// </summary>
		public sealed record Array(int[] Values, App.Record2Namespace.Record[] Records) : Message(_ArrayTag);
		/// <summary>
		/// Is handled by <see cref=""App.Core.Actions.Actions.Generic""/>
		/// </summary>
		public sealed record Generic(IImmutableDictionary<string, IImmutableList<Message>> Messages) : Message(_GenericTag);
		/// <summary>
		/// Is handled by <see cref=""App.Core.Actions.Actions.Nullable""/>
		/// </summary>
		public sealed record Nullable(int? Value, TimeSpan? Duration, Message? Message) : Message(_NullableTag);
		/// <summary>
		/// Is handled by <see cref=""App.Core.Actions.Actions.SecondRecord""/>
		/// </summary>
		public sealed record SecondRecord(App.Record2Namespace.Record SecondRecordValue) : Message(_SecondRecordTag);
		/// <summary>
		/// Is handled by <see cref=""App.Core.Actions.Actions.SetValue""/>
		/// </summary>
		public sealed record SetValue(int Value) : Message(_SetValueTag);

		/// $App.Core.Actions.CounterActions
		/// <summary>
		/// Is handled by <see cref=""App.Core.Actions.CounterActions.FirstRecord""/>
		/// </summary>
		public sealed record FirstRecord(App.Record1Namespace.Record FirstRecordValue) : Message(_FirstRecordTag);
		/// <summary>
		/// Is handled by <see cref=""App.Core.Actions.CounterActions.Increment""/>
		/// </summary>
		public sealed record Increment(int ByValue) : Message(_IncrementTag);
		/// <summary>
		/// Is handled by <see cref=""App.Core.Actions.CounterActions.Reset""/>
		/// </summary>
		public sealed record Reset() : Message(_ResetTag);

		// Prevent external inheritance
		private Message(int _tag)
		{
			_Tag = _tag;
		}

		public int _Tag { get; }
	}
}
");
	}
}
