﻿namespace AbreVinci.Embryo.App.Internal;

internal class WorkerThreadAction<TThreadState> : IWorkerThreadAction<TThreadState>
{
    private readonly Action<IWorkerThreadContext<TThreadState>> _action;
    private readonly TaskCompletionSource<object?> _taskCompletionSource;

    public WorkerThreadAction(Action<IWorkerThreadContext<TThreadState>> action, TaskCompletionSource<object?> taskCompletionSource)
    {
        _action = action;
        _taskCompletionSource = taskCompletionSource;
    }

    public void Execute(IWorkerThreadContext<TThreadState> context)
    {
        try
        {
            _action(context);
            if (context.WasCancellationRequested)
                _taskCompletionSource.SetCanceled();
            else
                _taskCompletionSource.SetResult(null);
        }
        catch (Exception exception)
        {
            _taskCompletionSource.SetException(exception);
        }
    }
}

internal class WorkerThreadAction : IWorkerThreadAction
{
	private readonly Action<IWorkerThreadContext> _action;
	private readonly TaskCompletionSource<object?> _taskCompletionSource;

	public WorkerThreadAction(Action<IWorkerThreadContext> action, TaskCompletionSource<object?> taskCompletionSource)
	{
		_action = action;
		_taskCompletionSource = taskCompletionSource;
	}

	public void Execute(IWorkerThreadContext context)
	{
		try
		{
			_action(context);
			if (context.WasCancellationRequested)
				_taskCompletionSource.SetCanceled();
			else
				_taskCompletionSource.SetResult(null);
		}
		catch (Exception exception)
		{
			_taskCompletionSource.SetException(exception);
		}
	}
}
