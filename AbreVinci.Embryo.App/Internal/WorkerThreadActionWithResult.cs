﻿namespace AbreVinci.Embryo.App.Internal;

internal class WorkerThreadActionWithResult<TThreadState, TResult> : IWorkerThreadAction<TThreadState>
{
	private readonly Func<IWorkerThreadContext<TThreadState>, TResult> _action;
	private readonly TaskCompletionSource<TResult> _taskCompletionSource;

	public WorkerThreadActionWithResult(Func<IWorkerThreadContext<TThreadState>, TResult> action, TaskCompletionSource<TResult> taskCompletionSource)
	{
		_action = action;
		_taskCompletionSource = taskCompletionSource;
	}

	public void Execute(IWorkerThreadContext<TThreadState> context)
	{
		try
		{
			var result = _action(context);
			if (context.WasCancellationRequested)
				_taskCompletionSource.SetCanceled();
			else
				_taskCompletionSource.SetResult(result);
		}
		catch (Exception exception)
		{
			_taskCompletionSource.SetException(exception);
		}
	}
}

internal class WorkerThreadActionWithResult<TResult> : IWorkerThreadAction
{
	private readonly Func<IWorkerThreadContext, TResult> _action;
	private readonly TaskCompletionSource<TResult> _taskCompletionSource;

	public WorkerThreadActionWithResult(Func<IWorkerThreadContext, TResult> action, TaskCompletionSource<TResult> taskCompletionSource)
	{
		_action = action;
		_taskCompletionSource = taskCompletionSource;
	}

	public void Execute(IWorkerThreadContext context)
	{
		try
		{
			var result = _action(context);
			if (context.WasCancellationRequested)
				_taskCompletionSource.SetCanceled();
			else
				_taskCompletionSource.SetResult(result);
		}
		catch (Exception exception)
		{
			_taskCompletionSource.SetException(exception);
		}
	}
}
