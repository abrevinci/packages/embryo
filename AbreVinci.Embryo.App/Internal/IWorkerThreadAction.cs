﻿namespace AbreVinci.Embryo.App.Internal;

internal interface IWorkerThreadAction<TState>
{
    void Execute(IWorkerThreadContext<TState> context);
}

internal interface IWorkerThreadAction
{
	void Execute(IWorkerThreadContext context);
}
