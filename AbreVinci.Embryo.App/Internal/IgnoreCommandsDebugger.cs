﻿namespace AbreVinci.Embryo.App.Internal;

internal class IgnoreCommandsDebugger<TState, TMessage> : IDebugger<TState, TMessage, object>
{
    private readonly IDebugger<TState, TMessage> _debugger;

    public IgnoreCommandsDebugger(IDebugger<TState, TMessage> debugger)
    {
        _debugger = debugger;
    }

    public bool IsPaused => _debugger.IsPaused;

    public void OnInit(TState state, IImmutableList<object> commands)
    {
	    _debugger.OnInit(state);
    }

    public void OnUpdate(TState stateBefore, TMessage message, TState stateAfter, IImmutableList<object> commands)
    {
        _debugger.OnUpdate(stateBefore, message, stateAfter);
    }

    public IDisposable Subscribe(IObserver<TState> observer)
    {
	    return _debugger.Subscribe(observer);
    }
}
