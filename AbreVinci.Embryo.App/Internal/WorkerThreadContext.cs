﻿namespace AbreVinci.Embryo.App.Internal;

internal class WorkerThreadContext<TThreadState> : IWorkerThreadContext<TThreadState>
{
    private readonly ManualResetEventSlim _cancelSignal;

    public WorkerThreadContext(TThreadState state, ManualResetEventSlim cancelSignal)
    {
        State = state;
        _cancelSignal = cancelSignal;
    }

    public TThreadState State { get; set; }

    public bool WasCancellationRequested => _cancelSignal.Wait(0);

    public void Sleep(TimeSpan duration)
    {
        _cancelSignal.Wait(duration);
    }
}

internal class WorkerThreadContext : IWorkerThreadContext
{
	private readonly ManualResetEventSlim _cancelSignal;

	public WorkerThreadContext(ManualResetEventSlim cancelSignal)
	{
		_cancelSignal = cancelSignal;
	}
    
	public bool WasCancellationRequested => _cancelSignal.Wait(0);

	public void Sleep(TimeSpan duration)
	{
		_cancelSignal.Wait(duration);
	}
}
