﻿namespace AbreVinci.Embryo.App;

/// <summary>
/// Allows you to listen to a stream of commands.
/// </summary>
/// <typeparam name="TCommand">The type of commands.</typeparam>
public interface ICommandSubject<out TCommand>
{
	/// <summary>
	/// An observable stream of commands. When implemented by <see cref="Program{TState, TMessage, TCommand}"/>, 
	/// this will be emitting the commands that are returned from your init and update functions.
	/// </summary>
	IObservable<TCommand> Commands { get; }
}
