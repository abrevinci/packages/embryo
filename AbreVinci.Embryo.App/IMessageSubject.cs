﻿namespace AbreVinci.Embryo.App;

/// <summary>
/// Allows you to listen to a stream of messages.
/// </summary>
/// <typeparam name="TMessage">The type of messages.</typeparam>
public interface IMessageSubject<out TMessage>
{
	/// <summary>
	/// An observable stream of messages. When implemented by <see cref="Program{TState, TMessage, TCommand}"/>, 
	/// this will be emitting the messages that have been dispatched using <see cref="Program{TState, TMessage, TCommand}.DispatchMessage(TMessage)"/>.
	/// </summary>
	IObservable<TMessage> Messages { get; }
}
