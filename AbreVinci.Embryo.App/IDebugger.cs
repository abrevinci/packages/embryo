namespace AbreVinci.Embryo.App;

/// <summary>
/// Interface to allow hooking in time machine debugging into a <see cref="IProgram{TState, TMessage, TCommand}"/>.
/// The debugger itself is an observable of the program state that the debugger wants to apply during time travel.
/// </summary>
/// <typeparam name="TState">The state type matching the state type of the program.</typeparam>
/// <typeparam name="TMessage">The message type matching the message type of the program.</typeparam>
/// <typeparam name="TCommand">The command type matching the command type of the program.</typeparam>
public interface IDebugger<TState, in TMessage, TCommand> : IObservable<TState>
{
	/// <summary>
	/// Gets a value indicating if the debugger is currently paused. When implementing time travel debugging, 
	/// this should return true when scrubbing and inspecting past messages.
	/// </summary>
	bool IsPaused { get; }

	/// <summary>
	/// This allows the debugger to inspect the result of the init function of your program's reducer. />
	/// </summary>
	/// <param name="state">The state that was returned from init.</param>
	/// <param name="commands">The commands that were returned from init.</param>
	void OnInit(TState state, IImmutableList<TCommand> commands);

	/// <summary>
	/// This allows the debugger to inspect incoming messages and their effect on the state. It also provides the list of commands that might have been emitted.
	/// </summary>
	/// <param name="stateBefore">The state before the update function is called.</param>
	/// <param name="message">The incoming message.</param>
	/// <param name="stateAfter">The state after the update function is called.</param>
	/// <param name="commands">Any commands returned by the update function.</param>
	void OnUpdate(TState stateBefore, TMessage message, TState stateAfter, IImmutableList<TCommand> commands);
}

/// <summary>
/// Interface to allow hooking in time machine debugging into a <see cref="IProgram{TState, TMessage}"/>.
/// The debugger itself is an observable of the program state that the debugger wants to apply during time travel.
/// </summary>
/// <typeparam name="TState">The state type matching the state type of the program.</typeparam>
/// <typeparam name="TMessage">The message type matching the message type of the program.</typeparam>
public interface IDebugger<TState, in TMessage> : IObservable<TState>
{
	/// <summary>
	/// Gets a value indicating if the debugger is currently paused. When implementing time travel debugging, 
	/// this should return true when scrubbing and inspecting past messages.
	/// </summary>
	bool IsPaused { get; }

	/// <summary>
	/// This allows the debugger to inspect the initial state from your init function in your program's reducer.
	/// </summary>
	/// <param name="state">The state that was returned from init.</param>
	void OnInit(TState state);

	/// <summary>
	/// This allows the debugger to inspect incoming messages and their effect on the state.
	/// </summary>
	/// <param name="stateBefore">The state before the update function is called.</param>
	/// <param name="message">The incoming message.</param>
	/// <param name="stateAfter">The state after the update function is called.</param>
	void OnUpdate(TState stateBefore, TMessage message, TState stateAfter);
}
