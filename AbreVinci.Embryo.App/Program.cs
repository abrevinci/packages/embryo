using AbreVinci.Embryo.App.Internal;
using AbreVinci.Embryo.Core;

namespace AbreVinci.Embryo.App;

/// <summary>
/// Main application state machine following the non-view part of elm architecture.
/// </summary>
/// <typeparam name="TState">The application state type</typeparam>
/// <typeparam name="TMessage">The application message type</typeparam>
/// <typeparam name="TCommand">The application command type</typeparam>
public class Program<TState, TMessage, TCommand> : IProgram<TState, TMessage, TCommand>
{
	private readonly Func<ActionContext<TState, TCommand>, TMessage, ActionContext<TState, TCommand>> _update;
	private readonly IScheduler _dispatcherScheduler;
	private readonly IDebugger<TState, TMessage, TCommand>? _debugger;

	private TState _state;
	private readonly ISubject<TState> _states;
	private readonly ISubject<TMessage> _messages;
	private readonly ISubject<TCommand> _commands;

	/// <summary>
	/// Program constructor.
	/// </summary>
	/// <param name="init">A pure function returning an action context containing the desired initial state and optionally a set of commands to queue up immediately.</param>
	/// <param name="update">A pure function taking in an action context with the current state and a dispatched message and returning a new action context based on those. It must treat the action context and message as immutable.</param>
	/// <param name="dispatcherScheduler">The scheduler that will be used to dispatch messages.</param>
	/// <param name="stateBufferSize">The number of previous states to store internally, available for the next subscriber to <see cref="States"/>.</param>
	/// <param name="messageBufferSize">The number of previous messages to store internally, available for the next subscriber to <see cref="Messages"/>.</param>
	/// <param name="commandBufferSize">The number of previous commands to store internally, available for the next subscriber to <see cref="Commands"/>. Make sure this is bigger than the number of commands returned in <paramref name="init"/>!</param>
	/// <param name="debugger">An optional debugger instance that is able to pause state machine live execution and do time travel debugging of states, messages, and commands.</param>
	/// <remarks>
	/// Will call <paramref name="init"/> to set state and initial commands.
	/// </remarks>
	public Program(
		Func<ActionContext<TState, TCommand>> init,
		Func<ActionContext<TState, TCommand>, TMessage, ActionContext<TState, TCommand>> update,
		IScheduler dispatcherScheduler,
		int stateBufferSize = 1,
		int messageBufferSize = 0,
		int commandBufferSize = 10,
		IDebugger<TState, TMessage, TCommand>? debugger = null)
	{
		_update = update;
		_dispatcherScheduler = dispatcherScheduler;
		_debugger = debugger;

		_states = stateBufferSize > 0 ?
			new ReplaySubject<TState>(stateBufferSize) :
			new Subject<TState>();

		_messages = messageBufferSize > 0 ?
			new ReplaySubject<TMessage>(messageBufferSize) :
			new Subject<TMessage>();
		
		_commands = commandBufferSize > 0 ?
			new ReplaySubject<TCommand>(commandBufferSize) :
			new Subject<TCommand>();

		(_state, var initialCommands) = init();
		_debugger?.OnInit(_state, initialCommands);
		_states.OnNext(_state);
		foreach (var command in initialCommands)
		{
			_commands.OnNext(command);
		}

		_debugger?.Subscribe(s => _states.OnNext(s));
	}
	
	/// <summary>
	/// Allows for subscriptions to the stream of states.
	/// </summary>
	public IObservable<TState> States => _states.DistinctUntilChanged();

	/// <summary>
	/// Allows for subscriptions to the stream of dispatched messages.
	/// </summary>
	public IObservable<TMessage> Messages => _messages;

	/// <summary>
	/// Allows for subscriptions to the stream of requested commands.
	/// </summary>
	public IObservable<TCommand> Commands => _commands;

	/// <summary>
	/// Dispatches a message causing the update function to be called in order to compute a new state.
	/// </summary>
	/// <param name="message">The message to dispatch.</param>
	/// <remarks>
	/// Emits the new state to the <see cref="States"/> observable.
	/// Emits the dispatched message to the <see cref="Messages"/> observable.
	/// Emits all commands returned from update to the <see cref="Commands"/> observable.
	/// </remarks>
	public void DispatchMessage(TMessage message)
	{
		_dispatcherScheduler.Schedule(() =>
		{
			if (_debugger?.IsPaused == true)
				return;

			var stateBefore = _state;
			var (stateAfter, commands) = _update(new ActionContext<TState, TCommand>(stateBefore), message);

			_debugger?.OnUpdate(stateBefore, message, stateAfter, commands);

			_state = stateAfter;
			_states.OnNext(_state);
			_messages.OnNext(message);

			foreach (var command in commands)
			{
				_commands.OnNext(command);
			}
		});
	}
}

/// <summary>
/// Contains program factory methods for different use cases.
/// </summary>
public static class Program
{
	/// <summary>
	/// Program factory method.
	/// </summary>
	/// <typeparam name="TState">The application state type</typeparam>
	/// <typeparam name="TMessage">The application message type</typeparam>
	/// <typeparam name="TCommand">The application command type</typeparam>
	/// <param name="init">A pure function returning the desired initial action context.</param>
	/// <param name="update">A pure function taking in the current action context and a dispatched message and returning a new action context based on those. It must treat the state and message as immutable.</param>
	/// <param name="dispatcherScheduler">The scheduler that will be used to dispatch messages.</param>
	/// <param name="stateBufferSize">The number of previous states to store internally, available for the next subscriber to <see cref="IStateSubject{TState}.States"/>.</param>
	/// <param name="messageBufferSize">The number of previous messages to store internally, available for the next subscriber to <see cref="IMessageSubject{TMessage}.Messages"/>.</param>
	/// <param name="commandBufferSize">The number of previous commands to store internally, available for the next subscriber to <see cref="ICommandSubject{TCommand}.Commands"/>. Make sure this is bigger than the number of commands returned in <paramref name="init"/>!</param>
	/// <param name="debugger">An optional debugger instance that is able to pause state machine live execution and do time travel debugging of states, messages, and commands.</param>
	/// <remarks>
	/// Will call <paramref name="init"/> to set state and initial commands.
	/// </remarks>
	public static IProgram<TState, TMessage, TCommand> Create<TState, TMessage, TCommand>(
		Func<ActionContext<TState, TCommand>> init,
		Func<ActionContext<TState, TCommand>, TMessage, ActionContext<TState, TCommand>> update,
		IScheduler dispatcherScheduler,
		int stateBufferSize = 1,
		int messageBufferSize = 0,
		int commandBufferSize = 10,
		IDebugger<TState, TMessage, TCommand>? debugger = null)
	{
		return new Program<TState, TMessage, TCommand>(
			init, 
			update,
			dispatcherScheduler, 
			stateBufferSize, 
			messageBufferSize, 
			commandBufferSize, 
			debugger);
	}

	/// <summary>
	/// Program factory method.
	/// </summary>
	/// <typeparam name="TState">The application state type</typeparam>
	/// <typeparam name="TMessage">The application message type</typeparam>
	/// <typeparam name="TCommand">The application command type</typeparam>
	/// <param name="init">A pure function returning the desired initial state and optionally a set of commands to queue up immediately.</param>
	/// <param name="update">A pure function taking in the current action context and a dispatched message and returning a new action context based on those. It must treat the state and message as immutable.</param>
	/// <param name="dispatcherScheduler">The scheduler that will be used to dispatch messages.</param>
	/// <param name="stateBufferSize">The number of previous states to store internally, available for the next subscriber to <see cref="IStateSubject{TState}.States"/>.</param>
	/// <param name="messageBufferSize">The number of previous messages to store internally, available for the next subscriber to <see cref="IMessageSubject{TMessage}.Messages"/>.</param>
	/// <param name="commandBufferSize">The number of previous commands to store internally, available for the next subscriber to <see cref="ICommandSubject{TCommand}.Commands"/>.</param>
	/// <param name="debugger">An optional debugger instance that is able to pause state machine live execution and do time travel debugging of states, messages, and commands.</param>
	/// <remarks>
	/// Will call <paramref name="init"/> to set state.
	/// </remarks>
	public static IProgram<TState, TMessage, TCommand> Create<TState, TMessage, TCommand>(
		Func<TState> init,
		Func<ActionContext<TState, TCommand>, TMessage, ActionContext<TState, TCommand>> update,
		IScheduler dispatcherScheduler,
		int stateBufferSize = 1,
		int messageBufferSize = 0,
		int commandBufferSize = 10,
		IDebugger<TState, TMessage, TCommand>? debugger = null)
	{
		return new Program<TState, TMessage, TCommand>(
			() => new ActionContext<TState, TCommand>(init()),
			update,
			dispatcherScheduler,
			stateBufferSize,
			messageBufferSize,
			commandBufferSize,
			debugger);
	}

	/// <summary>
	/// Program factory method.
	/// </summary>
	/// <typeparam name="TState">The application state type</typeparam>
	/// <typeparam name="TMessage">The application message type</typeparam>
	/// <typeparam name="TCommand">The application command type</typeparam>
	/// <param name="initialState">A pure function returning the desired initial state and optionally a set of commands to queue up immediately.</param>
	/// <param name="update">A pure function taking in the current action context and a dispatched message and returning a new action context based on those. It must treat the state and message as immutable.</param>
	/// <param name="dispatcherScheduler">The scheduler that will be used to dispatch messages.</param>
	/// <param name="stateBufferSize">The number of previous states to store internally, available for the next subscriber to <see cref="IStateSubject{TState}.States"/>.</param>
	/// <param name="messageBufferSize">The number of previous messages to store internally, available for the next subscriber to <see cref="IMessageSubject{TMessage}.Messages"/>.</param>
	/// <param name="commandBufferSize">The number of previous commands to store internally, available for the next subscriber to <see cref="ICommandSubject{TCommand}.Commands"/>.</param>
	/// <param name="debugger">An optional debugger instance that is able to pause state machine live execution and do time travel debugging of states, messages, and commands.</param>
	public static IProgram<TState, TMessage, TCommand> Create<TState, TMessage, TCommand>(
		TState initialState,
		Func<ActionContext<TState, TCommand>, TMessage, ActionContext<TState, TCommand>> update,
		IScheduler dispatcherScheduler,
		int stateBufferSize = 1,
		int messageBufferSize = 0,
		int commandBufferSize = 10,
		IDebugger<TState, TMessage, TCommand>? debugger = null)
	{
		return new Program<TState, TMessage, TCommand>(
			() => new ActionContext<TState, TCommand>(initialState),
			update,
			dispatcherScheduler,
			stateBufferSize,
			messageBufferSize,
			commandBufferSize,
			debugger);
	}

	/// <summary>
	/// A simplifier program factory method if not working with commands.
	/// </summary>
	/// <typeparam name="TState">The application state type</typeparam>
	/// <typeparam name="TMessage">The application message type</typeparam>
	/// <param name="init">A pure function returning the desired initial state and optionally a set of commands to queue up immediately.</param>
	/// <param name="update">A pure function taking in the current state and a dispatched message and returning a new state based on those. It must treat the state and message as immutable.</param>
	/// <param name="dispatcherScheduler">The scheduler that will be used to dispatch messages.</param>
	/// <param name="stateBufferSize">The number of previous states to store internally, available for the next subscriber to <see cref="IStateSubject{TState}.States"/>.</param>
	/// <param name="messageBufferSize">The number of previous messages to store internally, available for the next subscriber to <see cref="IMessageSubject{TMessage}.Messages"/>.</param>
	/// <param name="debugger">An optional debugger instance that is able to pause state machine live execution and do time travel debugging of states and messages.</param>
	/// <returns></returns>
	public static IProgram<TState, TMessage> Create<TState, TMessage>(
		Func<TState> init,
		Func<TState, TMessage, TState> update,
		IScheduler dispatcherScheduler,
		int stateBufferSize = 1,
		int messageBufferSize = 0,
		IDebugger<TState, TMessage>? debugger = null)
	{
		return new Program<TState, TMessage, object>(
			() => new ActionContext<TState, object>(init()),
			(context, message) => new ActionContext<TState, object>(update(context.State, message)),
			dispatcherScheduler, 
			stateBufferSize, 
			messageBufferSize, 
			0,
			debugger != null ? new IgnoreCommandsDebugger<TState, TMessage>(debugger) : null);
	}

	/// <summary>
	/// A simplifier program factory method if not working with commands.
	/// </summary>
	/// <typeparam name="TState">The application state type</typeparam>
	/// <typeparam name="TMessage">The application message type</typeparam>
	/// <param name="initialState">The starting state of the state machine.</param>
	/// <param name="update">A pure function taking in the current state and a dispatched message and returning a new state based on those. It must treat the state and message as immutable.</param>
	/// <param name="dispatcherScheduler">The scheduler that will be used to dispatch messages.</param>
	/// <param name="stateBufferSize">The number of previous states to store internally, available for the next subscriber to <see cref="IStateSubject{TState}.States"/>.</param>
	/// <param name="messageBufferSize">The number of previous messages to store internally, available for the next subscriber to <see cref="IMessageSubject{TMessage}.Messages"/>.</param>
	/// <param name="debugger">An optional debugger instance that is able to pause state machine live execution and do time travel debugging of states and messages.</param>
	/// <returns></returns>
	public static IProgram<TState, TMessage> Create<TState, TMessage>(
		TState initialState, 
		Func<TState, TMessage, TState> update, 
		IScheduler dispatcherScheduler, 
		int stateBufferSize = 1, 
		int messageBufferSize = 0, 
		IDebugger<TState, TMessage>? debugger = null)
    {
		return new Program<TState, TMessage, object>(
			() => new ActionContext<TState, object>(initialState),
			(context, message) => new ActionContext<TState, object>(update(context.State, message)),
			dispatcherScheduler, 
			stateBufferSize, 
			messageBufferSize, 
			0,
			debugger != null ? new IgnoreCommandsDebugger<TState, TMessage>(debugger) : null);
    }
}
