﻿namespace AbreVinci.Embryo.App;

/// <summary>
/// Allows the application to define feature toggles for use with message and command filters and with views.
/// </summary>
/// <typeparam name="TFeatureToggle">The feature toggle type used by the application.</typeparam>
public interface IFeatureToggleDictionary<TFeatureToggle> where TFeatureToggle : notnull
{
	/// <summary>
	/// Checks if the given <paramref name="featureToggle"/> is enabled or not.
	/// </summary>
	/// <param name="featureToggle">The feature toggle to check.</param>
	/// <returns>True if the toggle is enabled, false otherwise.</returns>
	bool IsFeatureEnabled(TFeatureToggle featureToggle);

	/// <summary>
	/// Allows the implementer to notify if any toggles have changed their enabled state.
	/// </summary>
	event Action<TFeatureToggle> FeatureToggleChanged;
}
