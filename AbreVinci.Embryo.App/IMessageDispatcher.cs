﻿namespace AbreVinci.Embryo.App;

/// <summary>
/// Allows you to dispatch messages.
/// </summary>
/// <typeparam name="TMessage">The type of message.</typeparam>
public interface IMessageDispatcher<in TMessage>
{
	/// <summary>
	/// Dispatches a message. When implemented by <see cref="Program{TState, TMessage, TCommand}"/>, 
	/// this will cause your update function to run and the program state to be updated.
	/// </summary>
	/// <param name="message">The message to dispatch.</param>
	void DispatchMessage(TMessage message);
}
