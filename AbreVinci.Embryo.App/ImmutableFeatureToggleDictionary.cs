﻿namespace AbreVinci.Embryo.App;

/// <summary>
/// An utility implementation for a feature toggle dictionary that does not change any toggle state after creation.
/// This can be used if toggles are only loaded at application startup.
/// </summary>
/// <typeparam name="TFeatureToggle">The type of feature toggle used by the application.</typeparam>
public class ImmutableFeatureToggleDictionary<TFeatureToggle> : IFeatureToggleDictionary<TFeatureToggle> where TFeatureToggle : notnull
{
	private readonly IImmutableDictionary<TFeatureToggle, bool> _dictionary;

	/// <summary>
	/// Constructor.
	/// </summary>
	/// <param name="dictionary">A dictionary to use as the underlying implementation.</param>
	public ImmutableFeatureToggleDictionary(IImmutableDictionary<TFeatureToggle, bool> dictionary)
	{
		_dictionary = dictionary;
	}

	/// <summary>
	/// Checks if the given <paramref name="featureToggle"/> is enabled or not. If the feature does not exist
	/// in the underlying dictionary, this will return false.
	/// </summary>
	/// <param name="featureToggle">The feature toggle to check.</param>
	/// <returns>True if the toggle is enabled, false otherwise.</returns>
	public bool IsFeatureEnabled(TFeatureToggle featureToggle)
	{
		return _dictionary.TryGetValue(featureToggle, out bool isEnabled) && isEnabled;
	}

	event Action<TFeatureToggle> IFeatureToggleDictionary<TFeatureToggle>.FeatureToggleChanged { add { } remove { } }
}