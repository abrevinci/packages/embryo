﻿namespace AbreVinci.Embryo.App;

/// <summary>
/// Provides extension methods for <see cref="IProgram{TState, TMessage, TCommand}"/>.
/// </summary>
public static class ProgramExtensions
{
    /// <summary>
    /// Applies the given <paramref name="messageFilter"/> and/or <paramref name="commandFilter"/> to the program.
    /// Works by wrapping the program inside a <see cref="FilteredProgram{TState, TMessage, TCommand}"/> instance.
    /// </summary>
    /// <typeparam name="TState">The application state type.</typeparam>
    /// <typeparam name="TMessage">The application message type.</typeparam>
    /// <typeparam name="TCommand">The application command type.</typeparam>
    /// <param name="program">The program to apply the filters to.</param>
    /// <param name="messageFilter">An optional message filter to apply.</param>
    /// <param name="commandFilter">An optional command filter to apply.</param>
    /// <returns></returns>
    public static IProgram<TState, TMessage, TCommand> ApplyFilters<TState, TMessage, TCommand>(
        this IProgram<TState, TMessage, TCommand> program,
        IMessageFilter<TMessage>? messageFilter = null,
        ICommandFilter<TCommand>? commandFilter = null) where TMessage : notnull where TCommand : notnull
    {
        return new FilteredProgram<TState, TMessage, TCommand>(program, messageFilter, commandFilter);
    }

    /// <summary>
    /// Applies the given <paramref name="messageFilter"/> to the program.
    /// Works by wrapping the program inside a <see cref="FilteredProgram{TState, TMessage}"/> instance.
    /// </summary>
    /// <typeparam name="TState">The application state type.</typeparam>
    /// <typeparam name="TMessage">The application message type.</typeparam>
    /// <param name="program">The program to apply the filters to.</param>
    /// <param name="messageFilter">The message filter to apply.</param>
    /// <returns></returns>
    public static IProgram<TState, TMessage> ApplyMessageFilter<TState, TMessage>(
        this IProgram<TState, TMessage> program,
        IMessageFilter<TMessage> messageFilter) where TMessage : notnull
    {
        return new FilteredProgram<TState, TMessage>(program, messageFilter);
    }
}