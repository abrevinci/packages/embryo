﻿using AbreVinci.Embryo.Core;

namespace AbreVinci.Embryo.App;

/// <summary>
/// A program wrapper that allows you to filter incoming messages and outgoing commands.
/// </summary>
/// <typeparam name="TState">The application state type.</typeparam>
/// <typeparam name="TMessage">The application message type.</typeparam>
/// <typeparam name="TCommand">The application command type.</typeparam>
public class FilteredProgram<TState, TMessage, TCommand> : IProgram<TState, TMessage, TCommand> where TMessage : notnull where TCommand : notnull
{
    private readonly IProgram<TState, TMessage, TCommand> _program;
    private readonly IMessageFilter<TMessage>? _messageFilter;
    private readonly ICommandFilter<TCommand>? _commandFilter;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="program">The program to wrap</param>
    /// <param name="messageFilter">An optional message filter to apply.</param>
    /// <param name="commandFilter">An optional command filter to apply.</param>
    public FilteredProgram(IProgram<TState, TMessage, TCommand> program, IMessageFilter<TMessage>? messageFilter, ICommandFilter<TCommand>? commandFilter)
    {
        _program = program;
        _messageFilter = messageFilter;
        _commandFilter = commandFilter;
    }

    /// <summary>
    /// Exposes the underlying stream of states.
    /// </summary>
    public IObservable<TState> States => _program.States;

    /// <summary>
    /// Exposes the underlying (but potentially filtered) stream of messages.
    /// </summary>
    public IObservable<TMessage> Messages => _program.Messages;

    /// <summary>
    /// Exposes the underlying (but potentially filtered) stream of commands.
    /// </summary>
    public IObservable<TCommand> Commands => _commandFilter != null ?
        _program.Commands.SelectNotNull(_commandFilter.FilterCommand) :
        _program.Commands;

    /// <summary>
    /// Dispatches a message to the underlying program if it passes any message filter that has been applied.
    /// </summary>
    /// <param name="message">The message to dispatch.</param>
    public void DispatchMessage(TMessage message)
    {
        if (_messageFilter == null)
        {
            _program.DispatchMessage(message);
        } 
        else if (_messageFilter.FilterMessage(message) is { } filteredMessage)
        {
            _program.DispatchMessage(filteredMessage);
        }
    }
}

/// <summary>
/// A program wrapper that allows you to filter incoming messages (use this variant if your program does not use commands).
/// </summary>
/// <typeparam name="TState">The application state type.</typeparam>
/// <typeparam name="TMessage">The application message type.</typeparam>
public class FilteredProgram<TState, TMessage> : IProgram<TState, TMessage> where TMessage : notnull
{
    private readonly IProgram<TState, TMessage> _program;
    private readonly IMessageFilter<TMessage> _messageFilter;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="program">The program to wrap</param>
    /// <param name="messageFilter">An optional message filter to apply.</param>
    public FilteredProgram(IProgram<TState, TMessage> program, IMessageFilter<TMessage> messageFilter)
    {
        _program = program;
        _messageFilter = messageFilter;
    }

    /// <summary>
    /// Exposes the underlying stream of states.
    /// </summary>
    public IObservable<TState> States => _program.States;

    /// <summary>
    /// Exposes the underlying (but filtered) stream of messages.
    /// </summary>
    public IObservable<TMessage> Messages => _program.Messages;

    /// <summary>
    /// Dispatches a message to the underlying program if it passes the message filter that has been applied.
    /// </summary>
    /// <param name="message">The message to dispatch.</param>
    public void DispatchMessage(TMessage message)
    {
        if (_messageFilter.FilterMessage(message) is { } filteredMessage)
        {
            _program.DispatchMessage(filteredMessage);
        }
    }
}
