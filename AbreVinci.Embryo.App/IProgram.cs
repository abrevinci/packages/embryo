﻿namespace AbreVinci.Embryo.App;

/// <summary>
/// Interface for <see cref="Program{TState, TMessage, TCommand}"/> excluding commands.
/// </summary>
/// <typeparam name="TState">The program state type.</typeparam>
/// <typeparam name="TMessage">The program message type.</typeparam>
public interface IProgram<out TState, TMessage> : IStateSubject<TState>, IMessageSubject<TMessage>, IMessageDispatcher<TMessage>
{
}

/// <summary>
/// Interface for <see cref="Program{TState, TMessage, TCommand}"/> including commands.
/// </summary>
/// <typeparam name="TState">The program state type.</typeparam>
/// <typeparam name="TMessage">The program message type.</typeparam>
/// <typeparam name="TCommand">The program command type.</typeparam>
public interface IProgram<out TState, TMessage, out TCommand> : IProgram<TState, TMessage>, ICommandSubject<TCommand>
{
}
