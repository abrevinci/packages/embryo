﻿using AbreVinci.Embryo.App.Internal;

namespace AbreVinci.Embryo.App;

/// <summary>
/// A worker thread that accepts incoming actions and executes them as tasks on a single thread (as opposed to the normal thread pool).
/// This is useful when writing code controlling a system that needs to run on a different thread with the means of commands.
/// </summary>
/// <typeparam name="TThreadState">The internal state type of the thread, accessible by each action running on it.</typeparam>
public sealed class WorkerThread<TThreadState> : IWorkerThread<TThreadState>, IDisposable
{
    private readonly ConcurrentQueue<IWorkerThreadAction<TThreadState>> _actionQueue;
    private readonly SemaphoreSlim _actionsAvailable;
    private readonly ManualResetEventSlim _exitSignal;
    private readonly ManualResetEventSlim _cancelSignal;
    private readonly Func<TThreadState> _initState;
    private readonly Action<TThreadState> _cleanupState;
    private readonly Thread _thread;

    /// <summary>
    /// Constructor that starts the worker thread and tells it how to initiate and clean up its state.
    /// </summary>
    /// <param name="initState">Initiate the thread state. This will run on the thread itself before any incoming actions are executed.</param>
    /// <param name="cleanupState">Cleans up the thread state. This will run on the thread itself after it has stopped listening for actions (essentially when this class is disposed).</param>
    /// <param name="threadName">Optionally assign a thread name to the worker thread.</param>
    /// <param name="isBackground">Set to true if you want the worker thread to be a background thread.</param>
    /// <param name="threadPriority">Allows you to specify what thread priority to use for the worker thread.</param>
    public WorkerThread(Func<TThreadState> initState, Action<TThreadState> cleanupState, string? threadName = null, bool isBackground = false, ThreadPriority threadPriority = ThreadPriority.Normal)
    {
        _actionQueue = new ConcurrentQueue<IWorkerThreadAction<TThreadState>>();
        _actionsAvailable = new SemaphoreSlim(0);
        _exitSignal = new ManualResetEventSlim(false);
        _cancelSignal = new ManualResetEventSlim(false);
        _initState = initState;
        _cleanupState = cleanupState;
        _thread = new Thread(ThreadMain)
        {
            Name = threadName,
            IsBackground = isBackground,
            Priority = threadPriority
        };
        _thread.Start();
    }

    /// <summary>
    /// The managed thread id of the worker thread.
    /// </summary>
    public int ThreadId => _thread.ManagedThreadId;

    /// <summary>
    /// Queues up the given <paramref name="action"/> to be executed on the thread once all previous actions have completed.
    /// </summary>
    /// <param name="action">The action to execute.</param>
    /// <returns>An awaitable task that completes once the action has finished executing or cancels if <see cref="RequestCancellationAsync"/> is called before it finishes executing. If the action throws an exception, the task will pick this up too.</returns>
    public Task InvokeAsync(Action<IWorkerThreadContext<TThreadState>> action)
    {
        var taskCompletionSource = new TaskCompletionSource<object?>();
        _actionQueue.Enqueue(new WorkerThreadAction<TThreadState>(action, taskCompletionSource));
        _actionsAvailable.Release(1);
        return taskCompletionSource.Task;
    }

    /// <summary>
    /// Queues up the given <paramref name="action"/> to be executed on the thread once all previous actions have completed.
    /// </summary>
    /// <param name="action">The action to execute.</param>
    /// <returns>An awaitable task that completes with the action's return value once the action has finished executing or cancels if <see cref="RequestCancellationAsync"/> is called before it finishes executing. If the action throws an exception, the task will pick this up too.</returns>
    public Task<TResult> InvokeAsync<TResult>(Func<IWorkerThreadContext<TThreadState>, TResult> action)
    {
        var taskCompletionSource = new TaskCompletionSource<TResult>();
        _actionQueue.Enqueue(new WorkerThreadActionWithResult<TThreadState, TResult>(action, taskCompletionSource));
        _actionsAvailable.Release(1);
        return taskCompletionSource.Task;
    }

    /// <summary>
    /// Request any unfinished actions that have previously been queued up to be cancelled.
    /// You can use this to break out of loops (the check the <see cref="IWorkerThreadContext{TState}.WasCancellationRequested"/>) or sleeps in actions.
    /// Once you have called this, you may again invoke new actions.
    /// </summary>
    /// <returns>An awaitable task completing once all actions have been successfully cancelled.</returns>
    public Task RequestCancellationAsync()
    {
        _cancelSignal.Set();
        // the following allows new actions to be queued up.
        return InvokeAsync(_ => _cancelSignal.Reset());
    }

    /// <summary>
    /// Cancels the running action, breaks out of the thread loop and cleans up the thread state before joining the thread to the current thread.
    /// </summary>
    /// <exception cref="System.InvalidOperationException">Thrown in case this is called from the thread itself, which is illegal and would wait forever.</exception>
    public void Dispose()
    {
        if (ReferenceEquals(_thread, Thread.CurrentThread))
        {
            // WARNING: This check is CRUCIAL as this join will wait indefinitely if StopThread was called from _thread. 
            throw new InvalidOperationException("Illegal call to Dispose(). The thread cannot stop itself.");
        }

        _exitSignal.Set();
        _cancelSignal.Set();
        _actionsAvailable.Release(1);
        _thread.Join();
        _actionsAvailable.Dispose();
    }

    private void ThreadMain()
    {
        var state = _initState();

        while (!_exitSignal.Wait(0))
        {
            _actionsAvailable.Wait();
            if (_actionQueue.TryDequeue(out var action))
            {
                var context = new WorkerThreadContext<TThreadState>(state, _cancelSignal);
                action.Execute(context);
                state = context.State;
            }
        }

        _cleanupState(state);
    }
}

/// <summary>
/// A worker thread that accepts incoming actions and executes them as tasks on a single thread (as opposed to the normal thread pool).
/// This is useful when writing code controlling a system that needs to run on a different thread with the means of commands. If you
/// need to store state in your thread you should use the <see cref="WorkerThread{TThreadState}"/> class instead.
/// </summary>
public sealed class WorkerThread : IWorkerThread, IDisposable
{
    private readonly ConcurrentQueue<IWorkerThreadAction> _actionQueue;
    private readonly SemaphoreSlim _actionsAvailable;
    private readonly ManualResetEventSlim _exitSignal;
    private readonly ManualResetEventSlim _cancelSignal;
    private readonly Thread _thread;

    /// <summary>
    /// Constructor that starts the worker thread.
    /// </summary>
    /// <param name="threadName">Optionally assign a thread name to the worker thread.</param>
    /// <param name="isBackground">Set to true if you want the worker thread to be a background thread.</param>
    /// <param name="threadPriority">Allows you to specify what thread priority to use for the worker thread.</param>
    public WorkerThread(string? threadName = null, bool isBackground = false, ThreadPriority threadPriority = ThreadPriority.Normal)
    {
        _actionQueue = new ConcurrentQueue<IWorkerThreadAction>();
        _actionsAvailable = new SemaphoreSlim(0);
        _exitSignal = new ManualResetEventSlim(false);
        _cancelSignal = new ManualResetEventSlim(false);
        _thread = new Thread(ThreadMain)
        {
            Name = threadName,
            IsBackground = isBackground,
            Priority = threadPriority
        };
        _thread.Start();
    }

    /// <summary>
    /// The managed thread id of the worker thread.
    /// </summary>
    public int ThreadId => _thread.ManagedThreadId;

    /// <summary>
    /// Queues up the given <paramref name="action"/> to be executed on the thread once all previous actions have completed.
    /// </summary>
    /// <param name="action">The action to execute.</param>
    /// <returns>An awaitable task that completes once the action has finished executing or cancels if <see cref="RequestCancellationAsync"/> is called before it finishes executing. If the action throws an exception, the task will pick this up too.</returns>
    public Task InvokeAsync(Action<IWorkerThreadContext> action)
    {
        var taskCompletionSource = new TaskCompletionSource<object?>();
        _actionQueue.Enqueue(new WorkerThreadAction(action, taskCompletionSource));
        _actionsAvailable.Release(1);
        return taskCompletionSource.Task;
    }

    /// <summary>
    /// Queues up the given <paramref name="action"/> to be executed on the thread once all previous actions have completed.
    /// </summary>
    /// <param name="action">The action to execute.</param>
    /// <returns>An awaitable task that completes with the action's return value once the action has finished executing or cancels if <see cref="RequestCancellationAsync"/> is called before it finishes executing. If the action throws an exception, the task will pick this up too.</returns>
    public Task<TResult> InvokeAsync<TResult>(Func<IWorkerThreadContext, TResult> action)
    {
        var taskCompletionSource = new TaskCompletionSource<TResult>();
        _actionQueue.Enqueue(new WorkerThreadActionWithResult<TResult>(action, taskCompletionSource));
        _actionsAvailable.Release(1);
        return taskCompletionSource.Task;
    }

    /// <summary>
    /// Request any unfinished actions that have previously been queued up to be cancelled.
    /// You can use this to break out of loops (the check the <see cref="IWorkerThreadContext{TState}.WasCancellationRequested"/>) or sleeps in actions.
    /// Once you have called this, you may again invoke new actions.
    /// </summary>
    /// <returns>An awaitable task completing once all actions have been successfully cancelled.</returns>
    public Task RequestCancellationAsync()
    {
        _cancelSignal.Set();
        // the following allows new actions to be queued up.
        return InvokeAsync(_ => _cancelSignal.Reset());
    }

    /// <summary>
    /// Cancels the running action and breaks out of the thread loop before joining the thread to the current thread.
    /// </summary>
    /// <exception cref="System.InvalidOperationException">Thrown in case this is called from the thread itself, which is illegal and would wait forever.</exception>
    public void Dispose()
    {
        if (ReferenceEquals(_thread, Thread.CurrentThread))
        {
            // WARNING: This check is CRUCIAL as this join will wait indefinitely if StopThread was called from _thread. 
            throw new InvalidOperationException("Illegal call to Dispose(). The thread cannot stop itself.");
        }

        _exitSignal.Set();
        _cancelSignal.Set();
        _actionsAvailable.Release(1);
        _thread.Join();
        _actionsAvailable.Dispose();
    }

    private void ThreadMain()
    {
        while (!_exitSignal.Wait(0))
        {
            _actionsAvailable.Wait();
            if (_actionQueue.TryDequeue(out var action))
            {
                var context = new WorkerThreadContext(_cancelSignal);
                action.Execute(context);
            }
        }
    }
}
