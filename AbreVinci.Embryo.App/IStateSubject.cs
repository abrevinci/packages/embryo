﻿namespace AbreVinci.Embryo.App;

/// <summary>
/// Allows you to listen to a stream of states.
/// </summary>
/// <typeparam name="TState">The type of state.</typeparam>
public interface IStateSubject<out TState>
{
	/// <summary>
	/// An observable stream of states. When implemented by <see cref="Program{TState, TMessage, TCommand}"/>,
	/// this will be emitting the program state at each message dispatch.
	/// </summary>
	IObservable<TState> States { get; }
}
