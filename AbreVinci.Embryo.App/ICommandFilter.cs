﻿namespace AbreVinci.Embryo.App;

/// <summary>
/// Allows you to filter which commands get emitted from your program. For use with the FilteredProgram wrapper.
/// This is useful for things like feature toggles and authorization.
/// </summary>
/// <typeparam name="TCommand">The application command type.</typeparam>
public interface ICommandFilter<TCommand> where TCommand : notnull
{
    /// <summary>
    /// Filters the given command allowing you to let it pass through, skip it, or even transform it to a different command.
    /// </summary>
    /// <param name="command">The incoming command.</param>
    /// <returns>The command to be emitted, or null if no command should be emitted.</returns>
    TCommand? FilterCommand(TCommand command);
}
