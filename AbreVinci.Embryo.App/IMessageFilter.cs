﻿namespace AbreVinci.Embryo.App;

/// <summary>
/// Allows you to filter what messages get dispatched to your program. For use with the FilteredProgram wrapper.
/// This is useful for things like feature toggles and authorization, but can also be used to implement advanced
/// features such as transactions and message merging.
/// </summary>
/// <typeparam name="TMessage">The application message type.</typeparam>
public interface IMessageFilter<TMessage> where TMessage : notnull
{
    /// <summary>
    /// Filters the given message allowing you to let it pass through, skip it, or even transform it to a different message.
    /// </summary>
    /// <param name="message">The incoming message.</param>
    /// <returns>The message to be dispatched, or null if no message should be dispatched.</returns>
    TMessage? FilterMessage(TMessage message);
}
