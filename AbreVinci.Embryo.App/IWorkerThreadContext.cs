﻿namespace AbreVinci.Embryo.App;

/// <summary>
/// Context passed to actions invoked on a <see cref="WorkerThread{TThreadState}"/>.
/// </summary>
/// <typeparam name="TThreadState">The internal state type of the thread.</typeparam>
public interface IWorkerThreadContext<TThreadState>
{
    /// <summary>
    /// Access the internal state of the thread.
    /// </summary>
    TThreadState State { get; set; }

    /// <summary>
    /// Check if cancellation has been requested. You should always check this if doing long-running loops in actions.
    /// </summary>
    bool WasCancellationRequested { get; }

    /// <summary>
    /// Always call this instead of <see cref="System.Threading.Thread.Sleep(System.TimeSpan)"/>. This respects cancellation and thread stopping mechanisms.
    /// </summary>
    /// <param name="duration">The duration to sleep for (unless cancelled).</param>
    void Sleep(TimeSpan duration);
}

/// <summary>
/// Context passed to actions invoked on a stateless <see cref="WorkerThread"/>.
/// </summary>
public interface IWorkerThreadContext
{
	/// <summary>
	/// Check if cancellation has been requested. You should always check this if doing long-running loops in actions.
	/// </summary>
	bool WasCancellationRequested { get; }

	/// <summary>
	/// Always call this instead of <see cref="System.Threading.Thread.Sleep(System.TimeSpan)"/>. This respects cancellation and thread stopping mechanisms.
	/// </summary>
	/// <param name="duration">The duration to sleep for (unless cancelled).</param>
	void Sleep(TimeSpan duration);
}
