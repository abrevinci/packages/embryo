﻿namespace AbreVinci.Embryo.App;

/// <summary>
/// Represents a worker thread that accepts incoming actions and executes them as tasks on a single thread (as opposed to the normal thread pool).
/// This is useful when writing code controlling a system that needs to run on a different thread with the means of commands.
/// </summary>
/// <typeparam name="TThreadState">The internal state type of the thread, accessible by each action running on it.</typeparam>
public interface IWorkerThread<TThreadState>
{
    /// <summary>
    /// Queues up the given <paramref name="action"/> to be executed on the thread once all previous actions have completed.
    /// </summary>
    /// <param name="action">The action to execute.</param>
    /// <returns>An awaitable task that completes once the action has finished executing or cancels if <see cref="RequestCancellationAsync"/> is called before it finishes executing. If the action throws an exception, the task will pick this up too.</returns>
    Task InvokeAsync(Action<IWorkerThreadContext<TThreadState>> action);

    /// <summary>
    /// Queues up the given <paramref name="action"/> to be executed on the thread once all previous actions have completed.
    /// </summary>
    /// <param name="action">The action to execute.</param>
    /// <returns>An awaitable task that completes with the action's return value once the action has finished executing or cancels if <see cref="RequestCancellationAsync"/> is called before it finishes executing. If the action throws an exception, the task will pick this up too.</returns>
    Task<TResult> InvokeAsync<TResult>(Func<IWorkerThreadContext<TThreadState>, TResult> action);

    /// <summary>
    /// Request any unfinished actions that have previously been queued up to be cancelled.
    /// You can use this to break out of loops (the check the <see cref="IWorkerThreadContext{TState}.WasCancellationRequested"/>) or sleeps in actions.
    /// Once you have called this, you may again invoke new actions.
    /// </summary>
    /// <returns>An awaitable task completing once all actions have been successfully cancelled.</returns>
    Task RequestCancellationAsync();
}

/// <summary>
/// Represents a worker thread that accepts incoming actions and executes them as tasks on a single thread (as opposed to the normal thread pool).
/// This is useful when writing code controlling a system that needs to run on a different thread with the means of commands. If you
/// need to store state in your thread you should use the <see cref="IWorkerThread{TThreadState}"/> interface instead.
/// </summary>
public interface IWorkerThread
{
    /// <summary>
    /// Queues up the given <paramref name="action"/> to be executed on the thread once all previous actions have completed.
    /// </summary>
    /// <param name="action">The action to execute.</param>
    /// <returns>An awaitable task that completes once the action has finished executing or cancels if <see cref="RequestCancellationAsync"/> is called before it finishes executing. If the action throws an exception, the task will pick this up too.</returns>
    Task InvokeAsync(Action<IWorkerThreadContext> action);

    /// <summary>
    /// Queues up the given <paramref name="action"/> to be executed on the thread once all previous actions have completed.
    /// </summary>
    /// <param name="action">The action to execute.</param>
    /// <returns>An awaitable task that completes with the action's return value once the action has finished executing or cancels if <see cref="RequestCancellationAsync"/> is called before it finishes executing. If the action throws an exception, the task will pick this up too.</returns>
    Task<TResult> InvokeAsync<TResult>(Func<IWorkerThreadContext, TResult> action);

    /// <summary>
    /// Request any unfinished actions that have previously been queued up to be cancelled.
    /// You can use this to break out of loops (the check the <see cref="IWorkerThreadContext.WasCancellationRequested"/>) or sleeps in actions.
    /// Once you have called this, you may again invoke new actions.
    /// </summary>
    /// <returns>An awaitable task completing once all actions have been successfully cancelled.</returns>
    Task RequestCancellationAsync();
}
