﻿namespace AbreVinci.Embryo.Core.Tests.EffectContextTests;

public class Dispatch
{
    public record TestMessage(int Value);
    
    [Fact]
    public void ShouldDispatchTheGivenMessage()
    {
        var dispatchedMessages = new List<TestMessage>();
        var effectContext = new EffectContext<TestMessage>(dispatchedMessages.Add);

        effectContext.Dispatch(new TestMessage(12));

        dispatchedMessages.Should().ContainSingle().Which.Should().Be(new TestMessage(12));
    }
}
