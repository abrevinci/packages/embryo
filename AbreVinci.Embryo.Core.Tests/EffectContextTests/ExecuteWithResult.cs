﻿namespace AbreVinci.Embryo.Core.Tests.EffectContextTests;

public class ExecuteWithResult
{
	public record TestMessage(string Value);

	[Fact]
	public void ShouldExecuteTheGivenAction()
	{
		var wasExecuted = false;
		var effectContext = new EffectContext<TestMessage>(_ => { });

		effectContext.Execute(() => { wasExecuted = true; return "One"; });

		wasExecuted.Should().BeTrue();
	}

	[Fact]
	public void ShouldNotDispatchMessagesByDefault()
	{
		var messagesDispatched = false;
		var effectContext = new EffectContext<TestMessage>(_ => messagesDispatched = true);

		effectContext.Execute(() => "One");

		messagesDispatched.Should().BeFalse();
	}

	[Fact]
	public void ShouldDispatchStartMessageWhenStartMessageFactoryFunctionIsGiven()
	{
		var dispatchedMessages = new List<TestMessage>();
		var effectContext = new EffectContext<TestMessage>(dispatchedMessages.Add);

		effectContext.Execute(() => "One", createOnStartMessage: () => new TestMessage("Started"));

		dispatchedMessages.Should().ContainSingle().Which.Should().Be(new TestMessage("Started"));
	}

	[Fact]
	public void ShouldDispatchCompletionMessageWhenCompletionMessageFactoryFunctionIsGivenAndTheActionCompletes()
	{
		var dispatchedMessages = new List<TestMessage>();
		var effectContext = new EffectContext<TestMessage>(dispatchedMessages.Add);

		effectContext.Execute(() => "One", createOnCompletionMessage: result => new TestMessage(result));

		dispatchedMessages.Should().ContainSingle().Which.Should().Be(new TestMessage("One"));
	}

	[Fact]
	public void ShouldThrowWhenExceptionMessageFactoryIsNotGivenAndTheActionThrows()
	{
		var effectContext = new EffectContext<TestMessage>(_ => { });

		var f = () => effectContext.Execute<string>(() => throw new Exception("Error"));

		f.Should().Throw<Exception>().WithMessage("Error");
	}

	[Fact]
	public void ShouldDispatchExceptionMessageWhenExceptionMessageFactoryFunctionIsGivenAndTheActionThrows()
	{
		var dispatchedMessages = new List<TestMessage>();
		var effectContext = new EffectContext<TestMessage>(dispatchedMessages.Add);

		effectContext.Execute<string>(() => throw new Exception("Error"), createOnExceptionMessage: exception => new TestMessage(exception.Message));

		dispatchedMessages.Should().ContainSingle().Which.Should().Be(new TestMessage("Error"));
	}

	[Fact]
	public void ShouldNotDispatchExceptionMessageWhenExceptionMessageFactoryIsGivenButTheActionDoesNotThrow()
	{
		var dispatchedMessages = new List<TestMessage>();
		var effectContext = new EffectContext<TestMessage>(dispatchedMessages.Add);

		effectContext.Execute(() => "One", createOnExceptionMessage: exception => new TestMessage(exception.Message));

		dispatchedMessages.Should().BeEmpty();
	}
}
