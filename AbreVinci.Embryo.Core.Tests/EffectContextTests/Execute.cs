﻿namespace AbreVinci.Embryo.Core.Tests.EffectContextTests;

public class Execute
{
	public record TestMessage(string Value);
	
	[Fact]
	public void ShouldExecuteTheGivenAction()
	{
		var wasExecuted = false;
		var effectContext = new EffectContext<TestMessage>(_ => { });

		effectContext.Execute(() => { wasExecuted = true; });

		wasExecuted.Should().BeTrue();
	}

	[Fact]
	public void ShouldNotDispatchMessagesByDefault()
	{
		var messagesDispatched = false;
		var effectContext = new EffectContext<TestMessage>(_ => messagesDispatched = true);

		effectContext.Execute(() => { });

		messagesDispatched.Should().BeFalse();
	}

	[Fact]
	public void ShouldDispatchStartMessageWhenStartMessageFactoryFunctionIsGiven()
	{
		var dispatchedMessages = new List<TestMessage>();
		var effectContext = new EffectContext<TestMessage>(dispatchedMessages.Add);

		effectContext.Execute(() => { }, createOnStartMessage: () => new TestMessage("Started"));

		dispatchedMessages.Should().ContainSingle().Which.Should().Be(new TestMessage("Started"));
	}

	[Fact]
	public void ShouldDispatchCompletionMessageWhenCompletionMessageFactoryFunctionIsGivenAndTheActionCompletes()
	{
		var dispatchedMessages = new List<TestMessage>();
		var effectContext = new EffectContext<TestMessage>(dispatchedMessages.Add);

		effectContext.Execute(() => { }, createOnCompletionMessage: () => new TestMessage("Succeeded"));

		dispatchedMessages.Should().ContainSingle().Which.Should().Be(new TestMessage("Succeeded"));
	}

	[Fact]
	public void ShouldThrowWhenExceptionMessageFactoryIsNotGivenAndTheActionThrows()
	{
		var effectContext = new EffectContext<TestMessage>(_ => { });

		var f = () => effectContext.Execute(() => throw new Exception("Error"));

		f.Should().Throw<Exception>().WithMessage("Error");
	}

	[Fact]
	public void ShouldDispatchExceptionMessageWhenExceptionMessageFactoryFunctionIsGivenAndTheActionThrows()
	{
		var dispatchedMessages = new List<TestMessage>();
		var effectContext = new EffectContext<TestMessage>(dispatchedMessages.Add);

		effectContext.Execute(() => throw new Exception("Error"), createOnExceptionMessage: exception => new TestMessage(exception.Message));

		dispatchedMessages.Should().ContainSingle().Which.Should().Be(new TestMessage("Error"));
	}

	[Fact]
	public void ShouldNotDispatchExceptionMessageWhenExceptionMessageFactoryIsGivenButTheActionDoesNotThrow()
	{
		var dispatchedMessages = new List<TestMessage>();
		var effectContext = new EffectContext<TestMessage>(dispatchedMessages.Add);

		effectContext.Execute(() => { }, createOnExceptionMessage: exception => new TestMessage(exception.Message));

		dispatchedMessages.Should().BeEmpty();
	}
}
