﻿namespace AbreVinci.Embryo.Core.Tests.EffectContextTests;

public class ExecuteAsyncWithResult
{
	public record TestMessage(string Value);
	
	[Fact]
	public async Task ShouldExecuteTheGivenAction()
	{
		var wasExecuted = false;
		var effectContext = new EffectContext<TestMessage>(_ => { });

		await effectContext.ExecuteAsync(() => { wasExecuted = true; return Task.CompletedTask; });

		wasExecuted.Should().BeTrue();
	}

	[Fact]
	public async Task ShouldNotDispatchMessagesByDefault()
	{
		var messagesDispatched = false;
		var effectContext = new EffectContext<TestMessage>(_ => messagesDispatched = true);

		await effectContext.ExecuteAsync(() => Task.FromResult("One"));

		messagesDispatched.Should().BeFalse();
	}

	[Fact]
	public async Task ShouldDispatchStartMessageWhenStartMessageFactoryFunctionIsGiven()
	{
		var dispatchedMessages = new List<TestMessage>();
		var effectContext = new EffectContext<TestMessage>(dispatchedMessages.Add);

		await effectContext.ExecuteAsync(() => Task.FromResult("One"), createOnStartMessage: () => new TestMessage("Started"));

		dispatchedMessages.Should().ContainSingle().Which.Should().Be(new TestMessage("Started"));
	}

	[Fact]
	public async Task ShouldDispatchCompletionMessageWhenCompletionMessageFactoryFunctionIsGivenAndTheActionCompletes()
	{
		var dispatchedMessages = new List<TestMessage>();
		var effectContext = new EffectContext<TestMessage>(dispatchedMessages.Add);

		await effectContext.ExecuteAsync(() => Task.FromResult("One"), createOnCompletionMessage: result => new TestMessage(result));

		dispatchedMessages.Should().ContainSingle().Which.Should().Be(new TestMessage("One"));
	}

	[Fact]
	public async Task ShouldThrowWhenExceptionMessageFactoryIsNotGivenAndTheActionThrows()
	{
		var effectContext = new EffectContext<TestMessage>(_ => { });

		var f = async () => await effectContext.ExecuteAsync<string>(() => throw new Exception("Error"));

		await f.Should().ThrowAsync<Exception>().WithMessage("Error");
	}

	[Fact]
	public async Task ShouldDispatchExceptionMessageWhenExceptionMessageFactoryFunctionIsGivenAndTheActionThrows()
	{
		var dispatchedMessages = new List<TestMessage>();
		var effectContext = new EffectContext<TestMessage>(dispatchedMessages.Add);

		await effectContext.ExecuteAsync<string>(() => throw new Exception("Error"), createOnExceptionMessage: exception => new TestMessage(exception.Message));

		dispatchedMessages.Should().ContainSingle().Which.Should().Be(new TestMessage("Error"));
	}

	[Fact]
	public async Task ShouldNotDispatchExceptionMessageWhenExceptionMessageFactoryIsGivenButTheActionDoesNotThrow()
	{
		var dispatchedMessages = new List<TestMessage>();
		var effectContext = new EffectContext<TestMessage>(dispatchedMessages.Add);

		await effectContext.ExecuteAsync(() => Task.FromResult("One"), createOnExceptionMessage: exception => new TestMessage(exception.Message));

		dispatchedMessages.Should().BeEmpty();
	}

	[Fact]
	public async Task ShouldDispatchCancelledMessageWhenCancelledMessageFactoryFunctionIsGivenWhenTheActionIsCancelled()
	{
		var dispatchedMessages = new List<TestMessage>();
		var effectContext = new EffectContext<TestMessage>(dispatchedMessages.Add);

		var cancellationTokenSource = new CancellationTokenSource();
		cancellationTokenSource.Cancel();
		await effectContext.ExecuteAsync(() => Task.FromCanceled<string>(cancellationTokenSource.Token), createOnCancelledMessage: () => new TestMessage("Cancelled"));

		dispatchedMessages.Should().ContainSingle().Which.Should().Be(new TestMessage("Cancelled"));
	}

	[Fact]
	public async Task ShouldNotDispatchCancelledMessageWhenCancelledMessageFactoryFunctionIsGivenButTheActionIsNotCancelled()
	{
		var dispatchedMessages = new List<TestMessage>();
		var effectContext = new EffectContext<TestMessage>(dispatchedMessages.Add);

		await effectContext.ExecuteAsync(() => Task.FromResult("One"), createOnCancelledMessage: () => new TestMessage("Cancelled"));

		dispatchedMessages.Should().BeEmpty();
	}
}
