﻿namespace AbreVinci.Embryo.Core.Tests.ActionContextTests;

public class WithState
{
	private record TestState(int Value);
	private record TestCommand;

	[Fact]
	public void ShouldReturnANewContextWithTheGivenState()
	{
		var context = new ActionContext<TestState, TestCommand>(new TestState(1));

		var newContext = context.WithState(new TestState(2));

		newContext.State.Value.Should().Be(2);
		context.State.Value.Should().Be(1);
	}
}
