﻿namespace AbreVinci.Embryo.Core.Tests.ActionContextTests;

public class WithCommand
{
	private record TestState;
	private record TestCommand(int Value);

	[Fact]
	public void ShouldReturnANewContextWithTheGivenCommand()
	{
		var context = new ActionContext<TestState, TestCommand>(new TestState());

		var newContext = context.WithCommand(new TestCommand(1));

		newContext.Commands.Should().ContainSingle().Which.Value.Should().Be(1);
		context.Commands.Should().BeEmpty();
	}
}
