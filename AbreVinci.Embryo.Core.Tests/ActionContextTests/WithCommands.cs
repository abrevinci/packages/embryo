﻿namespace AbreVinci.Embryo.Core.Tests.ActionContextTests;

public class WithCommands
{
	private record TestState;
	private record TestCommand(int Value);

	[Fact]
	public void ShouldReturnANewContextWithTheGivenCommands()
	{
		var context = new ActionContext<TestState, TestCommand>(new TestState());

		var newContext = context.WithCommands(new TestCommand(1), new TestCommand(2));

		newContext.Commands.Should().HaveCount(2);
		newContext.Commands[0].Value.Should().Be(1);
		newContext.Commands[1].Value.Should().Be(2);
		context.Commands.Should().BeEmpty();
	}
}
