﻿namespace AbreVinci.Embryo.Core.Tests.ActionContextTests;

public class ClearCommands
{
	private record TestState;
	private record TestCommand(int Value);

	[Fact]
	public void ShouldReturnANewContextWithTheAllCommandsRemoved()
	{
		var context = new ActionContext<TestState, TestCommand>(new TestState()).WithCommand(new TestCommand(1));

		var newContext = context.ClearCommands();

		context.Commands.Should().ContainSingle().Which.Value.Should().Be(1);
		newContext.Commands.Should().BeEmpty();
	}
}