﻿namespace AbreVinci.Embryo.Core.Tests.ObservableExtensionsTests;

public class SelectNotNull
{
	public class ValueTypeOverride
	{
		private record TestState(int? Value);

		[Fact]
		public void ShouldEmitValueThatIsNotNull()
		{
			var state = new Subject<TestState>();
			var value = state.SelectNotNull(s => s.Value);
			var emittedValues = new List<int>();
			value.Subscribe(emittedValues.Add);

			state.OnNext(new TestState(11));

			emittedValues.Should().BeEquivalentTo(new[] { 11 });
		}

		[Fact]
		public void ShouldNotEmitValueThatIsNull()
		{
			var state = new Subject<TestState>();
			var value = state.SelectNotNull(s => s.Value);
			var emittedValues = new List<int>();
			value.Subscribe(emittedValues.Add);

			state.OnNext(new TestState(null));

			emittedValues.Should().BeEmpty();
		}
	}

	public class ReferenceTypeOverride
	{
		private record TestState(string? Value);

		[Fact]
		public void ShouldEmitValueThatIsNotNull()
		{
			var state = new Subject<TestState>();
			var value = state.SelectNotNull(s => s.Value);
			var emittedValues = new List<string>();
			value.Subscribe(emittedValues.Add);

			state.OnNext(new TestState("text"));

			emittedValues.Should().BeEquivalentTo("text");
		}

		[Fact]
		public void ShouldNotEmitValueThatIsNull()
		{
			var state = new Subject<TestState>();
			var value = state.SelectNotNull(s => s.Value);
			var emittedValues = new List<string>();
			value.Subscribe(emittedValues.Add);

			state.OnNext(new TestState(null));

			emittedValues.Should().BeEmpty();
		}
	}
}
