﻿namespace AbreVinci.Embryo.Core.Tests.ObservableExtensionsTests;

public class SelectNotNullDistinct
{
	public class ValueTypeOverride
	{
		// ReSharper disable once NotAccessedPositionalProperty.Local
		private record TestState(int? Value, bool DontCare);

		[Fact]
		public void ShouldEmitNonNullTransformedValueIfFirstValue()
		{
			var state = new Subject<TestState>();
			var value = state.SelectNotNullDistinct(s => s.Value);
			var emittedValues = new List<int>();
			value.Subscribe(emittedValues.Add);

			state.OnNext(new TestState(11, false));

			emittedValues.Should().BeEquivalentTo(new [] { 11 });
		}

		[Fact]
		public void ShouldNotEmitNonNullTransformedValueIfItIsSameAsPreviousTransformedValue()
		{
			var state = new Subject<TestState>();
			var value = state.SelectNotNullDistinct(s => s.Value);
			var emittedValues = new List<int>();
			value.Subscribe(emittedValues.Add);
			state.OnNext(new TestState(11, true));

			state.OnNext(new TestState(11, false));

			emittedValues.Should().BeEquivalentTo(new[] { 11 });
		}

		[Fact]
		public void ShouldEmitNonNullTransformedValueIfItIsDifferentThanPreviousTransformedValue()
		{
			var state = new Subject<TestState>();
			var value = state.SelectNotNullDistinct(s => s.Value);
			var emittedValues = new List<int>();
			value.Subscribe(emittedValues.Add);
			state.OnNext(new TestState(11, true));

			state.OnNext(new TestState(12, true));

			emittedValues.Should().BeEquivalentTo(new[] { 11, 12 });
		}

		[Fact]
		public void ShouldNotEmitNullTransformedValueEvenIfFirstValue()
		{
			var state = new Subject<TestState>();
			var value = state.SelectNotNullDistinct(s => s.Value);
			var emittedValues = new List<int>();
			value.Subscribe(emittedValues.Add);

			state.OnNext(new TestState(null, false));

			emittedValues.Should().BeEquivalentTo(new string?[] { });
		}

		[Fact]
		public void ShouldNotEmitNullTransformedValueIfItIsSameAsPreviousTransformedValue()
		{
			var state = new Subject<TestState>();
			var value = state.SelectNotNullDistinct(s => s.Value);
			var emittedValues = new List<int>();
			value.Subscribe(emittedValues.Add);
			state.OnNext(new TestState(null, true));

			state.OnNext(new TestState(null, false));

			emittedValues.Should().BeEquivalentTo(new int?[] { });
		}
	}

	public class ReferenceTypeOverride
	{
		// ReSharper disable once NotAccessedPositionalProperty.Local
		private record TestState(string? Value, bool DontCare);

		[Fact]
		public void ShouldEmitNonNullTransformedValueIfFirstValue()
		{
			var state = new Subject<TestState>();
			var value = state.SelectNotNullDistinct(s => s.Value);
			var emittedValues = new List<string>();
			value.Subscribe(emittedValues.Add);

			state.OnNext(new TestState("text", false));

			emittedValues.Should().BeEquivalentTo(new [] { "text" });
		}

		[Fact]
		public void ShouldNotEmitNonNullTransformedValueIfItIsSameAsPreviousTransformedValue()
		{
			var state = new Subject<TestState>();
			var value = state.SelectNotNullDistinct(s => s.Value);
			var emittedValues = new List<string>();
			value.Subscribe(emittedValues.Add);
			state.OnNext(new TestState("text", true));

			state.OnNext(new TestState("text", false));

			emittedValues.Should().BeEquivalentTo(new[] { "text" });
		}

		[Fact]
		public void ShouldEmitNonNullTransformedValueIfItIsDifferentThanPreviousTransformedValue()
		{
			var state = new Subject<TestState>();
			var value = state.SelectNotNullDistinct(s => s.Value);
			var emittedValues = new List<string>();
			value.Subscribe(emittedValues.Add);
			state.OnNext(new TestState("text", true));

			state.OnNext(new TestState("text2", true));

			emittedValues.Should().BeEquivalentTo(new[] { "text", "text2" });
		}

		[Fact]
		public void ShouldNotEmitNullTransformedValueEvenIfFirstValue()
		{
			var state = new Subject<TestState>();
			var value = state.SelectNotNullDistinct(s => s.Value);
			var emittedValues = new List<string>();
			value.Subscribe(emittedValues.Add);

			state.OnNext(new TestState(null, false));

			emittedValues.Should().BeEquivalentTo(new string?[] { });
		}

		[Fact]
		public void ShouldNotEmitNullTransformedValueIfItIsSameAsPreviousTransformedValue()
		{
			var state = new Subject<TestState>();
			var value = state.SelectNotNullDistinct(s => s.Value);
			var emittedValues = new List<string>();
			value.Subscribe(emittedValues.Add);
			state.OnNext(new TestState(null, true));

			state.OnNext(new TestState(null, false));

			emittedValues.Should().BeEquivalentTo(new string?[] { });
		}
	}
}
