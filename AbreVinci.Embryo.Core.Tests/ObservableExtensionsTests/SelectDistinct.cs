﻿namespace AbreVinci.Embryo.Core.Tests.ObservableExtensionsTests;

public class SelectDistinct
{
	// ReSharper disable once NotAccessedPositionalProperty.Local
	private record TestState(int Value, bool DontCare);

	[Fact]
	public void ShouldEmitTransformedValueIfFirstValue()
	{
		var state = new Subject<TestState>();
		var value = state.SelectDistinct(s => s.Value);
		var emittedValues = new List<int>();
		value.Subscribe(emittedValues.Add);

		state.OnNext(new TestState(11, false));

		emittedValues.Should().BeEquivalentTo(new [] { 11 });
	}

	[Fact]
	public void ShouldNotEmitTransformedValueIfItIsSameAsPreviousTransformedValue()
	{
		var state = new Subject<TestState>();
		var value = state.SelectDistinct(s => s.Value);
		var emittedValues = new List<int>();
		value.Subscribe(emittedValues.Add);
		state.OnNext(new TestState(11, true));

		state.OnNext(new TestState(11, false));

		emittedValues.Should().BeEquivalentTo(new[] { 11 });
	}

	[Fact]
	public void ShouldEmitTransformedValueIfItIsDifferentThanPreviousTransformedValue()
	{
		var state = new Subject<TestState>();
		var value = state.SelectDistinct(s => s.Value);
		var emittedValues = new List<int>();
		value.Subscribe(emittedValues.Add);
		state.OnNext(new TestState(11, true));

		state.OnNext(new TestState(12, true));

		emittedValues.Should().BeEquivalentTo(new[] { 11, 12 });
	}
}
