﻿using System.Linq;

namespace AbreVinci.Embryo.Core.Tests.ObservableExtensionsTests;

public class SubscribeAsyncConcurrent
{
	[Fact]
	public async Task ShouldNotExecuteTasksIfNoValuesAreEmitted()
	{
		var results = new List<int>();
		var task = async (int v) =>
		{
			results.Add(v);
			await Task.Delay(v);
			results.Add(v);
		};

		var observable = new Subject<int>();
		using var subscription = observable.SubscribeAsyncConcurrent(task);

		await Task.Delay(100);

		results.Should().BeEmpty();
	}

	[Fact]
	public async Task ShouldExecuteTasksInParallel()
	{
		var results = new List<int>();
		var task = async (int v) =>
		{
			results.Add(v);
			await Task.Delay(v);
			results.Add(v);
		};

		var observable = new Subject<int>();
		using var subscription = observable.SubscribeAsyncConcurrent(task);
		observable.OnNext(100);
		observable.OnNext(99);

		while (results.Count < 4)
			await Task.Delay(10);

		results.Take(2).Should().BeEquivalentTo(new[] { 100, 99 }, options => options.WithoutStrictOrdering());
		results.Skip(2).Take(2).Should().BeEquivalentTo(new[] { 100, 99 }, options => options.WithoutStrictOrdering());
	}
}
