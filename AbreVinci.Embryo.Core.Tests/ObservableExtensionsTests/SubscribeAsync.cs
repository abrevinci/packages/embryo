﻿namespace AbreVinci.Embryo.Core.Tests.ObservableExtensionsTests;

public class SubscribeAsync
{
	[Fact]
	public async Task ShouldNotExecuteTasksIfNoValuesAreEmitted()
	{
		var results = new List<int>();
		var task = (int v) =>
		{
			results.Add(v);
			return Task.CompletedTask;
		};

		var observable = new Subject<int>();
		using var subscription = observable.SubscribeAsync(task);

		await Task.Delay(100);

		results.Should().BeEmpty();
	}

	[Fact]
	public async Task ShouldExecuteTasksInOrder()
	{
		var results = new List<int>();
		var task = (int v) =>
		{
			results.Add(v);
			return Task.CompletedTask;
		};

		var observable = new Subject<int>();
		using var subscription = observable.SubscribeAsync(task);
		observable.OnNext(100);
		observable.OnNext(99);
		observable.OnNext(50);

		while (results.Count < 3)
			await Task.Delay(10);

		results.Should().BeEquivalentTo(new[] { 100, 99, 50 }, options => options.WithStrictOrdering());
	}
}
