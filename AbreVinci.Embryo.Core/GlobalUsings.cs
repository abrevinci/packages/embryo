﻿global using System;
global using System.Collections.Generic;
global using System.Collections.Immutable;
global using System.Reactive.Linq;
global using System.Threading.Tasks;

// ReSharper disable once CheckNamespace
namespace System.Runtime.CompilerServices
{
	internal static class IsExternalInit { }
}
