﻿namespace AbreVinci.Embryo.Core;

/// <summary>
/// Useful extensions when selecting state.
/// </summary>
public static class ObservableExtensions
{
	/// <summary>
	/// Shorthand for Select(select).DistinctUntilChanged().
	/// This will emit a transformed value only when it is different from the last emitted transformed value.
	/// </summary>
	/// <typeparam name="TOriginal">The value type of the observable stream.</typeparam>
	/// <typeparam name="TTransformed">The transformed value returned from select.</typeparam>
	/// <param name="observable">The observable stream to transform.</param>
	/// <param name="select">The select function to apply.</param>
	/// <returns>A transformed observable.</returns>
	public static IObservable<TTransformed> SelectDistinct<TOriginal, TTransformed>(
		this IObservable<TOriginal> observable, 
		Func<TOriginal, TTransformed> select)
	{
		return observable.Select(select).DistinctUntilChanged();
	}

	/// <summary>
	/// Shorthand for Select(select).Where(v => v.HasValue).Select(v => v!.Value).
	/// This will emit a transformed nullable value only when it is not null.
	/// </summary>
	/// <typeparam name="TOriginal">The value type of the observable stream.</typeparam>
	/// <typeparam name="TTransformed">The transformed nullable value returned from select.</typeparam>
	/// <param name="observable">The observable stream to transform.</param>
	/// <param name="select">The select function to apply.</param>
	/// <returns>A transformed observable.</returns>
	public static IObservable<TTransformed> SelectNotNull<TOriginal, TTransformed>(
		this IObservable<TOriginal> observable, 
		Func<TOriginal, TTransformed?> select) where TTransformed : struct
	{
		return observable.Select(select).Where(v => v.HasValue).Select(v => v!.Value);
	}

	/// <summary>
	/// Shorthand for Select(select).Where(v => v.HasValue).Select(v => v!.Value).
	/// This will emit a transformed nullable value only when it is not null.
	/// </summary>
	/// <typeparam name="TOriginal">The value type of the observable stream.</typeparam>
	/// <typeparam name="TTransformed">The transformed nullable value returned from select.</typeparam>
	/// <param name="observable">The observable stream to transform.</param>
	/// <param name="select">The select function to apply.</param>
	/// <returns>A transformed observable.</returns>
	public static IObservable<TTransformed> SelectNotNull<TOriginal, TTransformed>(
		this IObservable<TOriginal> observable, 
		Func<TOriginal, TTransformed?> select) where TTransformed : notnull
	{
		return observable.Select(select).Where(v => v != null).Select(v => v!);
	}

	/// <summary>
	/// Shorthand for Select(select).Where(v => v.HasValue).Select(v => v!.Value).DistinctUntilChanged().
	/// This will emit a transformed nullable value only when it is not null and emit only when
	/// it is different from the last emitted transformed value.
	/// </summary>
	/// <typeparam name="TOriginal">The value type of the observable stream.</typeparam>
	/// <typeparam name="TTransformed">The transformed nullable value returned from select.</typeparam>
	/// <param name="observable">The observable stream to transform.</param>
	/// <param name="select">The select function to apply.</param>
	/// <returns>A transformed observable.</returns>
	public static IObservable<TTransformed> SelectNotNullDistinct<TOriginal, TTransformed>(
		this IObservable<TOriginal> observable, 
		Func<TOriginal, TTransformed?> select) where TTransformed : struct
	{
		return observable.Select(select).Where(v => v.HasValue).Select(v => v!.Value).DistinctUntilChanged();
	}

	/// <summary>
	/// Shorthand for Select(select).Where(v => v.HasValue).Select(v => v!.Value).DistinctUntilChanged().
	/// This will emit a transformed nullable value only when it is not null and emit only when
	/// it is different from the last emitted transformed value.
	/// </summary>
	/// <typeparam name="TOriginal">The value type of the observable stream.</typeparam>
	/// <typeparam name="TTransformed">The transformed nullable value returned from select.</typeparam>
	/// <param name="observable">The observable stream to transform.</param>
	/// <param name="select">The select function to apply.</param>
	/// <returns>A transformed observable.</returns>
	public static IObservable<TTransformed> SelectNotNullDistinct<TOriginal, TTransformed>(
		this IObservable<TOriginal> observable, 
		Func<TOriginal, TTransformed?> select) where TTransformed : notnull
	{
		return observable.Select(select).Where(v => v != null).Select(v => v!).DistinctUntilChanged();
	}
	
	/// <summary>
	/// This will provide an asynchronous subscription using a select and concat combination. Actions will be executed in sequence.
	/// </summary>
	/// <typeparam name="T">The value type of the observable stream.</typeparam>
	/// <param name="observable">The observable stream to subscribe to.</param>
	/// <param name="onNextAsync">The asynchronous task to execute sequentially upon new emits.</param>
	/// <returns>A disposable object representing the subscription.</returns>
	public static IDisposable SubscribeAsync<T>(this IObservable<T> observable, Func<T, Task> onNextAsync)
	{
		return observable.Select(v => Observable.Defer(() => Observable.StartAsync(() => onNextAsync(v)))).Concat().Subscribe();
	}

	/// <summary>
	/// This will provide an asynchronous subscription using a select and merge combination. Actions will be executed in parallel.
	/// </summary>
	/// <typeparam name="T">The value type of the observable stream.</typeparam>
	/// <param name="observable">The observable stream to subscribe to.</param>
	/// <param name="onNextAsync">The asynchronous task to execute in parallel upon new emits.</param>
	/// <returns>A disposable object representing the subscription.</returns>
	public static IDisposable SubscribeAsyncConcurrent<T>(this IObservable<T> observable, Func<T, Task> onNextAsync)
	{
		return observable.Select(v => Observable.Defer(() => Observable.StartAsync(() => onNextAsync(v)))).Merge().Subscribe();
	}

	/// <summary>
	/// This will provide an asynchronous subscription using a select and merge combination. Actions will be executed in parallel.
	/// </summary>
	/// <typeparam name="T">The value type of the observable stream.</typeparam>
	/// <param name="observable">The observable stream to subscribe to.</param>
	/// <param name="onNextAsync">The asynchronous task to execute in parallel upon new emits.</param>
	/// <param name="maxConcurrent">The maximum number of concurrent operations.</param>
	/// <returns>A disposable object representing the subscription.</returns>
	public static IDisposable SubscribeAsyncConcurrent<T>(this IObservable<T> observable, Func<T, Task> onNextAsync, int maxConcurrent)
	{
		return observable.Select(v => Observable.Defer(() => Observable.StartAsync(() => onNextAsync(v)))).Merge(maxConcurrent).Subscribe();
	}
}
