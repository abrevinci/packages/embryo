﻿namespace AbreVinci.Embryo.Core;

/// <summary>
/// Utility construct for actions to use instead of the raw state and the state and command list pair so they are chainable and easier to work with.
/// </summary>
/// <typeparam name="TState">The program state type.</typeparam>
/// <typeparam name="TCommand">The program command type.</typeparam>
/// <param name="State">The state of this context.</param>
/// <param name="Commands">The commands in this context.</param>
public record ActionContext<TState, TCommand>(TState State, IImmutableList<TCommand> Commands)
{
	/// <summary>
	/// Constructs a new action context based on a state.
	/// </summary>
	/// <param name="state">The state to use.</param>
	public ActionContext(TState state) : this(state, ImmutableArray<TCommand>.Empty) { }

	/// <summary>
	/// Creates a new action context with the given state replacing the current state.
	/// </summary>
	/// <param name="state">The new state.</param>
	/// <returns>The updated context.</returns>
	/// <example>
	///	[!code-csharp[Example](../../DocSnippets/Core/ActionContext/WithState/Actions.cs#Snippet)]
	/// </example>
	public ActionContext<TState, TCommand> WithState(TState state) => this with { State = state };

	/// <summary>
	/// Creates a new action context with the given command added to the current commands.
	/// </summary>
	/// <param name="command">The command to add.</param>
	/// <returns>The updated context.</returns>
	/// <example>
	///	[!code-csharp[Example](../../DocSnippets/Core/ActionContext/WithCommand/Actions.cs#Snippet)]
	/// </example>
	public ActionContext<TState, TCommand> WithCommand(TCommand command) => this with { Commands = Commands.Add(command) };

	/// <summary>
	/// Creates a new action context with the given commands added to the current commands.
	/// </summary>
	/// <param name="commands">The commands to add.</param>
	/// <returns>The updated context.</returns>
	/// <example>
	///	[!code-csharp[Example](../../DocSnippets/Core/ActionContext/WithCommands/Actions.cs#Snippet)]
	/// </example>
	public ActionContext<TState, TCommand> WithCommands(params TCommand[] commands) => this with { Commands = Commands.AddRange(commands) };

	/// <summary>
	/// Creates a new action context with the given commands added to the current commands.
	/// </summary>
	/// <param name="commands">The commands to add.</param>
	/// <returns>The updated context.</returns>
	public ActionContext<TState, TCommand> WithCommands(IEnumerable<TCommand> commands) => this with { Commands = Commands.AddRange(commands) };

	/// <summary>
	/// Creates a new action context with all commands removed.
	/// </summary>
	/// <returns>The updated context.</returns>
	public ActionContext<TState, TCommand> ClearCommands() => this with { Commands = Commands.Clear() };
}
