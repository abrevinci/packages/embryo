﻿namespace AbreVinci.Embryo.Core;

/// <summary>
/// Utility construct for effects to use instead of the raw service and dispatch.
/// </summary>
/// <typeparam name="TService">The service type.</typeparam>
/// <typeparam name="TMessage">The program message type.</typeparam>
public class EffectContext<TService, TMessage>
{
	private readonly EffectContext<TMessage> _context;

	/// <summary>
	/// Constructor.
	/// </summary>
	/// <param name="service">The service instance to supply.</param>
	/// <param name="dispatch">The dispatch method to use.</param>
	public EffectContext(TService service, Action<TMessage> dispatch)
	{
		Service = service;
		_context = new EffectContext<TMessage>(dispatch);
	}

	/// <summary>
	/// The supplied service.
	/// </summary>
	public TService Service { get; }

	/// <summary>
	/// Dispatch the given message.
	/// </summary>
	/// <param name="message">The message to dispatch.</param>
	/// <example>
	///	[!code-csharp[Example](../../DocSnippets/Core/EffectContext/Dispatch/PersistanceEffects.cs#Snippet)]
	/// </example>
	public void Dispatch(TMessage message) => _context.Dispatch(message);

	/// <summary>
	/// Execute the given action with optional message responses.
	/// </summary>
	/// <typeparam name="TResult">The action result type.</typeparam>
	/// <param name="executeAsync">The action to execute.</param>
	/// <param name="createOnStartMessage">Optionally construct a message to dispatch right before the action starts.</param>
	/// <param name="createOnCompletionMessage">Optionally construct a message to dispatch if the action does not throw an exception.</param>
	/// <param name="createOnExceptionMessage">Optionally construct a message to dispatch if the action throws an exception. If not specified, exceptions will not be caught.</param>
	/// <param name="createOnCancelledMessage">Optionally construct a message to dispatch if the action was cancelled.</param>
	/// <returns>A task for the execution.</returns>
	/// <example>
	///	[!code-csharp[Example](../../DocSnippets/Core/EffectContext/ExecuteAsyncWithResult/PersistanceEffects.cs#Snippet)]
	/// </example>
	public Task ExecuteAsync<TResult>(
		Func<TService, Task<TResult>> executeAsync,
		Func<TMessage>? createOnStartMessage = null,
		Func<TResult, TMessage>? createOnCompletionMessage = null,
		Func<Exception, TMessage>? createOnExceptionMessage = null,
		Func<TMessage>? createOnCancelledMessage = null) =>
		_context.ExecuteAsync(
			() => executeAsync(Service),
			createOnStartMessage,
			createOnCompletionMessage,
			createOnExceptionMessage,
			createOnCancelledMessage);

	/// <summary>
	/// Execute the given action with optional message responses.
	/// </summary>
	/// <param name="executeAsync">The action to execute.</param>
	/// <param name="createOnStartMessage">Optionally construct a message to dispatch right before the action starts.</param>
	/// <param name="createOnCompletionMessage">Optionally construct a message to dispatch if the action does not throw an exception.</param>
	/// <param name="createOnExceptionMessage">Optionally construct a message to dispatch if the action throws an exception. If not specified, exceptions will not be caught.</param>
	/// <param name="createOnCancelledMessage">Optionally construct a message to dispatch if the action was cancelled.</param>
	/// <returns>A task for the execution.</returns>
	/// <example>
	///	[!code-csharp[Example](../../DocSnippets/Core/EffectContext/ExecuteAsync/PersistanceEffects.cs#Snippet)]
	/// </example>
	public Task ExecuteAsync(
		Func<TService, Task> executeAsync,
		Func<TMessage>? createOnStartMessage = null,
		Func<TMessage>? createOnCompletionMessage = null,
		Func<Exception, TMessage>? createOnExceptionMessage = null,
		Func<TMessage>? createOnCancelledMessage = null) =>
		_context.ExecuteAsync(
			() => executeAsync(Service),
			createOnStartMessage,
			createOnCompletionMessage,
			createOnExceptionMessage,
			createOnCancelledMessage);

	/// <summary>
	/// Execute the given action with optional message responses.
	/// </summary>
	/// <typeparam name="TResult">The action result type.</typeparam>
	/// <param name="execute">The action to execute.</param>
	/// <param name="createOnStartMessage">Optionally construct a message to dispatch right before the action starts.</param>
	/// <param name="createOnCompletionMessage">Optionally construct a message to dispatch if the action does not throw an exception.</param>
	/// <param name="createOnExceptionMessage">Optionally construct a message to dispatch if the action throws an exception. If not specified, exceptions will not be caught.</param>
	/// <example>
	///	[!code-csharp[Example](../../DocSnippets/Core/EffectContext/ExecuteWithResult/GameEffects.cs#Snippet)]
	/// </example>
	public void Execute<TResult>(
		Func<TService, TResult> execute,
		Func<TMessage>? createOnStartMessage = null,
		Func<TResult, TMessage>? createOnCompletionMessage = null,
		Func<Exception, TMessage>? createOnExceptionMessage = null) =>
		_context.Execute(
			() => execute(Service),
			createOnStartMessage,
			createOnCompletionMessage,
			createOnExceptionMessage);

	/// <summary>
	/// Execute the given action with optional message responses.
	/// </summary>
	/// <param name="execute">The action to execute.</param>
	/// <param name="createOnStartMessage">Optionally construct a message to dispatch right before the action starts.</param>
	/// <param name="createOnCompletionMessage">Optionally construct a message to dispatch if the action does not throw an exception.</param>
	/// <param name="createOnExceptionMessage">Optionally construct a message to dispatch if the action throws an exception. If not specified, exceptions will not be caught.</param>
	/// <example>
	///	[!code-csharp[Example](../../DocSnippets/Core/EffectContext/Execute/NotificationEffects.cs#Snippet)]
	/// </example>
	public void Execute(
		Action<TService> execute,
		Func<TMessage>? createOnStartMessage = null,
		Func<TMessage>? createOnCompletionMessage = null,
		Func<Exception, TMessage>? createOnExceptionMessage = null) =>
		_context.Execute(
			() => execute(Service),
			createOnStartMessage,
			createOnCompletionMessage,
			createOnExceptionMessage);
}

/// <summary>
/// Utility construct for effects to use instead of the raw dispatch.
/// </summary>
/// <typeparam name="TMessage">The program message type.</typeparam>
public class EffectContext<TMessage>
{
	private readonly Action<TMessage> _dispatch;

	/// <summary>
	/// Constructor.
	/// </summary>
	/// <param name="dispatch">The dispatch method to use.</param>
	public EffectContext(Action<TMessage> dispatch)
	{
		_dispatch = dispatch;
	}

	/// <summary>
	/// Dispatch the given message.
	/// </summary>
	/// <param name="message">The message to dispatch.</param>
	/// <example>
	///	[!code-csharp[Example](../../DocSnippets/Core/EffectContext/Dispatch/Effects.cs#Snippet)]
	/// </example>
	public void Dispatch(TMessage message)
	{
		_dispatch(message);
	}

	/// <summary>
	/// Execute the given action with optional message responses.
	/// </summary>
	/// <typeparam name="TResult">The action result type.</typeparam>
	/// <param name="executeAsync">The action to execute.</param>
	/// <param name="createOnStartMessage">Optionally construct a message to dispatch right before the action starts.</param>
	/// <param name="createOnCompletionMessage">Optionally construct a message to dispatch if the action does not throw an exception.</param>
	/// <param name="createOnExceptionMessage">Optionally construct a message to dispatch if the action throws an exception. If not specified, exceptions will not be caught.</param>
	/// <param name="createOnCancelledMessage">Optionally construct a message to dispatch if the action was cancelled.</param>
	/// <returns>A task for the execution.</returns>
	/// <example>
	///	[!code-csharp[Example](../../DocSnippets/Core/EffectContext/ExecuteAsyncWithResult/FileEffects.cs#Snippet)]
	/// </example>
	public async Task ExecuteAsync<TResult>(
		Func<Task<TResult>> executeAsync,
		Func<TMessage>? createOnStartMessage = null,
		Func<TResult, TMessage>? createOnCompletionMessage = null,
		Func<Exception, TMessage>? createOnExceptionMessage = null,
		Func<TMessage>? createOnCancelledMessage = null)
	{
		try
		{
			if (createOnStartMessage != null)
			{
				_dispatch(createOnStartMessage());
			}

			var result = await executeAsync();

			if (createOnCompletionMessage != null)
			{
				_dispatch(createOnCompletionMessage(result));
			}
		}
		catch (TaskCanceledException)
		{
			if (createOnCancelledMessage != null)
			{
				_dispatch(createOnCancelledMessage());
			}
		}
		catch (Exception exception)
		{
			if (createOnExceptionMessage != null)
			{
				_dispatch(createOnExceptionMessage(exception));
			}
			else
			{
				throw;
			}
		}
	}

	/// <summary>
	/// Execute the given action with optional message responses.
	/// </summary>
	/// <param name="executeAsync">The action to execute.</param>
	/// <param name="createOnStartMessage">Optionally construct a message to dispatch right before the action starts.</param>
	/// <param name="createOnCompletionMessage">Optionally construct a message to dispatch if the action does not throw an exception.</param>
	/// <param name="createOnExceptionMessage">Optionally construct a message to dispatch if the action throws an exception. If not specified, exceptions will not be caught.</param>
	/// <param name="createOnCancelledMessage">Optionally construct a message to dispatch if the action was cancelled.</param>
	/// <returns>A task for the execution.</returns>
	/// <example>
	///	[!code-csharp[Example](../../DocSnippets/Core/EffectContext/ExecuteAsync/FileEffects.cs#Snippet)]
	/// </example>
	public async Task ExecuteAsync(
		Func<Task> executeAsync,
		Func<TMessage>? createOnStartMessage = null,
		Func<TMessage>? createOnCompletionMessage = null,
		Func<Exception, TMessage>? createOnExceptionMessage = null,
		Func<TMessage>? createOnCancelledMessage = null)
	{
		try
		{
			if (createOnStartMessage != null)
			{
				_dispatch(createOnStartMessage());
			}

			await executeAsync();

			if (createOnCompletionMessage != null)
			{
				_dispatch(createOnCompletionMessage());
			}
		}
		catch (TaskCanceledException)
		{
			if (createOnCancelledMessage != null)
			{
				_dispatch(createOnCancelledMessage());
			}
		}
		catch (Exception exception)
		{
			if (createOnExceptionMessage != null)
			{
				_dispatch(createOnExceptionMessage(exception));
			}
			else
			{
				throw;
			}
		}
	}

	/// <summary>
	/// Execute the given action with optional message responses.
	/// </summary>
	/// <typeparam name="TResult">The action result type.</typeparam>
	/// <param name="execute">The action to execute.</param>
	/// <param name="createOnStartMessage">Optionally construct a message to dispatch right before the action starts.</param>
	/// <param name="createOnCompletionMessage">Optionally construct a message to dispatch if the action does not throw an exception.</param>
	/// <param name="createOnExceptionMessage">Optionally construct a message to dispatch if the action throws an exception. If not specified, exceptions will not be caught.</param>
	/// <example>
	///	[!code-csharp[Example](../../DocSnippets/Core/EffectContext/ExecuteWithResult/DriveEffects.cs#Snippet)]
	/// </example>
	public void Execute<TResult>(
		Func<TResult> execute,
		Func<TMessage>? createOnStartMessage = null,
		Func<TResult, TMessage>? createOnCompletionMessage = null,
		Func<Exception, TMessage>? createOnExceptionMessage = null)
	{
		try
		{
			if (createOnStartMessage != null)
			{
				_dispatch(createOnStartMessage());
			}

			var result = execute();

			if (createOnCompletionMessage != null)
			{
				_dispatch(createOnCompletionMessage(result));
			}
		}
		catch (Exception exception)
		{
			if (createOnExceptionMessage != null)
			{
				_dispatch(createOnExceptionMessage(exception));
			}
			else
			{
				throw;
			}
		}
	}

	/// <summary>
	/// Execute the given action with optional message responses.
	/// </summary>
	/// <param name="execute">The action to execute.</param>
	/// <param name="createOnStartMessage">Optionally construct a message to dispatch right before the action starts.</param>
	/// <param name="createOnCompletionMessage">Optionally construct a message to dispatch if the action does not throw an exception.</param>
	/// <param name="createOnExceptionMessage">Optionally construct a message to dispatch if the action throws an exception. If not specified, exceptions will not be caught.</param>
	/// <example>
	///	[!code-csharp[Example](../../DocSnippets/Core/EffectContext/Execute/BrowseEffects.cs#Snippet)]
	/// </example>
	public void Execute(
		Action execute,
		Func<TMessage>? createOnStartMessage = null,
		Func<TMessage>? createOnCompletionMessage = null,
		Func<Exception, TMessage>? createOnExceptionMessage = null)
	{
		try
		{
			if (createOnStartMessage != null)
			{
				_dispatch(createOnStartMessage());
			}

			execute();

			if (createOnCompletionMessage != null)
			{
				_dispatch(createOnCompletionMessage());
			}
		}
		catch (Exception exception)
		{
			if (createOnExceptionMessage != null)
			{
				_dispatch(createOnExceptionMessage(exception));
			}
			else
			{
				throw;
			}
		}
	}
}
