﻿using AbreVinci.Embryo.App;
using Todoist.Core;

namespace Todoist;

public class FeatureToggleFilter : IMessageFilter<Message>
{
	private readonly IFeatureToggleDictionary<FeatureToggle> _featureToggleDictionary;

	public FeatureToggleFilter(IFeatureToggleDictionary<FeatureToggle> featureToggleDictionary)
	{
		_featureToggleDictionary = featureToggleDictionary;
	}

    public Message? FilterMessage(Message message) =>
        message switch
        {
            Message.StartTodoInput({ }) => _featureToggleDictionary.IsFeatureEnabled(FeatureToggle.EditTodo) ? message : null,
            _ => message
        };
}
