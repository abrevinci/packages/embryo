﻿using AbreVinci.Embryo;
using AbreVinci.Embryo.App;
using AbreVinci.Embryo.Debugging.WPF;
using AbreVinci.Embryo.WPF;

namespace Todoist;

public class WindowFactory : ReactiveWindowFactory
{
	private readonly IFeatureToggleDictionary<FeatureToggle> _featureToggleDictionary;

	public WindowFactory(IFeatureToggleDictionary<FeatureToggle> featureToggleDictionary) 
		: base(new ViewResourceDictionary(), new DebuggerViewResourceDictionary())
	{
		_featureToggleDictionary = featureToggleDictionary;
		RegisterWindow("Main", "MainWindowView", CreateMainWindow);
		RegisterDebuggerWindow("Debugger", CreateDebuggerWindow);
	}

	private ReactiveWindow CreateMainWindow()
	{
		var window = new ReactiveWindow
		{
			Title = "Todoist",
			WindowState = WindowState.Normal,
			WindowStartupLocation = WindowStartupLocation.CenterScreen,
			Width = 1200,
			Height = 800
		};
		ViewFeatureToggles.SetContext(window, new ViewFeatureToggleContext<FeatureToggle>(_featureToggleDictionary));
		return window;
	}

	private ReactiveWindow CreateDebuggerWindow()
	{
		var window = new ReactiveWindow
		{
			Title = "Debugger",
			WindowState = WindowState.Normal,
			WindowStartupLocation = WindowStartupLocation.CenterScreen,
			Width = 1200,
			Height = 800
		};
		return window;
	}
}
