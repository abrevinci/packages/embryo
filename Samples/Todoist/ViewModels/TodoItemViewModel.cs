﻿using AbreVinci.Embryo.ReactiveUI;
using Todoist.Core;

namespace Todoist.ViewModels;

public class TodoItemViewModel : ReactiveViewModel
{
	public TodoItemViewModel(
		TodoId id, 
		IObservable<Todo> todo, 
		IObservable<TodoInput?> editInput,
		Action<Message> dispatch,
		Func<TodoInputViewModel> createTodoInput)
	{
		Id = id;

		Title = BindOneWay(todo.Select(t => t.Title));
		Description = BindOneWay(todo.Select(t => t.Description));

		Delete = BindCommand(() => dispatch(new Message.DeleteTodo(id)));
		Edit = BindCommand(() => dispatch(new Message.StartTodoInput(id)));

		TodoInput = BindOneWayAsViewModel(editInput, i => i != null ? createTodoInput() : null, (_, _) => true);
	}

	public TodoId Id { get; }

	public string Title { get; }
	public string Description { get; }

	public ICommand Delete { get; }
	public ICommand Edit { get; }

	public TodoInputViewModel? TodoInput { get; }
}
