﻿using AbreVinci.Embryo;
using AbreVinci.Embryo.ReactiveUI;

namespace Todoist.ViewModels;

public class MainWindowViewModel : ReactiveViewModel
{
	public MainWindowViewModel(Func<TodoListViewModel> createTodoListViewModel)
	{
		TodoList = AddChildViewModel(createTodoListViewModel);
	}

	public TodoListViewModel TodoList { get; }
}
