﻿using AbreVinci.Embryo;
using AbreVinci.Embryo.ReactiveUI;
using Todoist.Core;
using Todoist.Core.Actions;

namespace Todoist.ViewModels;

public class TodoInputViewModel : ReactiveViewModel
{
	public TodoInputViewModel(IObservable<TodoInput> todoInput, Action<Message> dispatch)
	{
		Title = BindTwoWay(todoInput.Select(i => i.Title), title => dispatch(new Message.InputTodoTitle(title)));
		Description = BindTwoWay(todoInput.Select(d => d.Description), description => dispatch(new Message.InputTodoDescription(description)));

		Save = BindCommand(() => dispatch(new Message.CommitTodoInput()), todoInput.Select(i => i.CanCommit()));
		Cancel = BindCommand(() => dispatch(new Message.CancelTodoInput()));
	}

	public string Title { get; set; }
	public string Description { get; set; }

	public ICommand Save { get; }
	public ICommand Cancel { get; }
}
