﻿using AbreVinci.Embryo.ReactiveUI;
using Todoist.Core;

namespace Todoist.ViewModels;

public class TodoListViewModel : ReactiveViewModel
{
	public TodoListViewModel(
		IObservable<string> searchString,
		IObservable<IImmutableList<TodoId>> todoIds, 
		IObservable<TodoInput?> addTodoInput, 
		Action<Message> dispatch, 
		Func<TodoId, TodoItemViewModel> createTodoItem, 
		Func<TodoInputViewModel> createTodoInput)
	{
		SearchString = BindTwoWay(searchString, v => dispatch(new Message.SetSearchString(v)));

		TodoItems = BindListAsViewModels(todoIds, createTodoItem, (vm, id) => vm.Id == id)
			.Filter(searchString, (s, t) => t.Title.Contains(s, StringComparison.InvariantCultureIgnoreCase))
			.Sort((a, b) => string.Compare(a.Title, b.Title, StringComparison.InvariantCultureIgnoreCase));

		AddTodoInput = BindOneWayAsViewModel(addTodoInput, i => i != null ? createTodoInput() : null, (_, _) => true);

		AddTodo = BindCommand(() => dispatch(new Message.StartTodoInput()));
	}

	public string SearchString { get; set; }
	public IEnumerable<TodoItemViewModel> TodoItems { get; }
	public TodoInputViewModel? AddTodoInput { get; }

	public ICommand AddTodo { get; }
}
