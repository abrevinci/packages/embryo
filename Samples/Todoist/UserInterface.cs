﻿using AbreVinci.Embryo.Debugging.ViewModels;
using AbreVinci.Embryo.ReactiveUI;
using Todoist.ViewModels;

namespace Todoist;

public class UserInterface : ReactiveUserInterface
{
	public UserInterface(
		ViewModelFactory viewModelFactory, 
		DebuggerViewModelFactory debuggerViewModelFactory, 
		IReactiveViewModelHost viewModelHost,
		IReactiveWindowFactory windowFactory)
		: base(viewModelHost, windowFactory)
	{
		MainWindow = AddWindow("Main", viewModelFactory.CreateMainWindowViewModel);
		DebuggerWindow = AddWindow("Debugger", debuggerViewModelFactory.CreateDebuggerViewModel);
	}

	public IReactiveWindowController<MainWindowViewModel> MainWindow { get; }
	public IReactiveWindowController<DebuggerViewModel> DebuggerWindow { get; }
}
