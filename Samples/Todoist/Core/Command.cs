﻿using AbreVinci.Embryo.Entities;

namespace Todoist.Core;

public abstract record Command
{
    public record Dispatch(Message Message) : Command;

    public record TodosCommand(EntityCommand Command) : Command;
    public record LoadTodos() : TodosCommand(new EntityCommand.Load());

    public record ProjectsCommand(EntityCommand Command) : Command;
    public record LoadProjects() : ProjectsCommand(new EntityCommand.Load());
    public record CreateProject(ProjectInput Input) : ProjectsCommand(new EntityCommand.Create<ProjectInput>(Input));

    private Command() { }
}
