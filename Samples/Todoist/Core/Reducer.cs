﻿using AbreVinci.Embryo.Core;
using Todoist.Core.Actions;

namespace Todoist.Core;

public static class Reducer
{
    [Pure]
    public static ActionContext<State, Command> Init()
    {
        var initialState = new State(
            TodoState: new(),
            ProjectState: new(),
            ProjectSections: new(),
            Labels: new(),
            null,
            string.Empty,
            null,
            null);
        return new ActionContext<State, Command>(initialState).WithCommands(new Command.LoadTodos(), new Command.LoadProjects());
    }

    [Pure]
    public static ActionContext<State, Command> Update(this ActionContext<State, Command> context, Message message)
    {
        return message switch
        {
            Message.SetSearchString(var searchString) => context.SetSearchString(searchString),

            Message.StartTodoInput(var editingTodoId) => context.StartTodoInput(editingTodoId),
            Message.CommitTodoInput => context.CommitTodoInput(),
            Message.CancelTodoInput => context.CancelTodoInput(),
            Message.InputTodoTitle(var title) => context.InputTodoTitle(title),
            Message.InputTodoDescription(var description) => context.InputTodoDescription(description),
            Message.TodoMessage(var todosMessage) => context.ApplyTodosReducer(todosMessage),

            Message.StartProjectInput => context.StartProjectInput(),
            Message.CommitProjectInput => context.CommitProjectInput(),
            Message.CancelProjectInput => context.CancelProjectInput(),
            Message.InputProjectName(var name) => context.InputProjectName(name),
            Message.InputProjectColor(var color) => context.InputProjectColor(color),
            Message.ProjectMessage(var projectsMessage) => context.ApplyProjectStateReducer(projectsMessage),

            _ => context
        };
    }
}
