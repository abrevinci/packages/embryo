﻿using AbreVinci.Embryo.Entities;

namespace Todoist.Core;

public record struct TodoId(Guid Id);
public record struct ProjectId(Guid Id);
public record struct ProjectSectionId(Guid Id);
public record struct LabelId(Guid Id);

public abstract record Schedule
{
    // todo: due date
    // todo: due date and time
    // todo: repeating schedule (from, to, frequency)

    private Schedule() { }
}

public record ProjectReference(
    ProjectId Project,
    ProjectSectionId? ProjectSection);

public record TodoInput(
    string Title, 
    string Description); // todo: add more properties here

// todo: reminders, sub-tasks, comments, and activity
public record Todo(
    TodoId Id, 
    string Title, 
    string Description,
    //todo: Schedule? Schedule,
    //todo: ProjectReference? Project,
    //todo: IImmutableList<LabelId> Labels,
    //todo: int Priority, 
    bool IsCompleted);

public record struct Color(
    byte Red, 
    byte Green,
    byte Blue)
{
    public static Color Gray { get; } = new (200, 200, 200);
}

public enum ProjectView
{
    List,
    Board
}

public record ProjectInput(
    string Name,
    Color Color);

public record Project(
    ProjectId Id, 
    string Name,
    Color Color/*, 
    bool IsFavorite, todo
    ProjectView View*/);

public record ProjectSection(
    ProjectSectionId Id,
    string Name,
    ProjectId Project);

public record Label(
    LabelId Id, 
    string Name, 
    Color Color, 
    bool IsFavorite);

public record State(
    EntityState<TodoId, Todo, TodoInput> TodoState,
    EntityState<ProjectId, Project, ProjectInput> ProjectState,
    EntityCollection<ProjectSectionId, ProjectSection> ProjectSections,
    EntityCollection<LabelId, Label> Labels,
    TodoId? EditingTodoId,
    string SearchString,
    TodoInput? TodoInput,
    ProjectInput? ProjectInput);
