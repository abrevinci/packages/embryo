﻿using System.Diagnostics;
using AbreVinci.Embryo.Core;

namespace Todoist.Core.Actions;

using ActionContext = ActionContext<State, Command>;

public static class TodoInputActions
{
    [Pure]
    public static ActionContext StartTodoInput(this ActionContext context, TodoId? editingTodoId)
    {
	    var state = context.State;

        Debug.Assert(state.TodoInput == null);

        var newTodoInput = editingTodoId is { } id && state.TodoState.Collection.Entities.TryGetValue(id, out var todo) ?
            new TodoInput(todo.Title, todo.Description) :
            new TodoInput(string.Empty, string.Empty);

        var newState = state with
        {
            EditingTodoId = editingTodoId,
            TodoInput = newTodoInput
        };

        return context.WithState(newState);
    }

    [Pure]
    public static bool CanCommit(this TodoInput input)
    {
        return !string.IsNullOrWhiteSpace(input.Title);
    }

    [Pure]
    public static ActionContext CommitTodoInput(this ActionContext context)
    {
	    var state = context.State;

        Debug.Assert(state.TodoInput != null);
        Debug.Assert(state.TodoInput.CanCommit());

        var newState = state with
        {
            EditingTodoId = null,
            TodoInput = null
        };

        if (state.EditingTodoId is { } id && state.TodoState.Collection.Entities.TryGetValue(id, out var todo))
        {
            var requestedTodo = todo with
            {
                Title = state.TodoInput.Title,
                Description = state.TodoInput.Description
            };
            return context.WithState(newState).WithCommand(new Command.Dispatch(new Message.UpdateTodo(requestedTodo)));
        }
        else
		{
            return context.WithState(newState).WithCommand(new Command.Dispatch(new Message.CreateTodo(state.TodoInput)));
		}
    }

    [Pure]
    public static ActionContext CancelTodoInput(this ActionContext context)
    {
	    var state = context.State;

        Debug.Assert(state.TodoInput != null);

        var newState = state with
        {
            TodoInput = null
        };

        return context.WithState(newState);
    }

    [Pure]
    public static ActionContext InputTodoTitle(this ActionContext context, string title)
    {
	    var state = context.State;

        Debug.Assert(state.TodoInput != null);

        var newState = state with
        {
            TodoInput = state.TodoInput with
            {
                Title = title
            }
        };

        return context.WithState(newState);
    }

    [Pure]
    public static ActionContext InputTodoDescription(this ActionContext context, string description)
    {
	    var state = context.State;

        Debug.Assert(state.TodoInput != null);

        var newState = state with
        {
            TodoInput = state.TodoInput with
            {
                Description = description
            }
        };

        return context.WithState(newState);
    }
}
