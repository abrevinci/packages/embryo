﻿using AbreVinci.Embryo.Core;
using AbreVinci.Embryo.Entities;

namespace Todoist.Core.Actions;

using ActionContext = ActionContext<State, Command>;

public static class TodoActions
{
    [Pure]
    public static ActionContext ApplyTodosReducer(this ActionContext context, EntityMessage message)
    {
	    return context.UpdateEntityState(message, s => s.TodoState, (s, t) => s with { TodoState = t }, c => new Command.TodosCommand(c));
    }
}
