﻿using AbreVinci.Embryo.Core;
using AbreVinci.Embryo.Entities;

namespace Todoist.Core.Actions;

using ActionContext = ActionContext<State, Command>;

public static class ProjectActions
{
    [Pure]
    public static ActionContext ApplyProjectStateReducer(this ActionContext context, EntityMessage message)
    {
	    return context.UpdateEntityState(message, s => s.ProjectState, (s, p) => s with { ProjectState = p }, c => new Command.ProjectsCommand(c));
    }
}
