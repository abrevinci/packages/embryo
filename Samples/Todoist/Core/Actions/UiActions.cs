﻿using AbreVinci.Embryo.Core;

namespace Todoist.Core.Actions;

using ActionContext = ActionContext<State, Command>;

public static class UiActions
{
	[Pure]
	public static ActionContext SetSearchString(this ActionContext context, string searchString)
	{
		var state = context.State;
		var newState = state with
		{
			SearchString = searchString
		};
		return context.WithState(newState);
	}
}
