﻿using System.Diagnostics;
using AbreVinci.Embryo.Core;

namespace Todoist.Core.Actions;

using ActionContext = ActionContext<State, Command>;

public static class ProjectInputActions
{
    [Pure]
    public static ActionContext StartProjectInput(this ActionContext context)
    {
	    var state = context.State;
	    Debug.Assert(state.ProjectInput == null);

        var newState = state with
        {
            ProjectInput = new ProjectInput(string.Empty, Color.Gray)
        };

        return context.WithState(newState);
    }

    [Pure]
    public static bool CanCommit(this ProjectInput input)
    {
        return !string.IsNullOrWhiteSpace(input.Name);
    }

    [Pure]
    public static ActionContext CommitProjectInput(this ActionContext context)
    {
	    var state = context.State;

        Debug.Assert(state.ProjectInput != null);
        Debug.Assert(state.ProjectInput.CanCommit());

        var newState = state with
        {
            ProjectInput = null
        };

        return context.WithState(newState).WithCommand(new Command.CreateProject(state.ProjectInput));
    }
    
    [Pure]
    public static ActionContext CancelProjectInput(this ActionContext context)
    {
	    var state = context.State;

        Debug.Assert(state.ProjectInput != null);

        var newState = state with
        {
            ProjectInput = null
        };

        return context.WithState(newState);
    }

    [Pure]
    public static ActionContext InputProjectName(this ActionContext context, string name)
    {
	    var state = context.State;

        Debug.Assert(state.ProjectInput != null);

        var newState = state with
        {
            ProjectInput = state.ProjectInput with
            {
                Name = name
            }
        };

        return context.WithState(newState);
    }

    [Pure]
    public static ActionContext InputProjectColor(this ActionContext context, Color color)
    {
	    var state = context.State;

        Debug.Assert(state.ProjectInput != null);

        var newState = state with
        {
            ProjectInput = state.ProjectInput with
            {
                Color = color
            }
        };

        return context.WithState(newState);
    }
}
