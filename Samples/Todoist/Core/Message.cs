﻿using AbreVinci.Embryo.Entities;

namespace Todoist.Core;

public abstract record Message
{
	public record SetSearchString(string SearchString) : Message;

    // todo input
    public record StartTodoInput(TodoId? EditingTodoId = null) : Message;
    public record CommitTodoInput : Message;
    public record CancelTodoInput : Message;
    public record InputTodoTitle(string Title) : Message;
    public record InputTodoDescription(string Description) : Message;

    public record TodoMessage(EntityMessage Message) : Message;
    public record CreateTodo(TodoInput Input) : TodoMessage(new EntityMessage.Create<TodoInput>(Input));
    public record UpdateTodo(Todo Todo) : TodoMessage(new EntityMessage.Update<TodoId, Todo>(Todo.Id, Todo));
    public record DeleteTodo(TodoId Id) : TodoMessage(new EntityMessage.Delete<TodoId>(Id));

    // project input
    public record StartProjectInput : Message;
    public record CommitProjectInput : Message;
    public record CancelProjectInput : Message;
    public record InputProjectName(string Name) : Message;
    public record InputProjectColor(Color Color) : Message;

    public record ProjectMessage(EntityMessage Message) : Message;

    private Message() { }
}
