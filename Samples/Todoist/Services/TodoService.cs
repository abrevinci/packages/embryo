﻿using AbreVinci.Embryo.Entities;
using Todoist.Core;

namespace Todoist.Services;

public class TodoService : IEntityService<TodoId, Todo, TodoInput>
{
    public Task<IImmutableList<(TodoId id, Todo entity)>> LoadAsync()
    {
        // this would normally call server code or work with a local database
        return Task.FromResult((IImmutableList<(TodoId id, Todo entity)>)ImmutableArray<(TodoId id, Todo entity)>.Empty);
    }

    public Task<(TodoId id, Todo entity)> CreateAsync(TodoInput input)
    {
        // this would normally call server code or work with a local database
        var id = new TodoId(Guid.NewGuid());
        return Task.FromResult((id, new Todo(id, input.Title, input.Description, false)));
    }

    public Task<Todo> UpdateAsync(TodoId id, Todo entity)
    {
        // this would normally call server code or work with a local database
        return Task.FromResult(entity);
    }

    public Task DeleteAsync(TodoId id)
    {
        // this would normally call server code or work with a local database
        return Task.CompletedTask;
    }
}
