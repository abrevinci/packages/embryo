﻿using AbreVinci.Embryo.Entities;
using Todoist.Core;

namespace Todoist.Services;

public class ProjectService : IEntityService<ProjectId, Project, ProjectInput>
{
    public Task<IImmutableList<(ProjectId id, Project entity)>> LoadAsync()
    {
        // this would normally call server code or work with a local database
        return Task.FromResult((IImmutableList<(ProjectId id, Project entity)>)ImmutableArray<(ProjectId id, Project entity)>.Empty);
    }

    public Task<(ProjectId id, Project entity)> CreateAsync(ProjectInput input)
    {
        // this would normally call server code or work with a local database
        var id = new ProjectId(Guid.NewGuid());
        return Task.FromResult((id, new Project(id, input.Name, input.Color)));
    }

    public Task<Project> UpdateAsync(ProjectId id, Project entity)
    {
        // this would normally call server code or work with a local database
        return Task.FromResult(entity);
    }

    public Task DeleteAsync(ProjectId id)
    {
        // this would normally call server code or work with a local database
        return Task.CompletedTask;
    }
}
