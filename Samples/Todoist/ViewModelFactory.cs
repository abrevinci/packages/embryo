﻿using AbreVinci.Embryo.Core;
using Todoist.Core;
using Todoist.ViewModels;

namespace Todoist;

public class ViewModelFactory
{
	private readonly IObservable<State> _state;
	private readonly Action<Message> _dispatch;

	public ViewModelFactory(IObservable<State> state, Action<Message> dispatch)
	{
		_state = state;
		_dispatch = dispatch;
	}

	public MainWindowViewModel CreateMainWindowViewModel()
	{
		var todoIds = _state.Select(s => s.TodoState.Collection.Ids);
		var todoInput = _state.SelectNotNull(s => s.TodoInput);
		var addTodoInput = _state.Select(s => s.EditingTodoId == null ? s.TodoInput : null);
		var searchString = _state.Select(s => s.SearchString);

		IObservable<Todo> SelectTodo(TodoId id) => _state.Select(s => s.TodoState.Collection.Entities[id]);
		IObservable<TodoInput?> SelectEditTodoInput(TodoId id) => _state.Select(s => s.EditingTodoId == id ? s.TodoInput : null);

		TodoInputViewModel CreateTodoInputViewModel() => new(todoInput!, _dispatch);
		TodoItemViewModel CreateTodoItemViewModel(TodoId id) => new(id, SelectTodo(id), SelectEditTodoInput(id), _dispatch, CreateTodoInputViewModel);
		TodoListViewModel CreateTodoListViewModel() => new(searchString, todoIds, addTodoInput, _dispatch, CreateTodoItemViewModel, CreateTodoInputViewModel);
		return new(CreateTodoListViewModel);
	}
}
