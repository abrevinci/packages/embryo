﻿using AbreVinci.Embryo.App;
using AbreVinci.Embryo.Debugging;
using AbreVinci.Embryo.Debugging.ViewModels;
using AbreVinci.Embryo.Entities;
using AbreVinci.Embryo.ReactiveUI;
using AbreVinci.Embryo.WPF;
using Todoist.Core;
using Todoist.Services;

namespace Todoist;

public class EntryPoint
{
    [STAThread]
    public static void Main()
    {
        try
        {
            var featureToggles = ImmutableDictionary<FeatureToggle, bool>.Empty
                .Add(FeatureToggle.EditTodo, false);

            var featureToggleDictionary = new ImmutableFeatureToggleDictionary<FeatureToggle>(featureToggles);

            var application = new Application();
            var scheduler = new UiScheduler(application.Dispatcher);

            var debugger = new Debugger<State, Message, Command>(new DebuggerSettings(), typeof(Guid));

            var program = Program.Create(Reducer.Init, Reducer.Update, scheduler, debugger: debugger)
                .ApplyFilters(messageFilter: new FeatureToggleFilter(featureToggleDictionary));

            using var commandHandler = new CommandHandler(
	            program.Commands, 
	            program.DispatchMessage,
	            new TodoService(),
	            new ProjectService());

            using var userInterface = new UserInterface(
	            new ViewModelFactory(program.States, program.DispatchMessage),
                new DebuggerViewModelFactory(debugger),
                new ReactiveViewModelHost(scheduler, false),
	            new WindowFactory(featureToggleDictionary));

            application.Run();
        }
        catch (Exception)
        {
            // ignore
        }
    }
}
