﻿using AbreVinci.Embryo.Core;
using AbreVinci.Embryo.Entities;
using Todoist.Core;

namespace Todoist;

public sealed class CommandHandler : IDisposable
{
    private readonly IDisposable _subscription;
    private readonly EffectContext<Message> _context;
    private readonly EffectContext<IEntityService<TodoId, Todo, TodoInput>, Message> _todoContext;
    private readonly EffectContext<IEntityService<ProjectId, Project, ProjectInput>, Message> _projectContext;

    public CommandHandler(
        IObservable<Command> commands,
        Action<Message> dispatch,
        IEntityService<TodoId, Todo, TodoInput> todoService, 
        IEntityService<ProjectId, Project, ProjectInput> projectService)
    {
        _context = new EffectContext<Message>(dispatch);
        _todoContext = new EffectContext<IEntityService<TodoId, Todo, TodoInput>, Message>(todoService, dispatch);
        _projectContext = new EffectContext<IEntityService<ProjectId, Project, ProjectInput>, Message>(projectService, dispatch);

        _subscription = commands.Subscribe(command => ExecuteAsync(command));
    }
    
    public Task ExecuteAsync(Command command)
    {
        return command switch
        {
            Command.Dispatch(var message) => WrapAsAsync(() => _context.Dispatch(message)),
            Command.TodosCommand(var todosCommand) => _todoContext.HandleEntityCommandAsync(todosCommand, m => new Message.TodoMessage(m)),
            Command.ProjectsCommand(var projectsCommand) => _projectContext.HandleEntityCommandAsync(projectsCommand, m => new Message.ProjectMessage(m)),
            _ => Task.CompletedTask
        };
    }

    private Task WrapAsAsync(Action action)
    {
	    action();
        return Task.CompletedTask;
    }

    public void Dispose()
    {
        _subscription.Dispose();
    }
}
