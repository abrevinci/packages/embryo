﻿using AbreVinci.Embryo;
using AbreVinci.Embryo.App;
using AbreVinci.Embryo.ReactiveUI;
using AbreVinci.Embryo.WPF;
using Counter.Core;
using Counter.Services;

namespace Counter;

public class EntryPoint
{
    [STAThread]
    public static void Main()
    {
        try
        {
            var application = new Application();
            var scheduler = new UiScheduler(application.Dispatcher);

            // program
            var program = Program.Create<State, Message, Command>(Reducer.Init, Reducer.Update, scheduler);

            // command handler
            var persistanceService = new PersistanceService();
            var random = new Random(Guid.NewGuid().GetHashCode());
            using var commandHandler = new CommandHandler(program.Commands, program.DispatchMessage, persistanceService, random);

            // user interface
            using var userInterface = new UserInterface(
	            new ViewModelFactory(program.States, program.DispatchMessage),
                new ReactiveViewModelHost(scheduler, false),
	            new WindowFactory());

            application.Run();
        }
        catch (Exception)
        {
            // ignore
        }
    }
}
