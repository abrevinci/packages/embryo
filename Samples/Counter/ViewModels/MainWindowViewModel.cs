﻿using AbreVinci.Embryo.ReactiveUI;

namespace Counter.ViewModels;

public class MainWindowViewModel : ReactiveViewModel
{
	public MainWindowViewModel(Func<CounterViewModel> createCounter)
	{
		Counter = AddChildViewModel(createCounter);
	}

	public CounterViewModel Counter { get; }
}
