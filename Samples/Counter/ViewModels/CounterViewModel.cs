﻿using System.Collections.Generic;
using AbreVinci.Embryo.ReactiveUI;
using Counter.Core;

namespace Counter.ViewModels;

public class CounterViewModel : ReactiveViewModel
{
	public CounterViewModel(IObservable<State> state, Action<Message> dispatch)
	{
		Counter = BindTwoWay(state.Select(s => s.Counter), c => dispatch(new Message.SetCounter(c)));
		LoadIndicatorVisibility = BindOneWay(state.Select(s => s.IsLoading ? Visibility.Visible : Visibility.Collapsed));
		SaveIndicatorVisibility = BindOneWay(state.Select(s => s.IsSaving ? Visibility.Visible : Visibility.Collapsed));
		Errors = BindList(state.Select(s => s.Errors));
		
		Increment = BindCommand(() => dispatch(new Message.Increment()));
		Randomize = BindCommand(() => dispatch(new Message.Randomize()));
		Save = BindCommand(() => dispatch(new Message.Save()), state.Select(s => !s.IsSaving));
		ClearErrors = BindCommand(() => dispatch(new Message.ClearErrors()));
	}

	public int Counter { get; set; }
	public Visibility LoadIndicatorVisibility { get; }
	public Visibility SaveIndicatorVisibility { get; }
	public IEnumerable<string> Errors { get; }

	public ICommand Increment { get; }
	public ICommand Randomize { get; }
	public ICommand Save { get; }
	public ICommand ClearErrors { get; }
}
