﻿using AbreVinci.Embryo.WPF;
using Counter.Views;

namespace Counter;

public class WindowFactory : ReactiveWindowFactory
{
	public WindowFactory() : base(new DataTemplateGenerator(typeof(MainWindowView)))
	{
		RegisterWindow(nameof(UserInterface.MainWindow), nameof(MainWindowView), CreateMainWindow);
	}
	
	private ReactiveWindow CreateMainWindow()
	{
		var window = new ReactiveWindow
		{
			Title = "Counter",
			WindowState = WindowState.Normal,
			WindowStartupLocation = WindowStartupLocation.CenterScreen,
			Width = 500,
			Height = 500
		};
		return window;
	}
}
