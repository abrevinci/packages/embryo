﻿using Counter.Core;
using Counter.ViewModels;

namespace Counter;

public class ViewModelFactory
{
	private readonly IObservable<State> _state;
	private readonly Action<Message> _dispatch;

	public ViewModelFactory(IObservable<State> state, Action<Message> dispatch)
	{
		_state = state;
		_dispatch = dispatch;
	}

	public MainWindowViewModel CreateMainWindowViewModel()
	{
		var createCounter = () => new CounterViewModel(_state, _dispatch);
		return new MainWindowViewModel(createCounter);
	}
}
