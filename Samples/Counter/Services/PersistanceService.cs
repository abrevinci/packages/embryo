﻿namespace Counter.Services;

public class PersistanceService : IPersistanceService
{
	private readonly Random _random = new();

    public async Task<int> LoadAsync()
    {
        await Task.Delay(5000); // simulate load time
        return 0;
    }

    public async Task SaveAsync(int value)
    {
        await Task.Delay(2000); // simulate save time
        if (_random.Next(2) == 0)
	        throw new Exception();
    }
}
