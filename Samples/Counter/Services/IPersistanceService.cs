﻿namespace Counter.Services;

public interface IPersistanceService
{
    Task<int> LoadAsync();
    Task SaveAsync(int value);
}
