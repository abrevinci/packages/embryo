﻿namespace Counter.Core;

public record State(int Counter, bool IsLoading, bool IsSaving, IImmutableList<string> Errors);
