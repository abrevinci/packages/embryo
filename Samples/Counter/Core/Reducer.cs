﻿using AbreVinci.Embryo.Core;
using AbreVinci.Embryo.Generator.Attributes;

namespace Counter.Core;

using ActionContext = ActionContext<State, Command>;

[GenerateReducerUpdate(typeof(State), typeof(Message), typeof(Command))]
public static partial class Reducer
{
	public static ActionContext Init() => new ActionContext(
		new State(0, true, false, ImmutableArray<string>.Empty))
		.WithCommand(new Command.LoadAsync());
}
