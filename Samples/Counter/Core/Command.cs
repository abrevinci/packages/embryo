﻿using AbreVinci.Embryo.Generator.Attributes;

namespace Counter.Core;

[GenerateCommandType]
public abstract partial record Command;
