﻿using AbreVinci.Embryo.Generator.Attributes;

namespace Counter.Core;

[GenerateMessageType]
public abstract partial record Message;
