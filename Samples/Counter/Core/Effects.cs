﻿using AbreVinci.Embryo.Core;
using AbreVinci.Embryo.Generator.Attributes;
using Counter.Services;

namespace Counter.Core;

using PersistanceContext = EffectContext<IPersistanceService, Message>;
using RandomContext = EffectContext<Random, Message>;

public static class Effects
{
	[Effect]
	public static Task LoadAsync(PersistanceContext context)
	{
		return context.ExecuteAsync(
			service => service.LoadAsync(), 
			createOnCompletionMessage: result => new Message.LoadComplete(result),
			createOnExceptionMessage: _ => new Message.LoadFailed());
	}

	[Effect]
	public static Task SaveAsync(PersistanceContext context, int value)
	{
		return context.ExecuteAsync(
			service => service.SaveAsync(value),
			createOnCompletionMessage: () => new Message.SaveComplete(),
			createOnExceptionMessage: _ => new Message.SaveFailed());
	}

	[Effect]
	public static void Randomize(RandomContext context)
	{
		context.Execute(
			random => random.Next(100),
			createOnCompletionMessage: result => new Message.SetCounter(result));
	}
}
