﻿using AbreVinci.Embryo.Core;
using AbreVinci.Embryo.Generator.Attributes;

namespace Counter.Core;

using ActionContext = ActionContext<State, Command>;

public static class Actions
{
	[Action]
	public static ActionContext Increment(this ActionContext context)
	{
		var state = context.State;
		var newState = state with
		{
			Counter = state.Counter + 1
		};
		return context.WithState(newState);
	}

	[Action]
	public static ActionContext Randomize(this ActionContext context)
	{
		return context.WithCommand(new Command.Randomize());
	}

	[Action]
	public static ActionContext SetCounter(this ActionContext context, int value)
	{
		var state = context.State;
		var newState = state with
		{
			Counter = value
		};
		return context.WithState(newState);
	}

	[Action]
	public static ActionContext LoadComplete(this ActionContext context, int value)
	{
		var state = context.State;
		var newState = state with
		{
			Counter = value,
			IsLoading = false
		};
		return context.WithState(newState);
	}

	[Action]
	public static ActionContext LoadFailed(this ActionContext context)
	{
		var state = context.State;
		var newState = state with
		{
			IsLoading = false,
			Errors = state.Errors.Add("Load failed!")
		};
		return context.WithState(newState);
	}

	[Action]
	public static ActionContext Save(this ActionContext context)
	{
		var state = context.State;
		var newState = state with
		{
			IsSaving = true
		};
		return context.WithState(newState).WithCommand(new Command.SaveAsync(state.Counter));
	}

	[Action]
	public static ActionContext SaveComplete(this ActionContext context)
	{
		var state = context.State;
		var newState = state with
		{
			IsSaving = false
		};
		return context.WithState(newState);
	}

	[Action]
	public static ActionContext SaveFailed(this ActionContext context)
	{
		var state = context.State;
		var newState = state with
		{
			IsSaving = false,
			Errors = state.Errors.Add("Save failed!")
		};
		return context.WithState(newState);
	}

	[Action]
	public static ActionContext ClearErrors(this ActionContext context)
	{
		var state = context.State;
		var newState = state with
		{
			Errors = state.Errors.Clear()
		};
		return context.WithState(newState);
	}
}
