﻿namespace AbreVinci.Embryo.App.Tests.WorkerThreadTests;

public class RequestCancellationAsync
{
	public class WithState
	{
		[Fact]
		public async Task ShouldSignalCancellationToActions()
		{
			using var workerThread = new WorkerThread<int>(() => 0, _ => { });

			var wasCancelled = false;
			_ = workerThread.InvokeAsync(context =>
			{
				while (!context.WasCancellationRequested)
				{
				}

				wasCancelled = true;
			});

			await workerThread.RequestCancellationAsync();

			wasCancelled.Should().BeTrue();
		}

		[Fact]
		public async Task ShouldCauseTaskToBeCancelled()
		{
			using var workerThread = new WorkerThread<int>(() => 0, _ => { });

			var task = workerThread.InvokeAsync(context =>
			{
				while (!context.WasCancellationRequested)
				{
				}
			});

			await workerThread.RequestCancellationAsync();

			Func<Task> f = async () => await task;

			await f.Should().ThrowAsync<TaskCanceledException>();
		}
	}

	public class WithoutState
	{
		[Fact]
		public async Task ShouldSignalCancellationToActions()
		{
			using var workerThread = new WorkerThread();

			var wasCancelled = false;
			_ = workerThread.InvokeAsync(context =>
			{
				while (!context.WasCancellationRequested)
				{
				}

				wasCancelled = true;
			});

			await workerThread.RequestCancellationAsync();

			wasCancelled.Should().BeTrue();
		}

		[Fact]
		public async Task ShouldCauseTaskToBeCancelled()
		{
			using var workerThread = new WorkerThread();

			var task = workerThread.InvokeAsync(context =>
			{
				while (!context.WasCancellationRequested)
				{
				}
			});

			await workerThread.RequestCancellationAsync();

			Func<Task> f = async () => await task;

			await f.Should().ThrowAsync<TaskCanceledException>();
		}
	}
}
