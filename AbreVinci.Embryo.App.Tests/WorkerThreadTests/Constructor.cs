﻿namespace AbreVinci.Embryo.App.Tests.WorkerThreadTests;

public class Constructor
{
	public class WithState
	{
		[Fact]
		public void ShouldExecuteStateInitializationOnWorkerThread()
		{
			int? initThreadId = null;
			int workerThreadId;

			int InitState()
			{
				initThreadId = Environment.CurrentManagedThreadId;
				return 0;
			}

			{
				using var workerThread = new WorkerThread<int>(InitState, _ => { });
				workerThreadId = workerThread.ThreadId;
			}

			initThreadId.Should().Be(workerThreadId);
		}

		[Fact]
		public void ShouldExecuteStateDeinitializationOnWorkerThread()
		{
			int? initThreadId = null;
			int workerThreadId;

			void DeinitState(int state)
			{
				initThreadId = Environment.CurrentManagedThreadId;
			}

			{
				using var workerThread = new WorkerThread<int>(() => 0, DeinitState);
				workerThreadId = workerThread.ThreadId;
			}

			initThreadId.Should().Be(workerThreadId);
		}

		[Fact]
		public void ShouldPassCorrectStateFromInitToDeinit()
		{
			int? deinitState = null;

			{
				using var workerThread = new WorkerThread<int>(() => 13, s => deinitState = s);
			}

			deinitState.Should().Be(13);
		}
    }
}
