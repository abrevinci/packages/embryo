﻿namespace AbreVinci.Embryo.App.Tests.WorkerThreadTests;

public class InvokeAsync
{
	public class WithState
	{
		[Fact]
		public async Task ShouldExecuteActionOnWorkerThread()
		{
			using var workerThread = new WorkerThread<int>(() => 0, _ => { });

			int? actionThreadId = null;
			await workerThread.InvokeAsync(_ => actionThreadId = Environment.CurrentManagedThreadId);

			actionThreadId.Should().Be(workerThread.ThreadId);
		}

		[Fact]
		public async Task ShouldExecuteActionsInOrder()
		{
			using var workerThread = new WorkerThread<int>(() => 0, _ => { });

			var numbers = new List<int>();
			_ = workerThread.InvokeAsync(_ => numbers.Add(0));
			_ = workerThread.InvokeAsync(_ => numbers.Add(1));
			_ = workerThread.InvokeAsync(_ => numbers.Add(2));
			_ = workerThread.InvokeAsync(_ => numbers.Add(3));
			_ = workerThread.InvokeAsync(_ => numbers.Add(4));
			_ = workerThread.InvokeAsync(_ => numbers.Add(5));
			_ = workerThread.InvokeAsync(_ => numbers.Add(6));
			_ = workerThread.InvokeAsync(_ => numbers.Add(7));
			_ = workerThread.InvokeAsync(_ => numbers.Add(8));
			await workerThread.InvokeAsync(_ => numbers.Add(9));

			numbers.Should().BeEquivalentTo(new[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
		}

		[Fact]
		public async Task ShouldPropagateExceptionWhenActionThrows()
		{
			using var workerThread = new WorkerThread<int>(() => 0, _ => { });

			// ReSharper disable once AccessToDisposedClosure
			Func<Task> f = async () => await workerThread.InvokeAsync(_ => throw new Exception("Error"));

			await f.Should().ThrowAsync<Exception>().WithMessage("Error");
		}

		[Fact]
		public async Task ShouldGiveActionAccessToThreadState()
		{
			using var workerThread = new WorkerThread<int>(() => 13, _ => { });

			int? stateInAction = null;
			await workerThread.InvokeAsync(context => stateInAction = context.State);

			stateInAction.Should().Be(13);
		}

		[Fact]
		public async Task ShouldAllowActionToModifyThreadState()
		{
			using var workerThread = new WorkerThread<int>(() => 13, _ => { });

			_ = workerThread.InvokeAsync(context => context.State = 45);

			int? stateInAction = null;
			await workerThread.InvokeAsync(context => stateInAction = context.State);

			stateInAction.Should().Be(45);
		}

		[Fact]
		public async Task ShouldAllowActionToSleep()
		{
			using var workerThread = new WorkerThread<int>(() => 0, _ => { });

			var before = DateTime.Now;
			await workerThread.InvokeAsync(context => context.Sleep(TimeSpan.FromSeconds(1.1)));
			var after = DateTime.Now;

			(after - before).TotalSeconds.Should().BeGreaterThan(1);
		}

		[Fact]
		public async Task ShouldReturnAnyReturnValueFromAction()
		{
			using var workerThread = new WorkerThread<int>(() => 0, _ => { });

			var result = await workerThread.InvokeAsync(_ => 13);

			result.Should().Be(13);
		}
	}

	public class WithoutState
	{
		[Fact]
		public async Task ShouldExecuteActionOnWorkerThread()
		{
			using var workerThread = new WorkerThread();

			int? actionThreadId = null;
			await workerThread.InvokeAsync(_ => actionThreadId = Environment.CurrentManagedThreadId);

			actionThreadId.Should().Be(workerThread.ThreadId);
		}

		[Fact]
		public async Task ShouldExecuteActionsInOrder()
		{
			using var workerThread = new WorkerThread();

			var numbers = new List<int>();
			_ = workerThread.InvokeAsync(_ => numbers.Add(0));
			_ = workerThread.InvokeAsync(_ => numbers.Add(1));
			_ = workerThread.InvokeAsync(_ => numbers.Add(2));
			_ = workerThread.InvokeAsync(_ => numbers.Add(3));
			_ = workerThread.InvokeAsync(_ => numbers.Add(4));
			_ = workerThread.InvokeAsync(_ => numbers.Add(5));
			_ = workerThread.InvokeAsync(_ => numbers.Add(6));
			_ = workerThread.InvokeAsync(_ => numbers.Add(7));
			_ = workerThread.InvokeAsync(_ => numbers.Add(8));
			await workerThread.InvokeAsync(_ => numbers.Add(9));

			numbers.Should().BeEquivalentTo(new[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
		}

		[Fact]
		public async Task ShouldPropagateExceptionWhenActionThrows()
		{
			using var workerThread = new WorkerThread();

			// ReSharper disable once AccessToDisposedClosure
			Func<Task> f = async () => await workerThread.InvokeAsync(_ => throw new Exception("Error"));

			await f.Should().ThrowAsync<Exception>().WithMessage("Error");
		}

		[Fact]
		public async Task ShouldAllowActionToSleep()
		{
			using var workerThread = new WorkerThread();

			var before = DateTime.Now;
			await workerThread.InvokeAsync(context => context.Sleep(TimeSpan.FromSeconds(1.1)));
			var after = DateTime.Now;

			(after - before).TotalSeconds.Should().BeGreaterThan(1);
		}

		[Fact]
		public async Task ShouldReturnAnyReturnValueFromAction()
		{
			using var workerThread = new WorkerThread();

			var result = await workerThread.InvokeAsync(_ => 13);

			result.Should().Be(13);
		}
	}
}
