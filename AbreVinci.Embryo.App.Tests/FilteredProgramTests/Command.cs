﻿using AbreVinci.Embryo.Core;

namespace AbreVinci.Embryo.App.Tests.FilteredProgramTests;

public class Command
{
    private record TestState;
    private record TestMessage;
    // ReSharper disable once NotAccessedPositionalProperty.Local
    private record TestCommand(bool WasTransformed);

    private class CommandFilter : ICommandFilter<TestCommand>
    {
        private readonly bool _allow;

        public CommandFilter(bool allow)
        {
            _allow = allow;
        }

        public TestCommand? FilterCommand(TestCommand command)
        {
	        return _allow ? command with { WasTransformed = true } : null;
        }
    }

    [Fact]
    public void ShouldNotFilterOutMessagesWhenNotIncludingAMessageFilterIrrespectiveOfCommandFilter()
    {
        ActionContext<TestState, TestCommand> Init() => new(new TestState());
        ActionContext<TestState, TestCommand> Update(ActionContext<TestState, TestCommand> context, TestMessage message) => context.WithCommand(new TestCommand(false));
        var testScheduler = new TestScheduler();
        var program = new Program<TestState, TestMessage, TestCommand>(Init, Update, testScheduler)
            .ApplyFilters(messageFilter: null, commandFilter: null);
        TestCommand? commandEmitted = null;
        program.Commands.Subscribe(c => commandEmitted = c);

        program.DispatchMessage(new TestMessage());
        testScheduler.Start();

        commandEmitted.Should().Be(new TestCommand(false));
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldFilterMessagesBasedOnMessageFilterIrrespectiveOfCommandFilter(bool allowCommands)
    {
	    ActionContext<TestState, TestCommand> Init() => new(new TestState());
	    ActionContext<TestState, TestCommand> Update(ActionContext<TestState, TestCommand> context, TestMessage message) => context.WithCommand(new TestCommand(false));
        var testScheduler = new TestScheduler();
        var program = new Program<TestState, TestMessage, TestCommand>(Init, Update, testScheduler)
            .ApplyFilters(messageFilter: null, commandFilter: new CommandFilter(allowCommands));
        TestCommand? commandEmitted = null;
        program.Commands.Subscribe(c => commandEmitted = c);

        program.DispatchMessage(new TestMessage());
        testScheduler.Start();

        commandEmitted.Should().Be(allowCommands ? new TestCommand(true) : null);
    }
}
