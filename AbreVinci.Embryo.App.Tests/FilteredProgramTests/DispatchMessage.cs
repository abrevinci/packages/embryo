﻿using AbreVinci.Embryo.Core;

namespace AbreVinci.Embryo.App.Tests.FilteredProgramTests;

public class DispatchMessage
{
    private record TestState;
    // ReSharper disable once NotAccessedPositionalProperty.Local
    private record TestMessage(bool WasTransformed);
    private record TestCommand;

    private class MessageFilter : IMessageFilter<TestMessage>
    {
        private readonly bool _allow;

        public MessageFilter(bool allow)
        {
            _allow = allow;
        }

        public TestMessage? FilterMessage(TestMessage message)
        {
            return _allow ? message with { WasTransformed = true } : null;
        }
    }

    [Fact]
    public void ShouldNotFilterOutMessagesWhenNotIncludingAMessageFilterIrrespectiveOfCommandFilter()
    {
        TestMessage? dispatchedMessage = null;
        ActionContext<TestState, TestCommand> Init() => new(new TestState());
        ActionContext<TestState, TestCommand> Update(ActionContext<TestState, TestCommand> context, TestMessage message)
        {
            dispatchedMessage = message;
            return context;
        }
        var testScheduler = new TestScheduler();
        var program = new Program<TestState, TestMessage, TestCommand>(Init, Update, testScheduler)
            .ApplyFilters(messageFilter: null, commandFilter: null);

        program.DispatchMessage(new TestMessage(false));
        testScheduler.Start();

        dispatchedMessage.Should().Be(new TestMessage(false));
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldFilterMessagesBasedOnMessageFilterIrrespectiveOfCommandFilter(bool allowMessages)
    {
        TestMessage? dispatchedMessage = null;
        ActionContext<TestState, TestCommand> Init() => new(new TestState());
        ActionContext<TestState, TestCommand> Update(ActionContext<TestState, TestCommand> context, TestMessage message)
        {
	        dispatchedMessage = message;
	        return context;
        }
        var testScheduler = new TestScheduler();
        var program = new Program<TestState, TestMessage, TestCommand>(Init, Update, testScheduler)
            .ApplyFilters(messageFilter: new MessageFilter(allowMessages), commandFilter: null);

        program.DispatchMessage(new TestMessage(false));
        testScheduler.Start();

        dispatchedMessage.Should().Be(allowMessages ? new TestMessage(true) : null);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public void ShouldFilterMessagesBasedOnMessageFilterWhenNotUsingCommands(bool allowMessages)
    {
	    TestMessage? dispatchedMessage = null;
        TestState Init() => new();
        TestState Update(TestState state, TestMessage message)
        {
            dispatchedMessage = message;
            return state;
        }
        var testScheduler = new TestScheduler();
        var program = Program.Create<TestState, TestMessage>(Init, Update, testScheduler)
            .ApplyMessageFilter(messageFilter: new MessageFilter(allowMessages));

        program.DispatchMessage(new TestMessage(false));
        testScheduler.Start();

        dispatchedMessage.Should().Be(allowMessages ? new TestMessage(true) : null);
    }
}
