﻿using AbreVinci.Embryo.Core;

namespace AbreVinci.Embryo.App.Tests.ProgramTests;

public class Commands
{
    public record TestState;
    public record TestMessage;
    public record TestCommand;

    [Fact]
    public void ShouldNotEmitAnythingWhenSubscribedIfNoInitialCommands()
    {
	    ActionContext<TestState, TestCommand> Init() => new(new TestState());
	    ActionContext<TestState, TestCommand> Update(ActionContext<TestState, TestCommand> context, TestMessage msg) => context;
        var testScheduler = new TestScheduler();
        var program = new Program<TestState, TestMessage, TestCommand>(Init, Update, testScheduler);

        var commandCount = 0;
        program.Commands.Subscribe(_ => commandCount++);

        commandCount.Should().Be(0);
    }

    [Fact]
    public void ShouldEmitInitialCommandsWhenSubscribed()
    {
	    ActionContext<TestState, TestCommand> Init() => new ActionContext<TestState, TestCommand>(new TestState()).WithCommands(new TestCommand(), new TestCommand());
	    ActionContext<TestState, TestCommand> Update(ActionContext<TestState, TestCommand> context, TestMessage msg) => context;
        var testScheduler = new TestScheduler();
        var program = new Program<TestState, TestMessage, TestCommand>(Init, Update, testScheduler);

        var commandCount = 0;
        program.Commands.Subscribe(_ => commandCount++);

        commandCount.Should().Be(2);
    }

    [Fact]
    public void ShouldEmitRequestedCommandsWhenSubscribed()
    {
	    ActionContext<TestState, TestCommand> Init() => new(new TestState());
	    ActionContext<TestState, TestCommand> Update(ActionContext<TestState, TestCommand> context, TestMessage msg) => context.WithCommand(new TestCommand());
        var testScheduler = new TestScheduler();
        var program = new Program<TestState, TestMessage, TestCommand>(Init, Update, testScheduler);

        var commandCount = 0;
        program.Commands.Subscribe(_ => commandCount++);

        program.DispatchMessage(new TestMessage());
        program.DispatchMessage(new TestMessage());
        program.DispatchMessage(new TestMessage());
        testScheduler.Start();

        commandCount.Should().Be(3);
    }

    [Theory]
    [InlineData(0)]
    [InlineData(1)]
    [InlineData(2)]
    [InlineData(3)]
    public void ShouldEmitBufferedCommandsToLateSubscribers(int commandBufferSize)
    {
	    ActionContext<TestState, TestCommand> Init() => new(new TestState());
	    ActionContext<TestState, TestCommand> Update(ActionContext<TestState, TestCommand> context, TestMessage msg) => context.WithCommand(new TestCommand());
        var testScheduler = new TestScheduler();
        var program = new Program<TestState, TestMessage, TestCommand>(Init, Update, testScheduler, commandBufferSize: commandBufferSize);

        program.DispatchMessage(new TestMessage());
        program.DispatchMessage(new TestMessage());
        program.DispatchMessage(new TestMessage());
        program.DispatchMessage(new TestMessage());
        program.DispatchMessage(new TestMessage());
        program.DispatchMessage(new TestMessage());
        program.DispatchMessage(new TestMessage());
        testScheduler.Start();

        var commandCount = 0;
        program.Commands.Subscribe(_ => commandCount++);

        commandCount.Should().Be(commandBufferSize);
    }
}
