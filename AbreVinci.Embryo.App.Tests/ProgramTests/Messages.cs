﻿using AbreVinci.Embryo.Core;

namespace AbreVinci.Embryo.App.Tests.ProgramTests;

public class Messages
{
    public record TestState;
    public record TestMessage;
    public record TestCommand;

    [Fact]
    public void ShouldNotEmitAnythingWhenSubscribed()
    {
	    ActionContext<TestState, TestCommand> Init() => new(new TestState());
	    ActionContext<TestState, TestCommand> Update(ActionContext<TestState, TestCommand> context, TestMessage msg) => context;
        var testScheduler = new TestScheduler();
        var program = new Program<TestState, TestMessage, TestCommand>(Init, Update, testScheduler);

        var messageCount = 0;
        program.Messages.Subscribe(_ => messageCount++);

        messageCount.Should().Be(0);
    }

    [Fact]
    public void ShouldEmitDispatchedMessagedWhileSubscribed()
    {
	    ActionContext<TestState, TestCommand> Init() => new(new TestState());
	    ActionContext<TestState, TestCommand> Update(ActionContext<TestState, TestCommand> context, TestMessage msg) => context;
        var testScheduler = new TestScheduler();
        var program = new Program<TestState, TestMessage, TestCommand>(Init, Update, testScheduler);

        var messageCount = 0;
        program.Messages.Subscribe(_ => messageCount++);

        program.DispatchMessage(new TestMessage());
        program.DispatchMessage(new TestMessage());
        testScheduler.Start();

        messageCount.Should().Be(2);
    }

    [Theory]
    [InlineData(0)]
    [InlineData(1)]
    [InlineData(2)]
    [InlineData(3)]
    public void ShouldEmitBufferedMessagesToLateSubscribers(int messageBufferSize)
    {
	    ActionContext<TestState, TestCommand> Init() => new(new TestState());
	    ActionContext<TestState, TestCommand> Update(ActionContext<TestState, TestCommand> context, TestMessage msg) => context;
        var testScheduler = new TestScheduler();
        var program = new Program<TestState, TestMessage, TestCommand>(Init, Update, testScheduler, messageBufferSize: messageBufferSize);

        program.DispatchMessage(new TestMessage());
        program.DispatchMessage(new TestMessage());
        program.DispatchMessage(new TestMessage());
        program.DispatchMessage(new TestMessage());
        program.DispatchMessage(new TestMessage());
        program.DispatchMessage(new TestMessage());
        program.DispatchMessage(new TestMessage());
        testScheduler.Start();

        var messageCount = 0;
        program.Messages.Subscribe(_ => messageCount++);

        messageCount.Should().Be(messageBufferSize);
    }
}
