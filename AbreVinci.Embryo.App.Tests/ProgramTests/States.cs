﻿using AbreVinci.Embryo.Core;

namespace AbreVinci.Embryo.App.Tests.ProgramTests;

public class States
{
    public record TestState(int A, int B);
    public record TestMessage;
    public record TestCommand;

    [Fact]
    public void ShouldEmitInitialStateUponSubscription()
    {
	    ActionContext<TestState, TestCommand> Init() => new(new TestState(1, 3));
	    ActionContext<TestState, TestCommand> Update(ActionContext<TestState, TestCommand> context, TestMessage msg) => context;
        var testScheduler = new TestScheduler();
        var program = new Program<TestState, TestMessage, TestCommand>(Init, Update, testScheduler);

        TestState? state = null;
        program.States.Subscribe(s => state = s);

        state.Should().Be(new TestState(1, 3));
    }

    [Fact]
    public void ShouldEmitUpdatedStateAfterDispatchingMessage()
    {
	    ActionContext<TestState, TestCommand> Init() => new(new TestState(1, 3));
	    ActionContext<TestState, TestCommand> Update(ActionContext<TestState, TestCommand> context, TestMessage msg) => context.WithState(context.State with { A = 2 });
        var testScheduler = new TestScheduler();
        var program = new Program<TestState, TestMessage, TestCommand>(Init, Update, testScheduler);

        TestState? state = null;
        program.States.Subscribe(s => state = s);

        program.DispatchMessage(new TestMessage());
        testScheduler.Start();

        state.Should().Be(new TestState(2, 3));
    }

    [Fact]
    public void ShouldEmitLatestStateToLateSubscribers()
    {
	    ActionContext<TestState, TestCommand> Init() => new(new TestState(1, 3));
	    ActionContext<TestState, TestCommand> Update(ActionContext<TestState, TestCommand> context, TestMessage msg) => context.WithState(context.State with { A = 2 });
        var testScheduler = new TestScheduler();
        var program = new Program<TestState, TestMessage, TestCommand>(Init, Update, testScheduler);

        program.DispatchMessage(new TestMessage());
        testScheduler.Start();

        TestState? state = null;
        program.States.Subscribe(s => state = s);

        state.Should().Be(new TestState(2, 3));
    }

    [Fact]
    public void ShouldAllowMultipleSubscribers()
    {
	    ActionContext<TestState, TestCommand> Init() => new(new TestState(1, 3));
	    ActionContext<TestState, TestCommand> Update(ActionContext<TestState, TestCommand> context, TestMessage msg) => context;
        var testScheduler = new TestScheduler();
        var program = new Program<TestState, TestMessage, TestCommand>(Init, Update, testScheduler);

        TestState? state1 = null;
        program.States.Subscribe(s => state1 = s);

        TestState? state2 = null;
        program.States.Subscribe(s => state2 = s);

        state1.Should().Be(new TestState(1, 3));
        state2.Should().Be(new TestState(1, 3));
    }

    [Theory]
    [InlineData(0)]
    [InlineData(1)]
    [InlineData(2)]
    [InlineData(3)]
    public void ShouldEmitBufferedStatesToLateSubscribers(int stateBufferSize)
    {
        ActionContext<TestState, TestCommand> Init() => new(new TestState(1, 3));
        ActionContext<TestState, TestCommand> Update(ActionContext<TestState, TestCommand> context, TestMessage msg) => context.WithState(context.State with { A = context.State.A + 1 });
        var testScheduler = new TestScheduler();
        var program = new Program<TestState, TestMessage, TestCommand>(Init, Update, testScheduler, stateBufferSize: stateBufferSize);

        program.DispatchMessage(new TestMessage());
        program.DispatchMessage(new TestMessage());
        program.DispatchMessage(new TestMessage());
        program.DispatchMessage(new TestMessage());
        program.DispatchMessage(new TestMessage());
        program.DispatchMessage(new TestMessage());
        program.DispatchMessage(new TestMessage());
        testScheduler.Start();

        var stateCount = 0;
        program.States.Subscribe(_ => stateCount++);

        stateCount.Should().Be(stateBufferSize);
    }
}
