﻿using AbreVinci.Embryo.Core;

namespace AbreVinci.Embryo.App.Tests.ProgramTests;

public class DispatchMessage
{
    public record TestState;
    public record TestMessage;
    public record TestCommand;

    [Fact]
    public void ShouldDispatchOnScheduler()
    {
	    ActionContext<TestState, TestCommand> Init() => new(new TestState());
	    ActionContext<TestState, TestCommand> Update(ActionContext<TestState, TestCommand> context, TestMessage msg) => context;
        var testScheduler = new TestScheduler();
        var program = new Program<TestState, TestMessage, TestCommand>(Init, Update, testScheduler);

        var messageCount = 0;
        program.Messages.Subscribe(_ => messageCount++);

        program.DispatchMessage(new TestMessage());

        messageCount.Should().Be(0);

        testScheduler.Start();

        messageCount.Should().Be(1);
    }

    [Fact]
    public void ShouldCallUpdateWithDispatchedMessage()
    {
        TestMessage? dispatchedMessage = null;
        ActionContext<TestState, TestCommand> Init() => new(new TestState());
        ActionContext<TestState, TestCommand> Update(ActionContext<TestState, TestCommand> context, TestMessage msg)
        {
            dispatchedMessage = msg;
            return context;
        }
        
        var testScheduler = new TestScheduler();
        var program = new Program<TestState, TestMessage, TestCommand>(Init, Update, testScheduler);

        program.DispatchMessage(new TestMessage());
        testScheduler.Start();

        dispatchedMessage.Should().Be(new TestMessage());
    }

    // the rest of DispatchMessage functionality is tested in Commands, States, and Messages files
}
