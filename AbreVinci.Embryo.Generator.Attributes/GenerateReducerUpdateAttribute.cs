﻿namespace AbreVinci.Embryo.Generator.Attributes;

/// <summary>
/// Use this to mark the reducer type that you want to generate an update function for. It must be declared as partial.
/// </summary>
[AttributeUsage(AttributeTargets.Class)]
[Conditional("CodeGeneration")]
public class GenerateReducerUpdateAttribute : Attribute
{
	/// <summary>
	/// Constructor to use when working with commands. This will cause the generated update function to use an ActionContext for the given state and command types.
	/// </summary>
	/// <param name="stateType">The program state type.</param>
	/// <param name="messageType">The program message type.</param>
	/// <param name="commandType">The program command type.</param>
	public GenerateReducerUpdateAttribute(Type stateType, Type messageType, Type commandType)
	{
		StateType = stateType;
		MessageType = messageType;
		CommandType = commandType;
	}

	/// <summary>
	/// Constructor to use when not working with commands. This will cause the generated update function to use the state directly.
	/// </summary>
	/// <param name="stateType">The program state type.</param>
	/// <param name="messageType">The program message type.</param>
	public GenerateReducerUpdateAttribute(Type stateType, Type messageType)
	{
		StateType = stateType;
		MessageType = messageType;
		CommandType = null;
	}

	/// <summary>
	/// The program state type.
	/// </summary>
	public Type StateType { get; }

	/// <summary>
	/// The program message type.
	/// </summary>
	public Type MessageType { get; }

	/// <summary>
	/// The program command type, if any.
	/// </summary>
	public Type? CommandType { get; }
}
