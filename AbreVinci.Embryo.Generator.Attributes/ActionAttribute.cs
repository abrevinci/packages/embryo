﻿namespace AbreVinci.Embryo.Generator.Attributes;

/// <summary>
/// Use this to mark action methods. They are expected to be chainable extension methods (returning the same type that they extend).
/// </summary>
[AttributeUsage(AttributeTargets.Method)]
[Conditional("CodeGeneration")]
public class ActionAttribute : Attribute
{
}
