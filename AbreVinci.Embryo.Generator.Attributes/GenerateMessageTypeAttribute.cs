﻿namespace AbreVinci.Embryo.Generator.Attributes;

/// <summary>
/// Use this to mark the message type that you want to generate. It must be declared as partial.
/// </summary>
[AttributeUsage(AttributeTargets.Class)]
[Conditional("CodeGeneration")]
public class GenerateMessageTypeAttribute : Attribute
{
}
