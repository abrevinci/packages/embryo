﻿namespace AbreVinci.Embryo.Generator.Attributes;

/// <summary>
/// Use this to mark the command handler type that you want to generate an implementation for. It must be declared as partial.
/// </summary>
[AttributeUsage(AttributeTargets.Class)]
[Conditional("CodeGeneration")]
public class GenerateCommandHandlerAttribute : Attribute
{
	/// <summary>
	/// Constructor. This will cause the generated code to handle commands of the given commandType and to dispatch messages of the given messageType.
	/// </summary>
	/// <param name="commandType">The program command type.</param>
	/// <param name="messageType">The program message type.</param>
	public GenerateCommandHandlerAttribute(Type commandType, Type messageType)
	{
		CommandType = commandType;
		MessageType = messageType;
	}

	/// <summary>
	/// The program command type.
	/// </summary>
	public Type CommandType { get; }

	/// <summary>
	/// The program message type.
	/// </summary>
	public Type MessageType { get; }
}
