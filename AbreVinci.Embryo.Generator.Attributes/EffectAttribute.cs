﻿namespace AbreVinci.Embryo.Generator.Attributes;

/// <summary>
/// Use this to mark effect methods. They are expected to be void methods or return an untyped Task and their first argument may be
/// an EffectContext&lt;TService, TMessage&gt; with any appropriate service type as the first type argument and the program's message type
/// as the second argument. The rest of the method arguments will be interpreted as command payloads.
/// </summary>
[AttributeUsage(AttributeTargets.Method)]
[Conditional("CodeGeneration")]
public class EffectAttribute : Attribute
{
}
