#tool nuget:?package=docfx.console&version=2.59.0

#addin nuget:?package=Sprache&version=2.3.1
#addin nuget:?package=DotNetEnv&version=2.3.0

if (string.IsNullOrWhiteSpace(EnvironmentVariable("GITLAB_CI")))
{
	DotNetEnv.Env.Load();
}

var solution = "AbreVinci.Embryo.sln";

var target = Argument("target", "Default");
var configuration = Argument("configuration", "Release");

var artifactsDir = new DirectoryPath("./Build/Artifacts");

var projectsToPack = new[]
{
	(project: "AbreVinci.Embryo.Core/AbreVinci.Embryo.Core.csproj", package: "AbreVinci.Embryo.Core"),
	(project: "AbreVinci.Embryo.App/AbreVinci.Embryo.App.csproj", package: "AbreVinci.Embryo.App"),
	(project: "AbreVinci.Embryo.ReactiveUI/AbreVinci.Embryo.ReactiveUI.csproj", package: "AbreVinci.Embryo.ReactiveUI"),
	(project: "AbreVinci.Embryo/AbreVinci.Embryo.csproj", package: "AbreVinci.Embryo"),
	(project: "AbreVinci.Embryo.Entities/AbreVinci.Embryo.Entities.csproj", package: "AbreVinci.Embryo.Entities"),
	(project: "AbreVinci.Embryo.Fody.Packaging/AbreVinci.Embryo.Fody.Packaging.csproj", package: "AbreVinci.Embryo.Fody"),
	(project: "AbreVinci.Embryo.Generator/AbreVinci.Embryo.Generator.csproj", package: "AbreVinci.Embryo.Generator"),
	(project: "AbreVinci.Embryo.Debugging/AbreVinci.Embryo.Debugging.csproj", package: "AbreVinci.Embryo.Debugging"),
	(project: "AbreVinci.Embryo.WPF/AbreVinci.Embryo.WPF.csproj", package: "AbreVinci.Embryo.WPF"),
	(project: "AbreVinci.Embryo.Debugging.WPF/AbreVinci.Embryo.Debugging.WPF.csproj", package: "AbreVinci.Embryo.Debugging.WPF"),
	(project: "Build/templates/AbreVinci.Embryo.Templates.csproj", package: "AbreVinci.Embryo.Templates")
};

var templateTests = new[]
{
	(name: "Empty", args: ""),
	(name: "EmptyFody", args: "--Fody"),
	(name: "EmptyGenerator", args: "--Generator"),
	(name: "EmptyFodyGenerator", args: "--Fody --Generator"),
	(name: "EmptyCommands", args: "--Commands"),
	(name: "EmptyCommandsFody", args: "--Commands --Fody"),
	(name: "EmptyCommandsGenerator", args: "--Commands --Generator"),
	(name: "EmptyCommandsFodyGenerator", args: "--Commands --Fody --Generator"),
	(name: "Counter", args: "--Counter"),
	(name: "CounterFody", args: "--Counter --Fody"),
	(name: "CounterGenerator", args: "--Counter --Generator"),
	(name: "CounterFodyGenerator", args: "--Counter --Fody --Generator"),
	(name: "CounterCommands", args: "--Counter --Commands"),
	(name: "CounterCommandsFody", args: "--Counter --Commands --Fody"),
	(name: "CounterCommandsGenerator", args: "--Counter --Commands --Generator"),
	(name: "CounterCommandsFodyGenerator", args: "--Counter --Commands --Fody --Generator"),
};

var version = new
{
	NuGetVersionV2 = EnvironmentVariable("NuGetVersionV2"),
	MajorMinorPatch = EnvironmentVariable("MajorMinorPatch"),
	InformationalVersion = EnvironmentVariable("InformationalVersion")
};

var isProtectedBranch = EnvironmentVariable("CI_COMMIT_REF_PROTECTED", false);

Task("Inform")
.Does(() =>
{
	Information($"Building {version.NuGetVersionV2}, {version.MajorMinorPatch}, {version.InformationalVersion}");
});

Task("Clean")
.IsDependentOn("Inform")
.Does(() =>
{
	CleanDirectory("./Build");
	CleanDirectory("./Documentation/_site");
});

Task("Build")
.IsDependentOn("Clean")
.Does(() =>
{
	var settings = new DotNetBuildSettings
	{
		Configuration = configuration,
		ArgumentCustomization = args => args
			.Append($"-p:Version={version.MajorMinorPatch}")
			.Append($"-p:InformationalVersion={version.InformationalVersion}")
	};

	DotNetBuild(solution, settings);
});

Task("BuildTemplates")
.IsDependentOn("Build")
.Does(() =>
{
	CopyDirectory(new DirectoryPath("./AbreVinci.Embryo.Templates/"), new DirectoryPath("./Build/templates/"));
	var templateProjectFile = MakeAbsolute(new FilePath("./Build/templates/templates/embryowpf/Embryowpf.csproj")).ToString();
	var contents = System.IO.File.ReadAllText(templateProjectFile);
	contents = contents.Replace("{VERSION}", version.NuGetVersionV2);
	System.IO.File.WriteAllText(templateProjectFile, contents);

	var settings = new DotNetBuildSettings
	{
		Configuration = configuration,
		ArgumentCustomization = args => args
			.Append($"-p:Version={version.MajorMinorPatch}")
			.Append($"-p:InformationalVersion={version.InformationalVersion}")
	};
	DotNetBuild("./Build/templates/AbreVinci.Embryo.Templates.csproj", settings);
});

Task("Test")
.IsDependentOn("Build")
.Does(() =>
{
	var settings = new DotNetTestSettings
	{
		Configuration = configuration,
		NoBuild = true
	};

	DotNetTest(solution, settings);
});

Task("Sign")
.IsDependentOn("Build")
.IsDependentOn("BuildTemplates")
.Does(() =>
{
	if (!isProtectedBranch)
	{
		Information("Signing can only be done on protected branches/tags.");
		return;
	}

	SignAssemblies(GetFiles("./Build/**/AbreVinci.*.dll"));
});

Task("Package")
.IsDependentOn("Sign")
.Does(() =>
{
	var settings = new DotNetPackSettings
	{
		Configuration = configuration,
		NoBuild = true,
		OutputDirectory = new DirectoryPath($"./Build/{configuration}"),
		ArgumentCustomization = args => args
			.Append($"-p:PackageVersion={version.NuGetVersionV2}")
	};

	foreach (var project in projectsToPack)
	{
		DotNetPack($"./{project.project}", settings);		
	}
});

Task("SignPackages")
.IsDependentOn("Package")
.Does(() =>
{
	if (!DirectoryExists(artifactsDir))
		CreateDirectory(artifactsDir);

	if (!isProtectedBranch)
	{
		Information("Signing can only be done on protected branches/tags.");
		foreach (var project in projectsToPack)
		{
			CopyFileToDirectory(new FilePath($"./Build/{configuration}/{project.package}.{version.NuGetVersionV2}.nupkg"), artifactsDir);
		}
		return;
	}

	foreach (var project in projectsToPack)
	{
		SignPackage(artifactsDir, new FilePath($"./Build/{configuration}/{project.package}.{version.NuGetVersionV2}.nupkg"));
	}
});

Task("TestTemplates")
.IsDependentOn("SignPackages")
.Does(() =>
{
	var packageSource = MakeAbsolute(new DirectoryPath("./Build/Artifacts/")).ToString().Replace("/", "\\");
	var templateTestDir = MakeAbsolute(new DirectoryPath("./Build/template-test")).ToString();
	try
	{
		StartProcessFailOnError("dotnet", new ProcessSettings {Arguments = $"nuget add source \"{packageSource}\" -n TestTemplatesSource"});
		if (!DirectoryExists("./Build/template-test"))
			CreateDirectory("./Build/template-test");

		StartProcessFailOnError("dotnet", new ProcessSettings {Arguments = $"new --install AbreVinci.Embryo.Templates::{version.NuGetVersionV2}", WorkingDirectory = templateTestDir});

		StartProcessFailOnError("dotnet", new ProcessSettings {Arguments = $"new sln -n TemplateTest", WorkingDirectory = templateTestDir});
		foreach (var test in templateTests)
		{
			StartProcessFailOnError("dotnet", new ProcessSettings {Arguments = $"new embryowpf -n {test.name} {test.args}", WorkingDirectory = templateTestDir});
			StartProcessFailOnError("dotnet", new ProcessSettings {Arguments = $"sln add {test.name}/{test.name}.csproj", WorkingDirectory = templateTestDir});

			StartProcessFailOnError("dotnet", new ProcessSettings {Arguments = $"new embryowpf -n {test.name}.NetFramework {test.args} --Framework net472", WorkingDirectory = templateTestDir});
			StartProcessFailOnError("dotnet", new ProcessSettings {Arguments = $"sln add {test.name}.NetFramework/{test.name}.NetFramework.csproj", WorkingDirectory = templateTestDir});
		}

		var settings = new DotNetBuildSettings
		{
			Configuration = configuration,
			NoRestore = true
		};
		DotNetBuild("./Build/template-test/TemplateTest.sln", settings);
	}
	finally
	{
		StartProcess("dotnet", new ProcessSettings {Arguments = $"new --uninstall AbreVinci.Embryo.Templates", WorkingDirectory = templateTestDir});
		StartProcess("dotnet", new ProcessSettings {Arguments = $"nuget remove source TestTemplatesSource"});
	}
});

Task("BuildDocs")
.IsDependentOn("Clean")
.Does(() => 
{ 
	var docsDirectory = MakeAbsolute(new DirectoryPath("./Documentation"));
	StartProcessFailOnError("./tools/docfx.console.2.59.0/tools/docfx.exe", new ProcessSettings {Arguments="metadata --force", WorkingDirectory=docsDirectory});
	StartProcessFailOnError("./tools/docfx.console.2.59.0/tools/docfx.exe", new ProcessSettings {Arguments="build", WorkingDirectory=docsDirectory});
});

Task("Default")
.IsDependentOn("Test")
.IsDependentOn("TestTemplates")
.IsDependentOn("BuildDocs")
.Does(() => { });

Information($"Running task: {target}");
RunTarget(target);

///////////////////////////////////////////////////////////////////////////////////////
// Utilities
///////////////////////////////////////////////////////////////////////////////////////

void SignAssemblies(IEnumerable<FilePath> files)
{
	var assembliesToSignFile = new FilePath("./Build/AssembliesToSign.txt");
	System.IO.File.WriteAllLines(MakeAbsolute(assembliesToSignFile).ToString(), files.Select(p => MakeAbsolute(p).ToString()).ToArray());

	var args = new ProcessArgumentBuilder();
	args.Append("sign");

	SetupSigning(args);

	args.Append("--input-file-list");
	args.AppendQuoted(MakeAbsolute(assembliesToSignFile).ToString());

	DotNetTool("azuresigntool", args);
}

void SignPackage(DirectoryPath outputPath, FilePath package)
{
	var outputPackage = outputPath.CombineWithFilePath(package.GetFilename());

	var args = new ProcessArgumentBuilder();
	args.Append("sign");

	SetupSigning(args);

	args.Append("-o");
	args.AppendQuoted(MakeAbsolute(outputPackage).ToString());

	args.AppendQuoted(MakeAbsolute(package).ToString());

	DotNetTool("nugetkeyvaultsigntool", args);
}

void SetupSigning(ProcessArgumentBuilder args)
{
	args.Append("--file-digest");
	args.Append("sha256");

	args.Append("--timestamp-rfc3161");
	args.AppendQuoted(EnvironmentVariable("DIGICERT_TIMESTAMP_URL"));

	args.Append("--timestamp-digest");
	args.Append("sha256");

	args.Append("--azure-key-vault-url");
	args.AppendQuoted(EnvironmentVariable("AZURE_KEY_VAULT_URL"));

	args.Append("--azure-key-vault-client-id");
	args.AppendQuoted(EnvironmentVariable("AZURE_KEY_VAULT_CLIENT_ID"));

	args.Append("--azure-key-vault-tenant-id");
	args.AppendQuoted(EnvironmentVariable("AZURE_KEY_VAULT_TENANT_ID"));

	args.Append("--azure-key-vault-client-secret");
	args.AppendQuoted(EnvironmentVariable("AZURE_KEY_VAULT_CLIENT_SECRET"));

	args.Append("--azure-key-vault-certificate");
	args.AppendQuoted(EnvironmentVariable("AZURE_KEY_VAULT_CERTIFICATE"));
}

void DotNetTool(string tool, ProcessArgumentBuilder args)
{
	StartProcessFailOnError("dotnet", new ProcessSettings {Arguments = $"tool run {tool} {args.Render()}"});
}

void StartProcessFailOnError(string process, ProcessSettings settings)
{
	var exitCode = StartProcess(process, settings);
	if (exitCode != 0)
	{
		Information($"Exit code: {exitCode}");
		throw new Exception($"Error executing {process} {settings.Arguments}");
	}
}
